<?php 
class Management_MenusController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    public function preDispatch() {
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }
    }
    
    public function indexAction()
    {
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        
        $tiposMenus = new Model_DbTable_TiposMenus();
        $this->view->registos = $tiposMenus->fetchAll();
        
    }
    
    public function itensAction(){
        
        $tipo_id = $this->getRequest()->getParam('tipo');
        $this->view->tipo_id = $tipo_id;
        
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        
        $tiposMenus = new Model_DbTable_TiposMenus();
        $this->view->tipoMenu = $tiposMenus->designacaoById($tipo_id);
        $menus = new Model_DbTable_Menus();
        //
        $select = $menus->select()->where('tipo_id = ?', $tipo_id)->where('idioma_iso = ?', $idioma)->where('itemsuperior_id IS NULL')->order('posicao ASC');
        $this->view->registos = $menus->fetchAll($select);
        
    }
    
    public function adicionarAction()
    {
        $tipo_id = $this->getRequest()->getParam('tipo');
        if(isset ($tipo_id)){
            $tiposMenus = new Model_DbTable_TiposMenus();
            $this->view->tipoMenu = $tiposMenus->fetchRow($tiposMenus->select()->where('id = ?', $tipo_id));
                        
            $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
            $this->view->idioma = $idioma;
            
            $menus = new Model_DbTable_Menus();
            $this->view->outrosMenus = $menus->fetchAll($menus->select()->where('itemsuperior_id IS NULL')->where('tipo_id = ?', $tipo_id)->where('idioma_iso = ?', $idioma)->where('idioma_iso = ?', $idioma)->order('posicao ASC'));
            
            if(isset ($_GET["qqfile"]) && strlen($_GET["qqfile"])){
                // list of valid extensions, ex. array("jpg","jpeg","gif","png","bmp")
                $allowedExtensions = array("jpg","jpeg","gif","png","bmp");
                // max file size in bytes
                $sizeLimit = 10 * 1024 * 1024;

                $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload('uploads/images/background-images/');
                // to pass data through iframe you will need to encode all html tags
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                exit;
            }
            else
            if ($this->getRequest()->isPost()) {
                
                $formData = $this->getRequest()->getPost();
                $funcoes = new App_Class_FriendlyURL();
                $conteudo_id = $this->getRequest()->getPost('conteudo_id');
//                  echo '<pre>';
//                echo htmlspecialchars(print_r($_POST, true));
//                echo '</pre>';
//                
//                $path = $formData['path'] != '' ? $formData['path'] : $funcoes->gerarLinkSEO($conteudo_id, $formData['designacao']);
//                
//                echo '<br />'.$path;
//                return;
                
                $path = $formData['path'] != '' ? $formData['path'] : $funcoes->gerarLinkSEO($conteudo_id, $formData['designacao']);

                if($menus->jaExistePath($idioma, $path)>0){
                    $this->_sessao->display = "Lamentamos, mas já essa hiperligação.";
                    $this->_redirect('/management/'.$this->_controller.'/itens/tipo/'.$tipo_id);
                }
                else{
                    $tipo_id = $tipo_id;
                    $item_id = $formData['item_id'];
                    $itemsuperior_id = ($formData['itemsuperior_id'] != '') ? $formData['itemsuperior_id'] : NULL;
                    $designacao = $formData['designacao'];
                    $link = $formData['link'];
                    $backgroundImage = $formData['backgroundImage'];
                    $descricaoLateralDireita = $this->cleanHTML($formData['descricao']) ;
                    
                    /*if($backgroundImage=='' && $itemsuperior_id != ''){
                        $menus = new Model_DbTable_Menus();
                        $menuItemSuperior = $menus->fetchRow($menus->select()->where('id = ?', $formData['itemsuperior_id']));
                        
                        $parametroItemSuperior = json_decode($menuItemSuperior->parametros,true);
                        if(!empty($parametroItemSuperior['background-image'])){
                            $backgroundImage = $parametroItemSuperior['background-image'];
                        }
                    }*/
                    
                    if($item_id == 1){
                        $parametros = '{"conteudo_id":"'.$conteudo_id.'","background-image":"'.$backgroundImage.'","lateral-direita":"'.$descricaoLateralDireita.'"}';
                    }  
                    else
                    if($item_id == 2 || $item_id == 3){
                         $parametros = '{"categoria_id":"'.$conteudo_id.'","background-image":"'.$backgroundImage.'","lateral-direita":"'.$descricaoLateralDireita.'"}';
                    }
                    else{
                        $parametros = '{"outro":"'.$conteudo_id.'","background-image":"'.$backgroundImage.'","lateral-direita":"'.$descricaoLateralDireita.'"}';
                    }

                    $posicao = $menus->getQtdPosicoes($idioma, $tipo_id, $item_id)+1;

                    $menus->addMenu($idioma, $tipo_id, $item_id, $itemsuperior_id, $designacao, $path, $link, $posicao, $parametros);
                        
                    $this->_sessao->display = "Item de menu criado com sucesso.";
                    $this->_redirect('/management/'.$this->_controller.'/itens/tipo/'.$tipo_id.'/idioma/'.$idioma);
                }
            }
        }else{
            $this->_sessao->display = "Por favor indique um tipo de menu.";
        }
    }
    
    public function editarAction()
    {
        $id = $this->getRequest()->getParam('id');
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        $tipo_id = $this->getRequest()->getParam('tipo');
        
        
        $menus = new Model_DbTable_Menus();
        $menu = $menus->fetchRow($menus->select()->where('id = ?', $id));
        $this->view->menu = $menu;
        
        $parametros = json_decode($menu->parametros,true);
        $this->view->backgroundImage = $backgroundImageMenu = (!empty($parametros['background-image']) ? $parametros['background-image'] : NULL);
        $this->view->lateralDireita = $lateralDireita = (!empty($parametros['lateral-direita']) ? $parametros['lateral-direita'] : NULL);
        
        /*echo '<pre>';
        print_r($parametros);
        echo '</pre>';*/
        
        
        $tiposMenus = new Model_DbTable_TiposMenus();
        $this->view->tipoMenu = $tiposMenus->fetchRow($tiposMenus->select()->where('id = ?', $tipo_id));
        
        $itensMenus = new Model_DbTable_ItensMenus();
        $this->view->tipoItem = $itensMenus->designacaoById($menu->item_id);//item_id
        
        $menus = new Model_DbTable_Menus();
        $this->view->outrosMenus = $menus->fetchAll($menus->select()->where('itemsuperior_id IS NULL')->where('tipo_id = ?', $tipo_id)->where('idioma_iso = ?', $idioma)->where('idioma_iso = ?', $idioma)->order('posicao ASC'));
        
        if(isset ($_GET["qqfile"]) && strlen($_GET["qqfile"])){
            // list of valid extensions, ex. array("jpg","jpeg","gif","png","bmp")
            $allowedExtensions = array("jpg","jpeg","gif","png","bmp");
            // max file size in bytes
            $sizeLimit = 10 * 1024 * 1024;

            $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload('uploads/images/background-images/');
            // to pass data through iframe you will need to encode all html tags
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            exit;
        }
        else
        if ($this->getRequest()->isPost()) {
                
                $formData = $this->getRequest()->getPost();
                $funcoes = new App_Class_FriendlyURL();
                $conteudo_id = $this->getRequest()->getPost('conteudo_id');
                
             /*   echo '<pre>';
                echo htmlspecialchars(print_r($_POST, true));
                echo '</pre>';
                return;*/
                    
               $path = $formData['path'] != '' ? $formData['path'] : $funcoes->gerarLinkSEO($conteudo_id, $formData['designacao']);

                if($menus->jaExistePathEditar($menu->id, $idioma, $path)>0){
                    $this->_sessao->display = "Lamentamos, mas já essa hiperligação.";
                }
                else{
                    $tipo_id = $tipo_id;
                    $item_id = $formData['item_id'];
                    $itemsuperior_id = ($formData['itemsuperior_id'] != '') ? $formData['itemsuperior_id'] : NULL;
                    $designacao = $formData['designacao'];
                    $link = $formData['link'];
                    $backgroundImage = $formData['backgroundImage'];
                    $descricaoLateralDireita = $this->cleanHTML($formData['descricao']) ;

                    if($backgroundImage=='' && $itemsuperior_id != ''){
                        $menus = new Model_DbTable_Menus();
                        $menuItemSuperior = $menus->fetchRow($menus->select()->where('id = ?', $formData['itemsuperior_id']));
                        
                        $parametroItemSuperior = json_decode($menuItemSuperior->parametros,true);
                        if(!empty($parametroItemSuperior['background-image'])){
                            $backgroundImage = $parametroItemSuperior['background-image'];
                        }
                    }
                    
                    if($item_id == 1){
                        $parametros = '{"conteudo_id":"'.$conteudo_id.'","background-image":"'.$backgroundImage.'","lateral-direita":"'.$descricaoLateralDireita.'"}';
                    }  
                    else
                    if($item_id == 2 || $item_id == 3){
                         $parametros = '{"categoria_id":"'.$conteudo_id.'","background-image":"'.$backgroundImage.'","lateral-direita":"'.$descricaoLateralDireita.'"}';
                    }
                    else{
                        $parametros = '{"outro":"'.$conteudo_id.'","background-image":"'.$backgroundImage.'","lateral-direita":"'.$descricaoLateralDireita.'"}';
                    }


                    $menus->updateMenu($id, $menu->idioma_iso, $tipo_id, $item_id, $itemsuperior_id, $designacao, $path, $link, $menu->posicao, $parametros);
                            
                        
                    $this->_sessao->display = "Item de menu actualizado com sucesso.";
                    $this->_redirect('/management/'.$this->_controller.'/itens/tipo/'.$tipo_id.'/idioma/'.$menu->idioma_iso);
                }
        }
        
        
    }
    
   function cleanHTML($html) {
       $string = preg_replace('/<!--\[if[^\]]*]>.*?<!\[endif\]-->/i', '', $html);
       
       return $string;
   }
    
    public function eliminarImagemAction(){
        $this->_helper->layout()->disableLayout();
        $imagem = $this->getRequest()->getParam('imagem');
        $menu_id = $this->getRequest()->getParam('menu_id');
        
        if(unlink('uploads/images/background-images/'.$imagem)){
            if($menu_id != NULL && $menu_id !=0){
                $menus = new Model_DbTable_Menus();
                $menu = $menus->fetchRow($menus->select()->where('id = ?', $menu_id));
                $this->view->menu = $menu;

                $parametros = json_decode($menu->parametros,true);
                
                $conteudo_id = (!empty($parametros['conteudo_id']) ? $parametros['bconteudo_id'] : NULL);
                $outro = (!empty($parametros['outro']) ? $parametros['outro'] : NULL);
                $backgroundImage = "";
                $lateralDireita = (!empty($parametros['lateral-direita']) ? $parametros['lateral-direita'] : NULL);
                
                $novosparametros = '{';
                $novosparametros .= '"conteudo_id":"'.$conteudo_id.'",';
                if($outro){
                    $novosparametros .= '"outro":"'.$outro.'",';
                }
                if($lateralDireita){
                    $lateralDireita .= '"lateral-direita":"'.$lateralDireita.'",';
                }
                $lateralDireita .= '"background-image":"'.$backgroundImage.'",';
                $novosparametros .= '}';
                
                $menus->updateOpcao($menu_id, 'parametros', $novosparametros);
            }
            echo 'sucess';
        }else{
            echo 'error';
        }
    }
    
    public function apagarAction() {
        $id = $this->getRequest()->getParam('id');
        $menus = new Model_DbTable_Menus();
        $tipo_id = $menus->getTipoIDById($id);
        
        $menuDependeOutro = $menus->temItemSuperiorID($id);
        
        if($menuDependeOutro == TRUE){
            //apagamos apenas o menu 
            // echo "DELETE menus WHERE id = '" . $id. "' <br />";
            $menus->deleteMenu($id);
        }else{
            //apagamos os menus que estao relacionados 
            $select = $menus->select()
		    ->where('itemsuperior_id  = ?', $id);
            $submenus = $menus->fetchAll($select);
            
            $menus = new Model_DbTable_Menus();
            foreach ($submenus as $submenu)
            {
                   //  echo "DELETE menus WHERE id = '" . $submenu->id . "' <br />";
                   $menu = $menus->fetchRow($menus->select()->where('id = ?', $submenu->id));
                   $parametros = json_decode($menu->parametros,true);
                   if(!empty($parametros['background-image'])){
                       unlink('uploads/images/background-images/'.$parametros['background-image']);
                   }
                   $menus->deleteMenu($submenu->id);
            }
            //e o menu actual
            // echo "DELETE menus WHERE id = '" . $id. "' <br />";
            $menu = $menus->fetchRow($menus->select()->where('id = ?', $id));
            $parametros = json_decode($menu->parametros,true);
            if(!empty($parametros['background-image'])){
                unlink('uploads/images/background-images/'.$parametros['background-image']);
            }
            $menus->deleteMenu($id);
        }
       
        $this->_sessao->display = "Item de menu eliminado com sucesso.";
        $this->_redirect('/management/'.$this->_controller.'/itens/tipo/'.$tipo_id);
    }
    
    public function configuracoesAction()
    {
        $this->_helper->layout()->disableLayout();
        
        $item_id = $this->getRequest()->getParam('item');
        
        $idioma = $this->getRequest()->getParam('idioma');
        $this->getRequest()->setParam('idioma', $idioma);
        
        $this->_forward('item-'.$item_id);
    }
    
    public function item1Action()
    {
        $this->_helper->layout()->disableLayout();
        
        $idioma = $this->getRequest()->getParam('idioma');
        $conteudos = new Model_DbTable_Conteudos();
        $this->view->conteudos = $conteudos->fetchAll($conteudos->select()->where('idioma_iso = ?', $idioma));
        
    }
    
    public function item2Action()
    {
        $this->_helper->layout()->disableLayout();
        
        $tipos = new Model_DbTable_TiposConteudos(); //NAO aplicavel
        $this->view->tiposConteudos = $tipos->fetchAll();
    }
    
    public function item3Action()
    {
        $this->_helper->layout()->disableLayout();
    }
    
    public function item4Action()
    {
        $this->_helper->layout()->disableLayout();
        
        $idioma = $this->getRequest()->getParam('idioma');
        $tipos = new Model_DbTable_TiposConteudos();
        $this->view->tiposConteudos = $tipos->fetchAll($tipos->select()->where('idioma_iso = ?', $idioma));
        
    }
    
    public function item5Action()
    {
        $this->_helper->layout()->disableLayout();
    }
    
    public function item6Action()
    {
        $this->_helper->layout()->disableLayout();
    }
    
    public function ordenarAction()
    {
        $this->_helper->layout()->disableLayout();
        $dadosPost = $this->getRequest()->getPost();
        if (($dadosPost)) {
            $list = $_REQUEST["list"];

            $menus = new Model_DbTable_Menus();
           
          /*  
           echo '<pre>';
            print_r($children);
            echo '</pre>'; */
            
            foreach ($list as $key => $value) {
              //echo "UPDATE menus SET posicao = '" . $key . "' WHERE id = '" . $value['id'] . "' AND itemsuperior_id = NULL\n";
                $menus->updatePosicao($key, $value['id'], NULL);
              foreach ($value['children'] as $key => $child) {
                 // echo "UPDATE submenus SET posicao = '" . $key . "' WHERE id = '" . $child['id'] . "' AND itemsuperior_id = '" . $value['id'] . "' \n";
                  $menus->updatePosicao($key, $child['id'], $value['id']);
              }
            }
            echo json_encode( array('success'=>true,  'msg'=>'Ordenação do menu, efectuada com sucesso!') );
            
        }else{
            echo json_encode( array('success'=>false,  'msg'=>'Lamentamos, não foi possível efectuar a ordenação!') );
        }
        
    }
    

    
	
}

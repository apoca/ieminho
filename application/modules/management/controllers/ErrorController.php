<?php

class Management_ErrorController extends Zend_Controller_Action {


    public function init() {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $this->view->action = strtolower($request->getActionName());
        $this->view->controller = strtolower($request->getControllerName());
        $this->view->module = strtolower($request->getModuleName());
        
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    function curPageURL() {
        $pageURL = 'http';
        $_SERVER["HTTPS"] = NULL;
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public function errorAction() {
        $this->view->title = "Ups... Ocorreu um erro";
        $this->view->titulo = "Ups... Ocorreu um erro";
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = '404 - Página não encontrada';
                $this->view->stack_trace = $this->_getFullErrorMessage($errors);
                //echo "<strong>Stack trace: </strong><pre>".$errors->exception->getTraceAsString()."</pre>";
                //echo "<strong>Erros de excepção: </strong>".$errors->exception;
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = '500 - Internal Server Error';
                $this->view->stack_trace = $this->_getFullErrorMessage($errors);
                //echo "<strong>Erros de excepção: </strong>".$errors->exception;
                //echo "<strong>Erros de request: </strong>".$errors->request;
                break;
        }

        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;
    }

    protected function _getFullErrorMessage($error) {
        $message = '<br />';

        if (!empty($_SERVER['SERVER_ADDR'])) {
            $message .= "Endereço IP: " . $_SERVER['SERVER_ADDR'] . "<br />";
        }

        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $message .= "Browser: " . $_SERVER['HTTP_USER_AGENT'] . "<br />";
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            $message .= "Request tipo: " . $_SERVER['HTTP_X_REQUESTED_WITH'] . "<br />";
        }

        $message .= "Servidor (hora): " . date("Y-m-d H:i:s") . "<br />";
        $message .= "RequestURI: " . $error->request->getRequestUri() . "<br />";

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $message .= "Referer: " . $_SERVER['HTTP_REFERER'] . "<br />";
        }

        $message .= "<br />Mensagem: " . $error->exception->getMessage() . "<br /><br />";
        // $message .= "Trace:\n" . $error->exception->getTraceAsString() . "<br /><br />";
        //  $message .= "Dados do Request: " . var_export($error->request->getParams(), true) . "<br /><br />";


        /* $it = $_SESSION;

          $message .= "Dados existentes em sessão:<br /><br />";
          foreach ($it as $key => $value) {
          $message .= $key . ": " . var_export($value, true) . "<br />";
          }
          $message .= "<br />";

          $message .= "Cookie data:\n\n";
          foreach ($_COOKIES as $key => $value) {
          $message .= $key . ": " . var_export($value, true) . "\n";
          }
          $message .= "\n"; */

        return $message;
    }

}
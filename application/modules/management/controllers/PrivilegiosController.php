<?php 
class Management_PrivilegiosController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    public function preDispatch() {
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = "A sua sessão terminou. Faça login novamente.";
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }
    }
    
    public function indexAction(){
        
        $role = $this->getRequest()->getParam('role');
        if($role == NULL){
            $role = 1;
        }
        $this->view->role = $role;
        $recursos = new Model_DbTable_Recursos();
        $this->view->recursos = $recursos->getAllRecursosActivos();
        
        $roles = new Model_DbTable_Roles();
        $this->view->roles = $roles->getAllRolesActivasMenosSU();
        
        $controllersActions = new App_Class_ControllersActions();
        $this->view->controllersActions = $controllersActions->byModule('management');
        
        
                        
    }
    
    public function configurarAction() {
        $recursos = new Model_DbTable_Recursos(); $this->view->recursosModel = $recursos;
        $this->view->recursos = $recursos->getAllRecursosActivos();
                
        $privilegios    = new Model_DbTable_Privilegios();
        $this->view->privilegiosModel = $privilegios;
        
        $controllersActions = new App_Class_ControllersActions();
        $this->view->controllersActions = $controllersActions->byModule('management');
        
    }
    
    public function adicionarRecursoAction(){
        
        $this->_helper->layout()->disableLayout();
        
        
        if ($this->getRequest()->isPost()){
                
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
                        
            $recursos = new Model_DbTable_Recursos();
                        
            if($recursos->addRecurso($dadosPOST['descricao'], $dadosPOST['controladorNome'], 'management', 'A', NULL)){
                echo json_encode(array('success'=>true) );
            }
            else{
                echo json_encode(array('success'=>false) );
            }
            
        }
        
    }
    
    public function adicionarPrivilegioAction(){
        
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
                        
            $privilegios = new Model_DbTable_Privilegios();    
            if($privilegios->addPrivilegio($dadosPOST['acao'], $dadosPOST['descricao'], $dadosPOST['controladorNome'], 'management', 'A')){
                echo json_encode(array('success'=>true));
            }
            else{
                echo json_encode(array('success'=>false));
            }
            
        }
    }


    public function recursosAction()
    {
        $this->_helper->layout()->disableLayout();
        $role = $this->getRequest()->getParam('role');
        if($role == NULL){
            $role = 1;
        }
        $this->view->role = $role;
        $this->view->regras = new Model_DbTable_Regras();
        $recurso = $this->getRequest()->getParam('recurso');
        
        $privilegios = new Model_DbTable_Privilegios();
        $this->view->privilegios = $privilegios->fetchAll($privilegios->select()->where('controladorNome = ?', $recurso)->order('acao ASC'));
    }
    
    public function alterarPrivilegioAction()
    {
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
                        
            $privilegio     = $dadosPOST['privilegio'];
            $role           = $dadosPOST['role'];
            $permitir       = $dadosPOST['permitir'];
            $privilegios    = new Model_DbTable_Privilegios();
            $priv           = $privilegios->getPrivilegio($privilegio);

            $role_id        = $role;
            $descricaoPr    = $priv['descricao'];
            $privilegio_id  = $priv['id_privilegio'];
            $modulo         = $priv['moduloNome'];
            $controller     = $priv['controladorNome'];
            $permitir       = $permitir;

            $regras = new Model_DbTable_Regras();
            //verificamos se regra ja existe inserida na BD
            //se existe alteramos o estado PERMITIR = $permitir
            if (($regras->existeRegra($role, $privilegio_id)>0)){
                
                $regras = new Model_DbTable_Regras();
                if($regras->updatePermitirRegra($permitir, $role, $privilegio_id))
                {
                    $opcao = (($permitir == 'S') ? 'adicionado' : 'retirado' );
                    echo json_encode(array('success'=>true, 'mensagem' => 'O privilégio '.$descricaoPr.' foi '.$opcao.' com sucesso!') );
                }else{
                    echo json_encode(array('success'=>false, 'mensagem' => 'Erro ao atribuir privilégio') );
                }
            }else{
                    $regras = new Model_DbTable_Regras();
                    if($regras->addRegra($role_id, $privilegio_id, $modulo, $controller, $permitir))
                    {
                        $opcao = (($permitir == 'S') ? 'adicionado' : 'retirado' );
                        echo json_encode(array('success'=>true, 'mensagem' => 'O privilégio '.$descricaoPr.' foi '.$opcao.' com sucesso!') );

                    }else{
                            echo json_encode(array('success'=>false, 'mensagem' => 'Erro ao atribuir privilégio') );
                    }
                }

            }
    }
	
}

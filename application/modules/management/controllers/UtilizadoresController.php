<?php 
class Management_UtilizadoresController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }
    
    public function preDispatch() {
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }
    }

    public function indexAction()
    {
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        
        $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
        $this->view->utilizadores = $adminUtilizadores->fetchAll();
    }
    
    public function imagensAction()
    {
        $this->_helper->layout()->disableLayout();
        
        if(isset($_FILES['uploadfile']['name'])){
            
            $ext = explode('.',$_FILES['uploadfile']['name']);
            $extension = $ext[1];
            
            $newname = $ext[0].'_'.time();
            $file = './uploads/images/utilizadores/'.$newname.'.'.$extension ;
            
            if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
                 echo json_encode( array('success'=>true,  'file'=>$newname.'.'.$extension) );
            } else {
                 echo json_encode(array('success'=>false, 'file'=>'') );
            }
        } 
    }  
    
    public function eliminarImagemAction(){
        $this->_helper->layout()->disableLayout();
        $imagem = $this->getRequest()->getParam('imagem');
        if(unlink('uploads/images/utilizadores/'.$imagem)){
            echo 'sucess';
        }else{
            echo 'error';
        }
        return;
    }
    
    public function detalheAction(){
        
        $id = $this->getRequest()->getParam('id');
        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();

        $this->view->userSession = $infoUtilizador;
        if($id==NULL){
            $id = $infoUtilizador->id;  
        }
        $this->view->id = $id;
        
        
        $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
        $utilizador = $adminUtilizadores->fetchRow($adminUtilizadores->select()->where('id = ?', $id));
        $this->view->utilizador = $utilizador;
        
        $roles = new Model_DbTable_Roles();
        $this->view->role         = $roles->descricaoById($utilizador->role_id);
        
    }
    
    public function perfilAction()
    {
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();

        $this->view->utilizador = $infoUtilizador;
        $id = $infoUtilizador->id;
        $this->view->id = $id;
                
        if ($this->getRequest()->isPost()) {
            $nome		= $this->getRequest()->getPost('nome');
            $apelido 		= $this->getRequest()->getPost('apelido');
            $email		= $this->getRequest()->getPost('email');
            $login		= $this->getRequest()->getPost('login');
            $password 		= $this->getRequest()->getPost('password');
            $foto 		= $this->getRequest()->getPost('fileImage');
            $estado 		= $this->getRequest()->getPost('estado');
            $role_id 		= $this->getRequest()->getPost('role');
            
            $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
            $utilizador = $adminUtilizadores->fetchRow($adminUtilizadores->select()->where('id = ?', $id));
            
            if ($password == null || $password == '') {
                $password = $utilizador->password;
            } else {
                $password = sha1($password);
            }
            
            if ($estado == null || $estado == '') {
                $estado = $utilizador->estado;
            }
            
            if ($role_id == null || $role_id == '') {
                $role_id = $utilizador->role_id;
            }
            
            $adminUtilizadores->updateUtilizador($id, $nome, $apelido, $login, $password,$email,$foto,$role_id,$estado,$_SERVER['REMOTE_ADDR']);
            
             
             $storage = new Zend_Auth_Storage_Session();
             $id = $infoUtilizador->id;
             $storage->clear();
            
             $authSession = new Zend_Auth_Storage_Session();
             $storage->timeout = 300000;
             $userInfo = new stdClass;
                    
             $userInfo->id           = $id;
             $userInfo->nome         = $nome;
             $userInfo->apelido      = $apelido;
             $userInfo->login        = $login;
             $userInfo->password     = $password;
             $userInfo->email        = $email;
             $userInfo->foto         = $foto;
             $userInfo->estado       = $estado;
             $userInfo->role_id      = $role_id;

             $roles = new Model_DbTable_Roles();
             $userInfo->role         = $roles->descricaoById($role_id);
            
             $authSession->write($userInfo);
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');
            $this->_redirect('/management/'.$this->_controller.'');
             
        }
    }
    
    public function adicionarAction()
    {
        
        if ($this->getRequest()->isPost()) {
            
            $nome		= $this->getRequest()->getPost('nome');
            $apelido 		= $this->getRequest()->getPost('apelido');
            $login		= $this->getRequest()->getPost('login');
            $password 		= $this->getRequest()->getPost('password');
            $email		= $this->getRequest()->getPost('email');
            $foto 		= $this->getRequest()->getPost('fileImage');
            $role_id 		= $this->getRequest()->getPost('role_id');
            $estado 		= $this->getRequest()->getPost('estado');
            
            $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
            $adminUtilizadores->addUtilizador($nome, $apelido, $login, sha1($password), $email, $foto, $role_id, $estado, $_SERVER['REMOTE_ADDR']);
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_adicionadoSucesso');
            $this->_redirect('/management/'.$this->_controller.'/');
             
        }
    }
    
    public function editarAction()
    {        
        $id = $this->getRequest()->getParam('id');
        $this->view->id = $id;
                
        $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
        $utilizador = $adminUtilizadores->fetchRow($adminUtilizadores->select()->where('id = ?', $id));
        $this->view->utilizador = $utilizador;
        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();

        $this->view->userSession = $infoUtilizador;
                
        if ($this->getRequest()->isPost()) {
            
            $nome		= $this->getRequest()->getPost('nome');
            $apelido 		= $this->getRequest()->getPost('apelido');
            $login		= $this->getRequest()->getPost('login');
            $password 		= $this->getRequest()->getPost('password');
            $email		= $this->getRequest()->getPost('email');
            $foto 		= $this->getRequest()->getPost('fileImage');
            $role_id 		= $this->getRequest()->getPost('role_id');
            $estado 		= $this->getRequest()->getPost('estado');
            
            $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
            $utilizador = $adminUtilizadores->fetchRow($adminUtilizadores->select()->where('id = ?', $id));
            
            if ($password == null || $password == ''){
                $password = $utilizador->password;
            } else {
                $password = sha1($password);
            }
            
            $adminUtilizadores->updateUtilizador($id, $nome, $apelido, $login, $password,$email,$foto,$role_id,$estado,$_SERVER['REMOTE_ADDR']);
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');
            $this->_redirect('/management/'.$this->_controller.'/');
             
        }
    }
    
    
    public function verificaEmailAction() {
        $this->_helper->layout()->disableLayout();
        extract($_POST);
        $email = $this->getRequest()->getPost('email');
        if (isset($email)) {
            $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
            $existe = $adminUtilizadores->existeEmailUtilizador($email);
            echo ($existe > 0) ? 's' : 'n';
            return;
        }
    }
    
    public function verificaLoginAction() {
        $this->_helper->layout()->disableLayout();
        extract($_POST);
        $login = $this->getRequest()->getPost('login');
        if (isset($login)) {
            $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
            $existe = $adminUtilizadores->existeLoginUtilizador($login);
            echo ($existe > 0) ? 's' : 'n';
            return;
        }
    }
    
    
    
    public function apagarAction(){
        
        $id = $this->getRequest()->getParam('id');
        $this->view->id = $id;
                
        $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
        $adminUtilizadores->deleteUtilizador($id);
        
        $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_eliminadoSucesso');
        $this->_redirect('/management/'.$this->_controller.'/');
    }
    
}

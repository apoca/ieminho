<?php 
class Management_ConteudosController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }
    //tree view builder http://kotowicz.net/jquery-option-tree/demo/demo.html
    
    public function preDispatch() {        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }else{
            $isAllowed = Sbm_RuleChecker::isAllowed($infoUtilizador->roleName, $this->_action, $this->_controller);
            if(!$isAllowed){
                $this->_helper->layout()->disableLayout(); $this->_helper->layout->setLayout('denied');
            }
        }
    }
    
    public function indexAction(){
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        
        $tipoConteudos = new Model_DbTable_TiposConteudos();
        $this->view->registos = $tipoConteudos->fetchAll($tipoConteudos->select()->where('idioma_iso = ?', $idioma));
    }
    
    public function obterConteudosAction(){
        $this->_helper->layout()->disableLayout();
        $tipo_id = $this->getRequest()->getParam('tipo'); 
        $this->view->tipo_id = $tipo_id;
        
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        
        
        $conteudos = new Model_DbTable_Conteudos();
        $paginator = Zend_Paginator::factory($conteudos->fetchAll($conteudos->select()->where('tipo_id = ?', $tipo_id)->where('idioma_iso = ?', $idioma)->order('id DESC')));
        
        $this->view->nItens = $conteudos->fetchAll($conteudos->select()->where('tipo_id = ?', $tipo_id));

        $paginator->setCurrentPageNumber($this->_getParam("page")); 
        $paginator->setItemCountPerPage(10);//itens por pagina
        $paginator->setPageRange(5); //
        $this->view->conteudos = $paginator;      
    }
    
    public function adicionarAction()
    {
        $tipo_id = $this->getRequest()->getParam('tipo'); 
        if($tipo_id){
            $this->view->tipo_id = $tipo_id;;
            
            $this->view->imagensConteudo = array();
            
            $end_date = new Zend_Date(); //2013-04-17 18:03:35
            $this->view->inicioPublicacao = $end_date->get(Zend_Date::YEAR."-". Zend_Date::MONTH ."-".Zend_Date::DAY." ".Zend_Date::HOUR.":".Zend_Date::MINUTE.":".Zend_Date::SECOND );
            $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
            $this->view->idioma = $idioma;
            
            $tiposConteudo = new Model_DbTable_TiposConteudos();
            $this->view->tipoConteudo = $tiposConteudo->fetchRow($tiposConteudo->select()->where('id = ?', $tipo_id));
            
            
            $caractTipoC = new Model_DbTable_CaracteristicasTiposConteudos();
            $carTipoCont = $caractTipoC->getCaracteristicasTiposConteudos($tipo_id);
            $this->view->caracteristicas = $carTipoCont;
            
            $multiplefiles = false;
            foreach ($carTipoCont as $key => $record) {
                $atributosTipoCont = new Model_DbTable_AtributosTiposConteudos();
                $attTyC = $atributosTipoCont->fetchRow($atributosTipoCont->select()->where('caracteristica_id = ?', $record->id));
                
                if($attTyC->fieldtype == 'multiplefiles'){
                    $multiplefiles = true;
                }
            }
            
            

            $this->view->conteudosTiposConteudos = new Model_DbTable_ConteudosTiposConteudos();
            $this->view->caracteristicasTiposConteudos = new Model_DbTable_CaracteristicasTiposConteudos();
            $this->view->atributosTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
            
            if(isset ($_GET["qqfile"]) && strlen($_GET["qqfile"])){
                $allowedExtensions = array();
                // max file size in bytes
                $sizeLimit = 1 * 1024 * 1024;

                $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload('uploads/images/conteudos/');
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                exit;
            }
            else
            if ($this->getRequest()->isPost()) {
                $conteudos = new Model_DbTable_Conteudos();
                
               /* $formData = $this->getRequest()->getPost();
                
                echo '<pre>';
                echo htmlspecialchars(print_r($_POST, true));
                echo '</pre>';

                echo '<pre>';
                echo htmlspecialchars(print_r($_FILES, true));
                echo '</pre>';return; */
                
                $designacao         = $this->getRequest()->getPost('designacao');
                $descricao          = $this->getRequest()->getPost('descricao');
                $foto               = $this->getRequest()->getPost('foto');
                $estado             = $this->getRequest()->getPost('estado') != NULL ? $this->getRequest()->getPost('estado') : 'N';
                $inicioPublicacao   = $_POST['inicioPublicacao'];
                $fimPublicacao      = ($_POST['fimPublicacao'] != '') ? $_POST['fimPublicacao'] : '0000-00-00 00:00:00';
                
                $conteudos              = new Model_DbTable_Conteudos();
                $posicao = $conteudos->getQtdPosicoesIdiomaTipo($tipo_id, $idioma);
                $conteudos->addConteudo($designacao, $descricao, $foto, $posicao, $estado, 0, $idioma, $tipo_id, date("Y-m-d H:i:s"), $inicioPublicacao, $fimPublicacao);

                $db = Zend_Db_Table::getDefaultAdapter();
                $id = $conteudo_id = $db->lastInsertId();
                
                if($multiplefiles==true){
                   $filesConteudo = new Model_DbTable_ConteudosMultipleFiles();
                   $filesConteudo->updateTokenToConteudoId($_SESSION['token'], $id); 
                   unset($_SESSION['token']);
                }
                
                
                $conteudos->updateOpcao($id, 'main_id', $id) ;
                
                $caractTipoC = new Model_DbTable_CaracteristicasTiposConteudos();
                $caracteristicas = $caractTipoC->getCaracteristicasTiposConteudos($tipo_id);
                
                if ($caracteristicas > 0) {
                    foreach ($caracteristicas as $key => $caracteristica) {
                        if ($caracteristica->referencia == 'html') {
                            $atributosTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
                            $atributos = $atributosTiposConteudos->getAtributosTiposConteudos($caracteristica->id);
                            $valoresAttRef = new Model_DbTable_ValoresAtributosReferencia();

                            foreach ($atributos as $i => $atributo) {

                                    //se o atributo for image | file teremos, para alem de obter o atributo, fazer o UPLOAD
                                    if ($atributo->fieldtype == 'image' || $atributo->fieldtype == 'file') {
                                        //  echo 'Ficheiros: '.$_FILES[''.$atributo->name.'']["name"].'<br />';

                                        if ($atributo->fieldtype == 'image') {
                                            $caminho = PUBLIC_PATH . DS . 'imagens' . DS . 'conteudos';
                                        } else {
                                            $caminho = PUBLIC_PATH . DS . 'ficheiros' . DS . 'conteudos';
                                        }

                                        $newFile = new App_Class_File();
                                        $newName = $newFile->rename($_FILES['' . $atributo->name . '']["name"]);

                                        if (move_uploaded_file($_FILES['' . $atributo->name . '']['tmp_name'], $caminho . DS . $newName)) {
                                            echo 'Sucesso imagem!<br />';
                                        } else {
                                            //     echo 'Erro ao adicionar imagem.<br />';
                                        }

                                        $valoresAttRef->addValorAtributoReferencia($caracteristica->referencia, $atributo->id, $conteudo_id, $newName);
                                    } else {
                                        // echo 'O valor de ['.$atributo->name.'] é: '.$_POST[''.$atributo->name.''].'<br />';
                                        //verificar se existe
                                        $valoresAttRef->addValorAtributoReferencia($caracteristica->referencia, $atributo->id, $conteudo_id, $_POST['' . $atributo->name . '']);
                                    }
                            }
                        }else { // caracteristica->referencia == conteudo
                            $conteudosTiposConteudos = new Model_DbTable_ConteudosTiposConteudos();
                            $conteudosA = $conteudosTiposConteudos->getConteudosTiposConteudos($caracteristica->id);
                            $valoresAttRef = new Model_DbTable_ValoresAtributosReferencia();

                            foreach ($conteudosA as $j => $conteudo) {
                                //echo 'We must get value of: '.$conteudo->name.' thats is '.$_POST[''.$conteudo->name.''].' <br />';
                                $valoresAttRef->addValorAtributoReferencia($caracteristica->referencia, $conteudo->id, $conteudo_id, $_POST['' . $conteudo->name . '']);
                            }
                        }
                    }
                }
            
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_adicionadoSucesso');
                //verificar $tipo_id associado em cada idioma e se tiver adiciona
                //ex. Notícias (pt) News (en) ...
                if (count($this->_funcoes->outrosIdiomas($idioma))>0){
                    
                    foreach ($this->_funcoes->outrosIdiomas($idioma) as $key=>$outro){
                        
                        if ($key == 0) {
                            $proximoIdioma = $outro->idioma_iso;
                            $tipoContAssociado = $tiposConteudo->tipoConteudoIdioma($tipo_id, $outro->idioma_iso)->id;
                        }
                        $tipoConteudoIdioma_id = $tiposConteudo->tipoConteudoIdioma($tipo_id, $outro->idioma_iso)->id;
                        if($tipoConteudoIdioma_id != NULL){
                            $conteudos->addConteudo('', '', '', 0, 'I', $id, $outro->idioma_iso, $tipoConteudoIdioma_id, date("Y-m-d H:i:s"), $inicioPublicacao, $fimPublicacao);
                        }
                    }
                    $this->_redirect('/management/'.$this->_controller.'/editar/main_id/'.$id.'/idioma/'.$proximoIdioma.'/tipo/'.$tipoContAssociado);
                        
                }else{
                   $this->_redirect('/management/'.$this->_controller); 
                }
            }
        }
    }
    
    
    public function editarAction()
    {        
        $main_id = $this->getRequest()->getParam('main_id');
        $tipo_id = $this->getRequest()->getParam('tipo'); 
        $idioma = $this->getRequest()->getParam('idioma'); 
        if($main_id){
            $this->view->main_id    = $main_id;
            $conteudos              = new Model_DbTable_Conteudos();
            $conteudo               = $conteudos->fetchRow($conteudos->select()->where('main_id = ?', $main_id)->where('tipo_id = ?', $tipo_id)->where('idioma_iso = ?', $idioma));
            $this->view->conteudo   = $conteudo;
            $this->view->id         = $conteudo->id;
            
        }else{
            $id = $this->getRequest()->getParam('id');
            $this->view->id         = $id;
            $conteudos              = new Model_DbTable_Conteudos();
            $conteudo               = $conteudos->fetchRow($conteudos->select()->where('id = ?', $id));
            $this->view->conteudo   = $conteudo;
        }
        $tiposConteudo = new Model_DbTable_TiposConteudos();
        $this->view->tipoConteudo = $tiposConteudo->fetchRow($tiposConteudo->select()->where('id = ?', $conteudo->tipo_id));
        
        $tipo_id = $conteudo->tipo_id;
        $this->view->tipo_id = $tipo_id;
        $this->view->idioma = $conteudo->idioma_iso;
                
        $caractTipoC = new Model_DbTable_CaracteristicasTiposConteudos();
        $carTipoCont = $caractTipoC->getCaracteristicasTiposConteudos($tipo_id);
        $this->view->caracteristicas = $carTipoCont;

        $multiplefiles = false;
        foreach ($carTipoCont as $key => $record) {
            $atributosTipoCont = new Model_DbTable_AtributosTiposConteudos();
            $attTyC = $atributosTipoCont->fetchRow($atributosTipoCont->select()->where('caracteristica_id = ?', $record->id));

            if($attTyC->fieldtype == 'multiplefiles'){
                $multiplefiles = true;
            }
        }
        
        if($multiplefiles==true){
            $filesConteudo = new Model_DbTable_ConteudosMultipleFiles();
            $this->view->ficheirosConteudo = $filesConteudo->getAllFilesContentID($conteudo->id);
        }else{
            $this->view->ficheirosConteudo = array();
        }

        $this->view->conteudosTiposConteudos = new Model_DbTable_ConteudosTiposConteudos();
        $this->view->caracteristicasTiposConteudos = new Model_DbTable_CaracteristicasTiposConteudos();
        $this->view->atributosTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
        
        
        
        
        
        if(isset($_GET["qqfile"]) && strlen($_GET["qqfile"])){
            $allowedExtensions = array();
            // max file size in bytes
            $sizeLimit = 1 * 1024 * 1024;

            $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload('uploads/images/conteudos/');
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            exit;
        }
        else
        if ($this->getRequest()->isPost()) {
            
            $designacao         = $this->getRequest()->getPost('designacao');
            $descricao          = $this->getRequest()->getPost('descricao');
            $foto               = $this->getRequest()->getPost('foto');
            $estado             = $this->getRequest()->getPost('estado') != NULL ? $this->getRequest()->getPost('estado') : 'N';
            $inicioPublicacao   = $_POST['inicioPublicacao'];
            $fimPublicacao      = ($_POST['fimPublicacao'] != '') ? $_POST['fimPublicacao'] : '0000-00-00 00:00:00';

            $conteudos          = new Model_DbTable_Conteudos();
                            
            $conteudos              = new Model_DbTable_Conteudos();
            $conteudos->updateConteudo($conteudo->id, $designacao, $descricao, $foto, $conteudo->posicao, $estado, $conteudo->main_id, $conteudo->idioma_iso, $conteudo->tipo_id, date("Y-m-d H:i:s"), $inicioPublicacao, $fimPublicacao);
            
            $caractTipoC = new Model_DbTable_CaracteristicasTiposConteudos();
            $caracteristicas = $caractTipoC->getCaracteristicasTiposConteudos($tipo_id);
            
            if ($caracteristicas > 0) {
                foreach ($caracteristicas as $key => $caracteristica) {
                    if ($caracteristica->referencia == 'html') {
                        $atributosTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
                        $atributos = $atributosTiposConteudos->getAtributosTiposConteudos($caracteristica->id);
                        $valoresAttRef = new Model_DbTable_ValoresAtributosReferencia();

                        foreach ($atributos as $i => $atributo) {

                                //se o atributo for image | file teremos, para alem de obter o atributo, fazer o UPLOAD
                                if ($atributo->fieldtype == 'image' || $atributo->fieldtype == 'file') {
                                    //  echo 'Ficheiros: '.$_FILES[''.$atributo->name.'']["name"].'<br />';
                                    if ($atributo->fieldtype == 'image') {
                                        $caminho = PUBLIC_PATH . DS . 'imagens' . DS . 'conteudos';
                                    } else {
                                        $caminho = PUBLIC_PATH . DS . 'ficheiros' . DS . 'conteudos';
                                    }

                                    //    echo 'Val.:'. $_FILES[''.$atributo->name.'']["name"].' <br />';

                                    if ($_FILES['' . $atributo->name . '']["name"] != '') {

                                        $newFile = new App_Class_File();
                                        $newName = $newFile->rename($_FILES['' . $atributo->name . '']["name"]);

                                        if (move_uploaded_file($_FILES['' . $atributo->name . '']['tmp_name'], $caminho . DS . $newName)) {
                                            echo 'Sucesso imagem!<br />';
                                        } else {
                                            //     echo 'Erro ao adicionar imagem.<br />';
                                        }
                                    } else {
                                        $newName = $formData['' . $atributo->name . '_old'];
                                    }

                                    echo 'Val new name.:' . $newName . ' <br />';

                                    $valoresAttRef->updateValorAtributoReferencia($atributo->id, $conteudo->id, $newName);
                                } else {
                                    echo 'O valor de [' . $atributo->name . '] é: ' . $_POST['' . $atributo->name . ''] . '<br />';
                                    //verificar se existe

                                    if ($valoresAttRef->existeValorAtributoConteudo($atributo->id, $conteudo->id) > 0) {
                                        $valoresAttRef->updateValorAtributoReferencia($atributo->id, $conteudo->id, $_POST['' . $atributo->name . '']);
                                    } else {
                                        $valoresAttRef->addValorAtributoReferencia($caracteristica->referencia, $atributo->id, $conteudo->id, $_POST['' . $atributo->name . '']);
                                    }
                                }
                        }
                    } else { // caracteristica->referencia == conteudo
                        $conteudosTiposConteudos = new Model_DbTable_ConteudosTiposConteudos();
                        $conteudosA = $conteudosTiposConteudos->getConteudosTiposConteudos($caracteristica->id);
                        $valoresAttRef = new Model_DbTable_ValoresAtributosReferencia();

                        foreach ($conteudosA as $j => $conteudo) {
                            //echo 'We must get value of: '.$conteudo->name.' thats is '.$_POST[''.$conteudo->name.''].' <br />';
                            $valoresAttRef->updateValorAtributoReferencia($conteudo->id, $conteudo->id, $_POST['' . $conteudo->name . '']);
                        }
                    }
                }
            }
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');  
            
            if (count($this->_funcoes->outrosIdiomas($idioma))>0){
                    
                foreach ($this->_funcoes->outrosIdiomas($idioma) as $key=>$outro){
                    $registos = array();
                    $estado = $conteudos->conteudoIdioma($conteudo->main_id, $outro->idioma_iso)->estado;
                    if($estado=='I'){
                        $registos[] = array(
                                'main_id'	=> $conteudo->main_id,
                                'idioma_iso'	=> $outro->idioma_iso,
                                'tipo'          => $conteudos->conteudoIdioma($conteudo->main_id, $outro->idioma_iso)->tipo_id,
                        ); 
                    }
                }
                if(count($registos)>0){
                    $this->_redirect('/management/'.$this->_controller.'/editar/main_id/'.$registos[0]['main_id'].'/idioma/'.$registos[0]['idioma_iso'].'/tipo/'.$registos[0]['tipo']);
                }else{
                    $this->_redirect('/management/'.$this->_controller); 
                }
                
            }else{
                $this->_redirect('/management/'.$this->_controller); 
            }
            
             
        }
    }
    
    public function cropImagemAction()
    {
        $this->_helper->layout()->disableLayout();
        $image_data = file_get_contents($_REQUEST['url']);
        $tipo= $this->getRequest()->getParam('tipo');
        
        file_put_contents("uploads/images/".$tipo."/".$_REQUEST['image'],$image_data);
    }
    
    public function detalheAction()
    {
        $main_id = $this->getRequest()->getParam('main_id');
        $tipo_id = $this->getRequest()->getParam('tipo'); 
        $idioma = $this->getRequest()->getParam('idioma'); 
        if($main_id){
            $this->view->main_id    = $main_id;
            $conteudos              = new Model_DbTable_Conteudos();
            $conteudo               = $conteudos->fetchRow($conteudos->select()->where('main_id = ?', $main_id)->where('tipo_id = ?', $tipo_id)->where('idioma_iso = ?', $idioma));
            $this->view->conteudo   = $conteudo;
            $this->view->id         = $conteudo->id;
            
        }else{
            $id = $this->getRequest()->getParam('id');
            $this->view->id         = $id;
            $conteudos              = new Model_DbTable_Conteudos();
            $conteudo               = $conteudos->fetchRow($conteudos->select()->where('id = ?', $id));
            $this->view->conteudo   = $conteudo;
        }
        
        $tiposConteudo = new Model_DbTable_TiposConteudos();
        $this->view->tipoConteudo = $tiposConteudo->fetchRow($tiposConteudo->select()->where('id = ?', $conteudo->tipo_id));
        
        $tipo_id = $conteudo->tipo_id;
        $this->view->tipo_id = $tipo_id;
        $this->view->idioma = $conteudo->idioma_iso;
                
        $caractTipoC = new Model_DbTable_CaracteristicasTiposConteudos();
        $carTipoCont = $caractTipoC->getCaracteristicasTiposConteudos($tipo_id);
        $this->view->caracteristicas = $carTipoCont;

        $multiplefiles = false;
        foreach ($carTipoCont as $key => $record) {
            $atributosTipoCont = new Model_DbTable_AtributosTiposConteudos();
            $attTyC = $atributosTipoCont->fetchRow($atributosTipoCont->select()->where('caracteristica_id = ?', $record->id));

            if($attTyC->fieldtype == 'multiplefiles'){
                $multiplefiles = true;
            }
        }
        
        if($multiplefiles==true){
            $filesConteudo = new Model_DbTable_ConteudosMultipleFiles();
            $this->view->ficheirosConteudo = $filesConteudo->getAllFilesContentID($conteudo->id);
        }else{
            $this->view->ficheirosConteudo = array();
        }
    }
    
    public function pesquisaAction()
    {
        $this->_helper->layout()->disableLayout();
        $pesquisa = $this->getRequest()->getParam('item');
        
         
        $conteudos              = new Model_DbTable_Conteudos();       
              
        $this->view->registos = $conteudos->fetchAll($conteudos->select()->where('estado = ?', 'A')->where('designacao LIKE ?', '%'.$pesquisa.'%')->orWhere('descricao LIKE ?', '%'.$pesquisa.'%')->order('posicao ASC'));
        
    }
    
    
    public function eliminarImagemAction(){
        $this->_helper->layout()->disableLayout();
        $imagem = $this->getRequest()->getParam('imagem');
        $tipo = $this->getRequest()->getParam('tipo');
        $conteudo_id = $this->getRequest()->getParam('conteudo_id');
        
        if(unlink('uploads/images/'.$tipo.'/'.$imagem)){
            if($conteudo_id != NULL && $conteudo_id !=0){
                $conteudos = new Model_DbTable_Conteudos();
                $conteudos->updateOpcao($conteudo_id, 'foto', '');
            }
            echo 'sucess'; 
            return;
            
        }else{
            echo 'error'; return;
        }
    }
    
    
    
    public function apagarAction()
    {
        $id = $this->getRequest()->getParam('id');
                    
        $conteudos = new Model_DbTable_Conteudos();
        $conteudos->deleteConteudo($id);

        $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_eliminadoSucesso');
        $this->_redirect('/management/'.$this->_controller); 
         
    }
    
    
    public function ordenarAction(){
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost())
        {
                $categorias = new Model_DbTable_Categorias();
                $listItem = $_POST['order'];
                foreach ($listItem as $position=>$item){
                    //$sql[] = "UPDATE `areasnegocio` SET `posicao` = $position WHERE `id` = $item <br />";
                    $categorias->updatePosicao($item, $position);
                }
        }
        return;
    }
    
    /*galeria de ficheiros por conteúdo*/
    public function ordenarImagensAction() {
        $this->_helper->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $multiFiles = new Model_DbTable_ConteudosMultipleFiles();
            $listItem = $_POST['order'];
            foreach ($listItem as $position => $item) {
                $multiFiles->updatePosicao($item, $position);
            }
        }
        return;
    }

    public function adicionarFicheirosAction() {
        $this->_helper->layout()->disableLayout();
        $_SESSION['token'] = isset($_SESSION['token']) ? $_SESSION['token'] : md5(uniqid(rand(), true));
        //$_SESSION['token'] != '' ? md5(uniqid(rand(), true)) : $_SESSION['token'];

        if (isset($_GET["qqfile"]) && strlen($_GET["qqfile"])) {
            
            $attTiposConteudo = new Model_DbTable_AtributosTiposConteudos();
            $atributoTipoConteudo = $attTiposConteudo->fetchRow($attTiposConteudo->select()->where('id = ?', $_GET["atributo_id"]));
            
            $allowedExtensions = ($atributoTipoConteudo->restricoes != NULL ?  (array)explode(',', $atributoTipoConteudo->restricoes) : array());
            
            // max file size in bytes
            $sizeLimit = 10 * 1024 * 1024;

            $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
            
            $result = $uploader->handleUpload('uploads/images/conteudos-media/');
            
            if($result['success'] == true){
                
                $filesContent = new Model_DbTable_ConteudosMultipleFiles();
                if($_GET["accao"] == 'editar'){
                    $posicao = $filesContent->getQtdPosicoes($_GET['conteudo_id']);
                    //as action is edit we can update table with conteudo_id
                    $filesContent->addFileConteudo($result['nameFile'], $_SESSION['token'], $posicao, $_GET['conteudo_id'], $result['extension']);
                }else{
                    $posicao = $filesContent->getQtdPosicoesToken($_SESSION['token']);
                    //as we do not know the id of the content set record to 0 and after update
                    $filesContent->addFileConteudo($result['nameFile'], $_SESSION['token'], $posicao, 0, $result['extension']);
                }
                

                
                $db = Zend_Db_Table::getDefaultAdapter();
                $result['id'] = $db->lastInsertId();
                $result['success'] = true;
                $result['token'] = $_SESSION['token'];
                $result['posicao'] = $posicao;
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                exit;
                
            }else{
                
                echo htmlspecialchars(json_encode(array('error'=>$result['error'])), ENT_NOQUOTES);
            }
            
        }
    }

    public function eliminarFicheiroAction() {
        $this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()) {
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST');

            $filesContent = new Model_DbTable_ConteudosMultipleFiles();
            $file = $filesContent->ficheiroConteudoByToken($dadosPOST['imagem'], $dadosPOST['token']);
            $result = array();
            if ($filesContent->deleteFileConteudo($file->id)) {

                unlink(CAMINHO.DS."uploads".DS."images".DS."conteudos-media".DS.$dadosPOST['imagem']);
                $result['success'] = true;
            } else {
                $result['success'] = false;
            }
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        return;
    }
    
    
    
    
    
    

    
	
}

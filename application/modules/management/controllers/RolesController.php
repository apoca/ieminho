<?php 
class Management_RolesController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    public function preDispatch() {
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = "A sua sessão terminou. Faça login novamente.";
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }else{
            $isAllowed = Sbm_RuleChecker::isAllowed($infoUtilizador->roleName, $this->_action, $this->_controller);
            if(!$isAllowed){
                $this->_helper->layout()->disableLayout(); $this->_helper->layout->setLayout('denied');
            }
        }
    }
    
    public function indexAction() {
        $roles = new Model_DbTable_Roles();
        $this->view->roles = $roles->getAllRolesActivasMenosSU();
    }
    
    public function adicionarAction() {

        if ($this->getRequest()->isPost()) {
            $designacao		= $this->getRequest()->getPost('designacao');
            $descricao          = $this->getRequest()->getPost('descricao');
            $estado             = $this->getRequest()->getPost('estado');
            
            $roles = new Model_DbTable_Roles();
            $roles->addRole($designacao, $descricao, $estado);
            $db = Zend_Db_Table::getDefaultAdapter();
            $role_id = $db->lastInsertId();

            //adicionar regras basicas
            $regras = new Model_DbTable_Regras();
            //$regras->addRegra($role_id, '149', 'management', 'utilizadores', 'S');//poder editar o próprio perfil

            $this->_sessao->display = "Role adicionada com sucesso.";
            $this->_redirect('/management/roles/editar/id/'.$role_id);
        }
    }

    public function detalheAction() {
        $id = $this->getRequest()->getParam('id');
        $this->view->id = $id;
        
        $roles = new Model_DbTable_Roles();
        $this->view->role = $roles->fetchRow($roles->select()->where('id_role = ?', $id)); 
        
        $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
        $this->view->quantos = $adminUtilizadores->howManyByRole($id);
        
        $recursos = new Model_DbTable_Recursos();
       // $this->view->recursos = $recursos->getAllRecursosActivos();
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $sqlquery = "SELECT DISTINCT SUBSTR(recursoNome, 12) as controladorNome FROM admin_regras WHERE role_id ='".$id."' AND permitir ='S'" ; //12 = management:
        $this->view->recursos = $db->query($sqlquery)->fetchAll();
        
        

    }
    
    public function recursosAction()
    {
        $this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()){
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 

            $role = $dadosPOST['role'];
            $this->view->role = $role;
            $accao = $dadosPOST['accao'];
            $this->view->accao = $accao;
            $this->view->regras = new Model_DbTable_Regras();
            $recurso = $dadosPOST['recurso'];

            $privilegios = new Model_DbTable_Privilegios();
            $this->view->privilegios = $privilegios->fetchAll($privilegios->select()->where('controladorNome = ?', $recurso)->order('acao ASC'));
        }
    }
    
    public function editarAction() {
        $id = $this->getRequest()->getParam('id');
        $this->view->id = $id;
        
        $roles = new Model_DbTable_Roles();
        $this->view->role = $roles->fetchRow($roles->select()->where('id_role = ?', $id)); 
        
        $recursos = new Model_DbTable_Recursos();
        $this->view->recursos = $recursos->getAllRecursosActivos();
        
        
        
        if ($this->getRequest()->isPost()) {
            $designacao		= $this->getRequest()->getPost('designacao');
            $descricao          = $this->getRequest()->getPost('descricao');
            $estado             = $this->getRequest()->getPost('estado');

            $roles = new Model_DbTable_Roles();
            $roles->updateRole($id, $designacao, $descricao, $estado);

            $this->_sessao->display = "Role actualizada com sucesso.";
            $this->_redirect('/management/'.$this->_controller.'');
        }
    }
    
    public function updateRoleAction() {        
        $this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()) {
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
            
            $roles = new Model_DbTable_Roles();
            $roles->updateRole($dadosPOST['id'], $dadosPOST['designacao'], $dadosPOST['descricao'], $dadosPOST['estado']);
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');
            echo json_encode(array('success'=>true, 'mensagem' => Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso')) );
        }
    }
    
    public function alterarPrivilegioAction()
    {
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
                        
            $privilegio     = $dadosPOST['privilegio'];
            $role           = $dadosPOST['role'];
            $permitir       = $dadosPOST['permitir'];
            $privilegios    = new Model_DbTable_Privilegios();
            $priv           = $privilegios->getPrivilegio($privilegio);

            $role_id        = $role;
            $descricaoPr    = $priv['descricao'];
            $privilegio_id  = $priv['id_privilegio'];
            $modulo         = $priv['moduloNome'];
            $controller     = $priv['controladorNome'];
            $permitir       = $permitir;

            $regras = new Model_DbTable_Regras();
            
            //verificamos se regra ja existe inserida na BD
            //se existe alteramos o estado PERMITIR = $permitir
            if (($regras->existeRegra($role, $privilegio_id)>0)){
                
                $regras = new Model_DbTable_Regras();
                if($regras->updatePermitirRegra($permitir, $role, $privilegio_id))
                {
                    //how many recources by role ID 
                    $quantos = $regras->quantosRecursosDeRole($controller, $role); 
                    $opcao = (($permitir == 'S') ? 'adicionado' : 'retirado' );
                    echo json_encode(array('success'=>true, 'mensagem' => 'O privilégio '.$descricaoPr.' foi '.$opcao.' com sucesso!','recurso'=>$controller,'quantos'=>$quantos) );
                }else{
                    echo json_encode(array('success'=>false, 'mensagem' => 'Erro ao atribuir privilégio') );
                }
            }else{
                    $regras = new Model_DbTable_Regras();
                    if($regras->addRegra($role_id, $privilegio_id, $modulo, $controller, $permitir))
                    {
                        //how many recources by role ID 
                        $quantos = $regras->quantosRecursosDeRole($controller, $role);
                        $opcao = (($permitir == 'S') ? 'adicionado' : 'retirado' );
                        echo json_encode(array('success'=>true, 'mensagem' => 'O privilégio '.$descricaoPr.' foi '.$opcao.' com sucesso!','recurso'=>$controller,'quantos'=>$quantos) );

                    }else{
                            echo json_encode(array('success'=>false, 'mensagem' => 'Erro ao atribuir privilégio') );
                    }
                }

            }
    }

    public function apagarAction() {
        $id = $this->getRequest()->getParam('id');
        $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
        $quantos = $adminUtilizadores->howManyByRole($id);
        
        if($quantos>0){
            $this->_sessao->display = "Não foi possível eliminar. Existem utilizadores com este perfil";
        }else{
            $regras = new Model_DbTable_Regras();
            $regras->deleteRegraByRoleID($id);
            
            $roles = new Model_DbTable_Roles(); 
            $roles->deleteRole($id);
            $this->_sessao->display = "Role eliminada com sucesso!";
        }
        $this->_redirect('/management/'.$this->_controller.'');
    }
    
    public function adicionarRecursoAction(){
        
        $this->_helper->layout()->disableLayout();
        
        
        if ($this->getRequest()->isPost()){
                
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
                        
            $recursos = new Model_DbTable_Recursos();
                        
            if($recursos->addRecurso($dadosPOST['descricao'], $dadosPOST['controladorNome'], 'management', 'A', NULL)){
                echo json_encode(array('success'=>true) );
            }
            else{
                echo json_encode(array('success'=>false) );
            }
            
        }
        
    }
    
    public function adicionarPrivilegioAction(){
        
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST'); 
                        
            $privilegios = new Model_DbTable_Privilegios();    
            if($privilegios->addPrivilegio($dadosPOST['acao'], $dadosPOST['descricao'], $dadosPOST['controladorNome'], 'management', 'A')){
                echo json_encode(array('success'=>true));
            }
            else{
                echo json_encode(array('success'=>false));
            }
            
        }
    }
    
    public function configurarAction() {
        $recursos = new Model_DbTable_Recursos(); $this->view->recursosModel = $recursos;
        $this->view->recursos = $recursos->getAllRecursosActivos();
                
        $privilegios    = new Model_DbTable_Privilegios();
        $this->view->privilegiosModel = $privilegios;
        
        $controllersActions = new App_Class_ControllersActions();
        $this->view->controllersActions = $controllersActions->byModule('management');
        
    }
    
    
    public function obterLocalidadeAction() {
        $this->_helper->layout()->disableLayout();
        extract($_POST);
        $codPostal = $this->getRequest()->getPost('codPostal');
        if(isset ($codPostal)){
            list($CP1, $CP2) = split('[-]', $codPostal);
            $cp = $CP1.$CP2;
            // A URL com o serviço GIS do SAPO.
            $sapoUrl="http://services.sapo.pt/GIS/GetLocationByZipCode?zipCode=".urlencode($cp);

            // Obtemos a info do "XML response"
            $xml=@simplexml_load_string(@file_get_contents($sapoUrl));
            $localidade = $xml->GetLocationByZipCodeResult[0]->ParishName;
            $concelho   = $xml->GetLocationByZipCodeResult[0]->MunicipalityName;
            $distrito   = $xml->GetLocationByZipCodeResult[0]->DistrictName;
            $latitude   = $xml->GetLocationByZipCodeResult[0]->Latitude;
            $longitude  = $xml->GetLocationByZipCodeResult[0]->Longitude;
            
            echo "".($localidade)."\n";
            echo "".($concelho)."\n";
            echo "".($distrito)."\n";
            echo "".($latitude)."\n";
            echo "".($longitude)."\n";
        }
    }
	
}

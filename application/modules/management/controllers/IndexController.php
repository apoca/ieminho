<?php

class Management_IndexController extends Zend_Controller_Action {

    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    public function indexAction() {
        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login');
        }
        
        $config = new Model_DbTable_Configuracoes();
        
        $emailAnalytics         = $config->optionValue('email-analytics');
        $passwordAnalytics	= $config->optionValue('password-analytics');
        $profileIDAnalytics	= $config->optionValue('profile_id-analytics');
        
        if(!($emailAnalytics || $passwordAnalytics || $profileIDAnalytics)){
            $this->view->parameters = false;
        }else{
            $this->view->parameters = true;
            try{
                $client = Zend_Gdata_ClientLogin::getHttpClient($emailAnalytics, $passwordAnalytics, Zend_Gdata_Analytics::AUTH_SERVICE_NAME); 
            }
            catch (Zend_Gdata_App_CaptchaRequiredException $cre) {
                echo 'URL of CAPTCHA image: ' . $cre->getCaptchaUrl() . "\n";
                echo 'Token ID: ' . $cre->getCaptchaToken() . "\n";
            }catch (Zend_Gdata_App_AuthException $ae) {
                echo 'Problem authenticating: ' . $ae->exception() . "\n";
            }

            $service = new Zend_Gdata_Analytics($client);       

            $dataFormatada = new Zend_Date();
            $endDate = $dataFormatada->get(Zend_Date::YEAR . "-" . Zend_Date::MONTH . "-" . Zend_Date::DAY);

            //echo 'data: '.$endDate.'<br />';

            $later31 = $dataFormatada->subDay(31); 
            $startDate = $later31->get(Zend_Date::YEAR . "-" . Zend_Date::MONTH . "-" . Zend_Date::DAY);

            $query = $service->newDataQuery()->setProfileId($profileIDAnalytics)
                ->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_DATE)

            ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_VISITS)
                ->setStartDate($startDate) 
                ->setEndDate($endDate) 
                ->addSort(Zend_Gdata_Analytics_DataQuery::DIMENSION_DATE, true)
                ->setMaxResults(31);


            $this->view->results = $service->getDataFeed($query);
        }
        
                
    }

    public function loginAction() {
        $this->_helper->layout->setLayout('autenticacao');
        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();
        
        //$referer = $_GET['referer'];
        $referer = isset($_GET['referer']) ? $_GET['referer'] : '';
        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            $password = $this->getRequest()->getPost('password');
            
            if ($login == "Username" || $password == "Password") {
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_preeenchaOsCampos');
                return;
            }
            
            $adminUtilizadores = new Model_DbTable_AdminUtilizadores();
            $id = $adminUtilizadores->autenticacao($login, sha1($password));
            if($id){
                    $authSession = new Zend_Auth_Storage_Session();
                    $storage->timeout = 300000;
                    $utilizador = $adminUtilizadores->fetchRow($adminUtilizadores->select()->where('id = ?', $id));
                    
                    
                    $userInfo = new stdClass;
                    
                    $userInfo->id           = $utilizador->id;
                    $userInfo->nome         = $utilizador->nome;
                    $userInfo->apelido      = $utilizador->apelido;
                    $userInfo->login        = $utilizador->login;
                    $userInfo->password     = $utilizador->password;
                    $userInfo->email        = $utilizador->email;
                    $userInfo->foto         = $utilizador->foto;
                    $userInfo->estado       = $utilizador->estado;
                    $userInfo->role_id      = $utilizador->role_id;
                    
                    $roles = new Model_DbTable_Roles();
                    $userInfo->roleName     = $roles->designacaoById($utilizador->role_id);
                    $userInfo->role         = $roles->descricaoById($utilizador->role_id);
                    
                    $_SESSION['website_id'] = 1;
                    //alterar ultimo ip e data hora
                    
                    $authSession->write($userInfo);
                    $this->_redirect($referer!='' ? $referer : '/management/'.$this->_controller);
                    
            }else{
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_dadosInvalidos');
            }
            
            return;
        }
    }
    
    public function recuperarPassAction() {
        $this->_helper->layout->setLayout('autenticacao');
        
        if ($this->_request->isPost()) {
            $email = $this->getRequest()->getPost('email');

            $validator = new Zend_Validate_EmailAddress();
            if ($validator->isValid($email) == FALSE) {
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_emailInvalido');
                return;
            }
            
            
            if($res == TRUE){
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_passwdEnviadapara').' '.$email;
                $this->_redirect('/management/'.$this->_controller.'/login');
            }else{
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_emailNaoExistente');
                $this->_redirect('/management/'.$this->_controller.'/login');
            }
        }
     }

    function logoutAction() {
        $this->_helper->layout->setLayout('autenticacao');
        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();
        unset($_SESSION['lang']);
        $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoTerminada');
        
        $this->_redirect('/management/index/login');
    }

    public function websiteAction() {
        $this->_helper->layout()->disableLayout();

        $_SESSION['website_id'] = $this->getRequest()->getParam('id');
    }

}

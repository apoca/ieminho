<?php

class Management_GaleriasController extends Zend_Controller_Action {

    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;

    public function init() {
        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
        
    }
    public function preDispatch() {        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }/*else{
               $isAllowed = Sbm_RuleChecker::isAllowed($infoUtilizador->roleName, $this->_action, $this->_controller);
            if(!$isAllowed){
                $this->_helper->layout()->disableLayout(); $this->_helper->layout->setLayout('denied');
            }
        }*/
    }

    public function indexAction() {
        $this->view->idioma = $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
    }

    public function imagensAction() {
        $this->view->idioma = $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        
        $categoria_id = $this->getRequest()->getParam('categoria');
        $this->view->categoria_id = $categoria_id;

        $catGalerias = new Model_DbTable_GaleriasCategorias();
        $this->view->categoriasG = $catGalerias->fetchAll($catGalerias->select()->where('ligacao = ?', 'imagens')->where('categoria_pai is NULL')->where('idioma_iso = ?', $idioma));
        
        $in = $catGalerias->getAllCategoriesByLink('imagens', $idioma);
        $galerias = new Model_DbTable_Galerias();
        if($in)$this->view->registos = $galerias->fetchAll($galerias->select()->where('categoria_id IN ('.$in.')'));
        
        
        if(isset ($_GET["qqfile"]) && strlen($_GET["qqfile"])){
            // list of valid extensions, ex. array("jpg","jpeg","gif","png","bmp")
            $allowedExtensions = array("jpg","jpeg","gif","png","bmp");
            // max file size in bytes
            $sizeLimit = 10 * 1024 * 1024;

            $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload('uploads/images/galeria/');
            $categoria_id = $_GET["categoria_id"];
            $galerias = new Model_DbTable_Galerias();
            $id = $galerias->addGaleriaFile($result['nameFile'], '', 'N', 0, $categoria_id, 'S', $result['ext']);
            
            $catGalerias = new Model_DbTable_GaleriasCategorias();
            $result['categoria'] = $catGalerias->optionValue($categoria_id, 'designacao');
            $result['categoria_id'] = $categoria_id;
            $result['id'] = $id;
            
            list($width, $height, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'].'/uploads/images/galeria/'.$result['nameFile']); 
            $result['width'] = $width;
            $result['height'] = $height;
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            exit;
        }
        else
        $galerias = new Model_DbTable_Galerias();
        if ($categoria_id) {
            $select = $galerias->select()->where('categoria_id = ?', $categoria_id);
        } else {
            $select = $galerias->select();
        }
        $this->view->galeriasFotos = $galerias->fetchAll($select);

        $cat_from_id = $this->getRequest()->getParam('cat_from_id');
        $cat_to_id = $this->getRequest()->getParam('cat_to_id');
        $imagem_id = $this->getRequest()->getParam('id');
        
        if (isset($cat_from_id) && isset($cat_to_id) && isset($imagem_id)) {
            $data = array(
                'categoria_id' => $cat_to_id
            );
            $galerias->update($data, "id = {$imagem_id}");
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');               
            $this->_redirect('/management/galerias/imagens');
        }
    }
    
    public function filtrarCategoriasAction(){
        $this->_helper->layout()->disableLayout();
        $in = $this->getRequest()->getParam('in');
        $galerias = new Model_DbTable_Galerias();
        $this->view->registos = $galerias->fetchAll($galerias->select()->where('categoria_id IN ('.$in.')'));
    }

    public function categoriasAction() {
        $this->view->idioma = $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        
        $catGalerias = new Model_DbTable_GaleriasCategorias();
        $this->view->categoriasG = $catGalerias->fetchAll($catGalerias->select()->where('idioma_iso = ?', $idioma));
    }

    public function adicionarCategoriaAction() {
        $this->view->idioma = $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        
        $galCategorias = new Model_DbTable_GaleriasCategorias();
        $this->view->outrasGalerias = $galCategorias->fetchAll($galCategorias->select()->where('idioma_iso = ?', $idioma));

        if ($this->getRequest()->isPost()) {
            
            $designacao		= $this->getRequest()->getPost('designacao');
            $estado             = $this->getRequest()->getPost('estado');
            $apagar             = $this->getRequest()->getPost('apagar');
            $ligacao            = $this->getRequest()->getPost('ligacao');
            $subCategorias      = $this->getRequest()->getPost('subCategorias') != NULL ? $this->getRequest()->getPost('subCategorias') : 'N';//S ou N
            
            $galCategorias = new Model_DbTable_GaleriasCategorias();
            $galCategorias->addCategoria($designacao, NULL, $ligacao, $idioma, $estado, $apagar);
            
            $db = Zend_Db_Table::getDefaultAdapter();
            $categoria_pai = $db->lastInsertId();
            
            $subCategoria	= $this->getRequest()->getPost('subCategoriaItem'); //subcategorias lat. direito
            if($subCategorias == 'S' && count($subCategoria)>0){ // se existirem subcategorias e pelo menos uma
                for ($i=0; $i<count($subCategoria); $i++) {;
                    $galCategorias->addCategoria($subCategoria[$i], $categoria_pai, $ligacao, $idioma, $estado, $apagar);
                }
            }
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_adicionadoSucesso');               
            $this->_redirect('/management/galerias/categorias');
        }
    }
    
    public function editarCategoriaAction() {
        $this->view->idioma = $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $id = $this->getRequest()->getParam('id');
        $this->view->id = $id;

        $galCategorias = new Model_DbTable_GaleriasCategorias();
        $categoria = $galCategorias->fetchRow($galCategorias->select()->where('id = ?', $id));
        $this->view->categoria = $categoria;

        $this->view->outrasGalerias = $galCategorias->fetchAll();

        if ($this->getRequest()->isPost()) {
            
            $designacao		= $this->getRequest()->getPost('designacao');
            $estado             = $this->getRequest()->getPost('estado');
            $ligacao            = $this->getRequest()->getPost('ligacao');
            $apagar             = $this->getRequest()->getPost('apagar');
            
            $galCategorias = new Model_DbTable_GaleriasCategorias();
            $galCategorias->updateCategoria($id, $designacao, NULL, $ligacao, $idioma, $estado, $apagar) ;
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');               
            $this->_redirect('/management/galerias/categorias');
        }
    }
    
    public function adicionarSubCategoriaAction(){
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                extract($_POST);
                $dadosPOST = $this->getRequest()->getParam('dadosPOST');
                
                $galCategorias = new Model_DbTable_GaleriasCategorias();
                $ligacao = $galCategorias->optionValue($dadosPOST['categoria_pai'], $field);//get ligacao from parent cat.
                
                $galCategorias->addCategoria($dadosPOST['designacao'], $dadosPOST['categoria_pai'], $ligacao, $dadosPOST['idioma_iso'], 'A', 'S');
                
                $db = Zend_Db_Table::getDefaultAdapter();
                $categoria_id = $db->lastInsertId();
                echo $categoria_id;
        }
        return;
    }
    
    public function categoriasArrayAction(){
        $this->_helper->layout()->disableLayout();
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        
        sleep(1);

        $catGalerias = new Model_DbTable_GaleriasCategorias();
        $categoriasG = $catGalerias->fetchAll($catGalerias->select()->where('ligacao = ?', 'imagens')->where('categoria_pai is NULL')->where('idioma_iso = ?', $idioma));
        $result = array();
        foreach ($categoriasG as $key => $categoria) {
            $result[] = array(
                        'value'	=> $categoria->id,
                        'text'	=> $categoria->designacao,
                );
                $subcategorias = $catGalerias->fetchAll($catGalerias->select()->where('categoria_pai = ?', $categoria->id));
                if($subcategorias->count() > 0){
                    foreach ($subcategorias as $key => $subCategoria) {
                        $result[] = array(
                                'value'	=> $subCategoria->id,
                                'text'	=> '--'.$subCategoria->designacao,
                        );
                    }
                }
        }
        echo json_encode($result);
    }
    
    public function actualizarDesignacaoAction(){
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                
                $galCategorias = new Model_DbTable_GaleriasCategorias();
                if(isset($_POST['id'])){
                    $galCategorias->updateDesignacao($_POST['id'], $_POST['designacao']);
                }
        }
        return;
    }
    
    
    public function eliminarSubCategoriaAction(){
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost()){
                $galCategorias = new Model_DbTable_GaleriasCategorias();
                if(isset($_POST['id'])){
                    $galCategorias->deleteCategoria($_POST['id']);
                }
        }
        return;
    }
    
    public function eliminarFicheiroAction() {
        $this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()){
                extract($_POST);
                $dadosPOST = $this->getRequest()->getParam('dadosPOST');
                
                $galerias = new Model_DbTable_Galerias();
                $ficheiro = $galerias->optionValue($dadosPOST['id'], 'ficheiro');
                $result = array();
                if($galerias->deleteGaleria($dadosPOST['id'])){
                    
                    if($dadosPOST['tipo']=='imagem'){
                        $path = PUBLIC_PATH . "/uploads/images/galeria/{$ficheiro}";
                        unlink($path);
                        $result['success'] = true;
                    }else{
                        $result['success'] = false;
                    }
                }else{
                    $result['success'] = false;
                }
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                
        }
        return;
    }

    public function adicionarImagensAction() {
        $categoria_id = $this->getRequest()->getParam('categoria');
        $this->view->categoria_id = $categoria_id;

        //do it evento onchange categoria -> redirecciona e obter param
        $catGalerias = new Model_DbTable_GaleriasCategorias();
        $this->view->categoriasG = $catGalerias->fetchAll();

        if (isset($_GET["qqfile"]) && strlen($_GET["qqfile"])) {
            // list of valid extensions, ex. array("jpg","jpeg","gif","png","bmp")
            $allowedExtensions = array();
            // max file size in bytes
            $sizeLimit = 5 * 1024 * 1024;

            $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload('imagens/galeria-imagens/');

            if ($result['success'] == true) {
                $categoria_id = $_GET["categoria_id"];
                $galerias = new Model_DbTable_Galerias();
                $posicao = $galerias->getQtdPosicoes($categoria_id);
                $ficheiro = $result['nameFile'];
                $galerias->addGaleriaFile($ficheiro, '', 'N', $posicao, $categoria_id, 'S', $result['ext']);

                $db = Zend_Db_Table::getDefaultAdapter();
                $itemGaleria_id = $db->lastInsertId();
                //adicionarmos ao array que vai retornar o pedido json, o ID do registo na base de dados
                $novoElemento = array("id_bd" => $itemGaleria_id);

                $resultFinal = array_merge($result, $novoElemento);
                // to pass data through iframe you will need to encode all html tags ,
                echo htmlspecialchars(json_encode($resultFinal), ENT_NOQUOTES);
            } else {
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            }
            exit;
        } else
        if ($this->getRequest()->isPost()) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_adicionadoSucesso');               
            $this->_redirect('/management/galerias/imagens');
        }
    }



    public function ordenarImagensAction() {
        $this->_helper->layout()->disableLayout();

        parse_str($_POST['images'], $imagesOrder);

        $galerias = new Model_DbTable_Galerias();
        foreach ($imagesOrder['image'] as $pos => $image_id) {
            $galerias->updatePosicao($image_id, $pos);
            // echo("UPDATE galerias SET `posicao` = '$pos' WHERE `id` = '$image_id' <br />");
        }
        echo 'Ordenação efectuada com sucesso!';
        //print_r ($dadosPost);
    }

    public function destacarImagemAction() {
        
    }

    public function apagarCategoriaAction() {
        $id = $this->getRequest()->getParam('id');
        //verificar se existem galerias com esta categria
        $galerias = new Model_DbTable_Galerias();

        $qtas = $galerias->getQtdPosicoes($id);
        if ($qtas > 0) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('Registo não pode ser eliminado, uma vez que existem galerias associadas!');               
            $this->_redirect('/management/galerias/categorias');
            
        } else {
            $galCategorias = new Model_DbTable_GaleriasCategorias();
            $galCategorias->deleteCategoria($id);

            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_eliminadoSucesso');
            $this->_redirect('/management/galerias/categorias');
        }
    }

    public function apagarImagemAction() {
        $id = $this->getRequest()->getParam('id');
        $galerias = new Model_DbTable_Galerias();
        $imagem = $galerias->getGaleria($id);

        if ($galerias->deleteGaleria($id)) {
            $path = PUBLIC_PATH . "/imagens/galeria-imagens/{$imagem['ficheiro']}";
            unlink($path);
        }

        $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_eliminadoSucesso');
        $this->_redirect('/management/galerias/imagens');
    }

}

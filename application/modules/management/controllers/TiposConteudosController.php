<?php
class Management_TiposConteudosController extends Zend_Controller_Action {

	protected $_controller = NULL;
        protected $_action = NULL;
        protected $_sessao = NULL;
        protected $_funcoes = NULL;
        public function init(){

            $request = Zend_Controller_Front::getInstance()->getRequest();
            $this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
            $this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
            $this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
            $this->_funcoes = new App_Class_Funcoes();

            $tranlate = new App_Class_Translate();
            $tranlate->tranlate('pt');
        }

        public function preDispatch() {        
            $storage = new Zend_Auth_Storage_Session();
            $infoUtilizador = $storage->read();
            if (!$infoUtilizador) {
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
                $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
            }else{
                $isAllowed = Sbm_RuleChecker::isAllowed($infoUtilizador->roleName, $this->_action, $this->_controller);
                if(!$isAllowed){
                    $this->_helper->layout()->disableLayout(); $this->_helper->layout->setLayout('denied');
                }
            }
        }
	
	public function indexAction() {
            $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
            $this->view->idioma = $idioma;
        
            $tiposConteudos = new Model_DbTable_TiposConteudos();
            $this->view->tiposConteudos = $tiposConteudos->fetchAll($tiposConteudos->select()->where('idioma_iso = ?', $idioma));

        }

	public function adicionarAction() {
            $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
            $this->view->idioma = $idioma;
            
            if ($this->getRequest()->isPost()) {
                $formData = $this->getRequest()->getPost();
                $dimensoesImagemDestaque = $formData['temImagemDestaque'] == 'S' ? $formData['dimensoesImagemDestaque'] : NULL;
                
                $tiposConteudo = new Model_DbTable_TiposConteudos();
                $tiposConteudo->addTipoConteudo($formData['designacao'],$formData['temImagemDestaque'], $dimensoesImagemDestaque, $formData['temDescricao'], $formData['temLerMais'], $formData['podeApagar'], $formData['podeApagarConteudos'], $formData['podeSerReferencia'], $formData['newsletter'],0,$idioma, 'P');
                $db = Zend_Db_Table::getDefaultAdapter();
                $id = $db->lastInsertId();
                
                $tiposConteudo->updateOpcao($id, 'main_id', $id) ;

                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_adicionadoSucesso');
                if (count($this->_funcoes->outrosIdiomas($idioma))>0){
                    foreach ($this->_funcoes->outrosIdiomas($idioma) as $key=>$outro){
                        if ($key == 0) {
                            $proximoIdioma = $outro->idioma_iso;
                        }
                        $tiposConteudo->addTipoConteudo('','S', '654x245 píxeis', 'S', 'N', 'S', 'S', 'S', 'S',$id,$outro->idioma_iso, 'I');
                    }
                    $this->_redirect('/management/'.$this->_controller.'/editar/main_id/'.$id.'/idioma/'.$proximoIdioma);
                }
                $this->_redirect('/management/'.$this->_controller.'/');

            }
	}
        
        public function caracteristicasAction() {
            $tipo_id = $this->getRequest()->getParam('id');

            $tiposConteudos = new Model_DbTable_TiposConteudos();
            $this->view->tipoConteudo = $tiposConteudos->fetchRow($tiposConteudos->select()->where('id = ?', $tipo_id));

            $caractTipoC = new Model_DbTable_CaracteristicasTiposConteudos();
            $this->view->caracteristicasTP    = $caractTipoC->fetchAll($caractTipoC->select()->where('tipo_id = ?', $tipo_id));

            $this->view->conteudosTiposConteudos = new Model_DbTable_ConteudosTiposConteudos();

            $this->view->atributosTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
	}
        
        public function opcoesAction() {
		$this->_helper->layout()->disableLayout();
                $id = $this->getRequest()->getParam('id');
                if($id){
                    $opcao = $this->getRequest()->getParam('opcao');
                    $valor = $this->getRequest()->getParam('valor');
                    
                    $tiposConteudos = new Model_DbTable_TiposConteudos();
                    $tiposConteudos->updateOpcao($id, $opcao, $valor);
                    if($opcao=='temImagemDestaque' && $valor == 'N'){
                        $tiposConteudos->updateOpcao($id, 'dimensoesImagemDestaque', '');
                    }
                }
	}
	
	public function editarAction() {
            
            $main_id = $this->getRequest()->getParam('main_id');
            $idioma = $this->getRequest()->getParam('idioma');
            if($main_id){
                $this->view->main_id    = $main_id;
                $tiposConteudos = new Model_DbTable_TiposConteudos();
                $this->view->tipoConteudo = $tipoC = $tiposConteudos->fetchRow($tiposConteudos->select()->where('main_id = ?', $main_id)->where('idioma_iso = ?', $idioma));
                $this->view->idioma = $idioma;
            }else{
                $id = $this->getRequest()->getParam('id');
                $tiposConteudos = new Model_DbTable_TiposConteudos();
                $this->view->tipoConteudo = $tipoC = $tiposConteudos->fetchRow($tiposConteudos->select()->where('id = ?', $id));
                $this->view->idioma = $idioma = $tipoC->idioma_iso;
            }
            
            
            
            if ($this->getRequest()->isPost()) {
                
                $designacao         = $this->getRequest()->getPost('designacao');
                
                $dimensoesImagemDestaque = ($this->getRequest()->getPost('temImagemDestaque') == 'S') ? $this->getRequest()->getPost('dimensoesImagemDestaque') : NULL;
                
                $tiposConteudos = new Model_DbTable_TiposConteudos();
                $tiposConteudos->updateOpcao($tipoC->id, 'designacao', $designacao);
                $tiposConteudos->updateOpcao($tipoC->id, 'dimensoesImagemDestaque', $dimensoesImagemDestaque);
                if($tipoC->estado=='I'){
                    $tiposConteudos->updateOpcao($tipoC->id, 'estado', 'P');
                }
                
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso'); 
                
                if (count($this->_funcoes->outrosIdiomas($idioma))>0){
                     foreach ($this->_funcoes->outrosIdiomas($idioma) as $key=>$outro){
                        $registos = array();
                        $estado = $tiposConteudos->tipoConteudoIdioma($tipoC->main_id, $outro->idioma_iso)->estado;
                        if($estado=='I'){
                                $registos[] = array(
                                        'main_id'	=> $tipoC->main_id,
                                        'idioma_iso'	=> $outro->idioma_iso,
                                );
                        }
                     }
                     if(count($registos)>0){
                        $this->_redirect('/management/'.$this->_controller.'/editar/main_id/'.$registos[0]['main_id'].'/idioma/'.$registos[0]['idioma_iso']);
                     }else{
                         $this->_redirect('/management/'.$this->_controller); 
                     }
                }else{
                    $this->_redirect('/management/'.$this->_controller); 
                }
                
                
                
                
                if (count($this->_funcoes->outrosIdiomas($idioma))>0){
                        foreach ($this->_funcoes->outrosIdiomas($idioma) as $key=>$outro){

                        $estado = $tiposConteudos->tipoConteudoIdioma($tipoC->main_id, $outro->idioma_iso)->estado;
                        $existe = array();
                        if($estado=='I'){
                                $existe[] = array(
                                        'main_id'	=> $tipoC->main_id,
                                        'idioma'	=> $outro->idioma_iso,
                                );
                                //$this->_redirect('/management/'.$this->_controller.'/editar/main_id/'.$tipoC->main_id.'/idioma/'.$outro->idioma_iso);

                        }else{
                        // $this->_redirect('/management/'.$this->_controller.'#1'); 

                        }

                        }

                        if(count($existe)>0){
                            echo $existe[0]['main_id']. ' // '.$existe[0]['idioma'];
                        }
                }
            
            }
	}
        
        
        
        
	
	public function ordenarAction() {
		
	}
	
	public function apagarAction() {
            $id = $this->getRequest()->getParam('id');
            
            $tiposConteudos = new Model_DbTable_TiposConteudos();
            $tiposConteudos->deleteTipoConteudo($id);
                
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_eliminadoSucesso');
            $this->_redirect('/management/'.$this->_controller.'/');
	}
}

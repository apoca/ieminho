<?php 
class Management_BannersController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    public function init(){
        
        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    public function preDispatch() {        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }/* else{
            $isAllowed = Sbm_RuleChecker::isAllowed($infoUtilizador->roleName, $this->_action, $this->_controller);
            if(!$isAllowed){
                $this->_helper->layout()->disableLayout(); $this->_helper->layout->setLayout('denied');
            }
        }*/
    }
    //////////BANNERS ============ DestaquesSlideshow (Model)
    public function indexAction(){
       
        
    }
    
    public function editarAction(){
                
        $seccao = $this->getRequest()->getParam('seccao', 'homePage');
        $this->view->seccao = $seccao;
        $banners = new Model_DbTable_DestaqueSlideshow();
        $this->view->banners = $banners->fetchAll($banners->select()->where('seccao = ?', $seccao)->order('posicao ASC'));
        
        
        if(isset ($_GET["qqfile"]) && strlen($_GET["qqfile"]))
        {
                $seccao = $this->getRequest()->getParam('seccao');
                // list of valid extensions, ex. array("jpg","jpeg","gif","png","bmp")
                $allowedExtensions = array();
                // max file size in bytes
                $sizeLimit = 5 * 1024 * 1024;

                $uploader = new App_Class_QqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload('uploads/images/banners/');
                
                $banners = new Model_DbTable_DestaqueSlideshow();
                $posicao = $banners->getQtdImagens();
                $result['id'] = $banners->addImagemSlideshow('', '_self', $result['nameFile'], $posicao, $seccao, $estado='A');//addImagemSlideshow($result['nameFile'], $idioma_iso, $posicao);
                
                
                // to pass data through iframe you will need to encode all html tags
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                exit;
        }
        else
        if ($this->getRequest()->isPost()) {
            
            $id = $this->getRequest()->getPost('id');
            
            $banners = new Model_DbTable_DestaqueSlideshow();
            $banner  = $banners->fetchRow($banners->select()->where('id = ?', $id));
        
            
            $url                    = $this->getRequest()->getPost('url');
            $target                 = $this->getRequest()->getPost('target');
            
            $banners->updateFieldValue('url', $url, $id);
            $banners->updateFieldValue('target', $target, $id);
          /*  echo '<pre>';
            print_r($_POST);
            echo '</pre>Files:<br />';
            
            echo '<pre>';
            print_r($_FILES);
            echo '</pre>';*/
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');
        }
    }
    
    public function infoBannerAction(){
        $this->_helper->layout()->disableLayout();
        $id = $this->getRequest()->getParam('id');
        $banners = new Model_DbTable_DestaqueSlideshow();
        
        $this->view->banner = $result = $banners->fetchRow($banners->select()->where('id = ?', $id));
        //echo( json_encode($result->toArray()) );
    }
    
     public function ordenarAction(){
        $this->_helper->layout()->disableLayout();
        
        if ($this->getRequest()->isPost())
        {
                $banners = new Model_DbTable_DestaqueSlideshow();
                $listItem = $_POST['order'];
                foreach ($listItem as $position=>$item){
                    //$sql[] = "UPDATE `areasnegocio` SET `posicao` = $position WHERE `id` = $item <br />";
                    $banners->updatePosicao($item, $position);
                }
        }
        return;
    }
    
    public function imagensAction()
    {
        $this->_helper->layout()->disableLayout();
        
        if(isset($_FILES['uploadfile']['name'])){
            
            $uploaddir = './uploads/images/banners/'; 
            $file = $uploaddir . basename($_FILES['uploadfile']['name']); 

            if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) { 
                echo "success"; 
            } 
            else {
                echo "error";
            }
        }
        
    }
    
    public function cropImagemAction()
    {
        $this->_helper->layout()->disableLayout();
        $image_data = file_get_contents($_REQUEST['url']);
        file_put_contents("uploads/images/banners/".$_REQUEST['image'],$image_data);
    }
    
    public function eliminarImagemAction(){
        $this->_helper->layout()->disableLayout();
        $imagem = $this->getRequest()->getParam('imagem');
        $banners = new Model_DbTable_DestaqueSlideshow();
        $banners->deleteImagemSlideshow($imagem);
        
        if(unlink('uploads/images/banners/'.$imagem)){
            echo 'sucess';
        }else{
            echo 'error';
        }
        return;
    }
    
    
    
    public function editarImagemAction()
    {
        $this->view->imagem = $_GET['imagem'];
        $this->view->image_lst = $_GET['imagem'];
        $this->_helper->layout()->disableLayout();
        
        $image_lst = $_GET['imagem'];
        $ext = substr(strrchr($image_lst, '.'), 1);
        
        $src = 'uploads/images/banners/'.$image_lst;
        list($imagewidth, $imageheight, $imageType) = getimagesize($src);

        $this->view->height = $imageheight;
        $this->view->width = $imagewidth;
        
        $file = new App_Class_File();
        if($imagewidth>$imageheight){
            $this->view->dimensions = $file->image_resize($src, '1000');
        }else{
            $this->view->dimensions = $file->image_resize($src, '480');
        }
        
        if ($this->getRequest()->isPost()) {

            //Get the new coordinates to crop the image.
            $targ_w = 940;
            $targ_h = 440;
            $jpeg_quality = 90;
            $src = 'uploads/images/banners/'.$image_lst;
            list($imagewidth, $imageheight, $imageType) = getimagesize($src);
            
            $imageType = image_type_to_mime_type($imageType);
            
            switch ($imageType) {
                    case "image/gif":
                        $img_r = ImageCreateFromGif($src);
                        break;
                    case "image/pjpeg":
                    case "image/jpeg":
                    case "image/jpg":
                        $img_r = imagecreatefromjpeg($src);
                        break;
                    case "image/png":
                    case "image/x-png":
                        $img_r = ImageCreateFrompng($src);
                        break;
            }
          
            $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
            
            $x = floor($_POST['x']);
            $y = floor($_POST['y']);
            $w = floor($_POST['w']);
            $h = floor($_POST['h']);
            //imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h); 
            
            imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'], $targ_w,$targ_h,$_POST['w'],$_POST['h']);
            
            imagejpeg($dst_r, 'uploads/images/banners/'.$image_lst, $jpeg_quality);
            
            $this->view->cropDone = true;
          
        }else {
            $this->view->cropDone = false;
        }
    }
	
}

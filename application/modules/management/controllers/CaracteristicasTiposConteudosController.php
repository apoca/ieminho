<?php
class Management_CaracteristicasTiposConteudosController extends Zend_Controller_Action {

	protected $_controller = NULL;
        protected $_action = NULL;
        protected $_sessao = NULL;
        protected $_funcoes = NULL;
        public function init(){

            $request = Zend_Controller_Front::getInstance()->getRequest();
            $this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
            $this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
            $this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
            $this->_funcoes = new App_Class_Funcoes();

            $tranlate = new App_Class_Translate();
            $tranlate->tranlate('pt');
        }

        public function preDispatch() {
            $storage = new Zend_Auth_Storage_Session();
            $infoUtilizador = $storage->read();
            if (!$infoUtilizador) {
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
                $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
            }
        }
	
	public function indexAction() {
            $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
            $this->view->idioma = $idioma;
            
            $caractTipoCont = new Model_DbTable_CaracteristicasTiposConteudos();
            $this->view->caracteristicas = $caractTipoCont->getCaracteristicasTiposConteudosIdioma($idioma);//$caractTipoCont->fetchAll($caractTipoCont->select()->where('idioma_iso = ?', $idioma));
	}
        
        
	public function adicionarAction() {
            $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
            $this->view->idioma = $idioma;
        
		$tiposConteudos = new Model_DbTable_TiposConteudos();
		$this->view->tiposConteudos = $tiposConteudos->fetchAll($tiposConteudos->select()->where('idioma_iso = ?', $idioma));
                
                if ($this->getRequest()->isPost()) {
                    $formData = $this->getRequest()->getPost();
                    
                    $tipo_id = $formData['tipo_id'];
                    $designacao = $formData['designacao'];
                    $referencia = $formData['referencia'];
                    
                    $caractTipoCont = new Model_DbTable_CaracteristicasTiposConteudos();
                    $caractTipoCont->addCaracteristicaTipoConteudo($tipo_id, $designacao, $referencia);
                    
                    $db = Zend_Db_Table::getDefaultAdapter();
                    $caracteristica_id = $db->lastInsertId();
                    
                    if($referencia=='conteudo'){
                        
                        $conteudoTipoCont = new Model_DbTable_ConteudosTiposConteudos();
                        
                        $tipo_id = $formData['tipo_id'];
                        $labelSingular = $formData['labelSingular'];
                        $labelPlural = $formData['labelPlural'];
                        $name = $conteudoTipoCont->nameConteudoTipoConteudo($formData['name']); //check if exist
                        $fieldtype = $formData['fieldtype'];
                        $condicoes = $formData['condicoes'];
                        $restricoes = $formData['restricoes'];

                        $conteudoTipoCont->addConteudoTipoConteudo($caracteristica_id, $tipo_id, $labelSingular, $labelPlural, $name, $fieldtype, $condicoes, $restricoes);
                    }
                    else{
                        
                        $attTipoCont = new Model_DbTable_AtributosTiposConteudos();
                        
                        
                        $labelSingular = $formData['labelSingular'];
                        $labelPlural = $formData['labelPlural'];
                        $name = $attTipoCont->nameAtributoTipoConteudo($formData['name']);
                        $fieldtype = $formData['fieldtype'];
                        $valoresCampo = $formData['valoresCampo'];
                        $restricoes = $formData['restricoes'];
                        
                        $attTipoCont->addAtributoTipoConteudo($caracteristica_id, $labelSingular, $labelPlural, $name, $fieldtype, $valoresCampo, $restricoes);
                        
                    }
                    $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_adicionadoSucesso');               
                    $this->_redirect('/management/'.$this->_controller);
                }
	}
	/*echo '<pre>';
        print_r($formData);
        echo '</pre>';
        return;*/
                        
	public function editarAction() {
		$id = $this->getRequest()->getParam('id');
                $this->view->id = $id;
                
                
                $caractTipoCont = new Model_DbTable_CaracteristicasTiposConteudos();
                $caracteristica = $caractTipoCont->fetchRow($caractTipoCont->select()->where('id = ?', $id));
                $this->view->caracteristica   = $caracteristica;
                
                $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
                $this->view->idioma = $idioma;
        
                $tiposConteudos = new Model_DbTable_TiposConteudos();
                $tipo = $tiposConteudos->fetchRow($tiposConteudos->select()->where('id = ?', $caracteristica->tipo_id));
                
                
		$this->view->tiposConteudos = $tiposConteudos->fetchAll($tiposConteudos->select()->where('idioma_iso = ?', $tipo->idioma_iso));
                $this->view->tiposConteudosRef = $tiposConteudos->fetchAll($tiposConteudos->select()->where('podeSerReferencia = ?', 'S'));
                
                if($caracteristica->referencia=='conteudo'){
                    $conteudoTipoCont = new Model_DbTable_ConteudosTiposConteudos();
                    $extra = $conteudoTipoCont->fetchRow($conteudoTipoCont->select()->where('caracteristica_id  = ?', $caracteristica->id)); // ou $id
                    $this->view->extra   = $extra;
                }
                else
                if($caracteristica->referencia=='html'){
                    $attTipoCont = new Model_DbTable_AtributosTiposConteudos();
                    $extra = $attTipoCont->fetchRow($attTipoCont->select()->where('caracteristica_id  = ?', $caracteristica->id)); // ou $id
                    $this->view->extra   = $extra;
                }
                
                
                
                if ($this->getRequest()->isPost()) {
                    
                    
                    $formData = $this->getRequest()->getPost();
                    
                    
                    $tipo_id = $formData['tipo_id'];
                    $designacao = $formData['designacao'];
                    $referencia = $caracteristica->referencia; //vamos obter a 1ª uma vez que esta disabled
                    
                    $caractTipoCont = new Model_DbTable_CaracteristicasTiposConteudos();
                    $caractTipoCont->updateCaracteristicaTipoConteudo($id, $tipo_id, $designacao, $referencia);
                    
                    if($referencia=='conteudo'){
                        
                        $tipo_id = $formData['tipo_id'];
                        $labelSingular = $formData['labelSingular'];
                        $labelPlural = $formData['labelPlural'];
                        $name = $formData['name'];
                        $fieldtype = $formData['fieldtype'];
                        $condicoes = $formData['condicoes'];
                        $restricoes = $formData['restricoes'];

                        $conteudoTipoCont = new Model_DbTable_ConteudosTiposConteudos();
                        $conteudoTipoCont->updateConteudoTipoConteudo($caracteristica->id, $tipo_id, $labelSingular, $labelPlural, $name, $fieldtype, $condicoes, $restricoes);
                        
                    }
                    else
                    if($referencia=='html'){
                        
                        $labelSingular = $formData['labelSingular'];
                        $labelPlural = $formData['labelPlural'];
                        $name = $formData['name'];
                        $fieldtype = $formData['fieldtype'];
                        $valoresCampo = $formData['valoresCampo'];
                        $restricoes = $formData['restricoes'];
                        
                        $attTipoCont = new Model_DbTable_AtributosTiposConteudos();
                        $attTipoCont->updateAtributoTipoConteudo($caracteristica->id, $labelSingular, $labelPlural, $name, $fieldtype, $valoresCampo, $restricoes);
                        
                        
                    }
                    $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');               
                    $this->_redirect('/management/'.$this->_controller);
                }
	}
	
	public function ordenarAction() {
		
	}
        public function camposCaracteristicaHtmlAction() {
            $this->_helper->layout()->disableLayout();
            $id = $this->getRequest()->getParam('id');

        }
        public function camposCaracteristicaConteudoAction() {
            $this->_helper->layout()->disableLayout();
            
            $tiposConteudos = new Model_DbTable_TiposConteudos();
            $this->view->tiposConteudos = $tiposConteudos->fetchAll($tiposConteudos->select()->where('podeSerReferencia = ?', 'S'));
        }
        
        public function htmlAction() {
            $this->_helper->layout()->disableLayout();
            $caracteristica_id = $this->getRequest()->getParam('id');
            if ($caracteristica_id) {
                $attTipoCont = new Model_DbTable_AtributosTiposConteudos();
                $this->view->atributos = $attTipoCont->fetchRow($attTipoCont->select()->where('caracteristica_id = ?', $caracteristica_id));
            }
        }

        public function conteudoAction() {
            $this->_helper->layout()->disableLayout();
            $caracteristica_id = $this->getRequest()->getParam('id');
            if ($caracteristica_id) {


                $conteudosTipoCont = new Model_DbTable_ConteudosTiposConteudos();
                $this->view->atributos = $conteudosTipoCont->fetchRow($conteudosTipoCont->select()->where('caracteristica_id = ?', $caracteristica_id));
            }
        }
        
	
	public function apagarAction() {
		$id = $this->getRequest()->getParam('id');
                
                $caractTipoCont = new Model_DbTable_CaracteristicasTiposConteudos();
                $caract    = $caractTipoCont->fetchRow($caractTipoCont->select()->where('id = ?', $id));
                $referencia = $caract->referencia;
                $caractTipoCont->deleteCaracteristicaTipoConteudo($id);
                
                
                if($referencia == 'conteudo'){
                    //$caract->tipo_id
                    $conteudosTC = new Model_DbTable_ConteudosTiposConteudos();
                    //eliminar da tabela `conteudos_tiposconteudo` onde caracteristica_id = $caract->id
                    $conteudosTC->deleteConteudoTipoConteudoByCaracteristicaID($caract->id);
                    //obter o att ID
                    $atributo_id = $conteudosTC->getIDbyCaracteristicaID($caract->id);
                    
                }else{                    
                    $attTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
                    //eliminar da tabela `atributos_tiposconteudos` onde caracteristica_id = $caract->id
                    $attTiposConteudos->deleteAtributoTipoConteudoByCaracteristicaID($caract->id);
                    //obter o att ID
                    $atributo_id = $attTiposConteudos->getIDbyCaracteristicaID($caract->id);
                }
                
                $valoresAttRef = new Model_DbTable_ValoresAtributosReferencia();
                // eliminar da tabela `valores_atributos_referencia` onde atributo_id = $caract->id 
                $valoresAttRef->deleteValorAtributoRefByAttrID($atributo_id);
                
                $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_eliminadoSucesso');
                $this->_redirect('/management/'.$this->_controller);
	}
        
        /*$image_url = 'http://www.alpha-ropes.com/images/areasnegocio/en/img1.jpg';
            
            $this->cache_image($image_url); */
        function cache_image($image_url, $image_path = 'uploads/images/cache/'){
        //get the name of the file
        $exploded_image_url = explode("/",$image_url);
        $image_filename = end($exploded_image_url);
        $exploded_image_filename = explode(".",$image_filename);
        $extension = end($exploded_image_filename);
            //make sure its an image
            if($extension=="gif"||$extension=="jpg"||$extension=="png"){
                //get the remote image
                $image_to_fetch = file_get_contents($image_url);
                //save it
                $local_image_file = fopen($image_path.$image_filename, 'w+');
                chmod($image_path.$image_filename,0755);
                fwrite($local_image_file, $image_to_fetch);
                fclose($local_image_file);
            }
        }
}

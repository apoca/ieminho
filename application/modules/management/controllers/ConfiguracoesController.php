<?php 
class Management_ConfiguracoesController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    public function init(){

        $request = Zend_Controller_Front::getInstance()->getRequest();
	$this->view->action     = $request->getActionName(); $this->_action     = $request->getActionName();
	$this->view->controller = $request->getControllerName();$this->_controller = $request->getControllerName();
	$this->view->module     = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_funcoes = new App_Class_Funcoes();
        $tranlate = new App_Class_Translate();
        $tranlate->tranlate('pt');
    }

    public function preDispatch() {        
        $storage = new Zend_Auth_Storage_Session();
        $infoUtilizador = $storage->read();
        if (!$infoUtilizador) {
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_sessaoExpirou');
            $this->_redirect('/management/index/login?referer='.$this->getRequest()->getRequestUri());
        }else{
            $isAllowed = Sbm_RuleChecker::isAllowed($infoUtilizador->roleName, $this->_action, $this->_controller);
            if(!$isAllowed){
                $this->_helper->layout()->disableLayout(); $this->_helper->layout->setLayout('denied');
            }
        }
    }
    
    public function indexAction(){
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
        
        $adminEmail = new Model_DbTable_AdminEmails();
        $this->view->adminEmail = $adminEmail->fetchRow($adminEmail->select()->where('id = ?', 1));
    }
    
    public function idiomaAction(){
        $this->_helper->layout()->disableLayout();
        $idioma = $this->getRequest()->getParam('iso');
        $this->view->idioma = $idioma;
        $accao = $this->getRequest()->getParam('accao');
        $this->view->accao = $accao;
    }
    
    public function updateConfiguracoesIdiomaAction(){
        
        $this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()) {
            extract($_POST);
            $dadosPOST = $this->getRequest()->getParam('dadosPOST');
            
            
            $title = $dadosPOST['title'];
            $description = $dadosPOST['description'];
            $keywords = $dadosPOST['keywords'];
            $idioma_iso = $dadosPOST['idioma_iso'];
            
            $configuracoesIdioma = new Model_DbTable_ConfiguracoesIdioma();
            $configuracoesIdioma->updateOptionValuesIdioma('title', $title, $idioma_iso);
            $configuracoesIdioma->updateOptionValuesIdioma('description', $description, $idioma_iso);
            $configuracoesIdioma->updateOptionValuesIdioma('keywords', $keywords, $idioma_iso);
           
            
            echo 'Actualizado com sucesso!';
            
            
        }
    }
    
    public function editarAction(){
        $idioma = $this->getRequest()->getParam('idioma', $this->_funcoes->idiomaPadrao());
        $this->view->idioma = $idioma;
                
        $adminEmail = new Model_DbTable_AdminEmails();
        $this->view->adminEmail = $adminEmail->fetchRow($adminEmail->select()->where('id = ?', 1));
        if ($this->getRequest()->isPost()) {
            
            $config = new Model_DbTable_Configuracoes();
            $nome		= $this->getRequest()->getPost('nome');
            $email		= $this->getRequest()->getPost('email');
            $endereco		= $this->getRequest()->getPost('endereco');
            $facebook		= $this->getRequest()->getPost('facebook');
            $googleAnalytics	= $this->getRequest()->getPost('google-analytics');
            
            
            $config->updateOptionValues('title', $nome);
            $config->updateOptionValues('url', $endereco);
            $config->updateOptionValues('facebook', $facebook);
            $config->updateOptionValues('email', $email);
            $config->updateOptionValues('google-analytics', $googleAnalytics);
            
            
            
            $seguranca_system	= $this->getRequest()->getPost('seguranca_system');
            $server_system      = $this->getRequest()->getPost('server_system');
            $port_system        = $this->getRequest()->getPost('port_system');
            $email_system	= $this->getRequest()->getPost('email_system');
            $password_system	= $this->getRequest()->getPost('password_system');
            
            
            
            if ($password_system == '') {
                $adminEmail = new Model_DbTable_AdminEmails();
                $password_system =  $adminEmail->fieldValue(1, 'password');
            }else{
                $password_system = $password_system;
            }
            $adminEmail = new Model_DbTable_AdminEmails();
            $adminEmail->updateEmail(1, 'system', $seguranca_system, $server_system, $email_system, $port_system, $password_system);
            
            $this->_sessao->display = Zend_Registry::get('Zend_Translate')->_('_actualizadoSucesso');               
            $this->_redirect('/management/'.$this->_controller);
        }
    }

    
	
}

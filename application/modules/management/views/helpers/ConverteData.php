<?php

class Zend_View_Helper_ConverteData
{
        
    function converteData($data)
    {
        $end_date = new Zend_Date($data);
        return $end_date->get(Zend_Date::YEAR."-". Zend_Date::MONTH_NAME ."-".Zend_Date::DAY);
    }
}

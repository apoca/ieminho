<?php

//MessageBox
class Zend_View_Helper_InfoLogsNaoLidos {

    function infoLogsNaoLidos() {
        $logs = new Model_DbTable_Logs();
        return $logs->fetchAll($logs->select()->where('lida = ?', 'N'));
    }

    function arrayToObject($array) {
        if (!is_array($array)) {
            return $array;
        }

        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name => $value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = $this->arrayToObject($value);
                }
            }
            return $object;
        } else {
            return FALSE;
        }
    }

}

<?php

class Zend_View_Helper_SubCategoriasByCategoriaID
{
        
    function subCategoriasByCategoriaID($categoria_id)
    {
        $categorias = new Model_DbTable_Categorias();
        return $categorias->fetchAll($categorias->select()->where('categoria_pai = ?', $categoria_id)->order('posicao ASC'));
    }
}

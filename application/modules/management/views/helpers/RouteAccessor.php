<?php

class Zend_View_Helper_RouteAccessor extends Zend_View_Helper_Abstract 
{
    private $_routes;
    public function routeAccessor($routeName)
    {
        if($routeName == null)
            return false;
        
        $this->_routes = split("_", $routeName);

        if(empty($this->_routes[0]))
                $this->_routes[0] = null;

        if(empty($this->_routes[1]))
                $this->_routes[1] = null;

        if(empty($this->_routes[2]))
                $this->_routes[2] = null;


        $isAllowed = Sbm_RuleChecker::isAllowed($roleName = null, $this->_routes[2], $this->_routes[1], $this->_routes[0],  $callback = null, $params = null);

        return $isAllowed;
    }
}

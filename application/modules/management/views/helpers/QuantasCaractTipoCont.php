<?php

class Zend_View_Helper_QuantasCaractTipoCont
{
        
    function quantasCaractTipoCont($tipo_id)
    {
        $caracteristicasTC = new Model_DbTable_CaracteristicasTiposConteudos();

        $select = $caracteristicasTC->select()->where('tipo_id = ?', $tipo_id);
        return $caracteristicasTC->fetchAll($select)->count();
    }
}

<?php

class Zend_View_Helper_ObterSubMenus
{
        
    function obterSubMenus($item_id)
    {
        $menusBD = new Model_DbTable_Menus();

        $select = $menusBD->select()
                            ->where('itemsuperior_id = ?',$item_id)
                            ->order('posicao ASC');
        return $menusBD->fetchAll($select);
    }
}

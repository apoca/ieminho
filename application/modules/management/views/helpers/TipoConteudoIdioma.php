<?php

class Zend_View_Helper_TipoConteudoIdioma
{
        
    function tipoConteudoIdioma($main_id, $idioma_iso)
    {
        $tiposConteudos = new Model_DbTable_TiposConteudos();
        return $tiposConteudos->tipoConteudoIdioma($main_id, $idioma_iso);
    }
}

<?php

class Zend_View_Helper_AllowAddCampaign
{
        
    function allowAddCampaign()
    {
        $tipos = new Model_DbTable_CampanhasTipos();
        $clientes = new Model_DbTable_CampanhasClientes();
        $paises = new Model_DbTable_CampanhasPaises();
        $categorias = new Model_DbTable_CampanhasCategorias();
        
        if( ($tipos->howManyRecords()>0) &&
            ($clientes->howManyRecords()>0) &&
            ($paises->howManyRecords()>0) &&
            ($categorias->howManyRecords()>0) ){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

<?php

class Zend_View_Helper_ObterTipoConteudo
{
        
    function obterTipoConteudo($id)
    {
        $tiposConteudos = new Model_DbTable_TiposConteudos();
        return $tiposConteudos->designacaoTipoConteudo($id);
    }
}

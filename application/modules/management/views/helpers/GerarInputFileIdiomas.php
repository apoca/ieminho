<?php

class Zend_View_Helper_GerarInputFileIdiomas {
    
    

    function gerarInputFileIdiomas($descricaoLabel, $descricaoName, $values) {
        $idiomas = new Model_DbTable_Idiomas();
        $tdsIdiomas = $idiomas->pesquisarParaSeleccionar();
//            print_r($values);
//            $values['pt'];
        
        $html = '';

        foreach ($tdsIdiomas as $key => $idioma) {
            $html .= '<label for="' . $descricaoName . '_' . $idioma->iso . '">' . $descricaoLabel . ' (' . $idioma->designacao . '): </label>
                     <input type="file" name="' . $descricaoName . '_' . $idioma->iso . '" id="' . $descricaoName . '_' . $idioma->iso . '" value="'.$values['' . $idioma->iso . ''].'" /><br />
                     <input type="text" name="' . $descricaoName . '_' . $idioma->iso . '_old" id="' . $descricaoName . '_' . $idioma->iso . '_old" value="'.$values['' . $idioma->iso . ''].'" style="display:none;" />';
                     
        }
        return $html;
    }
    

}

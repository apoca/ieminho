<?php

class Zend_View_Helper_GerarTextAreaIdiomasSemTabs {
    /* <?=$this->gerarTextAreaIdiomasSemTabs('Título', 'designacao', $valores = array(
      'pt'=>'Ola mundo Portugal',
                      'es'=>'Holla mundo España',
                      'en'=>'Ola mundo inglês'))?> 
     */
    
    

    function gerarTextAreaIdiomasSemTabs($descricaoLabel, $descricaoName, $values) {
        $idiomas = new Model_DbTable_Idiomas();
        $tdsIdiomas = $idiomas->pesquisarParaSeleccionar();
        
        $html = '';
        foreach ($tdsIdiomas as $key => $idioma) {
            $html .= '<label for="' . $descricaoName . '_' . $idioma->iso . '">' . $descricaoLabel . ' (' . $idioma->designacao . '): </label>
                     <textarea name="' . $descricaoName . '_' . $idioma->iso . '" id="' . $descricaoName . '_' . $idioma->iso . '" rows="8" cols="20" >' . $values['' . $idioma->iso . ''] . '</textarea>';
                     
        }
        return $html;
        
        
    }
    

}

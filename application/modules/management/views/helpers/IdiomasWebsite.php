<?php

class Zend_View_Helper_IdiomasWebsite
{
        
    function idiomasWebsite(){     
        $configlang = new Model_DbTable_ConfiguracoesIdioma();
        return $configlang->fetchAll($configlang->select()->where('option_name = ?', 'status')->where('option_value = ?', 'A'));
        
    }
    
}

<?php

class Zend_View_Helper_GerarTextAreaIdiomas {
    /* <?=$this->gerarTextAreaIdiomas('Introdução', 'introducao', $valores = array(
      'pt'=>'Ola mundo Portugal',
      'en'=>'Ola mundo inglês',
      'fr'=>'',
      'de'=>'Ola mundo Deutshland'))?> 
     * 
                                <div class="tabbable">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#tab_pt">Section 1</a></li>
                                        <li class=""><a data-toggle="tab" href="#tab_en">Section 2</a></li>
                                        <li class=""><a data-toggle="tab" href="#tab_es">Section 3</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab_pt" class="tab-pane active">
                                            
     *                                      <div class="control-group">											
                                                <label class="control-label" for="descricao"><?=$this->translate('_descricao')?></label>
                                                <div class="controls">
                                                    <textarea class="input-xlarge" id="descricao" name="descricao" rows="3"></textarea>  
                                                </div> 		
                                                <button style="clear: both; float: left; margin-top: -2%;" class="btn btn-mini" type="button" onclick="insertReadMore('descricao', '<hr id=content-readmore />');"><?=$this->translate('_addLermais')?></button>
                                            </div> 
     * 
                                        </div>
                                        <div id="tab_en" class="tab-pane">
                                        <p>Howdy, I'm in Section 2.</p>
                                        </div>
                                        <div id="tab_es" class="tab-pane">
                                        <p>What up girl, this is Section 3.</p>
                                        </div>
                                    </div>
                                </div>
     * 
     */

    function gerarTextAreaIdiomas($descricaoLabel, $descricaoName, $values) {
        $configlang = new Model_DbTable_ConfiguracoesIdioma();
        $tdsIdiomas = $configlang->fetchAll($configlang->select()->where('option_name = ?', 'status')->where('option_value = ?', 'A'));
//            print_r($values);
//            $values['pt'];
        $html .= '<div class="control-group">';
        $html .= '<div class="tabbable">
                    <ul class="nav nav-tabs">';
                    
        foreach ($tdsIdiomas as $key => $idioma) {
            $html .= '  <li class="'.($key == 0 ? 'active' : '').'"><a data-toggle="tab" href="#' . $idioma->idioma_iso . '">' . $idioma->idioma_iso . '</a></li>';
        }
        $html .= '</ul>';
	
        $html .= '<div class="tab-content" style="width: 100%">';
        foreach ($tdsIdiomas as $key => $idioma) {
            $html .= '<div id="' . $idioma->idioma_iso . '" class="tab-pane '.($key == 0 ? 'active' : '').'">
                            <div class="control-group">
                            <label for="' . $descricaoName . '_' . $idioma->idioma_iso . '">' . $descricaoLabel . ' (' . $idioma->idioma_iso . '): </label>
                                <textarea class="input-xlarge" name="' . $descricaoName . '_' . $idioma->idioma_iso . '" id="' . $descricaoName . '_' . $idioma->idioma_iso . '" >' . $values['' . $idioma->idioma_iso . ''] . '</textarea>
                            </div>';
	    $html .='<button style="clear: both; float: left; margin-top: -2%;" class="btn btn-mini" type="button" onclick="insertReadMore(\''.$descricaoName.'_'.$idioma->idioma_iso.'\', \'<hr id=content-readmore />\');">Ler mais...</button>';
            $html .= '</div>';
        }
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
    
    function teste(){
        $text= '<!--:pt-->Ola mundo
        <blockquote>A Gambuzinos XPTO foi fundada em 1971 e tem, desde então, fornecido gambuzinos da mais alta qualidade aos seus clientes. Localizada em Gotham, a Gambuzinos XPTO emprega mais de 2,000 colaboradores e é um dos pilares da comunidade de Gotham.</blockquote>
        Visite <a href="http://localhost/wordpress/wp-admin/">o seu painel</a> para apagar esta página ou para criar mais páginas para o seu conteúdo. Divirta-se!<!--:-->
        
        <!--:es-->Holla mundo
        <blockquote>A Gambuzinos XPTO foi fundada em 1971 e tem, desde então, fornecido gambuzinos da mais alta qualidade aos seus clientes. Localizada em Gotham, a Gambuzinos XPTO emprega mais de 2,000 colaboradores e é um dos pilares da comunidade de Gotham.</blockquote>
        Visite <a href="http://localhost/wordpress/wp-admin/">o seu painel</a> para apagar esta página ou para criar mais páginas para o seu conteúdo. Divirta-se!<!--:-->
        
        <!--:en-->Hello world
        <blockquote>A Gambuzinos XPTO foi fundada em 1971 e tem, desde então, fornecido gambuzinos da mais alta qualidade aos seus clientes. Localizada em Gotham, a Gambuzinos XPTO emprega mais de 2,000 colaboradores e é um dos pilares da comunidade de Gotham.</blockquote>
        Visite <a href="http://localhost/wordpress/wp-admin/">o seu painel</a> para apagar esta página ou para criar mais páginas para o seu conteúdo. Divirta-se!<!--:-->';
        
        $split_regex = "#(<!--[^-]*-->|\[:[a-z]{2}\])#ism";
        $blocks = preg_split($split_regex, $text, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
	foreach($blocks as $block) {
            if(preg_match("#^<!--:([a-z]{2})-->$#ism", $block, $matches)) {
                $language = $matches[1];
                echo 'Lingua: '.$language.' <br />';
                $prefix = '<!--:'.$language.'-->';
                $suffix = '<!--:-->';
                $filterText = $prefix . array_shift(explode($suffix, array_pop(explode($prefix, $text)))) . $suffix;
                
                echo 'Bloco: '.$filterText.' <br />';
            }
        }
    }

}

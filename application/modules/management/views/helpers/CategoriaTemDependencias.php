<?php

class Zend_View_Helper_CategoriaTemDependencias
{
        
    function categoriaTemDependencias($categoria_id)
    {
        $categorias = new Model_DbTable_Categorias();
        $categoria = $categorias->fetchAll($categorias->select()->where('categoria_pai = ?', $categoria_id));
        return ($categoria->count() > 0) ? TRUE : FALSE;
    }
}

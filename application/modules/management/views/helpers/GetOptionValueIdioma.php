<?php

class Zend_View_Helper_GetOptionValueIdioma
{
        
    function getOptionValueIdioma($option_name, $idioma_iso='pt'){
        $configuracoesIdioma = new Model_DbTable_ConfiguracoesIdioma();
        return $configuracoesIdioma->optionValueIdioma($option_name, $idioma_iso);
    }
}

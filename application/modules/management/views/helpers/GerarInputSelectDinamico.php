<?php

class Zend_View_Helper_GerarInputSelectDinamico{	

	function gerarInputSelectDinamico($descricaoLabel, $descricaoName, $values, $fields, $selected) {
                $html = '';
            
                $html .= '<div class="control-group">';
		$html .= '<label class="control-label" for="' . $descricaoName . '">' . $descricaoLabel . ':</label>';
                $html .= '<div class="controls">';
		$html .= '<select name="' . $descricaoName . '" id="' . $descricaoName . '">';
                $html .= '<option value="">--Não aplicável--</option>';
                        
                if($values!=NULL){
                    foreach($values as $key=>$value){
                        
                            $html .= '<option value="' . $value->$fields[0] . '" '.($value->$fields[0] == $selected ? 'selected="selected"' : '').'>' . strip_tags($value->$fields[1]) . '</option>';
                    }
                }
		$html .= '</select>
                         </div> 			
                         </div>';
		
		return $html;
		
		
	}
	

}




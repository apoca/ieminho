<?php

//MessageBox
class Zend_View_Helper_InfoParticipante {

    function infoParticipante($id) {
        $utilizadores = new Model_DbTable_Utilizadores();
        $infoUser = $utilizadores->fetchRow($utilizadores->select()->where('id = ?', $id));
        $c = $this->arrayToObject($infoUser);
        return $c;
    }

    function arrayToObject($array) {
        if (!is_array($array)) {
            return $array;
        }

        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name => $value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = $this->arrayToObject($value);
                }
            }
            return $object;
        } else {
            return FALSE;
        }
    }

}

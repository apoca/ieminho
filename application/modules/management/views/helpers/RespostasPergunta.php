<?php

class Zend_View_Helper_RespostasPergunta
{
        
    function respostasPergunta($pergunta_id)
    {
        $respostasPerguntas = new Model_DbTable_RespostasPergunta();
        return $respostasPerguntas->fetchAll($respostasPerguntas->select()->where('pergunta_id = ?', $pergunta_id));
    }
}

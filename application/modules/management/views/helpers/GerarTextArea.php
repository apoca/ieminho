<?php

class Zend_View_Helper_GerarTextArea {
    /* <?=$this->gerarTextArea('Criado em', 'criado', '')?> 
     */
    

    function gerarTextArea($descricaoLabel, $descricaoName, $value, $temLerMais='N') {
            $html = '<div class="control-group">';
            $html .= '<label class="control-label" for="' . $descricaoName . '">' . $descricaoLabel . ' :</label>
                      <div class="controls">
                      <textarea name="' . $descricaoName . '" id="' . $descricaoName . '" >' . $value . '</textarea>
                      </div>';
            $html .= ($temLerMais == 'S' ? '<button style="clear: both; float: left; margin-top: 2%;" class="btn btn-mini" type="button" onclick="insertReadMore(\''.$descricaoName.'\', \'<hr id=content-readmore />\');">Adicionar Ler mais...</button>' : '');
            $html .='</div>';
        return $html;
    }
    

}

<?php

class Zend_View_Helper_OutrosIdiomas
{
        
    function outrosIdiomas($idioma_iso)
    {        
        $configlang = new Model_DbTable_ConfiguracoesIdioma();
        return $configlang->fetchAll($configlang->select()->where('idioma_iso != ?', $idioma_iso)->where('option_name = ?', 'status')->where('option_value = ?', 'A'));
    }
    
}

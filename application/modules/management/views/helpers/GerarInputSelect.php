<?php

class Zend_View_Helper_GerarInputSelect {	

/*
	<label for="hierarquico">É hierárquico: <span>*</span></label>
					<select name="hierarquico" id="hierarquico" >
						<option value="N" label="Não">Não</option>
						<option value="S" label="Sim">Sim</option>
					</select>
 * 
 * <?=$this->gerarInputSelect('É hierárquico <span>*</span>', 'hierarquico', $valores = array(
                            'N'=>'Não', 'S'=>'Sim'), 'S')?>
 * 
 * <?=$this->gerarInputSelect($atributo->labelSingular.' ', $atributo->name, $valoresCombo, '')?>

*/
	function gerarInputSelect($descricaoLabel, $descricaoName, $values, $selected) {
            
		$html .= '<label for="' . $descricaoName . '">' . $descricaoLabel . ':</label>';
		$html .= '<select name="' . $descricaoName . '" id="' . $descricaoName . '">';
                if($values!=NULL){
                    $html .= '<option value="">--Não Aplicável--</option>';
                    foreach($values as $key=>$value){
                            $html .= '<option value="' . $value . '" '.($value == $selected ? 'selected="selected"' : '').'>' . ucfirst($value)  . '</option>';
                    }
                }
		$html .= '</select>';
		
		return $html;
		
		
	}
	

}




<?php

class Zend_View_Helper_ConteudoIdioma
{
        
    function conteudoIdioma($main_id, $idioma_iso)
    {
        $conteudos = new Model_DbTable_Conteudos();
        return $conteudos->conteudoIdioma($main_id, $idioma_iso);
    }
}

<?php

class Zend_View_Helper_GetRoles
{
        
    function getRoles(){        
        $roles = new Model_DbTable_Roles();
        return $roles->fetchAll($roles->select()->where('estado = ?', 'A')->where('id_role != ?', 1)->order('id_role ASC'));
    }
    
}

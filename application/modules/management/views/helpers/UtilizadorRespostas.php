<?php

class Zend_View_Helper_UtilizadorRespostas
{
        
    function utilizadorRespostas($utilizador_id)
    {        
        /*$utilizadoresResposta = new Model_DbTable_UtilizadorRespostas();
        return $utilizadoresResposta->fetchAll($utilizadoresResposta->select()->where('utilizador_id = ?', $utilizador_id));*/
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                ->from(array('ur' => 'utilizador_respostas'))
                ->join(array('rp' => 'respostas_pergunta'),
                 'ur.resposta_id = rp.id', array('designacao as resposta'))
                ->join(array('p' => 'perguntas'),
                 'rp.pergunta_id = p.id', array('designacao as pergunta', 'label'))
                ->join(array('wcp' => 'website_categorias_pergunta'),
                 'p.categoria_id = wcp.id', array('designacao as seccao', 'id as id_seccao'))
                ->where('ur.utilizador_id = ?',$utilizador_id);
        

        $dados = $db->query($select)->fetchAll();
        return $dados;
         
    
    }
}

<?php

class Zend_View_Helper_LeadContent
{
        function leadContent($text,$length,$tail) 
        {
                $lead = explode('<hr id="content-readmore">', $text);
                
                if($lead[0]){
                    return $lead[0];
                }else{
                    if (strlen($text) > $length) {
                            $arr = str_word_count($text, 2);
                            $blubb = false;
                            foreach ($arr as $intPos => $strWord) {
                                    if ($blubb) return substr($text, 0, $intPos).$tail;
                                            if ($intPos > $length) $blubb = true;
                            }
                        }
                    return $text;
                }
                
        }
}

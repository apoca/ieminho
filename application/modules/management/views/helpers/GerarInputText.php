<?php

class Zend_View_Helper_GerarInputText {
    /* <?=$this->gerarInputText('Ficheiro', 'file', '')?> 
     * 
     <label for="criado">Criado em:</label>
    <input type="text" name="criado" id="criado" value="" />

    <label for="inicioPublicacao">Iniciar publicação:</label>
    <input type="text" name="inicioPublicacao" id="inicioPublicacao" value="" />

    <label for="fimPublicacao">Terminar publicação:</label>
    <input type="text" name="fimPublicacao" id="fimPublicacao" value="" />
     */
    
    function gerarInputText($descricaoLabel, $descricaoName, $value) {
            $html = '<div class="control-group">';
            $html .= '<label class="control-label" for="' . $descricaoName . '">' . $descricaoLabel . ':</label>
                        <div class="controls">
                            <input type="text" class="input-large" name="' . $descricaoName . '" id="' . $descricaoName . '" value="'.$value.'" />
                        </div> 			
                     </div>';
        return $html;
    }
    

}

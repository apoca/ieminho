<?php

class Zend_View_Helper_ValorAtributosReferenciaConteudoFK { 
    
    function valorAtributosReferenciaConteudoFK($caracteristica_id, $tipo_conteudo_id, $conteudo_id) {
        
        $conteudosTipoCtd = new Model_DbTable_ConteudosTiposConteudos();
        $queryCTypeC    = $conteudosTipoCtd->fetchRow($conteudosTipoCtd->select()->where('caracteristica_id = ?', $caracteristica_id)->where('tipo_id  = ?', $tipo_conteudo_id ));
        $atributo_id = $queryCTypeC->id;
        
        $valoresAttConteudo = new Model_DbTable_ValoresAtributosReferencia();
        
        $query    = $valoresAttConteudo->fetchRow($valoresAttConteudo->select()->where('referencia_id = ?', 'conteudo')->where('atributo_id  = ?', $atributo_id )->where('conteudo_id  = ?', $conteudo_id ));
        return $query->valor;
    }
    

}

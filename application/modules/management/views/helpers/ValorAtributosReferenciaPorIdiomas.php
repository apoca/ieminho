<?php

class Zend_View_Helper_ValorAtributosReferenciaPorIdiomas{ 
    
    function valorAtributosReferenciaPorIdiomas($atributo_id, $conteudo_id, $idioma_iso) {
        $valoresAttConteudo = new Model_DbTable_ValoresAtributosReferencia();
        $query    = $valoresAttConteudo->fetchRow($valoresAttConteudo->select()->where('atributo_id = ?', $atributo_id)->where('conteudo_id  = ?', $conteudo_id ));
        
        $prefix = '<!--:'.$idioma_iso.'-->';
        $suffix = '<!--:-->';
        return strip_tags($prefix . array_shift(explode($suffix, array_pop(explode($prefix, $query->valor)))) . $suffix);
        
    }
    

}

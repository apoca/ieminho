<?php

class Zend_View_Helper_AttributesHTML{

    function attributesHTML($html) {
        
        $domDoc = new DOMDocument;
        $domDoc->loadHTML($html);
        
        $img = $domDoc->getElementsByTagName("img")->item(0);
        $data = array();
        
        
        $data['name']          = basename($img->attributes->getNamedItem("src")->nodeValue);
        $data['width']         = $img->attributes->getNamedItem('width')->nodeValue;
        $data['height']        = $img->attributes->getNamedItem('height')->nodeValue;
  
       
        return $this->arrayToObject($data);
        
    }

    function arrayToObject($array) {
        if (!is_array($array)) {
            return $array;
        }

        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name => $value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = $this->arrayToObject($value);
                }
            }
            return $object;
        } else {
            return FALSE;
        }
    }

}

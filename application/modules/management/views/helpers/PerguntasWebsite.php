<?php

class Zend_View_Helper_PerguntasWebsite
{
        
    function perguntasWebsite($categoria_id)
    {
        $perguntas = new Model_DbTable_Perguntas();
        return $perguntas->fetchAll($perguntas->select()->where('categoria_id = ?', $categoria_id));
    }
}

<?php

class Zend_View_Helper_GerarInputTextIdiomas {
    /* <?=$this->gerarInputTextIdiomas('Título', 'designacao', $valores = array(
      'pt'=>'Ola mundo Portugal',
                      'es'=>'Holla mundo España',
                      'en'=>'Ola mundo inglês'))?> 
    
     * <label for="designacao_pt">Designação (Português): <span>*</span></label>
        <input type="text" name="designacao_pt" id="designacao_pt" value="" />

        <label for="designacao_es">Designação (Espanhol): <span>*</span></label>
        <input type="text" name="designacao_es" id="designacao_es" value="" />

        <label for="designacao_en">Designação (English): <span>*</span></label>
        <input type="text" name="designacao_en" id="designacao_en" value="" />
     * 
     */
    
    

    function gerarInputTextIdiomas($descricaoLabel, $descricaoName, $values) {
        $configlang = new Model_DbTable_ConfiguracoesIdioma();
        $tdsIdiomas = $configlang->fetchAll($configlang->select()->where('option_name = ?', 'status')->where('option_value = ?', 'A'));
//            print_r($values);
//            $values['pt'];
        $html = '';
        foreach ($tdsIdiomas as $key => $idioma) {
            $html .= '<div class="control-group">';
            $html .= '<label class="control-label" for="' . $descricaoName . '_' . $idioma->idioma_iso . '">' . $descricaoLabel . ' (' . $idioma->idioma_iso . '): </label>';
            $html .= '<div class="controls">
                      <input type="text" class="input-large" name="' . $descricaoName . '_' . $idioma->idioma_iso . '" id="' . $descricaoName . '_' . $idioma->idioma_iso . '" value="'.$values['' . $idioma->idioma_iso . ''].'" />
                      </div> ';
            $html .= '</div>';
        }
        return $html;
    }
    
    function teste(){
        $text= '<!--:pt-->Ola mundo<!--:-->
        
        <!--:es-->Holla mundo<!--:-->
        
        <!--:en-->Hello world<!--:-->';
        
        $split_regex = "#(<!--[^-]*-->|\[:[a-z]{2}\])#ism";
        $blocks = preg_split($split_regex, $text, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
	foreach($blocks as $block) {
            if(preg_match("#^<!--:([a-z]{2})-->$#ism", $block, $matches)) {
                $language = $matches[1];
                echo 'Lingua: '.$language.' <br />';
                $prefix = '<!--:'.$language.'-->';
                $suffix = '<!--:-->';
                $filterText = $prefix . array_shift(explode($suffix, array_pop(explode($prefix, $text)))) . $suffix;
                
                echo 'Bloco: '.$filterText.' <br />';
            }
        }
    }

}

<?php

class Zend_View_Helper_GerarInputSelectKey {	

	function gerarInputSelectKey($descricaoLabel, $descricaoName, $values, $selected, $disabled=FALSE) {
            
                $html = '';
            
                $html .= '<div class="control-group">';
		$html .= '<label class="control-label" for="' . $descricaoName . '">' . $descricaoLabel . ':</label>';
                $html .= '<div class="controls">';
		$html .= '<select name="' . $descricaoName . '" id="' . $descricaoName . '" '.($disabled == TRUE ? 'disabled=""' : '').'>'; //
                $html .= '<option value="">--Não aplicável--</option>';
                        
                if($values!=NULL){
                    foreach($values as $key=>$value){/*key p value*/
                            $html .= '<option value="' . $key . '" '.($key == $selected ? 'selected="selected"' : '').'>' . $value . '</option>';
                    }
                }
		$html .= '</select>
                         </div> 			
                         </div>';
		
		return $html;
		
		
	}
	

}




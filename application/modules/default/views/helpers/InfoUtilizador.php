<?php

//MessageBox
class Zend_View_Helper_InfoUtilizador {

    function infoUtilizador() {
        $storage = new Zend_Auth_Storage_Session('utilizadores');
        $infoCliente = $storage->read();
       // return $infoCliente;
        $c = $this->arrayToObject($infoCliente);
        return $c;
    }

    function arrayToObject($array) {
        if (!is_array($array)) {
            return $array;
        }

        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name => $value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = $this->arrayToObject($value);
                }
            }
            return $object;
        } else {
            return FALSE;
        }
    }

}

<?php

/* class MessageBox.php  -> require   MessageBox.js
 * how to use in controllers (default)
 * 
 * 2 parameters in array
 * array(@type = (erro || aviso || sucesso) , @message = (String) )
 * 
 * eg. $this->_sessao->messageBox = array('sucesso' => 'Record inserted in database.');
 */

class Zend_View_Helper_MessageBox {
    
    function messageBox() {
        
        $display = new Zend_Session_Namespace('display');
        if (isset($display->messageBox)){
            foreach($display->messageBox as $tipo => $alert) {
           
                switch ($tipo){
                    case 'erro':
                        echo "<script>$(document).ready(function () {messageBox('<span style=\"color:red;\">".$alert."</span>', '<span style=\"color:red;text-shadow:1px 1px 0 #fff;\">Erro</span>', 'Ok')});</script>";
                    break;
                    case 'aviso':
                        echo "<script>$(document).ready(function () {messageBox('<span style=\"\">".$alert."</span>', '<span style=\"\">Aviso</span>', 'Ok')});</script>";
                    break;
                    default :
                        echo "<script>$(document).ready(function () {messageBox('<span style=\"\">".$alert."</span>', '<span style=\"\">Informação</span>', 'Ok')});</script>";
                    break;
                }
                unset($display->messageBox);

            }
        }
        
        
    }
    
    

}

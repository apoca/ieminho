<?php

class Zend_View_Helper_OutrosIdiomas
{
        
    function outrosIdiomas($idioma_iso)
    {
        $configlang = new Model_DbTable_ConfiguracoesIdioma();
        return $configlang->fetchAll($configlang->select()->where('option_name = ?', 'status')->where('option_value = ?', 'A')->where('idioma_iso != ?', $idioma_iso));
        
    }    
    /*   $soap = new Zend_Soap_Client(DOMINIO."/rest?wsdl");
        $outrosIdiomas = $soap->getOutrosIdiomas($this->website_id, $this->idioma_iso);
        
        $idiomas = array();
        foreach ($outrosIdiomas as $key => $outro) {
           
            for ($i = 0; $i <= count($outro)-1; $i++) {
                $idiomas[] = array(
                        'iso'		=> $outro[$i]->iso,
                        'designacao'	=> $outro[$i]->designacao,
                );
            }
        }*/
}

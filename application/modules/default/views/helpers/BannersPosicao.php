<?php

class Zend_View_Helper_BannersPosicao
{
        
    function bannersPosicao($seccao)
    {
        $banners = new Model_DbTable_DestaqueSlideshow();
        return $banners->fetchAll($banners->select()->where('seccao = ?', $seccao)->order('posicao ASC'));
    } 
}

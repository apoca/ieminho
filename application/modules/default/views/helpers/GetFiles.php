<?php

class Zend_View_Helper_GetFiles
{
        
    function getFiles($directory, $filter = array( "*" ))
    {
        $results = array(); // Result array
        $filter = (array) $filter; // Cast to array if string given

        // Open directory
        $handler = opendir( $directory );

        // Loop through files
        while ( $file = readdir($handler) ) {

            // Jump over directories.
            if( is_dir( $file ) )
                continue;

            // Prepare file extension
            $extension = end( explode( ".", $file ) ); // Eg. "jpg"

            // If extension fits add it to array
            if ( $file != "." && $file != ".." && ( in_array( $extension, $filter ) || in_array( "*", $filter ) ) ) {
                $results[] = $file;
            }
        }

        // Close handler
        closedir($handler);

        // Return
        return $results;
    }
    
}

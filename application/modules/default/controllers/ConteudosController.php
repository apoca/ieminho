<?php

class ConteudosController extends Zend_Controller_Action
{
    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    protected $_idioma_iso = 'pt';

    public function init() {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->view->action = $request->getActionName(); $this->_action = $request->getActionName();
        $this->view->controller = $request->getControllerName(); $this->_controller = $request->getControllerName();
        $this->view->module = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_idioma_iso = $this->view->idioma_iso = $this->getRequest()->getParam('idioma');
        $this->_funcoes = new App_Class_Funcoes();
        $this->_funcoes->infoHead($this->_idioma_iso);
        
        $menus = new Model_DbTable_Menus();
        $menu = $menus->fetchRow($menus->select()->where('link  = ?', $this->_funcoes->getActualUrl($this->getRequest()->getRequestUri()))->where('idioma_iso = ?', $this->_idioma_iso));
        $this->view->menu = $menu;
    }

    public function conteudoAction() {
        $id = $this->getRequest()->getParam('id');

        $conteudo = new Model_DbTable_Conteudos();
        $cnt = $conteudo->fetchRow('id =' . $id);
        $this->view->conteudo = $cnt;

        
    }
}

<?php

class NewsletterController extends Zend_Controller_Action {

    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    protected $_idioma_iso = 'pt';

    public function init() {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->view->action = $request->getActionName(); $this->_action = $request->getActionName();
        $this->view->controller = $request->getControllerName(); $this->_controller = $request->getControllerName();
        $this->view->module = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_idioma_iso = $this->view->idioma_iso = $this->getRequest()->getParam('idioma');
        $this->_funcoes = new App_Class_Funcoes();
        $this->_funcoes->infoHead($this->_idioma_iso);
        
        $menus = new Model_DbTable_Menus();
        $menu = $menus->fetchRow($menus->select()->where('link  = ?', $this->_funcoes->getActualUrl($this->getRequest()->getRequestUri()))->where('idioma_iso = ?', $this->_idioma_iso));
        $this->view->menu = $menu;
    }

    public function indexAction() {
        if ($this->getRequest()->isPost()) {
            $email = $this->getRequest()->getPost('email');

            $validator = new Zend_Validate_EmailAddress();
            if ($validator->isValid($email) == FALSE) {
                //erro
            } else {
                $subscritores = new Model_DbTable_Subscritores();
                if ($subscritores->jaExiste($email, $this->_website_id, $this->_idioma_iso) > 0) {
                    //"Endereço de e-mail: <em>{$email} </em> já existe na base de dados!
                } else {
                    $hash = md5(rand(0, 1000));
                    $subscritores->addSubscritor($email, date("Y-m-d H:i:s"), $this->_website_id, $this->_idioma_iso, $hash, 'A');
                    //Endereço de e-mail: <em>{$email} </em> adicionado com sucesso!
                }
            }
        }
    }
}

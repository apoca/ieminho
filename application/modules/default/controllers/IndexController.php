<?php

class IndexController extends Zend_Controller_Action {

    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    protected $_idioma_iso = 'pt';

    public function init() {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->view->action = $request->getActionName(); $this->_action = $request->getActionName();
        $this->view->controller = $request->getControllerName(); $this->_controller = $request->getControllerName();
        $this->view->module = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_idioma_iso = $this->view->idioma_iso = $this->getRequest()->getParam('idioma');
        $this->_funcoes = new App_Class_Funcoes();
        $this->_funcoes->infoHead($this->_idioma_iso);
        
        $menus = new Model_DbTable_Menus();
        $menu = $menus->fetchRow($menus->select()->where('link  = ?', $this->_funcoes->getActualUrl($this->getRequest()->getRequestUri()))->where('idioma_iso = ?', $this->_idioma_iso));
        $this->view->menu = $menu;
    }

    public function indexAction() {
        
    }
    public function notificarAdminAction() {
        //$this->_helper->layout()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $dataFormatada = new Zend_Date();
            $message = $this->getRequest()->getPost('message');
            $exception = $this->getRequest()->getPost('exception');
            $extraData = $this->getRequest()->getPost('extraData');
            $mensagemEnviar = '
                            <tr>
                            <td bgcolor="#FFFFFF" style="padding:10px 20px; background:#ffffff;background-color:#ffffff;" valign="top">
                                    <span style="color:#999999; font-size:8pt;">' . $dataFormatada->get(Zend_Date::WEEKDAY . ", " . Zend_Date::DAY . " " . Zend_Date::MONTH_NAME_SHORT . " " . Zend_Date::YEAR) . '</span><br>
                                    <p style="padding:0; margin:0 0 11pt 0;line-height:160%; font-size:18px;">
                                    Surgiu uma excepção/erro na aplicação</p>
                                    ' . $extraData . '<br />
                                    <p>Mensagem: ' . $message . '</p>
                                    <pre>' . $exception . '</pre> <br />
                                     ';
            $mensagemEnviar .= '
                </td>
            </tr>';
            $this->enviarEmail('luis.filipe@setima.pt', 'erro//backoffice', $mensagemEnviar);

            $this->_redirect('?notificou=1');
        }
    }

    public function enviarEmail($para, $assunto, $mensagem) {
        $smtp = MAIL_SMTP;
        $conta = MAIL_NEWSLETTER;
        $senha = MAIL_PASSWORD;
        $de = MAIL_NEWSLETTER;

        try {
            $config = array(
                'auth' => 'login',
                'username' => $conta,
                'password' => $senha,
                'ssl' => MAIL_SSL,
                'port' => MAIL_SSL_PORT
            );

            $mailTransport = new Zend_Mail_Transport_Smtp($smtp, $config);

            $mail = new Zend_Mail('utf-8');
            $mail->setFrom($de, MAIL_NAME);
            $mail->addTo($para);
            $mail->setBodyHtml($mensagem);
            $mail->setSubject($assunto);
            $mail->addHeader('X-MailGenerator', 'Sétima SW v1.0');

            $mail->send($mailTransport);
        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

}

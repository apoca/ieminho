<?php

class NoticiasController extends Zend_Controller_Action {

    protected $_controller = NULL;
    protected $_action = NULL;
    protected $_sessao = NULL;
    protected $_funcoes = NULL;
    protected $_idioma_iso = 'pt';

    public function init() {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->view->action = $request->getActionName(); $this->_action = $request->getActionName();
        $this->view->controller = $request->getControllerName(); $this->_controller = $request->getControllerName();
        $this->view->module = $request->getModuleName(); $this->_sessao = new Zend_Session_Namespace('display');
        $this->_idioma_iso = $this->view->idioma_iso = $this->getRequest()->getParam('idioma');
        $this->_funcoes = new App_Class_Funcoes();
        $this->_funcoes->infoHead($this->_idioma_iso);
        
        $menus = new Model_DbTable_Menus();
        $menu = $menus->fetchRow($menus->select()->where('link  = ?', $this->_funcoes->getActualUrl($this->getRequest()->getRequestUri()))->where('idioma_iso = ?', $this->_idioma_iso));
        $this->view->menu = $menu;
    }

    public function indexAction() {
        $conteudos = new Model_DbTable_Conteudos();
        $arrayIdiomas = array(2 => 'pt');
        $tipo_id = array_search($this->_idioma_iso, $arrayIdiomas);
        
        $noticia = $conteudos->fetchRow($conteudos->select()->where('tipo_id = ?', $tipo_id)->where('estado = ?', 'P')->order('RAND()')->limit(1));
        $id = $noticia->id;
        $this->view->noticia = $noticia;

        $this->view->outrasNoticias = (isset($noticia->id) ? $conteudos->fetchAll($conteudos->select()->where('tipo_id = ?', $tipo_id)->where('estado = ?', 'P')->where('id != ?', $id)->order('id DESC')) : NULL );  
        
        
    }

    public function noticiaAction() {
        $id = $this->getRequest()->getParam('id');
                
        $conteudos = new Model_DbTable_Conteudos();
        $this->view->noticia = $noticia= $conteudos->fetchRow($conteudos->select()->where('id = ?', $id));
        $arrayIdiomas = array(2 => 'pt');
        $tipo_id = array_search($noticia->idioma_iso, $arrayIdiomas);
        
        $this->view->outrasNoticias = (isset($noticia) ? $conteudos->fetchAll($conteudos->select()->where('tipo_id = ?', $tipo_id)->where('estado = ?', 'P')->where('id != ?', $id)->order('id DESC')) : NULL );  
        
        
    }

}

<?php

class UtilizadoresController extends Zend_Controller_Action {


    public function init() {
    }

    public function verificaEmailAction() {
        $this->_helper->layout()->disableLayout();
        
         if ($this->getRequest()->isPost()) {
            $email = $this->getRequest()->getPost('email');
            $utilizadores = new Model_DbTable_Utilizadores();
            $existe = $utilizadores->existeEmailUtilizador($email);
            echo ($existe > 0) ? 's' : 'n';
            return;
         }
    }
}

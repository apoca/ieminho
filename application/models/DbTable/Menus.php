<?php

class Model_DbTable_Menus extends Zend_Db_Table_Abstract {

    protected $_name = 'menus';
    protected $_primary = 'id';

    public function getMenu($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar a registo com o id: $id");
        }
        return $row->toArray();
    }

    public function existeMenuIdioma($idioma_iso, $sharedKey) {
        $select = $this->select()->where('idioma_iso  = ?', $idioma_iso)
                ->where('sharedKey  = ?', $sharedKey);
        $existe = $this->fetchAll($select)->count();
    }

    public function addMenu($idioma_iso, $tipo_id, $item_id, $itemsuperior_id, $designacao, $path, $link, $posicao, $parametros) {
        $data = array(
            'idioma_iso' => $idioma_iso,
            'tipo_id' => $tipo_id,
            'item_id' => $item_id,
            'itemsuperior_id' => $itemsuperior_id,
            'designacao' => $designacao,
            'path' => $path,
            'link' => $link,
            'posicao' => $posicao,
            'parametros' => $parametros,
        );
        $this->insert($data);
    }

    function updateMenu($id, $idioma_iso, $tipo_id, $item_id, $itemsuperior_id, $designacao, $path, $link, $posicao, $parametros) {
        $data = array(
            'idioma_iso' => $idioma_iso,
            'tipo_id' => $tipo_id,
            'item_id' => $item_id,
            'itemsuperior_id' => $itemsuperior_id,
            'designacao' => $designacao,
            'path' => $path,
            'link' => $link,
            'posicao' => $posicao,
            'parametros' => $parametros,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    function updateOpcao($id, $campo, $valor) {
        $data = array(
            $campo => $valor,
        );
        return $this->update($data, 'id = ' . (int) $id);
    }

    function updateEstadoMenu($id, $estado) {
        $data = array(
            'estado' => $estado,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    public function getQtdPosicoes($idioma_iso, $tipo_id, $item_id) {
        $select = $this->select()->where('idioma_iso  = ?', $idioma_iso)
                ->where('tipo_id  = ?', $tipo_id)
                ->where('item_id  = ?', $item_id);
        return $this->fetchAll($select)->count();
    }

    public function getQtdPosicoesByTipoAndIdioma($idioma_iso, $tipo_id) {
        $select = $this->select()->where('idioma_iso  = ?', $idioma_iso)
                ->where('tipo_id  = ?', $tipo_id);
        return $this->fetchAll($select)->count();
    }

    public function jaExistePath($idioma_iso, $path) {
        if ($path == '#') {
            return 0;
        } else {
            $select = $this->select()->where('idioma_iso  = ?', $idioma_iso)
                    ->where('path  = ?', $path);
            return $this->fetchAll($select)->count();
        }
    }

    public function jaExistePathEditar($id, $idioma_iso, $path) {
        if ($path == '#') {
            return 0;
        } else {
            $select = $this->select()->where('idioma_iso  = ?', $idioma_iso)
                            ->where('path  = ?', $path)->where('id  != ?', $id);
            return $this->fetchAll($select)->count();
        }
    }

    public function getQtdPosicoesByID($id) {
        $select = $this->select()->where('id  = ?', $id);
        $query = $this->fetchRow($select);

        $idioma_iso = $query->idioma_iso;
        $tipo_id = $query->tipo_id;

        return $this->getQtdPosicoesByTipoAndIdioma($idioma_iso, $tipo_id);
    }

    function updatePosicao($posicao, $id, $itemsuperior_id) {
        $data = array(
            'posicao' => $posicao,
            'itemsuperior_id' => $itemsuperior_id,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    public function getPosicao($id) {
        $select = $this->select()->where('id = ?', $id);
        $query = $this->fetchRow($select);
        return $query->posicao;
    }

    public function temItemSuperiorID($id) {
        $query = $this->fetchRow($this->select()->where('id = ?', $id));

        return ($query->itemsuperior_id != NULL ? TRUE : FALSE);
    }

    public function getPathURL($id) {
        $select = $this->select()->where('id = ?', $id);
        $query = $this->fetchRow($select);
        return $query->path;
    }

    public function getTipoIDById($id) {
        $select = $this->select()->where('id = ?', $id);
        $query = $this->fetchRow($select);
        return $query->tipo_id;
    }

    public function getConteudoIDByPosicao($id, $posicao) {
        $tipo_id = $this->getTipoIDById($id);

        $select = $this->select()->where('tipo_id  = ?', $tipo_id)
                ->where('posicao = ?', $posicao);
        $query = $this->fetchRow($select);
        return $query->id;
    }

    function deleteMenu($id) {
        $this->delete('id =' . (int) $id);
    }

}
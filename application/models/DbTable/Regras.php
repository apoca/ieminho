<?php

class Model_DbTable_Regras extends Zend_Db_Table_Abstract {
    protected $_name = 'admin_regras';
    protected $_primary = 'id_regra';

    //protected $_dependentTables = array('BugsProducts');
    protected $_referenceMap = array(
        'FK_Role_Regra' => array(
            'columns'           => array('role_id'),
            'refTableClass'     => 'Admin_Models_Roles',
            'refColumns'        => array('id_role')
        ),
        'FK_Privilegio_Regra' => array(
            'columns'           => array('privilegio_id'),
            'refTableClass'     => 'Admin_Models_Privilegios',
            'refColumns'        => array('id_privilegio')
        )
    );

    /*
     * Retorna todas as regras
     */
    public function getAllRegras(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('ru' => $this->_name),array(
                                    'recurso' => 'ru.recursoNome',
                                    'permitir' => 'ru.permitir'))
                               ->join(
                                    array('r' => 'admin_roles'), 'ru.role_id = r.id_role',
                                    array('role' => 'r.designacao')
                                )
                               ->join(
                                    array('p' => 'admin_privilegios'), 'ru.privilegio_id = p.id_privilegio',
                                    array('privilegio' => 'p.acao')
                                );

        $dados = $db->query($select)->fetchAll();
        return $dados;
    }
    
    /* adicionar Regra*/
    public function addRegra($role_id, $privilegio_id, $modulo, $controller, $permitir)
    {
        $data = array(
            'role_id'           => $role_id,
            'privilegio_id'	=> $privilegio_id,
            'recursoNome' 	=> $modulo.':'.$controller,
            'permitir'          => $permitir,
        );
        return $this->insert($data); 
    }
    
    public function quantosRecursosDeRole($recurso, $role_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sqlquery = "SELECT COUNT(*) as quantos FROM admin_regras WHERE recursoNome LIKE '%:".$recurso."' AND role_id ='".$role_id."' AND permitir ='S'" ; 
        $linhas = $db->query($sqlquery)->fetchObject();
        
        return $linhas->quantos;
    }
    
    /* verificar se existe Regra*/
    public function existeRegra($role_id, $privilegio_id)
    {
        $select = $this->select()->where('role_id  = ?',$role_id)
                                ->where('privilegio_id  = ?',$privilegio_id);
        $tem = $this->fetchAll($select)->count();
        return $tem;
    }
    
    /* verificar se tem permissao S ou N*/
    public function temPermissao($role_id, $privilegio_id)
    {
       $select = $this->select()->where('role_id  = ?',$role_id)
                                ->where('privilegio_id  = ?',$privilegio_id)
                                ->where('permitir = ?', 'S');
       $permissao = $this->fetchRow($select);
       if (count($permissao)){
           return 'sim';
       }
       else{
           return 'nao';
       }
    }
    
    public function verificaPermissao($role_id, $privilegio_id)
    {
       $select = $this->select()->where('role_id  = ?',$role_id)
                                ->where('privilegio_id  = ?',$privilegio_id);
       $tem = $this->fetchAll($select)->count();
       
       if($tem>0)
       {//se tem permissao
          $regras = new Model_DbTable_Regras();
          if($regras->temPermissao($role_id, $privilegio_id) == 'sim'){
            return 1;
          }
          else
          {
              return 0;
          }
       }
       else
       {
           return 0;
       }
    }
    
    /*update estado da regra*/
    public function updatePermitirRegra($permitir, $role_id, $privilegio_id)
    {
        $data = array(
            'permitir' => $permitir,
        );
		
        $condicao = "role_id = ".$role_id." AND privilegio_id = " .$privilegio_id;
		
       return $this->update($data, $condicao);
    }
    
    function deleteRegraByRoleID($role_id) {
        $this->delete('role_id =' . (int) $role_id);
    }
}

<?php

class Model_DbTable_CaracteristicasTiposConteudos extends Zend_Db_Table_Abstract {

	protected $_name = 'caracteristicas_tiposconteudos';
        protected $_primary = 'id';

	public function getCaracteristicaTipoConteudo($id) {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if (!$row) {
                throw new Exception("Não é possível encontrar a registo com o id: $id");
            }
            return $row->toArray();
        }
        
        public function getCaracteristicasTiposConteudosIdioma($idioma_iso)
        {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
                ->from(array('c' => 'caracteristicas_tiposconteudos'))
                ->join(array('tc' => 'tiposconteudos'),
                'c.tipo_id = tc.id', array('designacao as tipo', 'idioma_iso'))
                ->where('tc.idioma_iso = ?',    $idioma_iso)
                ->order('id ASC');

        $executa = $db->query($select);
        $dados = $executa->fetchAll();
        return $dados;
        }
        
        public function existemCaracteristicasTiposConteudos($tipo_id) {
            $select = $this->select()->where('tipo_id  = ?', $tipo_id);
            return $this->fetchAll($select)->count();
        }
    
        public function getCaracteristicasTiposConteudos($tipo_id) {
            $select = $this->select()->where('tipo_id  = ?', $tipo_id);
            return $this->fetchAll($select);
        }

        public function idCaracteristicaTiposConteudosByTipoReferencia($tipo_id, $referencia) {
            $select = $this->select()->where('tipo_id  = ?', $tipo_id)->where('referencia  = ?', $referencia);
            $registo = $this->fetchRow($select);
            
            if($registo!=NULL){
                return $registo->id;
            }else{
                return NULL;
            }
        }
        
	public function addCaracteristicaTipoConteudo($tipo_id, $designacao, $referencia) {
            $data = array(
                'tipo_id' => $tipo_id,
                'designacao' => $designacao,
                'referencia' => $referencia,
            );
            $this->insert($data);
        }

	function updateCaracteristicaTipoConteudo($id, $tipo_id, $designacao, $referencia) {
            $data = array(
                    'tipo_id' => $tipo_id,
                    'designacao' => $designacao,
                    'referencia' => $referencia,
            );
            $this->update($data, 'id = ' . (int) $id);
        }

	function deleteCaracteristicaTipoConteudo($id) {
            $this->delete('id =' . (int) $id);
        }
        
        function deleteCaracteristicaTipoConteudoByTipoID($tipo_id) {
            $this->delete('tipo_id =' . (int) $tipo_id);
        }

}
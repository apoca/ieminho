<?php

class Model_DbTable_Configuracoes extends Zend_Db_Table_Abstract
{
    protected $_name    = 'configuracoes';
    protected $_primary = 'id';
	
    public function getConfiguracao($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar as configurações com o id: $id");
        }
        return $row->toArray();    
    }
    
    public function getConfiguracaoWebsiteFO($id)
    {
        $select = $this->select()->where('id = ?', $id);
        $dados = $this->fetchRow($select);
        return $dados;    
    }
    
    public function optionValue($option_name){
        
       $select = $this->select()->where('option_name = ?', $option_name);
       $dados = $this->fetchRow($select);
       return $dados->option_value;
    }
    
    public function updateOptionValues($option_name, $option_value){
        $data = array('option_value' => $option_value);
        $this->update($data, 'option_name = ' . '"' . $option_name . '"');
    }
    
    
        
    function updateConfiguracoes($id, $description, $keywords,$telefone,$fax, $email, $twitter, $facebook, $dominio, $robots, $utilizador_id)
    {
        $data = array(
            'description'       => $description,
            'keywords' 		=> $keywords,
            'telefone' 		=> $telefone,
            'fax' 		=> $fax,
            'email' 		=> $email,
            'twitter' 		=> $twitter,
            'facebook'          => $facebook,
            'dominio'           => $dominio,
            'robots'            => $robots,
            'utilizador_id'     => $utilizador_id,
        );
        $this->update($data, $this->_primary.' = '. (int)$id);
    }
    
    function deleteConfiguracoes($id)
    {
        $this->delete($this->_primary.' = '. (int)$id);
    }
}
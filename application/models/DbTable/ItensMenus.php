<?php

class Model_DbTable_ItensMenus extends Zend_Db_Table_Abstract {

	protected $_name = 'itens_menus';
        protected $_primary = 'id';

	public function getItemMenu($id) {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if (!$row) {
                throw new Exception("Não é possível encontrar a registo com o id: $id");
            }
            return $row->toArray();
        }
        
        public function designacaoById($id) {
            $select = $this->select()->where('id = ?', $id);
            $query = $this->fetchRow($select);
            return $query->designacao;
        }
    
        public function getItensMenus() {
            $select = $this->select()->fetchAll();
            return $select;
        }

	public function addItemMenu($designacao) {
            $data = array(
                            'designacao' => $designacao,
            );
            $this->insert($data);
        }

	function updateItemMenu($id, $designacao) {
            $data = array(
                            'designacao' => $designacao,
            );
            $this->update($data, 'id = ' . (int) $id);
        }

	function deleteItemMenu($id) {
            $this->delete('id =' . (int) $id);
        }

}
<?php

class Model_DbTable_ConfiguracoesIdioma extends Zend_Db_Table_Abstract
{
    protected $_name    = 'configuracoesidioma';
    protected $_primary = 'id';
	
    public function getConfiguracaoIdioma($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar as configurações com o id: $id");
        }
        return $row->toArray();    
    }
        
    public function optionValueIdioma($option_name, $idioma_iso){
        
       $select = $this->select()->where('option_name = ?', $option_name)
                                ->where('idioma_iso = ?', $idioma_iso);
       $dados = $this->fetchRow($select);
       return $dados->option_value;
    }
    
    public function updateOptionValuesIdioma($option_name, $option_value, $idioma_iso){
        $data = array('option_value' => $option_value);
        
        $condicao = array(
            'option_name = ?'=> $option_name,
            'idioma_iso = ?' => $idioma_iso
        );
       // $condicao = "option_name = ".$option_name." AND idioma_iso = " .$idioma_iso;
        
        $this->update($data, $condicao);
    }
    
    
        
    function updateConfiguracoesIdioma($id, $description, $keywords,$telefone,$fax, $email, $twitter, $facebook, $dominio, $robots, $utilizador_id)
    {
        $data = array(
            'description'       => $description,
            'keywords' 		=> $keywords,
            'telefone' 		=> $telefone,
            'fax' 		=> $fax,
            'email' 		=> $email,
            'twitter' 		=> $twitter,
            'facebook'          => $facebook,
            'dominio'           => $dominio,
            'robots'            => $robots,
            'utilizador_id'     => $utilizador_id,
        );
        $this->update($data, 'id  = '. (int)$id);
    }
    
    function deleteConfiguracoesIdioma($id)
    {
        $this->delete('id  = '. (int)$id);
    }
}
<?php

class Model_DbTable_AtributosTiposConteudos extends Zend_Db_Table_Abstract {

    protected $_name = 'atributos_tiposconteudos';
    protected $_primary = 'id';

    public function getAttrTipoConteudo($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar a registo com o id: $id");
        }
        return $row->toArray();
    }

    public function getAtributosTiposConteudos($caracteristica_id) {
        $select = $this->select()->where('caracteristica_id  = ?', $caracteristica_id);
        return $this->fetchAll($select);
    }
    
    public function valoresCampo($caracteristica_id) {
        $select = $this->select()->where('caracteristica_id  = ?', $caracteristica_id);
        $registo = $this->fetchRow($select);
        
        //verificar se replicarPorIdiomas == 'N' e fieldtype == combobox    $registo->replicarPorIdiomas == 'N' && 
        if($registo->fieldtype == 'combobox'){
            return $registo;
        }else{
            return NULL;
        }
        
    }
    
    public function nameAtributoTipoConteudo($name) {
        $select = $this->select()->where('name  = ?', $name);
        $existe = $this->fetchAll($select)->count();
        
        return ($existe > 0) ? $name.rand(10, 99) : $name;
    }

    public function getIDbyCaracteristicaID($caracteristica_id) {
        $select = $this->select()->where('caracteristica_id = ?', $caracteristica_id);
        $query = $this->fetchRow($select);
        return $query->id;
    }

    public function getAtributoTipoConteudo($caracteristica_id) {
        $select = $this->select()->where('caracteristica_id  = ?', $caracteristica_id);
        $query = $this->fetchRow($select);

        //$idiomas = ($query->replicarPorIdiomas == 'N' ? 'Único' : 'Replicar pelos idiomas');

        return '<b>' . ucfirst($query->fieldtype) . '</b> ';
    }

    public function addAtributoTipoConteudo($caracteristica_id, $labelSingular, $labelPlural, $name, $fieldtype, $valoresCampo, $restricoes) {
        $data = array(
            'caracteristica_id' => $caracteristica_id,
            'labelSingular' => $labelSingular,
            'labelPlural' => $labelPlural,
            'name' => $name,
            'fieldtype' => $fieldtype,
            'valoresCampo' => $valoresCampo,
            'restricoes' => $restricoes,
        );
        return $this->insert($data);
    }

    function updateAtributoTipoConteudo($caracteristica_id, $labelSingular, $labelPlural, $name, $fieldtype, $valoresCampo, $restricoes) {
        $data = array(
            'labelSingular' => $labelSingular,
            'labelPlural' => $labelPlural,
            'name' => $name,
            'fieldtype' => $fieldtype,
            'valoresCampo' => $valoresCampo,
            'restricoes' => $restricoes,
        );
        $this->update($data, 'caracteristica_id = ' . (int) $caracteristica_id);
    }

    function deleteAtributoTipoConteudoByCaracteristicaID($caracteristica_id) {
        $this->delete('caracteristica_id =' . (int) $caracteristica_id);
    }
    

    function deleteAtributoTipoConteudo($id) {
        $this->delete('id =' . (int) $id);
    }

    /* public function getConteudosTiposConteudos() {
      $select = $this->select()->fetchAll();
      return $select;
      } */

    /**
     * 
     * @param type $name
     * @return type
     */
    public function getAtributoTipoConteudoByName($name = 'categoria') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('atc' => $this->_name))
                ->join(
                        array('ctc' => 'caracteristicas_tiposconteudos'), 'ctc.id = atc.caracteristica_id', array('tipo_id' => 'ctc.tipo_id')
                )
                ->where('atc.name = ?', $name)
                ->where('atc.fieldtype = ?', 'combobox');
        $result = $db->query($select)->fetchObject();
        return $result;
    }
    
    public function getAtributoTipoConteudoByNewsletter() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('atc' => $this->_name))
                ->join(
                        array('ctc' => 'caracteristicas_tiposconteudos'), 'ctc.id = atc.caracteristica_id', array('tipo_id' => 'ctc.tipo_id')
                )
                ->join(
                        array('tc' => 'tiposconteudos'), 'tc.id = ctc.tipo_id', array('tipo_conteudo_id' => 'tc.id')
                )
                ->where('tc.newsletter = ?', 'S')
                ->where('atc.fieldtype = ?', 'combobox');
        $result = $db->query($select)->fetchAll();
        return $result;
    }

}
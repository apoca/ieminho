<?php

class Model_DbTable_TiposConteudos extends Zend_Db_Table_Abstract {

	protected $_name = 'tiposconteudos';
        protected $_primary = 'id';

	public function getTipoConteudo($id) {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if (!$row) {
                throw new Exception("Não é possível encontrar a registo com o id: $id");
            }
            return $row->toArray();
        }
        
        public function fieldValue($id, $fieldName) {

            $select = $this->select()->where('id = ?', $id);
            $dados = $this->fetchRow($select);
            return $dados->$fieldName;
        }
        
        public function getTiposConteudos() {
            $select = $this->select()->fetchAll();
            return $select;
        }
        
        public function tipoConteudoIdioma($main_id, $idioma_iso){
            return $this->fetchRow($this->select()->where('main_id = ?', $main_id)->where('idioma_iso = ?', $idioma_iso));
        }
        
        public function designacaoTipoConteudo($id)
	{
            $select = $this->select()->where('id = ?', $id);
            $query = $this->fetchRow($select);
            return $query->designacao;
	}

	public function addTipoConteudo($designacao, $temImagemDestaque, $dimensoesImagemDestaque, $temDescricao, $temLerMais, $podeApagar, $podeApagarConteudos, $podeSerReferencia, $newsletter, $main_id=0, $idioma_iso='pt', $estado='I') {
            $data = array(
                    'designacao' => $designacao,
                    'temImagemDestaque' => $temImagemDestaque,
                    'dimensoesImagemDestaque' => $dimensoesImagemDestaque,
                    'temDescricao' => $temDescricao,
                    'temLerMais' => $temLerMais,
                    'podeApagar' => $podeApagar,
                    'podeApagarConteudos' => $podeApagarConteudos,
                    'podeSerReferencia' => $podeSerReferencia,
                    'newsletter' => $newsletter,
                    'main_id' => $main_id,
                    'idioma_iso'=> $idioma_iso,
                    'estado' => $estado
            );
            return $this->insert($data);
        }
        
        function updateOpcao($id, $campo, $valor) {
            $data = array(
                   $campo => $valor,
            );
            return $this->update($data, 'id = ' . (int) $id);
        }

	function updateTipoConteudo($id, $designacao) {
            $data = array(
                          'designacao' => $designacao,
            );
            $this->update($data, 'id = ' . (int) $id);
        }

	function deleteTipoConteudo($id) {
            $select = $this->select()->where('id = ?', $id);
            $query = $this->fetchRow($select);
          
            if($query->podeApagar == 'S'){
                $caractTiposConteudos = new Model_DbTable_CaracteristicasTiposConteudos();
                $CTC = $caractTiposConteudos->fetchRow($caractTiposConteudos->select()->where('tipo_id = ?', $id));

                if($CTC->referencia=='html'){
                    $atributosTiposConteudos = new Model_DbTable_AtributosTiposConteudos();
                    $ATC = $atributosTiposConteudos->fetchRow($atributosTiposConteudos->select()->where('caracteristica_id = ?', $CTC->id));

                    $valoresAttReferencia = new Model_DbTable_ValoresAtributosReferencia(); //$CTC->referencia == 'html'
                    $VAR = $valoresAttReferencia->fetchRow($valoresAttReferencia->select()->where('referencia_id = ?', $CTC->referencia)->where('atributo_id = ?', $ATC->id));

                    $valoresAttReferencia->deleteByReferenciaEAtributo($CTC->referencia, $ATC->id);
                    $atributosTiposConteudos->deleteAtributoTipoConteudoByCaracteristicaID($CTC->id);

                    $caractTiposConteudos->deleteCaracteristicaTipoConteudoByTipoID($id);
                    
                    $conteudos = new Model_DbTable_Conteudos();
                    $conteudos->deleteConteudoByTipo($id);
                    $this->delete('id =' . (int) $id);
                }
                else if($CTC->referencia=='conteudo'){
                    $conteudosTiposConteudos = new Model_DbTable_ConteudosTiposConteudos();
                    $contTC = $conteudosTiposConteudos->fetchRow($conteudosTiposConteudos->select()->where('caracteristica_id = ?', $CTC->id));

                    $valoresAttReferencia = new Model_DbTable_ValoresAtributosReferencia(); //$CTC->referencia == 'conteudo'
                    $VAR = $valoresAttReferencia->fetchRow($valoresAttReferencia->select()->where('referencia_id = ?', $CTC->referencia)->where('atributo_id = ?', $contTC->id));

                    $valoresAttReferencia->deleteByReferenciaEAtributo($CTC->referencia, $contTC->id);

                    $conteudosTiposConteudos->deleteConteudoTipoConteudoByCaracteristicaID($CTC->id);
                    $conteudos = new Model_DbTable_Conteudos();
                    $conteudos->deleteConteudoByTipo($id);
                    
                    $this->delete('id =' . (int) $id);
                }
            }
            
        }

}
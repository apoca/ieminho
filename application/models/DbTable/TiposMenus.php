<?php

class Model_DbTable_TiposMenus extends Zend_Db_Table_Abstract {

	protected $_name = 'tipos_menus';
        protected $_primary = 'id';

	public function getTipoMenu($id) {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if (!$row) {
                throw new Exception("Não é possível encontrar a registo com o id: $id");
            }
            return $row->toArray();
        }
        
        public function designacaoById($id) {
            $select = $this->select()->where('id = ?', $id);
            $query = $this->fetchRow($select);
            return $query->designacao;
        }
    
        public function getTiposMenus() {
            $select = $this->select()->fetchAll();
            return $select;
        }

	public function addTipoMenu($designacao) {
            $data = array(
                            'designacao' => $designacao,
            );
            $this->insert($data);
        }

	function updateTipoMenu($id, $designacao) {
            $data = array(
                            'designacao' => $designacao,
            );
            $this->update($data, 'id = ' . (int) $id);
        }

	function deleteTipoMenu($id) {
            $this->delete('id =' . (int) $id);
        }

}
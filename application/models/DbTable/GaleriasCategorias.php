<?php

class Model_DbTable_GaleriasCategorias extends Zend_Db_Table_Abstract{
    
    protected $_name = 'galerias_categorias';
    protected $_primary = 'id';
	
    public function getCategoria($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o registo com o id: $id");
        }
        return $row->toArray();    
    }
    
    public function designacaoById($id) {
        $select = $this->select()->where('id = ?', $id);
        $query = $this->fetchRow($select);
        return $query->designacao;
    }
    
    public function optionValue($id, $field){
        
       $select = $this->select()->where('id = ?', $id);
       $dados = $this->fetchRow($select);
       return $dados->$field;
    }
    
    function updateLigacao($id, $ligacao) {
        $data = array(
            'ligacao' => $ligacao,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    public function addCategoria($designacao, $categoria_pai, $ligacao='imagens', $idioma_iso='pt', $estado='A', $apagar='S' ) {
        $data = array(
            'designacao' => $designacao,
            'ligacao' => $ligacao,
            'categoria_pai' => $categoria_pai,
            'idioma_iso' =>$idioma_iso,
            'estado' =>$estado,
            'apagar' =>$apagar,
            'dataHora' => new Zend_Db_Expr('NOW()')
        );
        
        $this->insert($data);
    }
    
    public function getAllCategoriesByLink($ligacao, $idioma_iso)
    {
        $str = '';
        $categorias =  $this->fetchAll($this->select()->where('ligacao = ?', $ligacao)->where('idioma_iso = ?', $idioma_iso));
        $numSubCat = $categorias->count();
        if($numSubCat>0){
            foreach($categorias as $cat){
                $str .= $cat->id.',';
            }
            return substr($str,0,-1);//removemos da string '$str' a última virgula para executar a query IN
        }
        else{
            return NULL;
        }
    }
    
    function updateDesignacao($id, $designacao) {
        $data = array(
            'designacao' => $designacao,
        );
        $this->update($data, 'id = ' . (int) $id);
    }
    
    public function getQtdPosicoes($idioma_iso) {
        $select = $this->select()->where('idioma_iso = ?', $idioma_iso);
        return $this->fetchAll($select)->count();
    }

    function updateCategoria($id, $designacao, $categoria_pai, $ligacao='imagens', $idioma_iso='pt', $estado='A', $apagar='S') {
        $data = array(
            'designacao' => $designacao,
            'ligacao' => $ligacao,
            'categoria_pai' => $categoria_pai,
            'idioma_iso' =>$idioma_iso,
            'estado' =>$estado,
            'apagar' =>$apagar,
        );
        $this->update($data, 'id = ' . (int) $id);
    }
    
    function deleteCategoria($id) {
        $this->delete('id =' . (int) $id);
    }
}

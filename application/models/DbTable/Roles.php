<?php

class Model_DbTable_Roles extends Zend_Db_Table_Abstract {
    
    protected $_name = 'admin_roles';
    protected $_primary = 'id_role';

    public function getRole($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id_role =' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar os dados com o id: $id");
        }
        return $row->toArray();
    }
    public function designacaoById($id)
    {
        
            $select = $this->select()->where('id_role = ?', $id);
            $query = $this->fetchRow($select);
            return $query->designacao;
    }
    public function descricaoById($id)
    {
            $select = $this->select()->where('id_role = ?', $id);
            $query = $this->fetchRow($select);
            return $query->descricao;
    }
    
    
    public function getRoleById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('r' => $this->_name))
                               ->where('r.id_role = ?', $id);

        $dados = $db->query($select)->fetchObject();
        return $dados;
    }

    public function addRole($designacao, $descricao, $estado) {
        $data = array(
            'designacao' => $designacao,
            'descricao' => $descricao,
            'estado' => $estado,
            
        );
        return $this->insert($data);
    }
    
    

    function updateRole($id, $designacao, $descricao, $estado) {
        $data = array(
            'designacao' => $designacao,
            'descricao' => $descricao,
            'estado' => $estado,
        );
        return $this->update($data, 'id_role =' . (int) $id);
    }
        
    public function deleteRole($id) {
        try {
            $this->delete('id_role =' . (int) $id);
        } catch (Zend_Db_Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function getAllRolesActivas() {
        $select = $this->select()->where('estado  = ?','A')->order('id_role ASC');
        return $this->fetchAll($select);
    }
    
    public function getAllRolesActivasMenosSU() {
        $select = $this->select()->where('estado  = ?','A')->where('id_role != ?', 1)->order('id_role ASC');
        return $this->fetchAll($select);
    }
    
    public function getAllRoles(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('r' => $this->_name))
                               ->order('r.id_role DESC');

        $dados = $db->query($select)->fetchAll();
        return $dados;
    }

    public function getAllRolesLessAdminGuest(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('r' => $this->_name))
                               ->where('r.id_role != ?', 1)
                                ->where('r.id_role != ?', 3)
                               ->order('r.id_role DESC');

        $dados = $db->query($select)->fetchAll();
        return $dados;
    }
    /* obter todos */
    public function obterTodos() {
        $select = $this->select();
        return $this->fetchAll($select);
    }

    /*
     * Converter Array em object
     */
    public function convertArrayToObject($array)
    {
      $object = new stdClass();
      if (is_array($array) && count($array) > 0)
      {
        foreach ($array as $name=>$value)
        {
          $name = strtolower(trim($name));
          if (!empty($name))
          {
            $object->$name = $value;
          }
        }
      }
      return $object;
    }

    /*
     * Converter Object em Array
     */
    public function convertObjectToArray($object)
    {
      $array = array();
      if (is_object($object))
      {
        $array = get_object_vars($object);
      }
      return $array;
    }
}

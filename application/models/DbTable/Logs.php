<?php

class Model_DbTable_Logs extends Zend_Db_Table_Abstract {

    protected $_name = 'admin_logs';
    protected $_primary = 'id';
    

    public function getLog($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar os dados com o id: $id");
        }
        return $row->toArray();
    }

    public function obterTodosLogs() {
        $select = $this->select()->order('id DESC');
        return $this->fetchAll($select);
    }

    public function addLog($mensagem) {
        $funcoes = new App_Class_Funcoes();
        $data = array(
            'hora' => date('Y-m-d H:i:s'),
            'ip' => $funcoes->getIP(),
            'mensagem' => $mensagem,
        );
        $inserir = $this->insert($data);
        if (!$inserir) {
            echo ("Não foi possível adicionar!!!");
            return;
        }
    }

    function deleteLog($id) {
        $eliminar = $this->delete('id =' . (int) $id);
        if (!$eliminar) {
            echo("Não foi possível eliminar!!!");
            return;
        }
    }
    
    function deleteLogs() {
        $eliminar = $this->delete();
        if (!$eliminar) {
            echo("Não foi possível eliminar!!!");
            return;
        }
    }

}

<?php

class Model_DbTable_AdminUtilizadores extends Zend_Db_Table_Abstract
{
    protected $_name = 'admin_utilizadores';
    protected $_primary = 'id';

    public function getUserById($id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('u' => $this->_name))
                               ->join(array('r' => 'admin_roles'),
                                'u.role_id = r.id_role', array('designacao as role'))
                                ->where('u.id = ?', $id);

        $dados = $db->query($select)->fetchObject();
        return $dados;
    }
    
    
    public function getUtilizador($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o conteúdo com o id: $id");
        }
        return $row->toArray();    
    }

    public function utilizadorByid($id)
    {
        $select = $this->select()->where('id = ?', $id);
        $usr = $this->fetchRow($select);
        return $usr->nome .' '.$usr->apelido;
    }
    
    public function addUtilizador($nome, $apelido, $login, $password,$email,$foto,$role_id,$estado,$ultimoIP)
    {
        $data = array(
            'nome' 			=> $nome,
            'apelido' 			=> $apelido,
            'login' 			=> $login,
            'password'                  => $password,
            'email'                     => $email,
            'foto'                      => $foto,
            'role_id'                   => $role_id,
            'estado'                    => $estado,
            'ultimoIP'                  => $ultimoIP,
        );
        return $this->insert($data);
    }

    
    function updateUtilizador($id, $nome, $apelido, $login, $password,$email,$foto,$role_id,$estado,$ultimoIP)
    {
        $data = array(
            'nome' 			=> $nome,
            'apelido' 			=> $apelido,
            'login' 			=> $login,
            'password'                  => $password,
            'email'                     => $email,
            'foto'                      => $foto,
            'role_id'                   => $role_id,
            'estado'                    => $estado,
            'ultimoIP'                  => $ultimoIP,
        );
        return $this->update($data, 'id = '. (int)$id);
        
    }
    
    public function howManyByRole($role_id) {
        $select = $this->select()->where('role_id  = ?', $role_id);
        return $this->fetchAll($select)->count();
    }
    
    function autenticacao($login, $password){
        $utilizador        = $this->fetchRow($this->select()->where('login = ?', $login)->where('password = ?', $password));
       return $utilizador->id;        
    }
    
    public function existeEmailUtilizador($email) {
        $select = $this->select()->where('email  = ?', $email);
        return $this->fetchAll($select)->count();
    }
    
    public function existeLoginUtilizador($login) {
        $select = $this->select()->where('login  = ?', $login);
        return $this->fetchAll($select)->count();
    }
	
    public function getQtdPosicoes()
    {
        $select = $this->select();
        return $this->fetchAll($select)->count();
    }
	
    function updatePosicao($id, $posicao){
        $data = array(
            'posicao' => $posicao,
        );
        $this->update($data, 'id = '. (int)$id);
    }

    function alterarUltimoIP($id, $ultimoIP)
    {
        $data = array(
            'ultimoIP' => $ultimoIP,
        );
        $this->update($data, 'id = '. $id);
    }
    
    function alterarUltimaDataHoraAcesso($id, $datahora){
        $data = array(
            'datahora' => $datahora,
        );
        $this->update($data, 'id = '. $id);
    }
	
    function alterarEstado($id, $estado){
        $data = array(
            'estado' => $estado,
        );
        $this->update($data, 'id = '. $id);
    }
    
    function deleteUtilizador($id){
        $this->delete('id =' . (int)$id);
    }
}
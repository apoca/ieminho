<?php

class Model_DbTable_ConteudosMultipleFiles extends Zend_Db_Table_Abstract {

    protected $_name = 'conteudos_multiplefiles';
    protected $_primary = 'id';
    
    public function addFileConteudo($ficheiro, $token, $posicao, $conteudo_id, $extensao='') {
        $data = array(
            'ficheiro' => $ficheiro,
            'token' => $token,
            'conteudo_id' => $conteudo_id,
            'posicao' => $posicao,
            'extensao' => $extensao
        );
        return $this->insert($data);
    }
    
    public function optionValue($id, $field){
        
       $select = $this->select()->where('id = ?', $id);
       $dados = $this->fetchRow($select);
       return $dados->$field;
    }

    public function getQtdPosicoes($conteudo_id) {
        $select = $this->select()->where('conteudo_id = ?', $conteudo_id);
        return $this->fetchAll($select)->count();
    }
    
    public function getAllFilesContentID($conteudo_id) {
        return $this->fetchAll($this->select()->where('conteudo_id = ?', $conteudo_id)->order('posicao ASC'));
    }
    
    function updateTokenToConteudoId($token, $conteudo_id) {

        $data = array(
            'conteudo_id' => $conteudo_id,
        );
        
        $condicao = array(
            'token = ?' => $token,
            'conteudo_id = ?' => '0'
        );

        $this->update($data, $condicao);
    }
    
    public function getQtdPosicoesToken($token) {
        $select = $this->select()->where('token = ?', $token);
        return $this->fetchAll($select)->count();
    }
    
    public function ficheiroConteudoByToken($ficheiro, $token){
        return $this->fetchRow($this->select()->where('ficheiro = ?', $ficheiro)->where('token = ?', $token));
    }

    function updatePosicao($id, $posicao) {
        $data = array(
            'posicao' => $posicao,
        );
        $this->update($data, 'id = ' . (int) $id);
    }
    
    function deleteFileConteudo($id) {
        return $this->delete('id =' . (int) $id);
    }

}
<?php

class Model_DbTable_Privilegios extends Zend_Db_Table_Abstract {
    protected $_name = 'admin_privilegios';
    protected $_primary = 'id_privilegio';

    
    /*
     *  Retorna os dados por ID
     */
    public function getPrivilegio($id) {
        
        $row = $this->fetchRow('id_privilegio = ' . (int) $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar os dados com o id: $id");
        }
        return $row->toArray();
    }
    
    public function addPrivilegio($acao, $descricao, $controladorNome, $moduloNome = 'management', $estado = 'A') {
        $data = array(
            'acao' => $acao,
            'descricao' => $descricao,
            'moduloNome' => $moduloNome,
            'controladorNome' => $controladorNome,
            'estado' => $estado
        );
        return $this->insert($data);
    }
    
    /*
     * Retorna todos 
     */
    public function getAllPrivilegios()
    {
        $select = $this->select();
        return $this->fetchAll();
    }
    
    public function existePrivilegio($controladorNome, $acao) {
        $select = $this->select()->where('controladorNome  = ?', $controladorNome)->where('acao = ?', $acao);
        $existe = $this->fetchAll($select)->count();
        
        return ($existe > 0) ? TRUE : FALSE;
    }
    
    
}

<?php

class Model_DbTable_ValoresAtributosReferencia extends Zend_Db_Table_Abstract {

    protected $_name = 'valores_atributos_referencia';
    protected $_primary = 'id';

    public function getValorAtributoReferencia($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar a registo com o id: $id");
        }
        return $row->toArray();
    }

    public function getValoresAtributosReferencia($atributo_id) {
        $select = $this->select()->where('atributo_id  = ?', $atributo_id);
        return $this->fetchAll($select);
    }

    public function existeValorConteudoEmUso($conteudo_id) {
        $select = $this->select()->where('referencia_id  = ?', 'conteudo')
                ->where('conteudo_id  = ?', $conteudo_id);
        return $this->fetchAll($select)->count();
    }

    public function addValorAtributoReferencia($referencia_id, $atributo_id, $conteudo_id, $valor) {
        $data = array(
            'referencia_id' => $referencia_id,
            'atributo_id' => $atributo_id,
            'conteudo_id' => $conteudo_id,
            'valor' => $valor,
        );
        $this->insert($data);
    }

    function updateValorAtributoReferencia($atributo_id, $conteudo_id, $valor) {

        $data = array(
            'valor' => $valor,
        );
        $condicao = "atributo_id = '" . $atributo_id . "' AND conteudo_id = '" . $conteudo_id . "'";

        $this->update($data, $condicao);
    }

    function existeValorAtributoConteudo($atributo_id, $conteudo_id) {
        $select = $this->select()->where('atributo_id  = ?', $atributo_id)
                ->where('conteudo_id  = ?', $conteudo_id);
        return $this->fetchAll($select)->count();
    }
    
    public function valueByAtributoIDConteudoID($atributo_id, $conteudo_id){
       $dados = $this->fetchRow($this->select()->where('atributo_id  = ?', $atributo_id)->where('conteudo_id  = ?', $conteudo_id));
       return $dados->valor;
    }

    function deleteValorAtributoRefByAttrID($atributo_id) {
        $this->delete('atributo_id =' . (int) $atributo_id);
    }

    function deleteValoresAtributosRefByConteudoID($conteudo_id) {
        $this->delete('conteudo_id =' . (int) $conteudo_id);
    }

    function deleteValorAtributoReferencia($id) {
        $this->delete('id =' . (int) $id);
    }
    
    public function deleteByReferenciaEAtributo($referencia_id, $atributo_id){
        $condicao = array(
            'referencia_id = ?'=> $referencia_id,
            'atributo_id = ?' => $atributo_id
        );
        
        $this->delete($condicao);
    }
    /* public function getConteudosTiposConteudos() {
      $select = $this->select()->fetchAll();
      return $select;
      } */

    public function getAtributsContents($valor, $referencia_id, $limite) {
        $select = $this->select()->where('valor = ?', $valor)
                ->where('referencia_id = ?', $referencia_id)
                ->order('id DESC')
                ->limit($limite);
        return $this->fetchAll($select);
    }

    public function getAtributContent($valor, $referencia_id) {
        $select = $this->select()->where('valor = ?', $valor)
                ->where('referencia_id = ?', $referencia_id)
                ->order('id DESC');
        return $this->fetchRow($select);
    }

    public function getAtributContents($valor, $referencia_id) {
        $select = $this->select()->where('valor = ?', $valor)
                ->where('referencia_id = ?', $referencia_id)
                ->order('id DESC');
        return $this->fetchAll($select);
    }

    public function getValorAtributoConteudo($conteudo_id) {
        $select = $this->select()->where('conteudo_id = ?', $conteudo_id)
                ->limit(1);
        return $this->fetchAll($select);
    }

    public function getConteudoByValor($valor, $referencia = 'html') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('var' => $this->_name))
                ->join(
                        array('c' => 'conteudos'), 'c.id = var.conteudo_id', array('conteudo_id' => 'c.id', 'titulo' => 'c.designacao')
                )
                ->where('var.valor = ?', $valor)
                ->where('var.referencia_id = ?', $referencia)
                ->order('var.id DESC');
        $result = $db->query($select)->fetchAll();
        return $result;
    }

}
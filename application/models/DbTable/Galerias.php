<?php

class Model_DbTable_Galerias extends Zend_Db_Table_Abstract {

    protected $_name = 'galerias';
    protected $_primary = 'id';
    

    public function getGaleria($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o registo com o id: $id");
        }
        return $row->toArray();
    }

    public function addGaleriaFile($ficheiro, $descricao, $destaque, $posicao, $categoria_id, $publicado, $extensao) {
        $data = array(
            'ficheiro' => $ficheiro,
            'descricao' => $descricao,
            'destaque' => $destaque,
            'posicao' => $posicao,
            'categoria_id' => $categoria_id,
            'publicado' => $publicado,
            'extensao' => $extensao,
            'dataHora' => new Zend_Db_Expr('NOW()')
        );
        return $this->insert($data);
    }

    function updateGaleriaFile($id, $ficheiro, $descricao, $destaque, $posicao, $categoria_id, $publicado) {
        $data = array(
            'ficheiro' => $ficheiro,
            'descricao' => $descricao,
            'destaque' => $destaque,
            'posicao' => $posicao,
            'categoria_id' => $categoria_id,
            'publicado' => $publicado,
        );
        $this->update($data, 'id = ' . (int) $id);
    }
    
    public function optionValue($id, $field){
        
       $select = $this->select()->where('id = ?', $id);
       $dados = $this->fetchRow($select);
       return $dados->$field;
    }

    public function getQtdPosicoes($categoria_id) {
        $select = $this->select()->where('categoria_id = ?', $categoria_id);
        return $this->fetchAll($select)->count();
    }
    public function getImages($categoria_id) {
        $select = $this->select()->where('categoria_id = ?', $categoria_id);
        return $this->fetchAll($select);
    }
    function updatePosicao($id, $posicao) {
        $data = array(
            'posicao' => $posicao,
        );
        $this->update($data, 'id = ' . (int) $id);
    }
    
    function updateDescricao($id, $descricao) {
        $data = array(
            'descricao' => $descricao,
        );
        $this->update($data, 'id = ' . (int) $id);
    }
    
    function updateCategoria($id, $categoria_id) {
        $data = array(
            'categoria_id' => $categoria_id,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    function updateDestaque($id, $destaque) {
        $data = array(
            'destaque' => $destaque,
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    function deleteGaleria($id) {
        return $this->delete('id =' . (int) $id);
    }

}
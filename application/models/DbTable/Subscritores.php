<?php

class Model_DbTable_Subscritores extends Zend_Db_Table
{
    protected $_name = 'subscritores';
	
    function jaExiste($email, $idioma_iso)
    {
      $select = $this->_db->select()
                     ->from($this->_name,array('email', 'idioma_iso'))
                     ->where('email=?',$email)
                     ->where('idioma_iso=?',$idioma_iso);
      $result = $this->getAdapter()->fetchOne($select);
      if($result)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
	
    public function getSubscritor($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Lamentamos, mas não é possível encontrar o subscritor com o id: $id");
        }
        return $row->toArray();    
    }

    public function getSubscritoresIdioma($idioma_iso)
    {
      $select = $this->select()->where('idioma_iso  = ?',$idioma_iso);
      return $this->fetchAll($select);
    }
    
    public function addSubscritor($email, $dataSubscricao, $idioma_iso, $hash, $estado)
    {
        $data = array(
            'email' 		=> $email,
            'dataSubscricao'    => $dataSubscricao,
            'idioma_iso'        => $idioma_iso,
            'hash'              => $hash,
            'estado' 		=> $estado,
        );
        $this->insert($data);
    }
	
	
    function updateSubscritor($id, $email, $dataSubscricao, $idioma_iso, $hash, $estado)
    {
        $data = array(
            'email' 		=> $email,
            'dataSubscricao'    => $dataSubscricao,
            'idioma_iso'        => $idioma_iso,
            'hash'              => $hash,
            'estado' 		=> $estado,
        );
        $this->update($data, 'id = '. $id);
    }
	
    function alterarEstadoSubscritor($email, $estado)
    {
        $data = array(
            'estado' => $estado,
        );
        $this->update($data, 'email = '. $email);
    }
	
    function alterarEstado($id, $estado)
    {
        $data = array(
            'estado' => $estado,
        );
        $this->update($data, 'id = '. $id);
    }
    public function getQtdPosicoes($idioma_iso)
    {
            $select = $this->select()->where('idioma_iso  = ?',$idioma_iso);
            return $this->fetchAll($select)->count();
    }

    public function getQtdSubscritores($idioma_iso)
    {
            $select = $this->select()->where('idioma_iso  = ?',$idioma_iso)->where('estado  = ?','A');
            return $this->fetchAll($select)->count();
    }
    
    
    function deleteSubscritor($id)
    {
        $this->delete('id =' . (int)$id);
    }
}

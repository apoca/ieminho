<?php

class Model_DbTable_Recursos extends Zend_Db_Table_Abstract {

    protected $_name = 'admin_recursos';
    protected $_primary = 'id_recurso';

    /*
     * Retornar todos os recursos do sistema
     */

    public function getAllRecursos() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from(array('r' => $this->_name))
                ->order('moduloNome ASC');

        $dados = $db->query($select)->fetchAll();
        return $dados;
    }

    public function addRecurso($descricao, $controladorNome, $moduloNome = 'management', $estado = 'A', $parent_id = NULL) {
        $data = array(
            'descricao' => $descricao,
            'moduloNome' => $moduloNome,
            'controladorNome' => $controladorNome,
            'estado' => $estado,
            'parent_id' => $parent_id
        );
        return $this->insert($data);
    }

    /*
     * obter todos os recursos Activos
     */

    public function getAllRecursosActivos() {
        $select = $this->select()->where('estado  = ?', 'A')->order('descricao ASC');
        return $this->fetchAll($select);
    }

    /*
     * retorna os dados de um determinado recurso by id
     */

    public function getRecursoByID($id) {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Lamentamos, mas não é possível obter dados com o id: $id");
        }
        return $row->toArray();
    }

    /*
     * retorna os dados de um determinado recurso by nome
     */

    public function getRecursoByDesignacao($controladorNome) {
        $select = $this->select()->where('controladorNome = ?', $controladorNome);
        $row = $this->fetchRow($select);
        if (!$row) {
            throw new Exception("Lamentamos, mas não é possível obter dados com a designacao: $controladorNome");
        }
        return $row->toArray();
    }

    public function fieldValue($id, $fieldName) {

        $select = $this->select()->where('id = ?', $id);
        $dados = $this->fetchRow($select);
        return $dados->$fieldName;
    }

    public function existeController($controladorNome) {
        $select = $this->select()->where('controladorNome  = ?', $controladorNome);
        $existe = $this->fetchAll($select)->count();

        return ($existe > 0) ? TRUE : FALSE;
    }

}

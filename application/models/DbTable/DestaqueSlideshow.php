<?php

class Model_DbTable_DestaqueSlideshow extends Zend_Db_Table_Abstract
{
    protected $_name = 'destaque_slideshow';
    protected $_primary = 'id';
	
    
    public function getBanner($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o registo com o id: $id");
        }
        return $row->toArray();    
    }

    public function getQtdImagens()
    {
        $select = $this->select();
        return $this->fetchAll($select)->count();
    }
    
    public function updateFieldValue($field, $value, $id){
        $data = array($field => $value);
        $where = array(
            'id = ?' => $id 
        );
        $this->update($data, $where);
    }

    function updatePosicao($imagem, $posicao)
    {
        $data = array(
            'posicao' => $posicao,
        );
        $where = array(
            'imagem = ?' => $imagem 
        );
        $this->update($data, $where);
    }
    
    public function addImagemSlideshow($url, $target, $imagem, $posicao, $seccao='homePage', $estado='A')
    {
        $data = array(
            'url'        => $url,
            'target'          => $target,
            'imagem'            => $imagem,
            'posicao' 		=> $posicao,
            'seccao' 		=> $seccao,
            'estado' 		=> $estado,
        );
        return $this->insert($data);
    }
    
    function deleteImagemSlideshow($imagem)
    {
        $where = array(
            'imagem = ?' => $imagem 
        );
        $this->delete($where);
    }
    
    
    function deleteImagemIDSlideshow($id)
    {
        $this->delete('id =' . (int)$id);
    }
    
    public function fieldValue($id, $fieldName){
        
       $select = $this->select()->where('id = ?', $id);
       $dados = $this->fetchRow($select);
       return $dados->$fieldName;
    }
}


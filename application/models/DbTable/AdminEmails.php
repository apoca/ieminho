<?php

class Model_DbTable_AdminEmails extends Zend_Db_Table_Abstract
{
    protected $_name = 'admin_emails';
    protected $_primary = 'id';

    
    
    public function getEmail($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o registo com o id: $id");
        }
        return $row->toArray();    
    }
    
    public function fieldValue($id, $fieldName){
        
       $select = $this->select()->where('id = ?', $id);
       $dados = $this->fetchRow($select);
       return $dados->$fieldName;
    }
    
    public function addEmail($tipo, $seguranca, $servidor, $email,$port,$password)
    {
        $data = array(
            'tipo' 			=> $tipo,
            'seguranca' 		=> $seguranca,
            'servidor' 			=> $servidor,
            'email'                     => $email,
            'port'                      => $port,
            'password'                  => $password,
        );
        $this->insert($data);
    }

    
    function updateEmail($id, $tipo, $seguranca, $servidor, $email,$port,$password)
    {
        $data = array(
            'tipo' 			=> $tipo,
            'seguranca' 		=> $seguranca,
            'servidor' 			=> $servidor,
            'email'                     => $email,
            'port'                      => $port,
            'password'                  => $password,
        );
        return $this->update($data, 'id = '. (int)$id); 
        
    }
    
    function deleteUtilizador($id)
    {
        $this->delete('id =' . (int)$id);
    }
}
<?php

class Model_DbTable_Conteudos extends Zend_Db_Table_Abstract
{
    protected $_name = 'conteudos';
    protected $_primary = 'id';
	
    public function getConteudo($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o registo com o id: $id");
        }
        return $row->toArray();    
    }
    
    public function fieldValue($id, $fieldName){
        
       $select = $this->select()->where('id = ?', $id);
       $dados = $this->fetchRow($select);
       return $dados->$fieldName;
    }
    
    public function conteudoIdioma($main_id, $idioma_iso){
       return $this->fetchRow($this->select()->where('main_id = ?', $main_id)->where('idioma_iso = ?', $idioma_iso));
    }

    public function getConteudos()
    {
      $select = $this->select()->order('posicao ASC');
      return $this->fetchAll($select);
    }
    
    public function addConteudo($designacao, $descricao, $foto, $posicao, $estado, $main_id, $idioma_iso, $tipo_id, $dataCriacao, $inicioPublicacao, $fimPublicacao)
    {
        $data = array(
            'designacao' 	=> $designacao,
            'descricao'         => $descricao,
            'foto'              => $foto,
            'posicao'           => $posicao,
            'estado'            => $estado,
            'main_id'           => $main_id,
            'idioma_iso'        => $idioma_iso,
            'tipo_id'           => $tipo_id,
            'dataCriacao'       => $dataCriacao,
            'inicioPublicacao'  => $inicioPublicacao,
            'fimPublicacao'      => $fimPublicacao,
        );
        return $this->insert($data);
        
    }

    function updateConteudo($id, $designacao, $descricao, $foto, $posicao, $estado, $main_id, $idioma_iso, $tipo_id, $dataCriacao, $inicioPublicacao, $fimPublicacao)
    {
        $data = array(
            'designacao' 	=> $designacao,
            'descricao'         => $descricao,
            'foto'              => $foto,
            'posicao'           => $posicao,
            'estado'            => $estado,
            'main_id'           => $main_id,
            'idioma_iso'        => $idioma_iso,
            'tipo_id'           => $tipo_id,
            'dataCriacao'       => $dataCriacao,
            'inicioPublicacao'  => $inicioPublicacao,
            'fimPublicacao'      => $fimPublicacao,
        );
        return $this->update($data, 'id = '. (int)$id);
        
    }

    public function getQtdPosicoes()
    {
            $select = $this->select();
            return $this->fetchAll($select)->count();
    }
    public function getQtdPosicoesIdiomaTipo($tipo_id, $idioma_iso)
    {
            $select = $this->select()->where('tipo_id  = ?', $tipo_id)->where('idioma_iso  = ?', $idioma_iso);
            return $this->fetchAll($select)->count();
    }

    function updateOpcao($id, $campo, $valor) {
        $data = array(
                $campo => $valor,
        );
        return $this->update($data, 'id = ' . (int) $id);
    }
        
    function updatePosicao($id, $posicao)
    {
        $data = array(
            'posicao' => $posicao,
        );
        $this->update($data, 'id = '. (int)$id);
    }
    function deleteConteudoByTipo($tipo_id) {
        $tiposConteudos = new Model_DbTable_TiposConteudos();
        $podeApagarConteudos = $tiposConteudos->fieldValue($tipo_id, 'podeApagarConteudos');
        if($podeApagarConteudos == 'S'){
            $this->delete('tipo_id =' . (int) $tipo_id);
        }
    }
        
    function deleteConteudo($id)
    {
        $this->delete('id =' . (int)$id);
    }
    
    function arrayToObject($array) {
        // First we convert the array to a json string
        $json = json_encode($array);

        // The we convert the json string to a stdClass()
        $object = json_decode($json);

        return $object;
    }

    /**
     * Convert a object to an array
     * 
     * @param   object  $object The object we want to convert
     * 
     * @return  array
     */
    function objectToArray($object) {
        // First we convert the object into a json string
        $json = json_encode($object);

        // Then we convert the json string to an array
        $array = json_decode($json, true);

        return $array;
    }
    
    function getAllTipo($tipo){
        $select = $this->select()
                        ->where('tipo_id  = ?', $tipo);
        return $this->fetchAll($select);
    }
}


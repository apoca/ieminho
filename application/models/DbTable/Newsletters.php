<?php

class Model_DbTable_Newsletters extends Zend_Db_Table_Abstract
{
    protected $_name = 'newsletters';
    protected $_primary = 'id';
	
    public function getNewsletter($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Não é possível encontrar o Conteudo com o id: $id");
        }
        return $row->toArray();    
    }

    public function assuntoNewsletterById($id)
    {
            $select = $this->select()->where('id = ?', $id);
            $n = $this->fetchRow($select);
            return $n->assunto;
    }
    
    
    
    public function addNewsletter($assunto, $mensagem, $estado, $idioma_iso, $utilizador_id)
    {
        $data = array(
            'assunto'           => $assunto,
            'mensagem'          => $mensagem,
            'estado'            => $estado,
            'idioma_iso'        => $idioma_iso,
            'utilizador_id'     => $utilizador_id,
        );
        $this->insert($data);
    }

    function updateNewsletter($id, $assunto, $mensagem, $estado, $idioma_iso, $utilizador_id)
    {
        $data = array(
            'assunto'           => $assunto,
            'mensagem'          => $mensagem,
            'estado'            => $estado,
            'idioma_iso'        => $idioma_iso,
            'utilizador_id'     => $utilizador_id,
        );
        $this->update($data, 'id = '. (int)$id);
    }

    public function getNewslettersIdioma($idioma_iso)
    {
      $select = $this->select()->where('idioma_iso  = ?',$idioma_iso);
      return $this->fetchAll($select);
    }

    public function getQtdPosicoes($idioma_iso)
    {
            $select = $this->select()->where('idioma_iso  = ?',$idioma_iso);
            return $this->fetchAll($select)->count();
    }

    function updateEstado($id, $estado)
    {
        $data = array(
            'estado' => $estado,
        );
        $this->update($data, 'id = '. (int)$id);
    }
    
    function deleteNewsletter($id)
    {
        $this->delete('id =' . (int)$id);
    }
}
<?php

class Model_DbTable_ConteudosTiposConteudos extends Zend_Db_Table_Abstract {

	protected $_name = 'conteudos_tiposconteudo';
        protected $_primary = 'id';

	public function getConteudoTipoConteudo($id) {
            $id = (int) $id;
            $row = $this->fetchRow('id = ' . $id);
            if (!$row) {
                throw new Exception("Não é possível encontrar a registo com o id: $id");
            }
            return $row->toArray();
        }
        
        public function getConteudosTiposConteudos($caracteristica_id) {
            $select = $this->select()->where('caracteristica_id  = ?', $caracteristica_id);
            return $this->fetchAll($select);
        }
        
        public function getIDbyCaracteristicaID($caracteristica_id) {
            $select = $this->select()->where('caracteristica_id = ?', $caracteristica_id);
            $query = $this->fetchRow($select);
            return $query->id;
        }
        
        public function getCaracteristicaTipoConteudo($caracteristica_id) {
            $select = $this->select()->where('caracteristica_id  = ?', $caracteristica_id);
            $query = $this->fetchRow($select);
            
            return '<b>'.$query->labelSingular.'</b> - <i>'.$query->fieldtype.'</i>';
        }
        
        public function nameConteudoTipoConteudo($name) {
            $select = $this->select()->where('name  = ?', $name);
            $existe = $this->fetchAll($select)->count();

            return ($existe > 0) ? $name.rand(10, 99) : $name;
        }

	public function addConteudoTipoConteudo($caracteristica_id, $tipo_id, $labelSingular, $labelPlural, $name, $fieldtype, $condicoes, $restricoes) {
            $data = array(
                'caracteristica_id' => $caracteristica_id,
                'tipo_id' => $tipo_id,
                'labelSingular' => $labelSingular,
                'labelPlural' => $labelPlural,
                'name' => $name,
                'fieldtype' => $fieldtype,
                'condicoes' => $condicoes,
                'restricoes' => $restricoes,
            );
            return $this->insert($data);
        }

	function updateConteudoTipoConteudo($caracteristica_id, $tipo_id, $labelSingular, $labelPlural, $name, $fieldtype, $condicoes, $restricoes) {
            $data = array(
                    'tipo_id' => $tipo_id,
                    'labelSingular' => $labelSingular,
                    'labelPlural' => $labelPlural,
                    'name' => $name,
                    'fieldtype' => $fieldtype,
                    'condicoes' => $condicoes,
                    'restricoes' => $restricoes,
            );
            $this->update($data, 'caracteristica_id = ' . (int) $caracteristica_id);
        }
        
        function deleteConteudoTipoConteudoByCaracteristicaID($caracteristica_id) {
            $this->delete('caracteristica_id =' . (int) $caracteristica_id);
        }

	function deleteConteudoTipoConteudo($id) {
            $this->delete('id =' . (int) $id);
        }
        
        /*public function getConteudosTiposConteudos() {
            $select = $this->select()->fetchAll();
            return $select;
        }*/

}
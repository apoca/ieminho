<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{	
    protected function _initAutoload()
    {
        $moduleLoader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '', 
            'basePath'  => APPLICATION_PATH));
        return $moduleLoader;
        
    }
    
    protected function _initDB() {
        $config = Sbm_Config::getConfig();
        // Registamos os dados e o default adapater no sistema
        if ($config->resources->db) {
            $dbAdapter = Zend_Db::factory($config->resources->db->adapter, $config->resources->db->params);
            Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
            Zend_Registry::set('db', $dbAdapter);
        }
    }
    
    protected function _initView()
    {
        // Initialize view
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');
        //$view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $config = new Zend_Config_Ini('config.ini', 'production');
        
        // Return it, so that it can be stored by the bootstrap
        return $view;
    }
    
    protected function _initLogger()
    {
        $logger = new Zend_Log();

        // A informação do debug irá para o ficheiro abaixo mencionado, caso exista erro
        $stream = ROOT_PATH . DS . '/error_log.txt';
        
        /** ZEND LOG **/
	$log = new Zend_Log_Writer_Stream(ROOT_PATH . DS . '/error_log.txt');
        if (is_writable($stream)) {
            $stdWritter = new Zend_Log_Writer_Stream($stream);
            $stdWritter->addFilter(Zend_Log::DEBUG);
            $stdWritter->addFilter(Zend_Log::INFO);
            $logger->addWriter($stdWritter);
        }else{

            throw new Exception('Erro ao escrever no ficheiro LOG');
        }

        // Create the Firebug writter - Development only
        if ($this->_application->getEnvironment() != APPLICATION_ENV) {
            // Init firebug logging
            $fbWriter = new Zend_Log_Writer_Firebug();
            $logger->addWriter($fbWriter);
        }

        return $logger;
    }
	

    public function _initRouter(array $options = null)
    {
        $ctrl  = Zend_Controller_Front::getInstance();
	$router = $ctrl->getRouter(); // returns a rewrite router by default

       
        
        $router->addRoute(
	  'idiomamodcontrolleraction',
	  new Zend_Controller_Router_Route(':idioma/:module/:controller/:action/*',
	    array('idioma' => ':idioma',
		  'module' => ':module',
		  'controller' => 'index',
		  'action' => 'index'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);
        
         $router->addRoute(
	  'idiomamodcontrolleraction',
	  new Zend_Controller_Router_Route(':idioma/:module/:controller/:action/*',
	    array('idioma' => ':idioma',
		  'module' => 'default',
		  'controller' => 'index',
		  'action' => 'index'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);

/*
        $router->addRoute(
	  'lan_controller_action_etc',
	  new Zend_Controller_Router_Route(':idioma/:module/:controller/:action/*',
	    array('idioma' => ':idioma',
		  'module' => ':module',
		  'controller' => ':controller',
		  'action' => ':action'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);

        $router->addRoute(
	  'lan_controller_action',
	  new Zend_Controller_Router_Route(':idioma/:module/:controller/:action',
	    array('idioma' => ':idioma',
		  'module' => ':module',
		  'controller' => ':controller',
		  'action' => ':action'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);

        $router->addRoute(
	  'controller',
	  new Zend_Controller_Router_Route(':controller',
	    array('idioma' => ':idioma',
		  'module' => 'default',
		  'controller' => 'index',
		  'action' => 'index'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);

        $router->addRoute(
	  'lang_controller',
	  new Zend_Controller_Router_Route(':idioma/:controller',
	    array('idioma' => ':idioma',
		  'module' => 'default',
		  'controller' => ':controller',
		  'action' => 'index'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);

        $router->addRoute(
	  'idiomaindex',
	  new Zend_Controller_Router_Route(':idioma',
	    array('idioma' => ':idioma',
		  'module' => 'default',
		  'controller' => 'index',
		  'action' => 'index'
	    ),
                  array('idioma' => '[a-z]{2}')
	  )
	);


      /*

       //Inicio route noticias/noticia
        $router->addRoute(
	  'noticiasNoticia',
	  new Zend_Controller_Router_Route_Regex(
            'noticias/noticia/(\d+)-(.+)',
	    array(
                    'module'        => 'default',
                    'controller'    => 'noticias',
                    'action'        => 'noticia'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'noticias/noticia/%d-%s'
            )
	);

         $router->addRoute(
	  'noticiasNoticiaPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/noticias/noticia/(\d+)-(.+)',
	    array(
                    'idioma' => 'pt',
                    'module'	=> 'default',
                    'controller'  => 'noticias',
                    'action'	=> 'noticia'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/noticias/noticia/%d-%s'
            )
	);

         $router->addRoute(
	  'noticiasNoticiaEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/noticias/noticia/(\d+)-(.+)',
	    array(
                    'idioma' => 'en',
                    'module'	=> 'default',
                    'controller'  => 'noticias',
                    'action'	=> 'noticia'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/noticias/noticia/%d-%s'
            )
	);
         
         $router->addRoute(
	  'noticiasNoticiaFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/noticias/noticia/(\d+)-(.+)',
	    array(
                    'idioma' => 'fr',
                    'module'	=> 'default',
                    'controller'  => 'noticias',
                    'action'	=> 'noticia'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/noticias/noticia/%d-%s'
            )
	);
        //FIM route noticias/noticia

        //Inicio route casos-estudo/caso-estudo
        $router->addRoute(
	  'casosEstudoCasoEstudo',
	  new Zend_Controller_Router_Route_Regex(
            'casos-estudo/caso-estudo/(\d+)-(.+)',
	    array(
                    'module'        => 'default',
                    'controller'    => 'casos-estudo',
                    'action'        => 'caso-estudo'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'casos-estudo/caso-estudo/%d-%s'
            )
	);

        $router->addRoute(
	  'casosEstudoCasoEstudoPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/casos-estudo/caso-estudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'casos-estudo',
                    'action'        => 'caso-estudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/casos-estudo/caso-estudo/%d-%s'
            )
	);

         $router->addRoute(
	  'casosEstudoCasoEstudoEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/casos-estudo/caso-estudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'casos-estudo',
                    'action'        => 'caso-estudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/casos-estudo/caso-estudo/%d-%s'
            )
	);
         
         $router->addRoute(
	  'casosEstudoCasoEstudoFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/casos-estudo/caso-estudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'fr',
                    'module'        => 'default',
                    'controller'    => 'casos-estudo',
                    'action'        => 'caso-estudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/casos-estudo/caso-estudo/%d-%s'
            )
	);
         //FIM route casos-estudo/caso-estudo

         //Inicio route inovacao-cotesi/caso-estudo
        $router->addRoute(
	  'inovacaoCotesiCasoEstudo',
	  new Zend_Controller_Router_Route_Regex(
            'inovacao-cotesi/caso-estudo/(\d+)-(.+)',
	    array(
                    'module'        => 'default',
                    'controller'    => 'inovacao-cotesi',
                    'action'        => 'caso-estudo'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'inovacao-cotesi/caso-estudo/%d-%s'
            )
	);

        $router->addRoute(
	  'inovacaoCotesiCasoEstudoPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/inovacao-cotesi/caso-estudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'inovacao-cotesi',
                    'action'        => 'caso-estudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/inovacao-cotesi/caso-estudo/%d-%s'
            )
	);

         $router->addRoute(
	  'inovacaoCotesiCasoEstudoEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/inovacao-cotesi/caso-estudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'inovacao-cotesi',
                    'action'        => 'caso-estudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/inovacao-cotesi/caso-estudo/%d-%s'
            )
	);
         
         $router->addRoute(
	  'inovacaoCotesiCasoEstudoFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/inovacao-cotesi/caso-estudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'fr',
                    'module'        => 'default',
                    'controller'    => 'inovacao-cotesi',
                    'action'        => 'caso-estudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/inovacao-cotesi/caso-estudo/%d-%s'
            )
	);
         //FIM route inovacao-cotesi/caso-estudo
         
         
         //Inicio route produtos-destaque/produto
        $router->addRoute(
	  'produtosDestaqueProduto',
	  new Zend_Controller_Router_Route_Regex(
            'produtos-destaque/produtoo/(\d+)-(.+)',
	    array(
                    'module'        => 'default',
                    'controller'    => 'produtos-destaque',
                    'action'        => 'produto'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'produtos-destaque/produto/%d-%s'
            )
	);

        $router->addRoute(
	  'produtosDestaqueProdutoPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/produtos-destaque/produto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'produtos-destaque',
                    'action'        => 'produto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/produtos-destaque/produto/%d-%s'
            )
	);

         $router->addRoute(
	  'produtosDestaqueProdutoEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/produtos-destaque/produto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'produtos-destaque',
                    'action'        => 'produto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/produtos-destaque/produto/%d-%s'
            )
	);
         
         //FIM route produtos-destaque/produto
         
         
        //Inicio route conteudos/conteudo
        $router->addRoute(
	  'conteudosConteudo',
	  new Zend_Controller_Router_Route_Regex(
            'conteudos/conteudo/(\d+)-(.+)',
	    array(
                    'module'	=> 'default',
                    'controller'  => 'conteudos',
                    'action'	=> 'conteudo'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'conteudos/conteudos/%d-%s'
            )
	);
        
        $router->addRoute(
	  'conteudoConteudosPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/conteudos/conteudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'conteudos',
                    'action'        => 'conteudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/conteudos/conteudo/%d-%s'
            )
	);

        $router->addRoute(
	  'conteudoConteudosEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/conteudos/conteudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'conteudos',
                    'action'        => 'conteudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/conteudos/conteudo/%d-%s'
            )
	);
        
        $router->addRoute(
	  'conteudoConteudosFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/conteudos/conteudo/(\d+)-(.+)',
	    array(
                    'idioma'        => 'fr',
                    'module'        => 'default',
                    'controller'    => 'conteudos',
                    'action'        => 'conteudo'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/conteudos/conteudo/%d-%s'
            )
	);
        //FIM route conteudos/conteudo

        //Inicio route contactos/contacto
        $router->addRoute(
	  'contactosContacto',
	  new Zend_Controller_Router_Route_Regex(
            'contactos/contacto/(\d+)-(.+)',
	    array(
                    'module'	=> 'default',
                    'controller'  => 'contactos',
                    'action'	=> 'contacto'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'contactos/contacto/%d-%s'
            )
	);

        $router->addRoute(
	  'contactosContactoPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/contactos/contacto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'contactos',
                    'action'        => 'contacto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/contactos/contacto/%d-%s'
            )
	);

        $router->addRoute(
	  'contactosContactoEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/contactos/contacto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'contactos',
                    'action'        => 'contacto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/contactos/contacto/%d-%s'
            )
	);
        
        $router->addRoute(
	  'contactosContactoFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/contactos/contacto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'fr',
                    'module'        => 'default',
                    'controller'    => 'contactos',
                    'action'        => 'contacto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/contactos/contacto/%d-%s'
            )
	);
        //FIM route contactos/contacto

        //Inicio route areas-negocio/area-negocio
        $router->addRoute(
	  'areasNareaN',
	  new Zend_Controller_Router_Route_Regex(
            'areas-negocio/area-negocio/(\d+)-(.+)',
	    array(
                    'module'	=> 'default',
                    'controller'  => 'areas-negocio',
                    'action'	=> 'area-negocio'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'areas-negocio/area-negocio/%d-%s'
            )
	);

        $router->addRoute(
	  'areasNareaNPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/areas-negocio/area-negocio/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'areas-negocio',
                    'action'        => 'area-negocio'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/areas-negocio/area-negocio/%d-%s'
            )
	);

        $router->addRoute(
	  'areasNareaNEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/areas-negocio/area-negocio/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'areas-negocio',
                    'action'        => 'area-negocio'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/areas-negocio/area-negocio/%d-%s'
            )
	);
        
        $router->addRoute(
	  'areasNareaNFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/areas-negocio/area-negocio/(\d+)-(.+)',
	    array(
                    'idioma'        => 'fr',
                    'module'        => 'default',
                    'controller'    => 'areas-negocio',
                    'action'        => 'area-negocio'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/areas-negocio/area-negocio/%d-%s'
            )
	);
        //FIM route areas-negocio/area-negocio

        //Inicio route produtos/produto
        $router->addRoute(
	  'produtosProduto',
	  new Zend_Controller_Router_Route_Regex(
            'produtos/produto/(\d+)-(.+)',
	    array(
                    'module'        => 'default',
                    'controller'    => 'produtos',
                    'action'        => 'produto'
                ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),

		'produtos/produto/%d-%s'
            )
	);

        $router->addRoute(
	  'produtosProdutoPT',
	  new Zend_Controller_Router_Route_Regex(
            'pt/produtos/produto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'pt',
                    'module'        => 'default',
                    'controller'    => 'produtos',
                    'action'        => 'produto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'pt/produtos/produto/%d-%s'
            )
	);

        $router->addRoute(
	  'produtosProdutoEN',
	  new Zend_Controller_Router_Route_Regex(
            'en/produtos/produto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'en',
                    'module'        => 'default',
                    'controller'    => 'produtos',
                    'action'        => 'produto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'en/produtos/produto/%d-%s'
            )
	);
        
        $router->addRoute(
	  'produtosProdutoFR',
	  new Zend_Controller_Router_Route_Regex(
            'fr/produtos/produto/(\d+)-(.+)',
	    array(
                    'idioma'        => 'fr',
                    'module'        => 'default',
                    'controller'    => 'produtos',
                    'action'        => 'produto'
	    ),
		    array(
			  1 => 'id',
			  2 => 'designacao'
		    ),
		'fr/produtos/produto/%d-%s'
            )
	);
        //FIM route produtos/produto
		
        */
        //INICIO ADMIN management
        $router->addRoute(
                  'managementAdmin',
                  new Zend_Controller_Router_Route_Regex(
                        'management',
                        array(
                                'lang'          => 'pt',
                                'module'        => 'management',
                                'controller'    => 'index',
                                'action'        => ''
                        ),
                        'management/index'
                    )
        );
        //FIM dashboard
    /*    
        $router->addRoute(
            'langMagementRoute',
            new Zend_Controller_Router_Route('/:lang/management',
                array(
                    'lang' => ':lang',
                    'module'        => 'management',
                    'controller'    => 'index'
                    )
            )
        );
        
        $router->addRoute(
            'langMagementComtRoute',
            new Zend_Controller_Router_Route('/:lang/management/:controller/*',
                array(
                    'lang' => ':lang',
                    'module'        => 'management',
                    )
            )
        );
        
        $router->addRoute(
            'fullRoute',
            new Zend_Controller_Router_Route('/:lang/management/:controller/:action',
                array('lang' => ':lang')
            )
        );
        
        
        
        $router->addRoute(
            'languageControllerAction',
            new Zend_Controller_Router_Route('/:lang/management/:controller/:action',
                array('lang' => ':lang')
            )
        );
        
        */
    }
    
    
}
<?php

class App_Controller_Plugin_Authorization extends Zend_Controller_Plugin_Abstract
{
	private $_auth;
	private $_acl;
 
	public function __construct(Zend_Auth $auth, Zend_Acl $acl)
	{
        	$this->_auth = $auth;
        	$this->_acl = $acl;
	}
 
	public function preDispatch ( Zend_Controller_Request_Abstract $request )
	{
		// revisa que exista una identidad
		// obtengo la identidad y el "role" del usuario, sino tiene le pone 'guest'
		$role = $this->_auth->hasIdentity() ? $this->_auth->getInstance()->getIdentity()->role : 'guest';
 
		// toma el nombre del recurso actual
		if( $this->_acl->has( $this->getRequest()->getActionName() ) )
			$resource = $this->getRequest()->getActionName();
		elseif( $this->_acl->has( $this->getRequest()->getControllerName() ) )
			$resource = $this->getRequest()->getControllerName();
		elseif( $this->_acl->has( $this->getRequest()->getModuleName() ) )
			$resource = $this->getRequest()->getModuleName();
 

		if (!$this->_acl->isAllowed($role, $resource) && $role == 'guest')
		{
			$request->setModuleName('autenticacao');
			$request->setControllerName('index');	
			$request->setActionName('index');		
		}

		elseif (!$this->_acl->isAllowed($role, $resource) && $role == 'editor')
		{
			$request->setModuleName('admin');
			$request->setControllerName('denied');
			$request->setActionName('index');
				
		}

	}
}
<?php
class App_Controller_Plugin_LangSelector extends Zend_Controller_Plugin_Abstract
{
    //protected $_idioma = LINGUA;
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $module = $request->getModuleName();
        if($module=='management'){
            $idioma = 'pt';
            $locale = 'pt_PT';
            
            $zl = new Zend_Locale();
            $zl->setLocale($locale);
            Zend_Registry::set('Zend_Locale', $zl);
            $translate = new Zend_Translate('tmx', APPLICATION_PATH . '/modules/management/lang/traducao.tmx', $idioma);
            Zend_Registry::set('Zend_Translate', $translate);
        }else{
            $idioma = $request->getParam('idioma','');

            if ($idioma !== 'es' && $idioma !== 'en')
                $request->setParam('idioma','pt');

            $idioma = $request->getParam('idioma');
            if ($idioma == 'en'){
                $locale = 'en_US';
            }
            else
            if ($idioma == 'es'){
                $locale = 'es_ES';
            }
            else
            $locale = 'pt_PT';
        
        
        
            //http://framework.zend.com/manual/en/zend.locale.appendix.html
            $zl = new Zend_Locale();
            $zl->setLocale($locale);
            Zend_Registry::set('Zend_Locale', $zl);
            //$translate = new Zend_Translate('csv', APPLICATION_PATH . '/lang/'. $idioma . '.csv' , $idioma);
            $translate = new Zend_Translate('tmx', APPLICATION_PATH . '/lang/traducao.tmx', $idioma);
            Zend_Registry::set('Zend_Translate', $translate);
        }
            
        

    }

}
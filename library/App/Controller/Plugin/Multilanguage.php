<?php
//http://www.m4d3l-network.com/2009/06/29/add-language-route-to-your-zend-framework-project/
class App_Controller_Plugin_Multilanguage extends Zend_Controller_Plugin_Abstract
{
	public function routeStartup (Zend_Controller_Request_Abstract $request)
    {
 		if (substr($request->getRequestUri(), 0, -1) == $request->getBaseUrl())
		{
			$request->setRequestUri($request->getRequestUri()."pt"."/");
			$request->setParam("language", "pt");
		}
    }
 
    public function routeShutdown (Zend_Controller_Request_Abstract $request)
    {
 	$language = $request->getParam("language", Zend_Registry::getInstance()->Zend_Locale->getLanguage());
		$locale = new Zend_Locale($language);
		Zend_Registry::getInstance()->Zend_Locale->setLocale($locale);
		$translate = Zend_Registry::getInstance()->Zend_Translate;
		$translate->getAdapter()->setLocale(Zend_Registry::getInstance()->Zend_Locale);
		Zend_Controller_Router_Route::setDefaultTranslator($translate);
    }
}
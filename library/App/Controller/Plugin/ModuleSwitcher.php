<?php 

class App_Controller_Plugin_ModuleSwitcher extends Zend_Controller_Plugin_Abstract
{
    protected $_baseIncludePath;
    protected $_view = null;

    public function __construct()
    {
        $this->_baseIncludePath = get_include_path();
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
		
        // Changes the Layout based on the module name
        $moduleName = $request->getModuleName();

        $frontController = Zend_Controller_Front::getInstance();
        $controllerDirectory = $frontController->getControllerDirectory($moduleName);

        $moduleDirectory = dirname($controllerDirectory);
        $modelsDirectory = APPLICATION_PATH . '/models/'; // se quiser que seja models separados mete-se '$moduleDirectory'
        $formsDirectory  = $moduleDirectory . '/forms/';

        Zend_Layout::startMvc();
        $layout = Zend_Layout::getMvcInstance();  
        $layout->setLayoutPath('../application/modules/' . $moduleName . '/layouts')->setLayout($moduleName);

        $view = new Zend_View();
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->init();      

        $this->_view = $viewRenderer->view;
        $this->_view->doctype(Zend_View_Helper_Doctype::XHTML1_STRICT);
        $this->_view->headMeta()->setHttpEquiv('content-type', 'text/html; charset=utf-8');
        $this->_view->addHelperPath($moduleDirectory . '/views/helpers/');
        $this->_view->setHelperPath($moduleDirectory . '/views/helpers');
        $this->_view->headTitle('')->setSeparator(' - ');

        
        set_include_path($modelsDirectory . PATH_SEPARATOR . $formsDirectory . PATH_SEPARATOR . $this->_baseIncludePath);
        return $request;
    }

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (!$request->isDispatched())
        {
            return;
        }
        $this->_view->headTitle($this->_view->title);
    }
}

<?php

class App_Class_File {

    public function __construct() {
        
    }

    public function rename($fileName) {
        if ($fileName == null)
            return false;

        $ext = $this->_getExtension($fileName);
        $newName = $this->_getUid() . '.' . $ext;

        return $newName;
    }

    public function _getExtension($fileName) {
        $fileName = strtolower($fileName);
        $exts = explode("[/\\.]", $fileName);
        $n = count($exts) - 1;
        $exts = $exts[$n];

        return $exts;
        /*
         $date = "04/30/1973";
            list($month, $day, $year) = split('[/.-]', $date);
            echo "Month: $month; Day: $day; Year: $year<br />\n";
         */
    }

    public function _getUid($prefix = '') {
        $gerarUuID = new App_Class_Uuid();
        $uuid = $gerarUuID->generate();

        return $uuid;
    }
    
    /*
    * Resizes a picture to a maximum (applies to both height and width)
    * input: $path_to_image – a fully qualified path to the image
    * ex: /myfolders/imagefolders/image_name.jpg
    * $target: maximum picture height or width in pixels
    *
    * returns: image width and height string ready to be used in an HTML tag
    */
    function image_resize($path_to_image, $target) {
        //get image dimensions
        $img_dimensions = getimagesize($path_to_image);
        $width = $img_dimensions[0]; //stored in first element of return array
        $height = $img_dimensions[1]; //stored in second element of return array
        unset ($img_dimensions); //free up memory
        //declare a ratio to be used to store image dimensions ratio. If ratio is
        //not declared prior to usage, you might get PHP warnings.
        $ratio = FALSE;
        //We’re looking for a maximum target size, so we must figure out which side
        //is larger: width or height, and then use that to produce the proper ratio
        if ($width > $height) {
        $ratio = ($target / $width);
        } else {
        $ratio = ($target / $height);
        }
        //apply ratio to dimensions
        $width = round($width * $ratio);
        $height = round($height * $ratio);
        //return the resized image dimensions in html image tag format
        return ' width="'.$width.'" height="'.$height.'" ';
    }

}
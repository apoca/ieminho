<?php
/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class App_Class_Captcha
{
	
	public static function validate($postPrefix = "captcha")
	{
		$captcha = $_POST[$postPrefix];
        	$captchaId = $captcha['id'];
        	$captchaInput = $captcha['input'];
        	$captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaId);

        	$captchaWord =   $captchaSession->word;
        	if( $captchaWord )
        		return $captchaInput == $captchaWord;
       		return false;
	}

	/**
	 * generates the captcha image and returns the image id.
	 *
	 * @param string $postPrefix  the prefix of the post value i.e. 'captcha[id]'
	 * @return string
	 */
	public static function generate($postPrefix = "captcha")
	{
		// replace the path/to with the correct path...
		// same with /images/captcha
		$captcha = new Zend_Captcha_Image(array(
		    'name' => $postPrefix,
		    'wordLen' => 6,
		    'timeout' => 600,
			'font' => FONTS."/trebuc.ttf",
			'imgdir' => CAPTCHA,
			'imgurl' => DOMINIO."/images/captcha"
		));

		return $captcha->generate();
	}

}
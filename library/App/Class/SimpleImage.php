<?php

 
class App_Class_SimpleImage {
   
   protected $wi;
   protected $hi;
   protected $file;
   protected $wf;
   protected $hf;
   protected $aux;
   protected $valores;
 
	public function resize($_w, $_h, $_path){
		
			//tamanho da caixa, div
			$wi = $_w;
			$hi = $_h;
			
			$file = $_path;
			
			//tamanho da imagem original
			$info = getimagesize($file);
			$w = $info[0];
			$h = $info[1];
			
			if ($w > $h){
			    $fator = ($w / $wi);
			}else{
			    $fator = ($h / $hi);    
			}
			    
			//novos tamanhos para o img    
			$wf = $w / $fator;
			$hf = $h / $fator;
			
			$aux = ($hi-$hf)/2;
			
			$valores = array();
			
			$valores[0] = $wf;
			$valores[1] = $hf;
			$valores[2] = $aux;
			$valores[3] = $wi;
			$valores[4] = $hi;
		
			return $valores;
	}
}


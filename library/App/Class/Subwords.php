<?php
/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class App_Class_Subwords {
	
	
	public function __construct(){
	}
	//$text,$length=64,$tail="..."
	function contaPalavras($text,$length,$tail) 
	{
                    if (strlen($text) > $length) {
                        $arr = str_word_count($text, 2);
                        $blubb = false;
                        foreach ($arr as $intPos => $strWord) {
                                if ($blubb) return substr($text, 0, $intPos).$tail;
                                        if ($intPos > $length) $blubb = true;
                        }
                    }
		return $text;
	}
}
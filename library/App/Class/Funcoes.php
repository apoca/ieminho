<?php

class App_Class_Funcoes {
	
	
	public function __construct(){
	}
        
        function infoHead($idioma_iso)
        {   $view = new Zend_View();
            
            $config = new Model_DbTable_Configuracoes();
            $configIdioma = new Model_DbTable_ConfiguracoesIdioma();

            $view->headTitle($configIdioma->optionValueIdioma('title',  $idioma_iso));
            
            $view->headMeta()->appendName('description', $configIdioma->optionValueIdioma('description',  $idioma_iso));
            
            $view->headMeta()->appendName('keywords', $configIdioma->optionValueIdioma('keywords', $idioma_iso));
            $view->headMeta()->appendName('robots', 'index, follow');
            $view->headMeta()->appendName('generator', 'SBM - Sétima Backoffice Management V2.0  (setima.pt)');
        }
        
        function infoHeadHTML($titulo, $separador)
        {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $action     = $request->getActionName();
            $controller = $request->getControllerName();
            $module     = $request->getModuleName();

            $view = new Zend_View();
            $view->headTitle($titulo);
            $view->headTitle()->setSeparator($separador);
            $view->headTitle($request->getActionName())->headTitle($request->getControllerName());
        }
	
        function friendlyName($string)
	{
		$string = preg_replace("`\[.*\]`U","",$string);
		$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
		$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
		
		return strtolower(trim($string, '-'));
	}

	function idiomaPadrao(){
            return 'pt';
        }
        
        function outrosIdiomas($idioma_iso)
        {        
            $configlang = new Model_DbTable_ConfiguracoesIdioma();
            return $configlang->fetchAll($configlang->select()->where('idioma_iso != ?', $idioma_iso)->where('option_name = ?', 'status')->where('option_value = ?', 'A'));
        }
        
        /**
        * Check number bi, NIPC or NIF is valid
        * @param nr $string
        * @return bool
        */
        
        function nipc_isvalid($nr) {
            
            if ($nr == '123456789' || strlen($nr) != 9 || (is_numeric($nr)==FALSE)) {
                return false;
                
            }else{
                while (strlen($nr) < 9) {
                    $nr = "0" . $bi;
                }
                $calc = 9 * $nr[0] + 8 * $nr[1] + 7 * $nr[2] + 6 * $nr[3] + 5 * $nr[4] + 4 * $nr[5] + 3 * $nr[6] + 2 * $nr[7] + $nr[8];

                if ($calc % 11 === 0) {
                    return true;
                } else {
                    return false;
                }
            }
            
        }
        
	function gerarPassword()
	{
		$chars = "abcdefghijkmnopqrstuvwxyz023456789";
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '' ;
		while ($i <= 7) {
			$num = rand() % 33;
			$tmp = substr($chars, $num, 1);
			$pass = $pass . $tmp;
			$i++;
		}

		return $pass;
	}

        

	function gerarHashCode()
	{
		// Generate random 32 character hash and assign it to a local variable.
		$hash = md5( rand(0,1000) ); 
		// Example output: f4552671f8909587cf485ea990207f3b
		return $hash;
	}
        /**
        * Create a fairly random 32 character MD5 token
        *
        * @return string
        */

        function token()
        {
            return md5(str_shuffle(chr(mt_rand(32, 126)). uniqid(). microtime(TRUE)));
        }
        
        /**
        * get relative url
        * @param url $string
        * @return string
        */

        function getActualUrl($url)
        {
             preg_match('~^/[a-z]{2}(?:/|$)~', $url, $output);
            $sliptURL = explode('/', $output[0]);

            $lang = $sliptURL[1];
            if($lang==''){
                $urlFinal = $url;
            }else{
                $urlFinal = substr($url, 3);
            }
            
            return $urlFinal;
        }

        /**
        * Encode a string so it is safe to pass through the URI
        * @param string $string
        * @return string
        */

        function base64_url_encode($string = NULL)
        {
            return strtr(base64_encode($string), '+/=', '-_~');
        }

        /**
        * Decode a string passed through the URI
        *
        * @param string $string
        * @return string
        */

        function base64_url_decode($string = NULL)
        {
            return base64_decode(strtr($string, '-_~','+/='));
        }

        function getIP()
	{
            	static $ip = FALSE;
   
                if( $ip ) {
                    return $ip;
                }
                //Get IP address - if proxy lets get the REAL IP address

                if (!empty($_SERVER['REMOTE_ADDR']) AND !empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
                    $ip = $_SERVER['REMOTE_ADDR'];
                } elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = '0.0.0.0';
                }

                //Clean the IP and return it
                return $ip = preg_replace('/[^0-9\.]+/', '', $ip);

	}
        
        //// underscored to upper-camelcase
        //// e.g. "this_method_name" -> "ThisMethodName"
        function upperCamelcase($string)
        {
                return preg_replace('/(?:^|-)(.?)/e',"strtoupper('$1')",$string);
        }

        //// underscored to lower-camelcase
        //// e.g. "this_method_name" -> "thisMethodName"
        function lowerCamelcase($string)
        {
                return preg_replace('/-(.?)/e',"strtoupper('$1')",$string);
        }      

        // camelcase (lower or upper) to hyphen
        // e.g. "thisMethodName" -> "this_method_name"
        // e.g. "ThisMethodName" -> "this_method_name"
        // Of course these aren't 100% symmetric.  For example...
        //  * this_is_a_string -> ThisIsAString -> this_is_astring
        //  * GetURLForString -> get_urlfor_string -> GetUrlforString
        function camelcaseToHyphen($string)
        {
                return strtolower(preg_replace('/([^A-Z])([A-Z])/', "$1-$2", $string));
        }


	public function enviarEmail($para, $assunto, $mensagem)
	{
			$smtp = MAIL_SMTP;
			$conta = MAIL_NEWSLETTER;
			$senha = MAIL_PASSWORD;
			$de = MAIL_NEWSLETTER;

			try {
                            $config = array (
                            'auth' => 'login',
                            'username' => $conta,
                            'password' => $senha,
                            'ssl' => MAIL_SSL,
                            'port' => MAIL_SSL_PORT
                            );

                            $mailTransport = new Zend_Mail_Transport_Smtp($smtp, $config);

                            $mail = new Zend_Mail('utf-8');
                            $mail->setFrom($de, MAIL_NAME);
                            $mail->addTo($para);
                            $mail->setBodyHtml($mensagem);
                            $mail->setSubject($assunto);
                            $mail->addHeader('X-MailGenerator', 'Sétima SW v1.0');

                            $mail->send($mailTransport);
			}
			catch (Exception $e)
			{
                            echo ($e->getMessage());
			}
	}
        
        function arrayToObject($array) {
            if (!is_array($array)) {
                return $array;
            }

            $object = new stdClass();
            if (is_array($array) && count($array) > 0) {
                foreach ($array as $name => $value) {
                    $name = strtolower(trim($name));
                    if (!empty($name)) {
                        $object->$name = $this->arrayToObject($value);
                    }
                }
                return $object;
            } else {
                return FALSE;
            }
        }
	
	function gerarPaginaSegundos()
	{
		$starttime = explode(' ', microtime());
		$starttime = $starttime[1] + $starttime[0];

		$mtime = explode(' ', microtime());
		$totaltime = $mtime[0] + $mtime[1] - $starttime;

		$segundos = 'Página gerada em '.number_format($totaltime, 2, '.', '').' segundos.';

		//printf('<p>Página gerada em %.2f segundos.</p>', $totaltime);
		return $segundos;
	}


}
<?php
/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class App_Class_Twitter {
	
	protected $_config;
	protected $_twitter;
	
	
	public function __construct(){
	  $this->_config  = new Zend_Config_Ini(APPLICATION_PATH.'/config.ini','production');

      // Conectamo-nos ao serviço do twitter
      $this->_twitter = new Zend_Service_Twitter($this->_config->twitter->username, $this->_config->twitter->password);
	  
      $response = $this->_twitter->account->verifyCredentials();
      
      if(!$response){
      	throw new Exception("Erro ao autenticar no twitter");
      }
	}
	
	public function addTwitte($string)
	{	
		
		$response = $this->_twitter->status->update($string);
		
		if(!$response){
      		throw new Exception("Erro ao autenticar no twitter");
      	}
	}
	
	public function getPunyURL($url="")
	{
		// A URL com o serviço PunyURL do SAPO.
		$sapoUrl="http://services.sapo.pt/PunyURL/GetCompressedURLByURL?url=".urlencode($url);
			
		// Obtemos a info do "XML response"
		$xml=@simplexml_load_string(@file_get_contents($sapoUrl));
		
		$puny=$xml->puny;
		$ascii=$xml->ascii;
		$preview=$xml->preview;
 
		return $preview;
		
		/* $surl="http://services.sapo.pt/PunyURL/GetCompressedURLByURL?url=".urlencode($url);
 
		// Get the information from XML response.
		$xml=@simplexml_load_string(@file_get_contents($surl));
		$shorten['puny']=$xml->puny;
		$shorten['ascii']=$xml->ascii;
		$shorten['preview']=$xml->preview;
		 
		return $shorten;*/
	}
}
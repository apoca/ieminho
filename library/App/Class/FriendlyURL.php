<?php
/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class App_Class_FriendlyURL {
	
	
	public function __construct(){
	}
	/**
	 * Função para retirar acentos, caracteres especiais de uma string
	 * @param $string
	 * @return $string
	 */
	
	function gerarLinkSEO($id, $string)
	{
		$string = preg_replace("`\[.*\]`U","",$string);
		$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
		$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
		
		return strtolower(trim($id.'-'.$string, '-'));
	}


}
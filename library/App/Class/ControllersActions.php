<?php

/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 * @source http://stackoverflow.com/posts/7810362/revisions 
 */
class App_Class_ControllersActions {

    protected $_funcoes = NULL;
    public function __construct() {
        $this->_funcoes = new App_Class_Funcoes();
    }

    public function byModule($moduleName='management') {
        
        $front  = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $front = $router->getFrontController();
        $acl = array();

        foreach ($front->getControllerDirectory() as $module => $path) {

            if ($module == $moduleName) {
                foreach (scandir($path) as $file) {

                    if (strstr($file, "Controller.php") !== false) {

                        include_once $path . DIRECTORY_SEPARATOR . $file;

                        foreach (get_declared_classes() as $class) {
                            
                            if (is_subclass_of($class, 'Zend_Controller_Action')) {

                                $controllerM = substr($class, 0, strpos($class, "Controller")); //list all name controllers eg.: Management_ConfiguracoesController
                                                  
                                $controllerSplit = explode(ucfirst($moduleName)."_", $controllerM); //extract only name controller eg.: ConfiguracoesController -----1-> //explode("Management_", $controllerM);

                                $controller = $this->_funcoes->camelcaseToHyphen($controllerSplit[1]);  // convert camelcase to hyphen eg.: configuracoe-controller

                                $actions = array();

                                foreach (get_class_methods($class) as $action) {

                                    if (strstr($action, "Action") !== false) {
                                        //remove name at the end of function ended by 'Action' eg.: UtilizadoresAction
                                        $view = explode("Action", $action); //eg.: Utilizadores

                                        //converte lowerCamelCase eg.: eliminarImagem to eliminar-imagem
                                        $actions[] = $this->_funcoes->camelcaseToHyphen($view[0]); //$view[0];//$action;
                                    }
                                }
                                
                            }
                        }
                        
                        //$acl[$module][$controller] = $actions;
                        $acl[$controller] = $actions;
                        
                        
                    }
                }
            }
        }
        
        return $acl;
    }

}
<?php

class App_Class_GetLinksInfo {

    public function __construct() {
        
    }

    function attr($s, $attrname) {
        //retrn html attribute
        preg_match_all('#\s*(' . $attrname . ')\s*=\s*["|\']([^"\']*)["|\']\s*#i', $s, $x);
        if (count($x) >= 3)
            return $x[2][0];
        return "";
    }

    function makeabsolute($url, $link) {
        if (strpos($link, "http://") === 0)
            return $link;
        $p = parse_url($url);
        if (strpos($link, "/") === 0)
            return "http://" . $p['host'] . $link;
        return str_replace(substr(strrchr($url, "/"), 1), "", $url) . $link;
    }

    function getRemoteFileSize($url) {
        if (substr($url, 0, 4) == 'http') {
            $x = array_change_key_case(get_headers($url, 1), CASE_LOWER);
            if (strcasecmp($x[0], 'HTTP/1.1 200 OK') != 0) {
                $x = $x['content-length'][1];
            } else {
                $x = $x['content-length'];
            }
        } else {
            $x = @filesize($url);
        }
        return $x;
    }

    function getLinksInfo($url) {
        $web_page = file_get_contents($url);

        $data['keywords'] = "";
        $data['description'] = "";
        $data['title'] = "";
        $data['favicon'] = "";
        $data['images'] = array();

        preg_match_all('#<title([^>]*)?>(.*)</title>#Uis', $web_page, $title_array);
        $data['title'] = $title_array[2][0];
        preg_match_all('#<meta([^>]*)(.*)>#Uis', $web_page, $meta_array);
        for ($i = 0; $i < count($meta_array[0]); $i++) {
            if (strtolower($this->attr($meta_array[0][$i], "name")) == 'description')
                $data['description'] = $this->attr($meta_array[0][$i], "content");
            if (strtolower($this->attr($meta_array[0][$i], "name")) == 'keywords')
                $data['keywords'] = $this->attr($meta_array[0][$i], "content");
        }
        preg_match_all('#<link([^>]*)(.*)>#Uis', $web_page, $link_array);
        for ($i = 0; $i < count($link_array[0]); $i++) {
            if (strtolower($this->attr($link_array[0][$i], "rel")) == 'shortcut icon')
                $data['favicon'] = $this->makeabsolute($url, $this->attr($link_array[0][$i], "href"));
        }
        preg_match_all('#<img([^>]*)(.*)/?>#Uis', $web_page, $imgs_array);
        $imgs = array();
        for ($i = 0; $i < count($imgs_array[0]); $i++) {
            if ($src = $this->attr($imgs_array[0][$i], "src")) {
                $src = $this->makeabsolute($url, $src);
                if ($this->getRemoteFileSize($src) > 15000)
                    array_push($imgs, $src);
            }
            if (count($imgs) > 5)
                break;
        }
        $data['images'] = $imgs;

        return $data;
    }

}
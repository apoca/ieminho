<?php
/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class App_Class_FTP {
	
	private $_senha_ftp;
        private $_usuario_ftp;
        private $_servidor_ftp;
        public $_caminho_absoluto_ftp;
        public $_id_conexao;
        public $_login;
        public $arquivo;
                    
	public function __construct(){
            $this->_id_conexao = "";
            // Configurações do servidor FTP e passa para a sessão
            $this->_servidor_ftp 		= '81.92.207.134';
            $this->_usuario_ftp 		= 'cotesi' ;
            $this->_senha_ftp                   = '4GPFVQTW3J';
            $this->_caminho_absoluto_ftp 	= "/home/cotesi/public_html/imagens";

            $this->conectarFTP();

            }

            public function conectarFTP () {

            if(!$this->_id_conexao) {
                $this->_id_conexao = ftp_connect($this->_servidor_ftp);

                // realiza o login com o nome de usuário e senha
                $this->_login = ftp_login($this->_id_conexao, $this->_usuario_ftp, $this->_senha_ftp);

                // verifica se houve sucesso na conexão
                if ((!$this->_id_conexao) || (!$this->_login)) {
                echo "Não foi possível abrir uma conexão FTP com o servidor ".$this->_servidor_ftp."";
                exit;
                }
            }

	}
        public function apagarArquivoFTP($arquivo) {

            ftp_delete($this->_id_conexao, $arquivo); // exclui o arquivo pelo método ftp

        }

        public function criarPastaFTP($pasta) {

            ftp_mkdir($this->_id_conexao, $this->_caminho_absoluto_ftp."/".$pasta);
        }

        public function usuarioPastaFTP($pasta){
            // concede à pasta o usuário do ftp ( para que o arquivo possa ser lido corretamente )
            ftp_site($this->_id_conexao,"CHOWN ".$this->_usuario_ftp." ".$this->_caminho_absoluto_ftp."/".$pasta."");

        }

        public function mudarDiretorioFTP($diretorio){
            // muda o diretório atual no ftp para o que foi passado em parâmetro.
            ftp_chdir($this->_id_conexao,$diretorio);
        }

        public function setaNomeArquivoFTP($arquivo){
            $this->arquivo = $arquivo;
        }

        public function enviarArquivoFTP($pasta, $arquivo) {

            //manda o arquivo pelo método ftp
            $upload = ftp_put($this->_id_conexao, $this->_caminho_absoluto_ftp.$pasta."/".$arquivo, $this->arquivo , FTP_BINARY);

            // controlo se o UPLOAD teve exito
            if ($upload != 1) {
            return false;
            } else {
            return true;
            }

        }

        public function downloadArquivoFTP(){

            // faz o download de um arquivo usando ftp
            $temp_file 		= $this->arquivo; // arquivo local
            $remote_file 	= $this->_caminho_absoluto_ftp.$this->arquivo; // arquivo remoto

            // traz o arquivo do servidor remoto ao servidor local
            ftp_get($this->_id_conexao, $temp_file, $remote_file, FTP_BINARY);

            // faz a leitura do arquivo
            $ponteiro = fopen($temp_file, "r");
            return fread($ponteiro, filesize ($temp_file));

        }

        
}
<?php
class App_Class_Acl extends Zend_Acl
{
    /**
     * @var Core_Services_Acl
     */
    public static $_instance = null;

    /**
     * @return Core_Services_Acl
     */
    public static function getInstance()
    {
        if (null == self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct()
    {
        //ini_set('max_execution_time', 180);
        $this->_buildRoles();
        $this->_buildResources();
        $this->_buildRules();
    }


    /**
     * Criar Roles
     */
    public function _buildRoles()
    {
        $rolesTable = new Core_Models_Roles();
        $roles = $rolesTable->getAllRoles();
//        echo "<pre>";
//        print_r($roles);
//        echo "</pre>";

        if (0 == count($roles)) {
            $rs = array();
        }

        /**
         * Now add to the ACL
         */
        $numRoles = count($roles);
        $i = 0;

            /**
             * Check every element in the db
             */
            foreach ($roles as $role => $parentRoles) {
                if (!$this->hasRole($parentRoles['designacao'])){
				
                    $this->addRole(new Zend_Acl_Role($parentRoles['designacao']));
                }
            }
    }

    /**
     * Cria as regras de acesso ao sistema
     */
    public function _buildRules()
    {
        $regrasTable = new Core_Models_Regras();
        $rules = $regrasTable->getAllRegras();
//        echo "<pre>";
//        print_r($rules);
//        echo "</pre>";
        if (count($rules) > 0) {
            foreach ($rules as $row) {
                if (!$this->hasRole($row['role'])) {

                    $this->addRole(new Zend_Acl_Role($row['role']));
                }
                /*
                 * A role "suadmin" é uma role fixa, por necessidade e/ou protecção é necessário que esta
                 * e a rola "admin" existem sempre.
                 */
                if ($row['permitir'] == 'S' && $row['role'] != 'suadmin') {
                    $this->allow($row['role'], $row['recurso'], $row['privilegio']);
                    $this->allow('suadmin', $row['recurso'], $row['privilegio']);
                }else {
                    $this->deny($row['role'], $row['recurso'], $row['privilegio']);
                }
            }
        }
    }

    /**
     * Criar recursos
     */
    public function _buildResources()
    {
        $recursosTable = new Core_Models_Recursos();
        $resources = $recursosTable->getAllRecursos();

        if (0 == count($resources)) {
        return;
        }

        $allResources = array();
        /**
         * Recurso "Map" id para o seu nome
         */
        $map = array();
        foreach ($resources as $row) {
            $allResources[] = $row['id_recurso'];
            $map[$row['id_recurso']] = $row['moduloNome'] . ':' . $row['controladorNome'];
        }
        foreach ($resources as $row) {
            if ($row['parent_id'] !== null && !empty($row['parent_id']) && !in_array($row['parent_id'], $allResources)) {
                throw new Zend_Acl_Exception('Recurso com o id "' . $row['parent_id'] . '" não existe!');
            }
        }

        $numResources = count($resources);
        $i = 0;
        while ($numResources > $i) {
            foreach ($resources as $row) {
                /**
                 * Verifica se o recurso dos "pais" (se houver) existem
                 * Só adiciona se este recurso ainda não tiver sido adicionado e/ou seu "pai" é conhecido
                 */
                $resId = $row['moduloNome'] . ':' . $row['controladorNome'];

                $has = false;
                if ($row['parent_id'] != null) {
                    $parentName = isset($map[$row['parent_id']]) ? $map[$row['parent_id']] : null;
                    if (null == $parentName) {
                        $has = false;
                    } else {
                        // $parentResId = $this->_formatResource($parentName);
                        $has = $this->has($parentName);
                    }
                }

                if (!$this->has($resId)) {
                    if ($has) {
                        $this->add(new Zend_Acl_Resource($resId));
                    } else {

                        $this->add(new Zend_Acl_Resource($resId));
                    }
                    $i++;
                }
            }
        }
    }

    public function isRoleAllowed($roleName, $module, $controller, $action = null)
    {
        if ($action != null) {
            $action = strtolower($action);
        }
        $resource = strtolower($module . ':' . $controller);
        /**
         * Se o recurso não existe
         */
        if (!$this->has($resource)) {
            return false;
        }

        if (($this->hasRole($roleName) && $this->isAllowed($roleName, $resource, $action))) {
            return true;
        }
        return false;
    }

    /**
     * @param array $searchRoles
     * @return bool
     */
    public function _hasAllRolesOf($searchRoles)
    {
        foreach ($searchRoles as $role) {
            if (!$this->hasRole($role)) {
                return false;
            }
        }
        return true;
    }

    
}
<?php

class App_Models_AclResource extends Zend_Db_Table_Abstract
{
    protected $_name = 'acl_resource';
    protected $_primary = 'acl_resource_id'; 
	protected $_dependentTables = array('AclPrivilege');
	protected $_referenceMap    = array(
    'fk_acl_resource_module' => array(
            'columns'           => 'acl_module_id',
            'refTableClass'     => 'AclModule',
            'refColumns'        => 'acl_module_id'
        ));
	
}
<?php
class App_Models_AclRoleProvilege extends Zend_Db_Table_Abstract
{
    protected $_name = 'acl_role_privilege';
    protected $_primary = array('acl_role_id', 'acl_privilege_id'); 
	protected $_referenceMap    = array(
    'fk_acl_role_privilege' => array(
            'columns'           => 'acl_role_id',
            'refTableClass'     => 'AclRole',
            'refColumns'        => 'acl_role_id'
     ),
     'fk_acl_privilege_role' => array(
            'columns'           => 'acl_privilege_id',
            'refTableClass'     => 'AclPrivilege',
            'refColumns'        => 'acl_privilege_id'
     ));
	
}
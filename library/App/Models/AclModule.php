<?php
class App_Models_AclModule extends Zend_Db_Table_Abstract
{
    protected $_name = 'acl_module';
    protected $_primary = 'acl_module_id'; 
	protected $_dependentTables = array('AclResource');
}
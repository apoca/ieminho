<?php
class App_Models_AclPrivilege extends Zend_Db_Table_Abstract
{
    protected $_name = 'acl_privilege';
    protected $_primary = 'acl_privilege_id'; 
	protected $_dependentTables = array('AclRolePrivilege');
	protected $_referenceMap    = array(
    'fk_acl_privilege_resource' => array(
            'columns'           => 'acl_resource_id',
            'refTableClass'     => 'AclResource',
            'refColumns'        => 'acl_resource_id'
        ));
	
}
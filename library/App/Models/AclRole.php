<?php
class App_Models_AclRole extends Zend_Db_Table_Abstract
{
    protected $_name = 'acl_role';
    protected $_primary = 'acl_role_id'; 
	protected $_dependentTables = array('AclRolePrivilege');

}
<?php
/**
 * SBM
 *
 * LICENSE
 *
 *
 * @copyright	Copyright (c) 2007-2010 Sétima - Tecnologias da Informação e Comunicação, Lda
 * @license     Sétima Backoffice Madular
 * @version 	2.0
 */

class Sbm_Utility_PaginatorAdapter extends Zend_Paginator_Adapter_Iterator
{
	public function __construct(Iterator $iterator, $count)
	{
		parent::__construct($iterator);
		$this->_count = $count;
	}
}

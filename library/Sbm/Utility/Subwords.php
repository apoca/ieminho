<?php
/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class Sbm_Utility_Subwords {
	
	
    public function __construct(){
    }
    //$text,$length=64,$tail="..."
    function contaPalavras($txt, $len, $tail = '...')
    {
        $strlen = strlen($txt);
        for ($i=0;$i<strlen($txt);$i++)
        {
            if ($txt{$i} == '.' || $txt{$i} == ':') $strlen -= 0.5;
            if ($txt{$i} == 'w' || $txt{$i} == '@') $strlen++;
        }
        $strlen = round($strlen);
        if ($strlen > $len)
        {
            $txt = substr($txt,0,($len-3)).$tail;
        }
        return $txt;

    }
}
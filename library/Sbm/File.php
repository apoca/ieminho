<?php
/**
 *
 * @uses       
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */

class Sbm_File
{
    public function rename($fileName)
    {
        if($fileName == null)
            return false;

        $ext = $this->_getExtension($fileName);
        $newName = $this->_getUid().'.'.$ext;

        return $newName;
    }

    public function _getExtension($fileName)
    {
        $fileName = strtolower($fileName) ;
        $exts = split("[/\\.]", $fileName) ;
        $n = count($exts)-1;
        $exts = $exts[$n];
        
        return $exts;
    }

    public function _getUid($prefix = '')
    {
        $uuid = Sbm_Utility_Uuid::generate();

        return $uuid;
    }

}
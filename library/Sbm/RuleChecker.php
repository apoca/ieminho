<?php

class Sbm_RuleChecker
{
    public static function isAllowed($roleName = null, $action = null, $controller = null, $module = 'management', $callback = null, $params = null)
    {
        $action = strtolower($action);

        /**
         * Get module and controller name
         */
        if (null == $action) {
            $action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        }
        if (null == $controller) {
            $controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        }
        if (null == $module) {
            $module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
        }
        if (null == $roleName) {
            $storage = new Zend_Auth_Storage_Session();
            $dados = $storage->read();
            $usersTable = new Model_DbTable_AdminUtilizadores();
            $user = $usersTable->getUtilizador($dados->id);
            $roleName = $user->role;
        }
        $isAllowed = Sbm_Acl::getInstance()->isRoleAllowed($roleName, $module, $controller, $action);

        if (!$isAllowed) {
            return false;
        }else{
            return true;
        }
        /*if (null != $callback) {
            if (false !== ($pos = strpos($callback, '::'))) {
                $callback = array(substr($callback, 0, $pos), substr($callback, $pos + 2));
            }
            return call_user_func_array($callback, $params);
        }*/

    }
}

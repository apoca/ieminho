<?php

/**
 *
 * @uses       
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */
class Sbm_ImgResizer {

    private $originalFile = '';

    public function __construct($originalFile = '') {
	$this->originalFile = $originalFile;
    }

    public function resize($newWidth, $targetFile, $ext) {
	if (empty($newWidth) || empty($targetFile) || empty($ext)) {
	    return false;
	}

	switch ($ext) {
	    case "jpg" || "jpeg" || "JPG" || "JPEG":
		$src = imagecreatefromjpeg($this->originalFile);
		break;
	    case "gif" || "GIF":
		$src = ImageCreateFromGif($this->originalFile);
		break;
	    case "png" || "PNG":
		$src = ImageCreateFrompng($this->originalFile);
		break;
	}

	list($width, $height) = getimagesize($this->originalFile);
	if ($width <= $newWidth) {
	    return false;
	}
	$newHeight = ($height / $width) * $newWidth;
	$tmp = imagecreatetruecolor($newWidth, $newHeight);
	imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

	if (file_exists($this->originalFile)) {
	    unlink($this->originalFile);
	}

	// 85 is my choice, make it between 0 – 100 for output image quality with 100 being the most luxurious
	switch ($ext) {
	    case "jpg" || "jpeg" || "JPG" || "JPEG":
		imagejpeg($tmp, $targetFile, 85);
		break;
	    case "gif" || "GIF":
		imagegif($tmp, $targetFile);
		break;
	    case "png" || "PNG":
		imagepng($tmp, $targetFile, 8);
		break;
	}
    }

}
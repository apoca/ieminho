<?php

class Sbm_Config
{
    /**
     * Get application config object
     *
     * @return Zend_Config
     */
    public static function getConfig()
    {
        // Inicializamos o ficheiro de configuração e a sua respectiva aplicação
        $config = new Zend_Config_Ini(APPLICATION_PATH . DS . 'config.ini', APPLICATION_ENV);
        Zend_Registry::set('config', $config);
        Zend_Registry::set('env', APPLICATION_ENV);

        // fazemos debugging
        if($config->debug) {
            error_reporting(E_ALL | E_STRICT);
            ini_set('display_errors', 'on');
        }

        return Zend_Registry::get('config');
    }
}

<?php

/**
 *
 * @uses
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */
class Sbm_Class_Date {

    /**
     * 
     * EX: "<?= $this->getDate($this->noticia->dataModificacao, "j F, Y") ?>"
     * http://framework.zend.com/manual/1.12/en/zend.date.constants.html
     * 
     * @param Zend_Date $date
     * @param type $string
     */
    function getDate($idioma_iso, $date, $string) {
        $idiomaBD = new Model_DbTable_Idiomas();
        $idioma_locale = $idiomaBD->getDateByIso($idioma_iso);
        $locale = new Zend_Locale($idioma_locale);
        Zend_Date::setOptions(array('format_type' => 'php'));
        $date = new Zend_Date($date, false, $locale);

        // outputs something like 'February 16, 2007, 3:36 am'
        return $date->toString($string);
    }

}

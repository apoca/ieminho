<?php

/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class Sbm_Class_Newsletter {

    protected $_translate;
    protected $_config;

    public function __construct() {
        // translate content
        $this->_translate = Zend_Registry::get('Zend_Translate');
        // config
        $this->_config = Zend_Registry::get('config');
    }

    public function gerarNewsletter($destaque_id, $listagens_ids, $idioma_iso = 'pt') {
        $config = new Model_DbTable_Configuracoes();
        // create view object
        $html = new Zend_View();
        $html->setScriptPath(MODULE_PATH . '/admin/layouts/newsletter/');

        if (isset($destaque_id)) {
            
        } else {
            $destaque_id = null;
        }

        if (isset($listagens_ids)) {
            
        } else {
            $listagens_ids = null;
        }

        // conteúdos
        $conteudo_destaque = $this->gerarDestaqueNewsletter($destaque_id, $idioma_iso);
        $conteudo_listagem = $this->gerarListagemNewsletter($listagens_ids, $idioma_iso);

        // assign values
        $html->assign('newsletter_baseurl', $config->optionValue('url'));
        $html->assign('newsletter_logo_site', "{$config->optionValue('url')}/images/admin/newsletter/header_news_{$idioma_iso}.jpg");
        $html->assign('newsletter_destaque_site', $conteudo_destaque);
        $html->assign('newsletter_listagem_site', $conteudo_listagem);
        $html->assign('newsletter_titulo_site', ucfirst($config->optionValue('title')));
        $html->assign('newsletter_morada_site', ucfirst($config->optionValue('address')));

        $bodyText = $html->render('newsletter.phtml');

        return $bodyText;
    }

    public function gerarDestaqueNewsletter($conteudo_id, $idioma_iso) {
        $conteudosTable = new Model_DbTable_Conteudos();
        $date = new Sbm_Class_Date();
        $config = new Model_DbTable_Configuracoes();

        if (isset($conteudo_id)) {
            $conteudo = $conteudosTable->obterConteudo($conteudo_id, $idioma_iso);
            $img = $conteudosTable->obterMediaFilesByConteudoID($conteudo_id, 'image');

            if (isset($img[0])) {
                $imagem_destaque = "<img alt='{$conteudo->designacao}' src='{$config->optionValue('url')}/image.php?image={$config->optionValue('url')}/imagens/conteudos/{$img[0]}&witdh=760'>";
            } else {
                $imagem_destaque = "";
            }

            $html = "<div style='width: 700px; display: block; overflow: hidden;'>
                        <h3 style='width: 450px; font-size: 16px;'>{$conteudo->designacao}</h3>
                        <p class='date' style='color: #79d5fa; margin: 10px 0 10px 0;'>{$date->getDate($idioma_iso, $conteudo->data, 'l , j F Y')}</p>
                            {$imagem_destaque}
                        <p style='margin: 40px 0 0 0;'>
                            {$conteudo->texto}
                        </p>
                        <p class='read_more' style='color: #79d5fa; margin: 10px 0 50px 0;'>
                            <a style='cursor: pointer; color: #000; text-decoration: none;' href='{$config->optionValue('url')}/{$idioma_iso}/{$conteudo_id}' title='{$conteudo->designacao}' target='_blank'>ver mais</a>
                        </p>
                    </div>";
        } else {
            $html = "Ainda sem destaque";
        }

        return $html;
    }

    public function gerarListagemNewsletter($listagens_ids, $idioma_iso) {
        $conteudosTable = new Model_DbTable_Conteudos();
        $tiposConteudosTable = new Model_DbTable_TiposConteudos();
        $valoresAtributosReferenciaTable = new Model_DbTable_ValoresAtributosReferencia();
        $atributosTiposConteudosTable = new Model_DbTable_AtributosTiposConteudos();
        $date = new Sbm_Class_Date();
        $config = new Model_DbTable_Configuracoes();

        if (isset($listagens_ids) && is_array($listagens_ids)) {
            $li_n_aplicavel = null;
            foreach ($listagens_ids as $key => $id) {
                //echo $id . '<br />';exit();
                $conteudo = $conteudosTable->obterConteudo($id, $idioma_iso);

                $categoria = $valoresAtributosReferenciaTable->fetchRow($valoresAtributosReferenciaTable->select()->where('conteudo_id = ?', $id));

                // do tipo categoria
                if (count($categoria) > 0) {
                    // é um conteudo do tipo - não aplicável -
                    if ($categoria->valor == null) {
                        $li_n_aplicavel .= "<li style='width: 400px; margin: 0; padding: 20px 0 0 0; border-bottom: 1px solid #d8d8d8;'>
                                            <a style='cursor: pointer; color: #000; text-decoration: none;' href='{$config->optionValue('url')}/{$idioma_iso}/{$conteudo->id}' title='{$conteudo->designacao}' target='_blank'>
                                                {$conteudo->designacao}
                                                <p style='color: #79d5fa;'>{$date->getDate($idioma_iso, $conteudo->data, 'l , j F Y')}</p>
                                            </a>
                                        </li>";
                    } else {
                        $categorias[] = array("valor_nome" => "{$categoria->valor}", 'valor_id' => $id);
                        //$html .= "{$categoria->valor} - <br />";
                    }
                } else {
                    $tipoConteudo = $tiposConteudosTable->getTipoConteudo($conteudo->tipo_id);
                    $tipos[] = array('tipo_id' => $tipoConteudo['id'], "tipo_nome" => "{$tipoConteudo['designacao']}", 'conteudo_id' => $id);
                    //$html .= "{$conteudo->tipo_id} - {$tipoConteudo['designacao']}<br />"; 
                }
            }

            if (isset($li_n_aplicavel)) {
                $html .= "<ul id='boletim' style='padding: 0; margin: 0 0 0 30px; width: 400px; list-style-type: none; border-top: 3px solid #00adf2;'>
                            {$li_n_aplicavel}
                          </ul>";
            }

            if (isset($categorias) && is_array($categorias)) {
                $atributosTiposConteudos = $atributosTiposConteudosTable->getAtributoTipoConteudoByName();
                $valoresCampoArray = explode(',', $atributosTiposConteudos->valoresCampo);

                foreach ($valoresCampoArray as $key => $cat_valor) {

                    $html .= "<ul id = 'boletim' style='padding: 0; margin: 0 0 0 30px; width: 400px; list-style-type: none; border-top: 3px solid #00adf2;'>";
                    $aux = 0;
                    foreach ($categorias as $i => $row) {
                        $conteudo = $conteudosTable->obterConteudo($row['valor_id'], $idioma_iso);
                        if ($cat_valor == $row['valor_nome']) {
                            if ($aux == 0) {
                                $html .= "<div id='boletim_title' style='width: 190px; height: 25px; margin: 0; padding: 0 0 0 10px; background-color: #00adf2; color: white; font-size: 15px;'>
                                " . ucfirst($cat_valor) . "
                              </div>";
                            }
                            $aux++;
                            $html .= "<li style=' width: 400px; margin: 0; padding: 20px 0 0 0; border-bottom: 1px solid #d8d8d8;'>
                                        <a style='cursor: pointer; color: #000; text-decoration: none;' href='{$config->optionValue('url')}/{$idioma_iso}/{$conteudo->id}' title='{$conteudo->designacao}' target='_blank'>
                                        {$conteudo->designacao}
                                        <p style='color: #79d5fa;'>{$date->getDate($idioma_iso, $conteudo->data, 'l , j F Y')}</p>
                                        </a>
                                      </li>";
                        }
                    }
                    $html .= "</ul>";
                }
            }

            if (isset($tipos) && is_array($tipos)) {
                $todosTiposConteudos = $tiposConteudosTable->getTiposConteudos();

                foreach ($todosTiposConteudos as $key2 => $todosTipos) {

                    $html .= "<ul id = 'boletim' style='padding: 0; margin: 0 0 0 30px; width: 400px; list-style-type: none; border-top: 3px solid #00adf2;'>";
                    $aux = 0;
                    foreach ($tipos as $i2 => $tipo) {

                        if ($todosTipos['designacao'] == $tipo['tipo_nome']) {
                            if ($aux == 0) {
                                $html .= "<div id='boletim_title' style='width: 190px; height: 25px; margin: 0; padding: 0 0 0 10px; background-color: #00adf2; color: white; font-size: 15px;'>
                                " . ucfirst($todosTipos['designacao']) . "
                              </div>";
                            }
                            $aux++;
                            $conteudo = $conteudosTable->obterConteudo($tipo['conteudo_id'], $idioma_iso);
                            $html .= "<li style='width: 400px; margin: 0; padding: 20px 0 0 0; border-bottom: 1px solid #d8d8d8;'>
                                        <a style='cursor: pointer; color: #000; text-decoration: none;' href='{$config->optionValue('url')}/{$idioma_iso}/{$conteudo->id}' title='{$conteudo->designacao}' target='_blank'>
                                        {$conteudo->designacao}
                                        <p style='color: #79d5fa;'>{$date->getDate($idioma_iso, $conteudo->data, 'l , j F Y')}</p>
                                        </a>
                                      </li>";
                        } else {
                            //$html .= "<li>todosTipos: {$todosTipos['designacao']} : {$tipo['tipo_nome']}</li>";
                        }
                    }
                    $html .= "</ul>";
                }
            }
        } else {
            $html = "Ainda sem listagem de conteúdos";
        }

        return $html;
    }

    public function conteudosDeEnvio() {
        $tiposConteudosTable = new Model_DbTable_TiposConteudos();
        $tiposConteudos = $tiposConteudosTable->getTiposConteudos();

        $result = null;
        if (count($tiposConteudos)) {
            $common = new Sbm_Common();
            $conteudosTable = new Model_DbTable_Conteudos();
            $atributosTiposConteudosTable = new Model_DbTable_AtributosTiposConteudos();
            $valoresAtributosReferenciaTable = new Model_DbTable_ValoresAtributosReferencia();
            $atributosTiposConteudos = $atributosTiposConteudosTable->getAtributoTipoConteudoByName('categoria');
            $valoresCampoArray = explode(',', $atributosTiposConteudos->valoresCampo);
            //Zend_Debug::dump($atributosTiposConteudos);
            //echo $atributosTiposConteudos->tipo_id;
            $result .= "<div id='lista-newsletter'>";
            $result .= "<form name='newsletter-tipos' action='' method='POST'>";
            foreach ($tiposConteudos as $key => $tipoConteudo) {
                $result .= "<div id='tipo-conteudo-{$tipoConteudo['id']}'>";
                if ($tipoConteudo['id'] == $atributosTiposConteudos->tipo_id) {
                    $conteudos_vazios = $valoresAtributosReferenciaTable->getConteudoByValor($valor = '');
                    $result .= "<span class='cat'>{$tipoConteudo['designacao']} -- Não Aplicável -- </span><br /><br />";

                    if (count($conteudos_vazios) > 0) {
                        $result .= "<ul id='conteudos-{$tipoConteudo['id']}'>";
                        foreach ($conteudos_vazios as $conteudo) {
                            $aux = $conteudosTable->obterConteudo($conteudo['conteudo_id'], 'pt');
                            $result .= "<li id='{$conteudo['conteudo_id']}'><input type=\"checkbox\" name=\"subtipos\" value=\"{$conteudo['conteudo_id']}\" /> " . $common->contaPalavras($aux->designacao, 42, '...') . "</li>";
                        }
                        $result .= "</ul>";
                    }

                    $result .= "<span class='sub-cat'>{$atributosTiposConteudos->labelPlural} de {$tipoConteudo['designacao']}:</span><br /><br />";
                    $result .= "<ul id='sub-tipos'>";
                    foreach ($valoresCampoArray as $key => $category) {
                        $conteudos_categorias = $valoresAtributosReferenciaTable->getConteudoByValor($valor = $category);
                        $result .= "<li id='{$key}'>";
                        $result .= "<span class='sub-cat'>" . ucfirst($category) . "</span> <br /><br />";

                        if (count($conteudos_categorias) > 0) {
                            $result .= "<ul id='conteudos-{$tipoConteudo['id']}'>";
                            foreach ($conteudos_categorias as $conteudo) {
                                $categoria = $valoresAtributosReferenciaTable->fetchRow($valoresAtributosReferenciaTable->select()->where('conteudo_id = ?', $conteudo['conteudo_id']));
                                $aux = $conteudosTable->obterConteudo($conteudo['conteudo_id'], 'pt');
                                $result .= "<li id='{$conteudo['conteudo_id']}'><input type=\"checkbox\" name=\"subtipos\" value=\"{$conteudo['conteudo_id']}\" /> " . $common->contaPalavras($aux->designacao, 42, '...') . "</li>";
                            }
                            $result .= "</ul>";
                        }

                        $result .= "</li>";
                    }
                    $result .= "</ul>";
                } else {
                    $conteudos_gerais = $conteudosTable->obterConteudoBlog($tipoConteudo['id'], 'pt');
                    $result .= "<span class='cat'>{$tipoConteudo['designacao']}</span> <br /><br />";

                    if (count($conteudos_gerais) > 0) {
                        $result .= "<ul id='conteudos-{$tipoConteudo['id']}'>";
                        foreach ($conteudos_gerais as $conteudo) {
                            $aux = $conteudosTable->obterConteudo($conteudo->id, 'pt');
                            $result .= "<li id='{$conteudo->id}'><input type=\"checkbox\" name=\"subtipos\" value=\"{$conteudo->id}\" /> " . $common->contaPalavras($aux->designacao, 42, '...') . "</li>";
                        }
                        $result .= "</ul>";
                    }
                }
                $result .= "</div>";
                //$result .= "<input type=\"checkbox\" name=\"chk_group[]\" value=\"value3\" />{$atributosTiposConteudos->name}<br />"; 
                //$result .= $tipoConteudo['id'].'\n';
            }
            $result .= "</form>";
            $result .= "</div>";
        } else {
            $result = "Sem tipos de conteúdos definidos!";
        }

        return $result;
    }

    public function emailSubscrever($subscritor_id) {
        $mail = new Sbm_Class_Mail();
        $config = new Model_DbTable_Configuracoes();
        $newsletterSubscritoresTable = new Model_DbTable_NewsletterSubscritores();
        $subscritor = $newsletterSubscritoresTable->getSubscritorbyId($subscritor_id);

        $subject = $this->_translate->_('_NEWSLETTER_SUBJECT_SUBSCRIBE');
        $message = "{$this->_translate->_('_NEWSLETTER_CONTENT_SUBSCRIBE')}: <a href='{$config->optionValue('url')}/index/newsletter/acao/subscrever/guid/{$subscritor->code}/' title='{$config->optionValue('url')}/index/newsletter/acao/subscrever/guid/{$subscritor->code}/' target='_blank'>{$config->optionValue('url')}/index/newsletter/acao/subscrever/guid/{$subscritor->code}/</a> ";

        $mail->sendNewsletter($acao = 'subscrever', $subscritor, $subject, $message);
    }

    public function emailRemover($guid) {
        $mail = new Sbm_Class_Mail();
        $config = new Model_DbTable_Configuracoes();
        $newsletterSubscritoresTable = new Model_DbTable_NewsletterSubscritores();
        $subscritor = $newsletterSubscritoresTable->getSubscritorbyGuid($guid);

        $subject = $this->_translate->_('_NEWSLETTER_SUBJECT_REMOVE');
        $message = "{$this->_translate->_('_NEWSLETTER_CONTENT_REMOVE')}: <a href='{$config->optionValue('url')}/index/newsletter/acao/subscrever/guid/{$subscritor->code}/' title='{$config->optionValue('url')}/index/newsletter/acao/subscrever/guid/{$subscritor->code}/' target='_blank'>{$config->optionValue('url')}/index/newsletter/acao/subscrever/guid/{$subscritor->code}/</a> ";

        $mail->sendNewsletter($acao = 'remover', $subscritor, $subject, $message);
    }

}
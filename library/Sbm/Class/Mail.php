<?php

/**
 *
 * @uses
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */
class Sbm_Class_Mail {

    protected $_secure;
    protected $_smtp;
    protected $_from;
    protected $_transport;
    protected $_email;
    protected $_password;
    protected $_port;
    protected $_translate;
    protected $_config;

    public function __construct($type) {
        // Config EMail
        $emailsTable = new Model_DbTable_AdminEmails();
        $config = new Model_DbTable_Configuracoes();
        $this->_translate = Zend_Registry::get('Zend_Translate');
        $this->_config = Zend_Registry::get('config');

        if (isset($type)) {
            $email = $emailsTable->getMailByType($type);
        } else {
            $email = $emailsTable->getMailByType($type = 'noreply');
        }

        $this->_smtp = $email->servidor;
        $this->_secure = $email->seguranca;
        $this->_from = ucfirst($config->optionValue('title'));
        $this->_email = $email->email;
        $this->_password = $email->password;
        $this->_port = $email->port;

        /**
         * Relay significa que enviamos o email para o servidor do cliente e este trata do relay de email
         * SMTP significa que o envio carece de login e password (por defeito)
         */
        if ($this->_config->mail->service == 'relay') {
            $config = array(
                'port' => $this->_port
            );
        } else {
            $config = array(
                'auth' => 'login',
                'ssl' => $this->_secure,
                'port' => $this->_port,
                'username' => $this->_email,
                'password' => $this->_password
            );
        }

        $this->_transport = new Zend_Mail_Transport_Smtp($this->_smtp, $config);
    }

    public function send($to, $subject, $message) {
        try {
            $mail = new Zend_Mail('utf-8');
            $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
            $mail->addHeader('X-MailGenerator', 'SBM v2.0');
            $mail->addHeader('X-Priority', '3');
            $mail->setFrom($this->_email, $this->_from);
            $mail->addTo($to);
            $mail->setBodyHtml($message);
            $mail->setSubject($subject);
            $mail->send($this->_transport);
            return true;
        } catch (Exception $e) {
            echo ($e->getMessage());
            return false;
        }
    }

    public function sendNewsletter($acao, $subscritor, $subject, $message) {
        $this->_translate = Zend_Registry::get('Zend_Translate');
        
        $config = new Model_DbTable_Configuracoes();
        // create view object
        $html = new Zend_View();
        $html->setScriptPath(MODULE_PATH . '/admin/layouts/newsletter/');

        // assign values
        $html->assign('mail_header_hi', ucfirst($this->_translate->_('_NEWSLETTER_BODY_HI')));
        $html->assign('mail_header_name', $subscritor->nome);
        $html->assign('mail_header_title', $config->optionValue('title'));
        $html->assign('mail_url', $config->optionValue('url'));
        $html->assign('mail_footer_info_removal1', ucfirst($this->_translate->_('_NEWSLETTER_SUBJECT_REMOVAL1')));
        $html->assign('mail_footer_info_removal2', ucfirst($this->_translate->_('_NEWSLETTER_SUBJECT_REMOVAL2')));
        $html->assign('mail_footer_info_removal3', ucfirst($this->_translate->_('_NEWSLETTER_SUBJECT_REMOVAL3')));
        $html->assign('mail_footer_info_removal_email', $subscritor->email);
        $html->assign('mail_body', $message);

        // render view
        if ($acao == 'subscrever') {
            $html->assign('mail_footer_info_removal_link', "{$config->optionValue('url')}/index/newsletter/acao/remover/guid/{$subscritor->code}/");
            $bodyText = $html->render('standard.phtml');
        } elseif ($acao == 'remover') {
            $html->assign('mail_footer_info_removal_link', "{$config->optionValue('url')}/index/newsletter/acao/remover/guid/{$subscritor->code}/");
            $bodyText = $html->render('standard.phtml');
        } elseif ($acao == 'enviar') {
            $html->assign('mail_footer_info_removal_link', "{$config->optionValue('url')}/index/newsletter/acao/remover/guid/{$subscritor->code}/");
            $bodyText = $html->render('standard.phtml');
        } elseif ($acao == 'teste') {
            $html->assign('mail_footer_info_removal_link', "");
            $bodyText = $html->render('standard.phtml');
        }

        // send notification email by type
        return $this->send($to = $subscritor->email, $subject, $bodyText);
    }

}

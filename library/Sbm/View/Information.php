<?php

class Sbm_View_Information
{
    private $_ns;
    private $_information;

    public function __construct()
    {

        if (Zend_Registry::isRegistered('information')) {
            $this->_information = Zend_Registry::get('information');
        } else {
            $m = new Zend_Session_Namespace('information');
            if (isset($m->information)) {
                $this->_information = $m->information;
            }
        }
    }

    public function clear()
    {
        unset($this->_information);
        $this->_updateNs();  
    }

    public function add($information)
    {
        $this->_information = $information;
        $this->_updateNs();
    }

    public function hasinformation()
    {
        if ($this->_information) {
            return true;
        }
    }

    public function get()
    {
        return $this->_information;
    }

    public function getLast()
    {
        $this->_updateNs();
        $m = (array)$this->_information;
        $last =  end($m);
        $this->clear();
        return $last;
    }

    private function _updateNs()
    {
        $m = new Zend_Session_Namespace('information');
        if (isset($this->_information)) {
            Zend_Registry::set('information',$this->_information);
            $m->information = $this->_information;
        } else {
            unset($m->information);
        }
    }
}
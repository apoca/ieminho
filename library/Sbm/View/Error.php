<?php
/**
 *
 * @uses       
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */

class Sbm_View_Error
{
    /**
     * the storage
     *
     * @var zend_session_namespace
     */
    private $_ns;

    /**
     * the errors stack
     *
     * @var array
     */
    private $_errors;

    /**
     * set up the session namespace and load the errors stack
     *
     */
    public function __construct()
    {
        $this->_ns = new Zend_Session_Namespace('errors');
        if (isset($this->_ns->errors)) {
            $this->_errors = $this->_ns->errors;
        }
    }

    /**
     * clear the errors stack
     *
     */
    public function clear()
    {
        unset($this->_errors);
        $this->_updateNs();
    }

    /**
     * add an error to the stack
     *
     * @param string $error
     */
    public function add($error)
    {
        $this->_errors[] = $error;
        $this->_updateNs();
    }

    /**
     * check to see if any errors are set
     *
     * @return bool
     */
    public function hasErrors()
    {
        if (count($this->_errors) > 0) {
            return true;
        }
    }

    /**
     * get the errors stack
     *
     * @return string
     */
    public function get()
    {
        return $this->_errors;
    }

    public function getLast()
    {
        $this->_updateNs();
        $erros = (array)$this->_ns->errors;
        $last =  end($erros);
        $this->clear();
        return $last;
    }

    /**
     * update the storage
     *
     */
    private function _updateNs()
    {
        if (isset($this->_errors)) {
            $this->_ns->errors = $this->_errors;
        } else {
            unset($this->_ns->errors);
        }
    }
}
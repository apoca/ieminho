<?php

class Sbm_View_warning
{
    private $_ns;
    private $_warning;

    public function __construct()
    {

        if (Zend_Registry::isRegistered('warning')) {
            $this->_warning = Zend_Registry::get('warning');
        } else {
            $m = new Zend_Session_Namespace('warning');
            if (isset($m->warning)) {
                $this->_warning = $m->warning;
            }
        }
    }

    public function clear()
    {
        unset($this->_warning);
        $this->_updateNs();  
    }

    public function add($warning)
    {
        $this->_warning = $warning;
        $this->_updateNs();
    }

    public function haswarning()
    {
        if ($this->_warning) {
            return true;
        }
    }

    public function get()
    {
        return $this->_warning;
    }

    public function getLast()
    {
        $this->_updateNs();
        $m = (array)$this->_warning;
        $last =  end($m);
        $this->clear();
        return $last;
    }

    private function _updateNs()
    {
        $m = new Zend_Session_Namespace('warning');
        if (isset($this->_warning)) {
            Zend_Registry::set('warning',$this->_warning);
            $m->warning = $this->_warning;
        } else {
            unset($m->warning);
        }
    }
}
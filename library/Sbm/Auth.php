<?php
/**
 *
 * @uses       Sbm_
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */
class Sbm_Auth
{
    /**
     * auth adapter
     *
     * @var zend_auth_adapter
     */
    protected $_authAdapter;

    private $_dbAdapter;

    /**
     * the passed username
     *
     * @var string
     */
    private $_username;

    /**
     * the passed password
     *
     * @var string
     */
    private $_password;

    /**
     * the user session storage
     *
     * @var zend_session_namespace
     */
    private $_storage;

    /**
     * the table that contains the user credentials
     *
     * @var string
     */
    protected  $_userTable = "utilizadores";

    /**
     * the indentity column
     *
     * @var string
     */
    protected  $_identityColumn = "login";

    /**
     * the credential column
     *
     * @var string
     */
    protected  $_credentialColumn = "password";

    const USER_NAMESPACE = 'userData';

    protected $_resultRowColumns = array('id', 'role_id', 'estado', 'username');

    /**
     * construir a solicitação de login
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username = null, $password = null)
    {
        // Configuração da autenticação
        $this->_dbAdapter = clone(Zend_Db_Table::getDefaultAdapter());
        $this->_dbAdapter->setFetchMode(zend_db::FETCH_ASSOC);

        $this->_authAdapter = new Zend_Auth_Adapter_DbTable($this->_dbAdapter, $this->_userTable, $this->_identityColumn, $this->_credentialColumn);

        $this->_username = $username;
        $this->_password = sha1($password);
        //$this->_rememberMe = $rememberMe;

        // configurar o "storage"
        // @todo: eu não posso chegar zend para persistir as identidades, por alguma razão .. descobrir o porquê
        $this->_storage = new Zend_Session_Namespace(self::USER_NAMESPACE);
    }

    /**
     * autenticar o pedido
     *
     * @return zend_auth_response
     */
    public function authenticate()
    {
        // Dados para autenticação
        $this->_authAdapter->setIdentity($this->_username);
        $this->_authAdapter->setCredential($this->_password);
        //$this->_authAdapter->setAmbiguityIdentity('isActive = 1'); // Verify if user is active

        $result = $this->_authAdapter->authenticate();

        if ($result->isValid()) {
            // Regista e guarda os dados de utilizador em sessão
            $row = $this->_authAdapter->getResultRowObject($this->_resultRowColumns);
            $this->_storage->user = $row;

            // Regista na tabela de acessos o respectivo log
            $logsTable = new Core_Models_LogsAcesso();
            
            $data = array(
                        'user_id'      => $this->getIdentity()->id_user,
                        'uri' 	       => $_SERVER['REQUEST_URI'],
                        'ip'           => $_SERVER['REMOTE_ADDR'],
                        'hostname'     => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                        'referer'      => $_SERVER['HTTP_REFERER'],
                        'browser'      => $_SERVER['HTTP_USER_AGENT'],
                        'software'     => $_SERVER['SERVER_SOFTWARE']
                    );
           // Registar logs
           $logsTable->add($data);

           // actualizar estado do utilizador
           $userTable = new Core_Models_Utilizadores();
           $userTable->toogleOnline($this->getIdentity()->id_user);

        }
        return $result;
    }

    /**
     * obtem a identidade do utilizador atual, se ela existir
     *
     * @return zend_auth_response
     */
    public static function getIdentity()
    {
        $storage = new Zend_Session_Namespace(self::USER_NAMESPACE);
        if (isset($storage->user)) {
            return $storage->user;
        }
    }

    /**
     * destrói a sessão atual do utilizador
     */
    public static function destroy()
    {
        $storage = new Zend_Session_Namespace(self::USER_NAMESPACE);
        $storage->unsetAll();
    }

}
<?php

/**
 *
 * @uses
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */

/**
 * Description of Upload
 *
 * @author MiguelVieira
 */
class Sbm_Upload {

    private $upload;
    private $full_path;
    private $file_ext;

    /*
     * Code:
     * 1 - Sell
     */

    public function __construct($last_inserted, $path_dir, $num_files) {
	$this->upload = new Zend_File_Transfer_Adapter_Http();
	$this->upload->addValidator('Extension', false, 'jpg,jpeg,png,gif,pdf')
		->addValidator('Count', false, array('min' => 1, 'max' => $num_files));

	$gen_uid = new Sbm_Utility_Uuid();
	$files = $this->upload->getFileInfo();

	foreach ($files as $fileID => $fileInfo) {
	    if (isset($fileInfo['name'])) {
		// get the file name of upload file
		$file_ext = $this->getExtension($fileInfo['name']);
		$this->file_ext = $file_ext;
		// generate a randon uid code with extension file
		$new_name = $gen_uid->generate() . '.' . $file_ext;

		$this->full_path = $path_dir . $new_name;

		if (!is_dir($path_dir)) {
		    mkdir($path_dir, 0755, true);
		}
		// rename file
		$this->upload->addFilter(new Zend_Filter_File_Rename(array('target' => $this->full_path, 'overwrite' => true)), null, $fileID);

		$this->upload->receive($fileID);

//		$work = new Sbm_ImgResizer($this->full_path); // me.jpg (800x600) is in directory ‘img’ in the same path as this php script.
//		$work->resize(800, $this->full_path, $this->file_ext); // the old me.jpg (800x600) is now replaced and overwritten with a smaller me.jpg (400x300).
		// To store resized image to a new file thus retaining the 800x600 version of me.jpg, go with this instead:
		// $work -> resize(400, 'img/me_smaller.jpg');

		if (file_exists($this->full_path)) {
                    $this->add($last_inserted, $fileInfo, $new_name);
		}
	    }
	}

	return $this;
    }

    public function getExtension($name) {
	$names = explode(".", $name);
	return $names[count($names) - 1];
    }


    public function add($last_inserted, $fileInfo, $new_name) {
	try {
	    $data = array(
		'utilizador_id' => $last_inserted,
		'doc_name' => $new_name,
		'doc_type' => $fileInfo['type'],
		'doc_size' => $fileInfo['size']
	    );
	    $documentosTable = new Model_DbTable_UtilizadoresDocumentos();
	    $documentosTable->addDoc($data);
	} catch (Exception $e) {
	    throw new Exception($e->getMessage());
	}
    }

    public function isValid() {
	return $this->upload->isValid();
    }

    public function getMessages() {
	return $this->upload->getMessages();
    }

    public function upload() {
	try {
	    $this->upload->receive();
	} catch (Zend_File_Transfer_Exception $e) {
	    //This is a tad dirty
	    throw new Exception($e->getMessage());
	}
	return $this;
    }

    public function getUpload() {
	return $this->upload;
    }

}

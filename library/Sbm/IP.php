<?php

/**
 * @uses       Zend_Controller_Action
 * @package    SBM - Sétima backOffice Manager
 * @subpackage Controller
 */
class Sbm_IP {

    public function __construct() {
        
    }

    /**
     * Função para obter IP
     * @param void
     * @return $string
     */
    function getIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
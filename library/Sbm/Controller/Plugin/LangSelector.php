<?php
/**
 *
 * @uses       Zend_Controller_Plugin_Abstract
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */
class Sbm_Controller_Plugin_LangSelector extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $lang = $request->getParam('lang','');
        $config = Zend_Registry::get('config');
        $session = new Zend_Session_Namespace('session');
        $module = $request->getModuleName();

        if(!empty($lang) && $lang != ":lang" && $lang != ":lang:lang")
        {
            $lang_1 = $lang;

            // register the session and set the cookie
            $session->lang = $lang_1;

            setcookie("lang", $lang, time() + (3600 * 24 * 30));
        }
        else if(isset($session->lang))
        {
            $lang = $session->lang;
        }
        else if(isset($_COOKIE['lang']))
        {
            $lang = $_COOKIE['lang'];
        }
        else
        {
            $locale = $config->resources->locale->default;
            $lang = isset($session->lang) ? $session->lang : substr($locale, 0, 2);
            $session->lang = $lang;

            $zl = new Zend_Locale();
            $zl->setLocale($locale);
            Zend_Registry::set('Zend_Locale', $zl);
        }

	$translate = new Zend_Translate('csv', SBM_LANG_PATH . DS . $lang.'.csv' , $lang);
	Zend_Registry::set('Zend_Translate', $translate);

    }

}
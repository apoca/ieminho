<?php

class Sbm_Controller_Plugin_Authorization extends Zend_Controller_Plugin_Abstract
{
    private $_auth;
    private $_acl;
    private $_user;

    public function __construct(Zend_Auth $auth, Zend_Acl $acl)
    {
        $this->_auth = $auth;
        $this->_acl = $acl;
        $this->_user = Sbm_Auth::getIdentity();
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if($this->_user){
            $usersTable = new Core_Models_Utilizadores();   
            $userData = $usersTable->getUserById($this->_user->id);
            
            $role = $userData->role;
        }else{
            $role = 'guest';
        }

        $isAllowed = Sbm_RuleChecker::isAllowed($role, $request->getActionName(), $request->getControllerName(), $request->getModuleName(),  $callback = null, $params = null);

        if (!$isAllowed && $role == 'guest') {

            /**
             * DON'T use redirect! as folow:
             * $this->getResponse()->setRedirect('/Login/');
             */
            $request->setModuleName('core')
                    ->setControllerName('login')
                    ->setActionName('index')
                    ->setDispatched(true);
        }

    }
}
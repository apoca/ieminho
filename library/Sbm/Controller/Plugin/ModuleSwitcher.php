<?php 
/**
 *
 * @uses       Zend_Controller_Plugin_Abstract
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */
class Sbm_Controller_Plugin_ModuleSwitcher extends Zend_Controller_Plugin_Abstract
{
    protected $_baseIncludePath;
    protected $_view = null;

    public function __construct()
    {
        $this->_baseIncludePath = get_include_path();
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        //load config
        $config = Zend_Registry::get('config');
		
        // Changes the Layout based on the module name
        $moduleName = $request->getModuleName();
        $controllerName = $request->getControllerName();

        $frontController = Zend_Controller_Front::getInstance();
        $controllerDirectory = $frontController->getControllerDirectory($moduleName);

        $moduleDirectory = dirname($controllerDirectory);
        $formsDirectory  = $moduleDirectory . '/forms/';
        $modelsDirectory  = $moduleDirectory . '/models/';
        
        Zend_Layout::startMvc();
        $layout = Zend_Layout::getMvcInstance();  
        /*
         *  Verify the layout to load
         */
        if(strtolower($moduleName) != "default"){
            $layout->setLayoutPath(SBM_TEMPLATES_PATH . '/' . 'admin' . '/layouts')->setLayout('admin');
        }else{
            $layout->setLayoutPath(SBM_TEMPLATES_PATH . '/' . $moduleName . '/layouts')->setLayout($moduleName);
        }

        set_include_path($modelsDirectory . PATH_SEPARATOR . $formsDirectory . PATH_SEPARATOR . $this->_baseIncludePath);

        return $request;
    }

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (!$request->isDispatched())
        {
            return;
        }   
    }
}

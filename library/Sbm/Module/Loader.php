<?php
/**
 * SBM
 *
 * LICENSE
 *
 *
 * @copyright	Copyright (c) 2007-2010 Sétima - Tecnologias da Informação e Comunicação, Lda
 * @license     Sétima Backoffice Madular
 * @version 	2.0
 */

class Sbm_Module_Loader
{
    /**
     * @var Sbm_Module_Loader
     */
    private static $_instance;

    /**
     * @var array
     */
    private $_moduleNames;

    /**
     * @return Sbm_Module_Loader
     */
    public static function getInstance()
    {
        if (null == self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        $this->_moduleNames = $this->_getModules();
    }

    public function getModuleNames()
    {
        return $this->_moduleNames;
    }

    /**
     * @return Zend_Controller_Router_Interface
     */
    public function getRoutes()
    {
        if (null == $this->_moduleNames) {
                return;
        }
        $router = new Zend_Controller_Router_Rewrite();

        foreach ($this->_moduleNames as $name) {
            $configFiles = $this->_loadRouteConfigs($name);

            foreach ($configFiles as $file) {
                $config = new Zend_Config_Ini($file, 'routes');
                $router->addConfig($config, 'routes');
            }
        }

        return $router;
    }

    /**
     * @return array
     */
    private function _getModules()
    {
        return Sbm_Utility_File::getSubDir(SBM_APPLICATION_PATH . DS . 'modules');
    }

    /**
     * @return array
     *
     * Caso seja necessário um módulo ter uma config é possivel "carregar" a mesma através do método a seguir
     */
    private function _loadRouteConfigs($moduleName)
    {
        $dir = SBM_APPLICATION_PATH . DS . 'modules' . DS . $moduleName . DS . 'config' . DS . 'routes';
        if (!is_dir($dir)) {
                return array();
        }

        $configFiles = array();

        $dirIterator = new DirectoryIterator($dir);

        foreach ($dirIterator as $file) {
            if ($file->isDot() || $file->isDir()) {
                continue;
            }
            $name = $file->getFilename();
            if (preg_match('/^[^a-z]/i', $name) || ('CVS' == $name)
                        || ('.svn' == strtolower($name))) {
                continue;
            }
            $configFiles[] = $dir . DS . $name;
        }

        return $configFiles;
    }
}

<?php
/**
 *
 * @uses       
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */

class Sbm_Module_Config
{
    public static function getConfig($module)
    {
        $file = SBM_APPLICATION_PATH . DS . 'modules' . DS . $module . DS . 'config' . DS . 'config.ini';
        if (!file_exists($file)) {
                return null;
        }
        $file = new Zend_Config_Ini($file);
        return $file;
    }
}

<?php
/**
 *
 * @uses
 * @package    SBM - Sétima BackOffice Manager
 * @subpackage Controller
 * @version    2.0
 *
 * @company    Sétima - Tecnologias da Informação e Comunicação, Lda
 * @site       http://www.setima.pt
 * @email      info@setima.pt
 *
 * SUPORT APPLICATION
 * @site       http://setima.pt/helpdesk/
 * @email      helpdesk@setima.pt
 * @info       http://setima.pt/setima/contactos
 *
 * REPORT BUGS
 * @site      http://bugs.setima.pt/
 *
 */

class Sbm_Mail
{
    protected $_smtp;

    protected $_from;

    protected $_transport;

    protected $_email;

    protected $_password;

    protected $_port;

    public function __construct($type)
    {
        if($type == null){
            // Config EMail
            $emailsTable = new Core_Models_ConfigEmail();
            $email = $emailsTable->getDefault();
        }

        $this->_smtp = $email->smtp;
        $this->_from = $email->from;
        $this->_email = $email->email;
        $this->_password = $email->password;
        $this->_port = $email->port;

        $config = array('auth' => 'login',
                        'username' => $this->_email,
                        'password' => $this->_password);

        $this->_transport = new Zend_Mail_Transport_Smtp($this->_smtp, $config);

    }

    public function send($to, $subject, $message)
    {
        try {
            $mail = new Zend_Mail('utf-8');
            $mail->setFrom($this->_email, $this->_from);
            $mail->addTo($to);
            $mail->setBodyHtml($message);
            $mail->setSubject($subject);
            $mail->addHeader('X-MailGenerator', 'SBM v2.0');
            $mail->send($this->_transport);
            return true;
        }
        catch (Exception $e)
        {
            echo ($e->getMessage());
            return false;
        }
    }
}

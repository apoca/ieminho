<?php
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
/*error_reporting(~E_ALL);
error_reporting(0);*/
// define these global path constants here
define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);
define('ROOT_PATH' , dirname( dirname( __FILE__ ) ) ) ;
define('LIB_PATH' , ROOT_PATH . '/library' ) ;
define('APPLICATION_PATH' , ROOT_PATH . '/application' ) ;
define('MODULE_PATH' , ROOT_PATH . '/application/modules' ) ;
define('MODELS_PATH' , ROOT_PATH . '/application/models' ) ;
define('DEFAULT_FORMS_PATH', ROOT_PATH . '/application/modules/default/forms');
define('ADMIN_FORMS_PATH', ROOT_PATH . '/application/modules/management/forms');
define('MODULES_CONTROLLERS_MANAGEMENT', ROOT_PATH . DS . 'application' . DS . 'modules' . DS . 'management' . DS . 'controllers');
define('BACKUPSQL_PATH' , ROOT_PATH . '/public_html/backupsql' ) ;
define('DOCUMENT_PATH' , ROOT_PATH . '/public_html/uploads/files/' ) ;
define('TRANSLATE_PATH', ROOT_PATH . '/translate');
define('FCKEDITOR', ROOT_PATH . '/public_html/js/admin/fckeditor');
define('DOMAIN', 'http://backoffice.setima.local/');

// Configuração do email da newsletter
define('MAIL_SMTP', 'smtp.gmail.com');
define('MAIL_NEWSLETTER', 'sp.nunoalmeida@gmail.com');
define('MAIL_PASSWORD', '1424nn09');
define('MAIL_NAME', 'MDresearch');
define('MAIL_SSL', 'tls');
define('MAIL_SSL_PORT', '587');

define('PROFILE_ID', '43891292');


//outros defines 
define('CAMINHO', ROOT_PATH . DS . 'public_html');
define('CAMINHOPDFS', ROOT_PATH . '');
define('WSDL', 'http://backoffice.setima.selfip.com/management/rest?wsdl');

define('GOOGLEMAPSKEY', 'ABQIAAAANrsB9NfXnQWO4I7w9OSJOxS9pDNdR4NtCla-Pw3FoBbxv93lrRRzAJ9giHUWExQYqCwDIGkQu30VXQ');
define('DOMINIO', 'http://backoffice.setima.local');
define('FONTS', ROOT_PATH.'/public_html/fonts');
define('CAPTCHA', ROOT_PATH.'/public_html/images/captcha/');



// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'production'));

// define the path for config.ini
define('CONFIG_PATH' , ROOT_PATH . '/application') ;


// set the php include path
set_include_path(
    APPLICATION_PATH . PATH_SEPARATOR .
    LIB_PATH . PATH_SEPARATOR .
    MODULE_PATH . PATH_SEPARATOR .
    DEFAULT_FORMS_PATH . PATH_SEPARATOR .
    ADMIN_FORMS_PATH . PATH_SEPARATOR .
	FCKEDITOR . PATH_SEPARATOR .
	BACKUPSQL_PATH . PATH_SEPARATOR .
	CAMINHO. PATH_SEPARATOR.
	
    get_include_path()

);
set_include_path('../library/third-party' . PATH_SEPARATOR . get_include_path());

/** Zend_Application */
require_once LIB_PATH.'/Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/config.ini'
);
$application->bootstrap()->run();

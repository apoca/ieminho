$(function() {

    $(window).scroll(function(e) {
        if ($(window).scrollTop() > 260) {
            $(".menuIEM").css("display", "block");
        } else {
            $(".menuIEM").css("display", "none");
        }

    });
    $(".menuDefaultIEM nav ul li form#search img").click(function() {
        $('.pesquisa_imput').fadeIn();
    });
    var widthnav = $('.base').width();
    $(".menuIEM").css("width", widthnav + "px");
    if (widthnav < 565) {
        $(".menuIEM nav ul li.first").css("display", "none");
    } else {
        $(".menuIEM nav ul li.first").css("display", "block");
    }

    if (widthnav < 940) {
        $(".infoBase").css("display", "none");
        $(".destaques").css("display", "none");
    } else {
        $(".infoBase").css("display", "block");
        $(".destaques").css("display", "block");
    }
    if (widthnav < 725) {
        $(".base div.topInfo div.row div.span4").css("display", "none");

    } else {
        $(".base div.topInfo div.row div.span4").css("display", "block");
    }

    $(window).resize(function() {

        var widthnavv = $('.base').width();
        $(".menuIEM").css("width", widthnavv + "px");

        if (widthnavv < 940) {
            $(".infoBase").css("display", "none");
            $(".destaques").css("display", "none");

        } else {
            $(".infoBase").css("display", "block");
            $(".destaques").css("display", "block");
        }
        if (widthnavv < 725) {

            $(".base div.topInfo div.row div.span4").css("display", "none");

        } else {
            $(".base div.topInfo div.row div.span4").css("display", "block");

        }
        if (widthnavv < 565) {
            $(".menuIEM nav ul li.first").css("display", "none");
        } else {
            $(".menuIEM nav ul li.first").css("display", "block");
        }
        console.log(widthnavv);
    });


});
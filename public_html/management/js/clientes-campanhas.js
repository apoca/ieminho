var controller = 'clientes-campanhas';
$(function(){
    
    $('.alert-success').delay(4000).fadeOut("slow");
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
            action: window.location.protocol + "//" + window.location.host + '/management/'+controller+'/imagens',
            name: 'uploadfile',
            onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                            // extension is not allowed 
                            alert(allowedFiles+' JPG, JPEG, PNG, GIF.');
                            return false;
                    }
                    $("#preview").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Uploading...."//>');
            },
            onComplete: function(file, response){
                    var data = $.parseJSON(response);
                    if(data.success===true){
                            $('#preview').html('<img width="121" height="47" src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/clientes/'+data.file+'" alt="" />');
                            $('span#deleteFile').delay(500).fadeIn(500);
                            $("#fileImage").val(data.file);
                    } else{
                            $('#preview').html('Não foi possível carregar o ficheiro.');
                    }
            }
    });
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
    
    $("#deleteFile").click(function() {
        var imagem = imagemNameByURL($("#upload #preview img").attr("src")); 
        
        bootbox.confirm('Tem a certeza que deseja remover a imagem?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/'+controller+'/eliminar-imagem',{imagem: imagem}, 
                function(data){
                    var dadosDevolvidos = data.split("\n");
                    var resultado = dadosDevolvidos[0];
                    if(resultado == 'sucess'){
                        $('span#deleteFile').delay(500).fadeOut(500);
                        $('#preview').html('');
                        $("#fileImage").val('');
                        $("span#imgRepres").css('display', 'block'); 

                    }else{
                        bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                    }
                });
        }});
    
    });
    
    $("#email").change(function(){
        var varEmail = $("#email").val();
        $.post(window.location.protocol + "//" + window.location.host + '/management/'+controller+'/verifica-email',{email: varEmail},
        function(data){
            var dadosDevolvidos = data.split("\n");
            var resultado = dadosDevolvidos[0];
            if(resultado == 's'){
                bootbox.alert('E-mail de cliente já existente ('+varEmail+')');
                $("#email").focus();
                return false;
            }else{
                return true;
            }
        });
    });
    
    $("#login").change(function(){
        var varLogin = $("#login").val();
        $.post(window.location.protocol + "//" + window.location.host + '/management/'+controller+'/verifica-login',{login: varLogin},
        function(data){
            var dadosDevolvidos = data.split("\n");
            var resultado = dadosDevolvidos[0];
            if(resultado == 's'){
                bootbox.alert('Login de cliente já existente ('+varLogin+')');
                $("#login").focus();
                return false;
            }else{
                return true;
            }
        });
    });
    
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    //alert(pathname.toSource());
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        minlength: 2,
	        required: true
	      },
	      email: {
	        required: true,
	        email: true
	      },
              login: {
                minlength: 4,
                required: true
              },
              password: {
                    required: false,
                    minlength: 5
              }
	    },
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });

    
})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
    
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function generatePasswd(){
    $.get(window.location.protocol + "//" + window.location.host + '/management/'+controller+'/gerar-password', function(data){
        bootbox.alert("Nova password gerada: " + data);
        $("#password").val(data);
    });
}
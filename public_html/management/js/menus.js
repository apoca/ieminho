$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
    $('ol.sortable').nestedSortable({
            update : function(serialized) {
                $('ol.sortable').css('opacity', '0.4');
                list = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});

                    $.post(window.location.protocol + "//" + window.location.host + '/management/menus/ordenar',{list: list}, 

                        function(data){
                            var data = $.parseJSON(data);
                            (data.success == false) ? bootbox.alert(data.msg) : '';
                        }
                    );
                $('ol.sortable').css('opacity', '1');
            },
            disableNesting: 'no-nest',
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li',
            maxLevels: 3,
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div'
     });

     $('.fancybox').fancybox();
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        minlength: 2,
	        required: true
	      },
	      link: {
	        required: true
	      }
	    }, 
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });
      
      var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
      var pieces = url.split("/menus/")[1].split("/");
      var action = pieces[0];

      if(action === 'editar'){
          var banner = $("#imgAN.control-group img").attr("src");

          if ($('#imgAN')) {
              if(!banner){
                  $("#uploadImgAN").css('display', 'block');
              }else{
                  $("#uploadImgAN").css('display', 'none');
              }
           }else{
              $("#imgAN").css('display', 'none'); 
          }   
      }
      
      

    
});

$(window).load(function() {
    
      if ($('#ficheirosmedia').is(':visible')) {
            createUploader();
      }
});

function createUploader(){  
        var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
        var pieces = url.split("/menus/")[1].split("/");
        var menu_id = pieces[6];
        var uploader = new qq.FileUploader({
            element: document.getElementById('ficheirosmedia'),
            listElement: document.getElementById('separate-list'),
            action: window.location.protocol + "//" + window.location.host + window.location.pathname,
            params: {
                menu_id: (menu_id!==null ? menu_id : 0)
            },
            debug: false,
            onComplete: function(id, fileName, responseJSON){
                if(responseJSON.success == true){
                    $("#uploadImgAN").css('display', 'none');
                    
                    $("#imgAN.control-group img").attr("src", "/uploads/images/background-images/"+responseJSON.nameFile+"");
                    $("#backgroundImage").val(responseJSON.nameFile);
                    $("#imgAN").css('display', 'block'); 
                    
                }else{
                    bootbox.alert(errorLoadingFile+ ' detalhe: '+responseJSON.errorMessage);
                }

            },
            template: '<div class="qq-uploader">' + 
                        '<div class="qq-upload-drop-area"><span>'+dropToUpload+'</span></div>' +
                        '<div class="qq-upload-button">'+uploadImage+'<br /> '+dimensionsRecommended+' 260x377 '+pxs+'</div>' +
                        '<ul class="qq-upload-list"></ul>' + 
                        '</div>',
            showMessage: function(message){
                bootbox.alert(message);
            }

        });           
}

function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}


function deleteImage(){
    
    var imagem = imagemNameByURL($('#imgAN.control-group').find('img').attr('src'));
    bootbox.confirm('Tem a certeza que deseja remover a imagem ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/menus/eliminar-imagem',{
                imagem: imagem
            }, 
            function(data){
                var dadosDevolvidos = data.split("\n");
                var resultado = dadosDevolvidos[0];
                if(resultado == 'sucess'){

                    $("#uploadImgAN").css('display', 'block');
                    $("#imgAN img").attr("src", "");
                    $("#imgAN").css('display', 'none');

                }else{ 
                    bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                }
            });
        }
    });
}

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
function ajax_request(item, descricao_menu, idioma) {
    parent.jQuery.fancybox.close();

    $("#item_id").attr("value", item);
    $("#descricao_menu").attr("value", descricao_menu);
    
    $("#designacao").val('');
    $("#path").val('');
    $("#link").val('');

    $('#configuracoes').html('<p style="float:left;margin:10px;" ><img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" width="16" height="16" /> A carregar configurações... </p>');

    $('#configuracoes').load(window.location.protocol + "//" + window.location.host + '/management/menus/configuracoes/item/'+item+'/idioma/'+idioma);

}
$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#titulo").focus();
    
    $('.demo1').bootstrapDualListbox({
        preserveselectiononmove: 'moved',
        moveonselect: true,
        infotext: 'Selec. {0} resultado(s)',
        infotextfiltered: '<span class="label label-warning">Res.:</span> {0} de {1}',
        infotextempty: 'Vazio'
    });
    
    $('#starDateTime').datetimepicker({
        format: 'yyyy-MM-dd hh:mm:ss'
    });
    
    $('#endDateTime').datetimepicker({
        format: 'yyyy-MM-dd hh:mm:ss'
    });
    
     
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
            
        $("ul.nav li").removeClass('active');
            
    });
    
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
            
            if($('div.controls .tipos').hasClass('active')){

                $('#tipo_'+localStorage['id']).removeClass('active');
                $('#tipo_'+localStorage['id']).slideUp('normal', function(){ });//.controls .tipos 
                
                if($(this).val() == 1 || $(this).val() == 2){
                    
                    $(this).val() == 2 ? $('#addressURL').css('display', 'none') : $('#addressURL').css('display', 'block');
                    
                    $('#tipo_1_2').slideDown('normal');
                    $('#tipo_1_2').addClass('active'); 
                    localStorage['id'] = '1_2';
                }else{
                    $('#tipo_'+$(this).val()).addClass('active');
                    $('#tipo_'+$(this).val()).slideDown('normal');
                    localStorage['id'] = $(this).val();
                }
                
                
            }else{
                if($(this).val() == 1 || $(this).val() == 2){
                    
                    $(this).val() == 2 ? $('#addressURL').css('display', 'none') : $('#addressURL').css('display', 'block');
                    
                    $('#tipo_1_2').slideDown('normal');
                    $('#tipo_1_2').addClass('active'); 
                    localStorage['id'] = '1_2';
                }else{
                    $('#tipo_'+$(this).val()).slideDown('normal');
                    $('#tipo_'+$(this).val()).addClass('active'); 
                    localStorage['id'] = $(this).val();
                }
            }
            
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
            
        }
        });
    });
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
        
            action: window.location.protocol + "//" + window.location.host + '/management/campanhas/imagens',
            
            name: 'uploadfile',
            onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                            // extension is not allowed 
                            alert(allowedFiles+' JPG, JPEG, PNG, GIF.');
                            return false;
                    }
                    //status.text('Uploading...');
                    $("#preview").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Uploading...."/>');
            },
            onComplete: function(file, response){
                    var data = $.parseJSON(response);
                    if(data.success===true){
                            $("span#imgRepres").css('display', 'none'); 
                            $('#preview').html('<img width="570" height="120" src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/campanhas/'+data.file+'" alt="" />');
                            $('span#deleteFile').css('display', 'block'); 
                            $("#width").val(data.width);
                            $("#height").val(data.height);
                            $('#dimensions').css('display', 'block'); 
                            $("#fileImage").val(data.file);
                            
                            $('.previewCampanha').html(livePreviewBanner($('input[name=clickUrl]').val(), data.file, data.width, data.height));
                    } else{
                            $('#preview').html('Erro ao efectuar upload');
                    }
            }
    });
    
    $("#deleteFile").click(function() {
        var imagem = imagemNameByURL($("#upload #preview img").attr("src")); 
        
        bootbox.confirm('Tem a certeza que deseja remover a imagem?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/campanhas/eliminar-imagem',{imagem: imagem}, 
                function(data){
                    var dadosDevolvidos = data.split("\n");
                    var resultado = dadosDevolvidos[0];
                    if(resultado == 'sucess'){
                        $('span#deleteFile').delay(500).fadeOut(500);
                        $('#preview').html('');
                        $("#fileImage").val('');
                        $("span#imgRepres").css('display', 'block'); 
                        $('#dimensions').css('display', 'none'); 

                    }else{
                        bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                    }
                });
        }});
    
    });
        
    $('.fancybox').fancybox();
    
    $('textarea[name=custombannercode]').keyup(function() {
        $('.previewCampanha').html($(this).val());
    });
    
    $('input[name=clickUrl]').blur(function() {
        var filter = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        var file = $('#fileImage').val();
        var width = $('#width').val();
        var height = $('#height').val();
        
        if(file){
           if(filter.test($(this).val())){
                $('.previewCampanha').html(livePreviewBanner($(this).val(), file, width, height));
            }else{
                $('.previewCampanha').html(livePreviewBanner('', file, width, height));
            } 
        }
        
    });
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
    
})(jQuery);

function livePreviewBanner(href, image, width, height){
    var preview = '';
    if(href!=''){
        preview = '<a href="'+href+'">';
        preview += '<img '+((width!='') ? 'width="'+width+'"' : '') + ((height!='') ? 'height="'+height+'"' : '')+' src="/uploads/images/campanhas/'+image+'">'; 
        preview += '</a>';
    }else{
        preview = '<img '+((width!='') ? 'width="'+width+'"' : '') + ((height!='') ? 'height="'+height+'"' : '')+' src="/uploads/images/campanhas/'+image+'">';
    }
    $('#custombannercode').val(preview);
    return preview;
}

function obterCampanhas(tipo, idioma_iso, page){
    $('#loading').css('display', 'block');
    $(".campanha").find('button.addNew').css('display', 'none');
    
    $('#rightSide').animate({
            right: '50%'
        });
    $('#leftSide ul#main-contentIdiomas-nav.nav li').removeClass('active');
    $("li#tipo_"+tipo).addClass('active');  
    $("li#tipo_"+tipo).find('button.addNew').css('display', 'block');
    
    

    $('#rightSide').load(window.location.protocol + "//" + window.location.host + '/management/categorizacao-campanhas/obter-campanhas/tipo/'+tipo+'/idioma/'+idioma_iso+'/page/'+page);
    
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
   
}

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}



$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
    
    
    var idioma_iso = $('ul.idiomas li a.selected').text();
    //alert(idioma_iso);
    
    
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
        
        $("#leftSide ul#main-contentIdiomas-nav.nav li.areaNegocio").removeClass('active');
        $('#loading').css('display', 'none');
        $('button.addNew').css('display', 'none');
            
    });
    
    $("#allImages").sortable({
        opacity: 0.7,
        connectWith: ".plan-container",
        stop: function(event, ui) {
            result = "";
            order = [];
            $('.plan-container').each(function(i, elm) {
                
                order.push(elm.id.split('_')[1]);
                changeIdDiv(elm.id, i);
            });
            
            $.post(window.location.protocol + "//" + window.location.host + '/management/conteudos/ordenar-imagens',{'order[]': order}, 
                function(data){   
                });
        }
    });
    
    $("#estado").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'P' : 'I' );
        if(val==true){
            $(".desc").text("ON");
            
            //$("#datas-publicacao").show();
            $("#datas-publicacao").stop(true).slideDown('fast');
        }else{
            $(".desc").text("OFF");
            
            //$("#datas-publicacao").hide();
            $("#datas-publicacao").stop(true).slideUp('fast');
        } 
    });
    var startDateTextBox = $('#inicioPublicacao');
    var endDateTextBox = $('#fimPublicacao');

    startDateTextBox.datetimepicker({ 
            onClose: function(dateText, inst) {
                    if (endDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datetimepicker('getDate');
                            var testEndDate = endDateTextBox.datetimepicker('getDate');
                            if (testStartDate > testEndDate)
                                    endDateTextBox.datetimepicker('setDate', testStartDate);
                    }
                    else {
                           // endDateTextBox.val(dateText);
                    }
            },
            onSelect: function (selectedDateTime){
                    endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
            }
    });
    
    endDateTextBox.datetimepicker({ 
            onClose: function(dateText, inst) {
                    if (startDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datetimepicker('getDate');
                            var testEndDate = endDateTextBox.datetimepicker('getDate');
                            if (testStartDate > testEndDate)
                                    startDateTextBox.datetimepicker('setDate', testEndDate);
                    }
                    else {
                           // startDateTextBox.val(dateText);
                    }
            },
            onSelect: function (selectedDateTime){
                    startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
            }
    });
     
     $("form.form-horizontal").submit(function () {
        
        if($("#starDateTime").val() == ''){
                bootbox.alert('Não se esqueça da data hora de início.');
                $('#starDateTime').focus();
                return false;
        }else
        if($("#endDateTime").val() == ''){
                bootbox.alert('Não se esqueça da data hora de fim.');
                $('#endDateTime').focus();
                return false;
        }else
        if(($("#starDateTime").val() >= $("#endDateTime").val()))
        {
            bootbox.alert("Atenção: A data e hora de fim terá de ser superior à data hora de início.");
            $('#endDateTime').focus();
            return false;
        }else
        if($("#pontos").val() == ''){
                bootbox.alert('Introduza os pontos');
                $('#pontos').focus();
                return false;
        }
        else{
            return true;
        }
    });
    
    //if already visible - slideDown
    //if not visible - slideUp
    $('#searchboxtrigger').toggle(
    function(){
        $('#searchbox').stop(true).slideDown('fast');
    },
    function(){
        $('#searchbox').stop(true).slideUp('fast');
    }
    );
    
    
    $("#ficheiroPDF").change(function() {
        var file = $(this).val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if(extension != '' && extension != 'pdf'){
            bootbox.alert("O ficheiro que submeteu não é um pdf.");
            $('.fileupload').fileupload('clear');
        }
    });
    
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
            
    $('.fancybox').fancybox();
    
    $('span#preview').live('click', function (e) {
        var designacao = $('input[name="designacao"]').val();
        var text = tinyMCE.activeEditor.getContent({format : 'raw'});
        var imagem = $("#imgAN.control-group img").attr("src");
        
        $("h3#tituloConteudo").text(designacao);
        
        if (text.match(/<hr\s+id=("|')content-readmore("|')\s*\/*>/i)) {
            var pieces = text.split('<hr id="content-readmore">');
            
            $(".introducaoConteudo").html('<p>'+pieces[0]+'</p>');
            $(".descricaoConteudo").html(pieces[1]);
        }
        else{
            $(".descricaoConteudo").html(text);
        }
        if(imagem){
            $(".imagem-detalhe-cs img").attr("src", ""+imagem+"");
        }
        
    });
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
                      
    var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
    var pieces = url.split("/conteudos/")[1].split("/");
    var accao = pieces[0];
    
    if(accao == 'editar'){
        var banner = $("#imgAN.control-group img").attr("src");
        
        if ($('#imgAN')) {
            if(!banner){
                $("#uploadImgAN").css('display', 'block');
            }else{
                $("#uploadImgAN").css('display', 'none');
            }
            
        }else{
            $("#imgAN").css('display', 'none'); 
        }  
        
        $(".plan-container .plan").hover(function() {
            $(this).css("background", "#FCFFE2");
            var delFile = $(this).children('.delFile');
            delFile.show();
        }, function() {
            $(this).css("background", "transparent");
            var delFile = $(this).children('.delFile');
            delFile.hide();
        });
    }
    
  /*  createUploader();
    
    createUploaderConteudo();*/
    
});

$(window).load(function() {
      if ($('#ficheirosmedia').is(':visible')) {
            createUploader();
      }
      
      if ($('#ficheirosmediaconteudos').is(':visible')) {
            createUploaderConteudo();
      }
});


function obterConteudos(tipo, idioma_iso, page){
    $('#loading').css('display', 'block');
    $(".areaNegocio").find('button.addNew').css('display', 'none');
    //var idioma_iso = $('ul.idiomas li a.selected').text();
    $('#rightSide').animate({
            right: '50%'
        });
    $("#leftSide ul#main-contentIdiomas-nav.nav li.areaNegocio").removeClass('active');
    $("li#tipo_"+tipo).addClass('active');  
    $("li#tipo_"+tipo).find('button.addNew').css('display', 'block');
    
    $('#rightSide').load(window.location.protocol + "//" + window.location.host + '/management/conteudos/obter-conteudos/tipo/'+tipo+'/idioma/'+idioma_iso+'/page/'+page);
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
}
function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function insertReadMore(element_name, html) {
    var content = tinyMCE.get(element_name).getContent();
    if (content.match(/<hr\s+id=("|')content-readmore("|')\s*\/*>/i)) {
        bootbox.alert("Já existe uma hiperligação para Ler mais...");
        return false;
    }
    else{
        tinyMCE.execCommand('mceInsertContent', false, html);
        return true;
    }
        
}
        
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}



function deleteImage(){
    
    var imagem = imagemNameByURL($('#imgAN.control-group').find('img').attr('src'));
    bootbox.confirm('Tem a certeza que deseja remover a imagem ?', function(result) { 
        if (result) { 
            
            var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
            var pieces = url.split("/conteudos/")[1].split("/");
            var conteudo_id = pieces[8];
            
            $.post(window.location.protocol + "//" + window.location.host + '/management/conteudos/eliminar-imagem',{
                imagem: imagem, 
                tipo: 'conteudos',
                conteudo_id: (conteudo_id!==null ? conteudo_id : 0)
            }, 
            function(data){
                var dadosDevolvidos = data.split("\n");
                var resultado = dadosDevolvidos[0];
                if(resultado == 'sucess'){

                    $("#uploadImgAN").css('display', 'block');
                    $("#imgAN img").attr("src", "");
                    $("#imgAN").css('display', 'none');

                }else{ 
                    bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                }
            });
        }
    });
}

function apagarFicheiro(imagem, token, div_id){
    bootbox.confirm(confirm+' ?', function(result) { 
    if (result) { 
        $('#'+div_id+'.plan-container').append('<div class="loader"></div>');
        $('.loader').css({'opacity' : '0.8'}).show();
        
        var dados = {};
        dados.imagem = imagem;
        dados.token = token;
        
         $.post(window.location.protocol + "//" + window.location.host + '/management/conteudos/eliminar-ficheiro',{dadosPOST: dados}, 
            function(data){
                var data = $.parseJSON(data);
                if(data.success===true){
                    $('#'+div_id+'.plan-container').remove();
                }else{
                    alert('Não foi possível apagar o ficheiro');
                }
                $(".loader").remove();
                return false;
            });
        
    }});
    
}
function changeIdDiv(div_id, newPos){
    $("#"+div_id+".plan-container .plan a.thumbnail span").html(newPos);
}

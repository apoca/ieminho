$(function(){
        
    $('.alert-success').delay(4000).fadeOut("slow");
    
    $("ul#main-websites-nav.nav li a").click(function(event){
            event.preventDefault();
            linkLocation = this.href;
            $(".widget-content").html(messageWaitingServer);
            $("ul#main-websites-nav.nav").fadeOut(1000, redirectPage(linkLocation));		
    });
    
    $(".group-action a#editbutton").click(function(event){
            event.preventDefault();
            linkLocation = this.href;
            $(".widget-content").html(messageWaitingServer);
            $("ul#main-websites-nav.nav").fadeOut(1000, redirectPage(linkLocation));		
    });
    
    function redirectPage(linkLocation) {
            window.location = linkLocation;
    }
    
    $('#title').focus();
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
            action: window.location.protocol + "//" + window.location.host + '/management/websites/imagens',
            name: 'uploadfile',
            onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                            alert(allowedFiles+' JPG, JPEG, PNG, GIF.');
                            return false;
                    }
                    $("#preview").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Uploading...."//>');
            },
            onComplete: function(file, response){
                    //On completion clear the status
                    //status.text('');
                    //Add uploaded file to list
                    if(response==="success"){
                            $('#preview').html('<img width="53" height="53" src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/logo/'+file+'" alt="" />');
                            $('span#deleteFile').delay(500).fadeIn(500);
                            $("#fileImage").val(file);
                    } else{
                            $('#preview').html(errorUploadingFile);
                    }
            }
    });
    
    $("#deleteFile").click(function() {
        var imagem = imagemNameByURL($("#upload #preview img").attr("src"));   
        
        bootbox.confirm(confirmDeletingFile+' ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/websites/eliminar-imagem',{imagem: imagem}, 
                function(data){
                    var dadosDevolvidos = data.split("\n");
                    var resultado = dadosDevolvidos[0];
                    if(resultado == 'sucess'){
                        $('span#deleteFile').delay(500).fadeOut(500);
                        $('#preview').html('');
                        $("#fileImage").val('');
                        $("span#imgRepres").css('display', 'block'); 

                    }else{
                        bootbox.alert(errorDeletingFile);
                    }
                });
        }});
    
    });
    
    /*
    $("#aplicar").click(function() {
        var idioma = $("ul.langs li a.lang-tab.selected").text();
        bootbox.alert("Actualizar configurações do idioma: "+idioma);
    });
    
    */
    
    
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    //alert(pathname.toSource());
    
     $('#edit-profile').validate({
	    rules: {
	      title: {
	        minlength: 2,
	        required: true
	      }
	    },
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });

    
})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
    
function configuracoesIdioma(id, idioma){        
        $('.fields-langs').html(pleaseWaiting);
        $('#configuracoesIdioma').load(window.location.protocol + "//" + window.location.host +"/management/websites/configuracoes-idioma/id/"+id+"/idioma/"+idioma);
}

function saveConfiguracoesIdioma(id, idioma){ 
    var dados = {};
    dados.title = $('input[name="title"]').val();
    dados.description = $('textarea[name="descricao"]').val();
    dados.keywords = $('textarea[name="keywords"]').val();
    dados.website_id = id;
    dados.idioma_iso = idioma;
    //alert(dados.toSource());
    
     $.post(window.location.protocol + "//" + window.location.host + '/management/websites/update-configuracoes-idioma',{dadosPOST: dados}, 
        function(data){
            var dadosDevolvidos = data.split("\n"); 
            bootbox.alert(dadosDevolvidos[0]);
        }
    ); 
    //bootbox.alert("guardar em "+idioma+" o website: "+id);
      
}

function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}
$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
    
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        minlength: 2,
	        required: true
	      },
	      descricao: {
	        required: true,
	        minlength: 2
	      }
	    }, 
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });

    
})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function configuracoesIdioma(idioma, action){
        $('.fields-langs').html('<div id="waiting"><img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Por favor aguarde...."//><p>Por favor aguarde.</p></div>');
       // bootbox.alert(' ' +idioma);
        $('#configuracoesIdioma').load(window.location.protocol + "//" + window.location.host +"/management/configuracoes/idioma/iso/"+idioma+"/accao/"+action);
}

function saveConfiguracoesIdioma(idioma){ 
    var dados = {};
    dados.title = $('input[name="title"]').val();
    dados.description = $('textarea[name="descricao"]').val();
    dados.keywords = $('textarea[name="keywords"]').val();
    dados.idioma_iso = idioma;
    
     $.post(window.location.protocol + "//" + window.location.host + '/management/configuracoes/update-configuracoes-idioma',{dadosPOST: dados}, 
        function(data){
            var dadosDevolvidos = data.split("\n"); 
            bootbox.alert(dadosDevolvidos[0]);
        }
    ); 
      
}
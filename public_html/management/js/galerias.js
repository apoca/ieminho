$(function(){
    
    $('.alert-success').delay(4000).fadeOut("slow");
    
    $("#titulo").focus();
    
    $(".uploadFiles").fancybox({
        afterClose : function() {
            location.reload();
            return;
        }
    });


    
    $("#finishSubcat").click(function() {
        $("#rightForm").css('display', 'none');
    });
    
    $("#estado").change(function() {
        
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'A' : 'I' );
        if(val==true){
            $(".desc").text("ON");
        }else{
            $(".desc").text("OFF");
        } 
    });
    
    $("#subCategorias").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'S' : 'N' );
        if(val==true){
            $(".descSubCat").text("SIM");//ex NÃO
            $("#rightForm").css('display', 'block'); 
        }else{
            $(".descSubCat").text("SIM");
            $("#rightForm").css('display', 'none'); 
        } 
    });
    
    $("#uploadImgAN").css('display', 'none');
    $('#categoria_id').change(function(){
       // $(this).attr('value') == '' ? $("#uploadImgAN").css('display', 'none') : $("#uploadImgAN").css('display', 'block');
       if($(this).attr('value') == ''){
           $("#uploadImgAN, .alert.category").css('display', 'none');
           
       }else{
           $("#uploadImgAN, .alert.category").css('display', 'block');
           $(".alert.category i").html("Adicionar ficheiros à categoria <b>"+$(this).find(":selected").text()+"</b>");
       }
        
        
    });
    
    $("input[name=categoria]:checkbox").click(function() {
        
        var selected = $("input[name=categoria]:checked").map(function () {return this.value;}).get().join(",")
        if(selected){
            $('#allProducts').append('<div class="loader"></div>');
            $('.loader').css({'opacity' : '0.8'}).show();
            $.get(window.location.protocol + "//" + window.location.host +"/management/galerias/filtrar-categorias/in/"+selected, function(data){
                $("#allProducts").html(data);
                $(".loader").remove();
                return false;

            });
        }
        
    });
    $.fn.editable.defaults.mode = 'popup';
    
    $('.descricaoFicheiro').editable({clear:false});
    
    $('.categoria_id').editable(); 
    
    $(".plan-container .plan").hover(function() {
        $(this).css("background", "#FCFFE2");
        var delFile = $(this).children('.delFile');
        delFile.show();
    }, function() {
        $(this).css("background", "transparent");
        var delFile = $(this).children('.delFile');
        delFile.hide();
    });
    
    
    $('input').change(function() { 
          somethingChanged = true;
    });
    
    $(".lang-tab").click(function() {
        if(somethingChanged == true){
            aviso();
        }
    });
    
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    if(action == 'editar'){    
           
    }
    
    
    $('#createItemSubCat').click(function(){
            var i = generateString(4);
            $('.listagem').append("<div class='subCategoriaItem' id='subCat_"+i+"'><input type='text' name='subCategoriaItem[]' id='focus_"+i+"' value='"+$('#subCategoria').val()+"' /><button class='del' onClick='javascript:deleteSubCatFromList(\""+i+"\");' type='button'>"+del+"</button><button class='edit' onClick='javascript:editSubCatFromList(\""+i+"\");' type='button'>"+edit+"</button></div>");
            
            var url = document.URL.split('id')[1];
            var dados = {};
            dados.designacao = $('#subCat_'+i+' input').val();
            dados.idioma_iso = $("ul.langs li a.lang-tab.selected").text();
            dados.categoria_pai = url.split('/')[1].split('/')[0];

            $.post(window.location.protocol + "//" + window.location.host + '/management/galerias/adicionar-sub-categoria',{dadosPOST: dados}, 
            function(data){
                    var dadosDevolvidos = data.split("\n"); 
                    var novo_id = dadosDevolvidos[0];

                    $("#subCat_"+i+" button.edit").attr("onClick","javascript:editSubCatFromList('"+novo_id+"');");
                    $("#subCat_"+i+" button.del").attr("onClick","javascript:deleteSubCatFromList('"+novo_id+"');");
                    $("#subCat_"+i+"").attr("id", "subCat_"+novo_id+"");  
            });
            
            actionsInputs();
    
    });
    
    $('#addSubCat').click(function(){
            var i = generateString(4);
            $('.listagem').append("<div class='subCategoriaItem' id='subCat_"+i+"'><input type='text' name='subCategoriaItem[]' id='focus_"+i+"' value='"+$('#subCategoria').val()+"' /><button class='del' onClick='javascript:deleteSubCatFromList(\""+i+"\");' type='button'>"+del+"</button><button class='edit' onClick='javascript:editSubCatFromList(\""+i+"\");' type='button'>"+edit+"</button></div>");
            
            $("#subCatShowHide").css('display', 'none'); 
            $("#createItemSubCat").css('display', 'block');
            
            $("input#subCategoria").val('');
            adicionarSubCategoria(i);
            actionsInputs();
            
    });
        
    $('#addSubCatToList').click(function(){
            var i = generateString(4);
            $('.listagem').append("<div class='subCategoriaItem' id='subCat_"+i+"'><input type='text' name='subCategoriaItem[]' id='focus_"+i+"' value='"+$('#subCategoria').val()+"' /><button class='del' onClick='javascript:deleteSubCatFromList(\""+i+"\");' type='button'>"+del+"</button><button class='edit' onClick='javascript:editSubCatFromList(\""+i+"\");' type='button'>"+edit+"</button></div>");
            
            $("#subCatShowHide").css('display', 'none'); 
            $("#createItemSubCat").css('display', 'block');
            
            $("input#subCategoria").val('');
            actionsInputs();
            
    });
    
    actionsInputs();
    createUploader();
    
    
    
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        required: true
	      }
	    },
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });
      
      
    
})(jQuery);

var somethingChanged = false;

function aviso(){
    $(window).on('beforeunload', function(){
        return confirmExit+' ?';
    });
}


function adicionarSubCategoria(subcat){
    
    var dados = {};
    dados.designacao = $('#subCat_'+subcat+' input').val();
    dados.categoria_pai = document.URL.split('id')[1].substring(document.URL.split('id')[1].lastIndexOf("/") + 1, document.URL.split('id')[1].length);
    
  //  alert('designação: '+ designacao+ ' pos: '+pos+ ' idioma: '+idioma_iso+ ' areamegocio_id: '+areamegocio_id);
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/galerias/adicionar-sub-categoria',{dadosPOST: dados}, 
    function(data){
            var dadosDevolvidos = data.split("\n"); 
            var novo_id = dadosDevolvidos[0];
    
            $("#subCat_"+subcat+" button.edit").attr("onClick","javascript:editSubCatFromList("+novo_id+");");
            $("#subCat_"+subcat+" button.del").attr("onClick","javascript:deleteSubCatFromList("+novo_id+");");
            $("#subCat_"+subcat+"").attr("id", "subCat_"+novo_id+"");  
    });
    
}

function apagarFicheiro(id, tipo){
    bootbox.confirm(confirm+' ?', function(result) { 
    if (result) { 
        $('#ficheiro_'+id+'.plan-container').append('<div class="loader"></div>');
        $('.loader').css({'opacity' : '0.8'}).show();

        var dados = {};
        dados.id = id;
        dados.tipo = tipo;

        $.post(window.location.protocol + "//" + window.location.host + '/management/galerias/eliminar-ficheiro',{dadosPOST: dados}, 
        function(data){
            var data = $.parseJSON(data);
            if(data.success===true){
                $('#ficheiro_'+id+'.plan-container').remove();
            }else{
                alert('Não foi possível apagar o ficheiro');
            }
            $(".loader").remove();
            return false;
        });
    }});
    
    
}

function actionsInputs(){
    
    $('.subCategoriaItem input').keydown(function(){ 
        
         $(this).parent().find("button.del").css('display', 'none'); 
         $(this).parent().find("button.edit").css('display', 'block'); 

    });
    
    $(".subCategoriaItem input").focusout(function() {
        
        if( $(this).val() == $(this).data('val') ){
            
            //var designacao = $('#subCat_'+subcat+' input').val();
            $(this).parent().find("button.edit").css('display', 'none'); 
            $(this).parent().find("button.del").css('display', 'block');
        }
    });
}

function language(lang){              
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/idioma',{lang: lang}, 
	function(data){
                window.location.reload();			
        });
}

function deleteSubCatFromList(subcat){  
        bootbox.confirm(confirm+' ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/galerias/eliminar-sub-categoria',{'id': subcat}, 
            function(data){
                    $("#subCat_"+subcat+".subCategoriaItem").remove();   
            });
            
        }});
}
function change_cat(cat_from_id, imagem_id) {
    var cat_to_id = $("select#categoria_id_"+imagem_id+" option").filter(":selected").val();
    if (cat_from_id && cat_to_id && imagem_id) {
        console.log('from: '+cat_from_id+' to: '+cat_to_id+' imagem_id: '+imagem_id);
        window.location = window.location.protocol + "//" + window.location.host +'/management/galerias/imagens/id/'+imagem_id+'/cat_from_id/' + cat_from_id + '/cat_to_id/' + cat_to_id; // redirect
    }
    return false;
}
function editSubCatFromList(subcat){
    var designacao = $('#subCat_'+subcat+' input').val();
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/galerias/actualizar-designacao',{'id': subcat, 'designacao': designacao}, 
    function(data){
            $('#subCat_'+subcat).parent().find("button.edit").css('display', 'none'); 
            $('#subCat_'+subcat).parent().find("button.del").css('display', 'block');   
    });
        
}

function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

function generateString(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
        if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
        if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
        if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
        if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;

}
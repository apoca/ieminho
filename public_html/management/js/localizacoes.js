

$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    //$("#designacao").focus();
    
    $("#estado").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'A' : 'I' );
        if(val==true){
            $(".desc").text("ON");
        }else{
            $(".desc").text("OFF");
        } 
    });
    
    $("#principal").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'S' : 'N' );
        if(val==true){
            $(".descPrincipal").text("SIM");
        }else{
            $(".descPrincipal").text("NÃO");
        } 
    });
    
    
    $("#areasNegocio").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'S' : 'N' );
        if(val==true){
            $(".descAreasNegocio").text("SIM");
        }else{
            $(".descAreasNegocio").text("NÃO");
        } 
    });
    
    
    $('input').change(function() { 
        somethingChanged = true;
    });
    
    $(".lang-tab").click(function() {
        if(somethingChanged == true){
            aviso();
        }
    });
        
    $('.fancybox').fancybox();
    
    $('#preview').live('click', function (e) {
        var designacao = $('input[name="designacao"]').val();
        var latitude = $('input[name="latitude"]').val();
        var longitude = $('input[name="longitude"]').val();
        var telefone = $('input[name="telefone"]').val();
        var fax = $('input[name="fax"]').val();
        var email = $('input[name="email"]').val();
        var descricao = $('textarea[name="descricao"]').val();
        $('#mapa').html('<img src="http://maps.google.com/maps/api/staticmap?center='+latitude+','+longitude+'&zoom=15&size=530x398&maptype=roadmap&sensor=false&markers=color:red|label:none|'+latitude+','+longitude+'" width="100%" height="100%">');
        
        $(".morada").html("<p>"+designacao+"</p> <p style='margin-bottom: 3px;'>"+descricao+"</p> <p>Tel.: "+telefone+"</p><p>Fax: "+fax+"</p><p>E-mail: "+email+"</p>");
                
        var imagem = $("#imgAN.control-group img").attr("src");
        
        /*
         *<div class="morada">
                        <p>New Zealand</p>
                        <p style="margin-bottom: 3px;">Av. do Mosteiro de Grijó, 4415 Vila Nova de Gaia, Portugal<br>
                        Coordenadas GPS:<br>
                        41º 01´43.03´´N<br>
                        08º34´59.38´´W</p>
                        <p>Phone.: 0800 2378 289  </p>
                        <p>Mobile: 021 0241 7775 </p>
                        <p>E-mail: info@cotesi.co.nz</p>
                    </div>
         **/
        
        
    });
    
    
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
          
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    if(action == 'detalhe'){
        initialize();
    }
    
    
})(jQuery);

var somethingChanged = false;

function aviso(){
    $(window).on('beforeunload', function(){
        return confirmExit;
    });
}


function language(lang){              
    $.post(window.location.protocol + "//" + window.location.host + '/management/index/idioma',{
        lang: lang
    }, 
    function(data){
        window.location.reload();			
    });
}



$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
    
    $('.fancybox').fancybox();
    
  $("table.tablesorter").tablesorter({
        theme : "bootstrap",
        widthFixed: true,
        headerTemplate : '{content} {icon}',
        widgets : [ "uitheme",  "zebra" ]
    }).tablesorterPager({
        container: $(".pager"),
        cssGoto  : ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
    });
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            
            var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
            var pieces = url.split("/tipos-conteudos/")[1].split("/");
            var accao = pieces[0];
            
            if(accao!='adicionar'){
                var array = name.split('_');
                alterarOpcao(array[0], $(this).val(), array[1]);
            }
            //alert(name+' // '+$(this).val());
            //name == 'temImagemDestaque'
            if(name.indexOf('temImagemDestaque') == 0 && $(this).val() == 'S'){
                $("#dimensoesImagem").stop(true).slideDown('fast');
            }else{
                $("#dimensoesImagem").stop(true).slideUp('fast');
            }
            hidden.val($(this).val());
            
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
 $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    }); 
    
})(jQuery);


function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function alterarOpcao(opcao, val, caracteristica_id){    
    $.post(window.location.protocol + "//" + window.location.host + '/management/tipos-conteudos/opcoes',{id: caracteristica_id, opcao:opcao, valor: val}, 
    function(data){});
    
}

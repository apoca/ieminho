

$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
    
    $.extend($.tablesorter.themes.bootstrap, {
        table      : 'table table-bordered',
        header     : 'bootstrap-header', // give the header a gradient background
        footerRow  : '',
        footerCells: '',
        icons      : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
        sortNone   : 'bootstrap-icon-unsorted',
        sortAsc    : 'icon-chevron-up',
        sortDesc   : 'icon-chevron-down',
        active     : '', // applied when column is sorted
        hover      : '', // use custom css here - bootstrap class may not override it
        filterRow  : '', // filter row class
        even       : '', // odd row zebra striping
        odd        : ''  // even row zebra striping
    });

    $("table.tablesorter").tablesorter({
        theme : "bootstrap",
        widthFixed: true,
        headerTemplate : '{content} {icon}',
        widgets : [ "uitheme", "filter", "zebra" ],

        widgetOptions : {
            zebra : ["even", "odd"],
            filter_reset : ".reset"

        }
    })
    .tablesorterPager({
        container: $(".pager"),
        cssGoto  : ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

    });
    
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
            
        $("ul.nav li").removeClass('active');
            
    });
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
    $("#estado").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'A' : 'I' );
        if(val==true){
            $(".desc").text("ON");
        }else{
            $(".desc").text("OFF");
        } 
    });
    
    
    $('input').change(function() { 
        somethingChanged = true;
    });
    
    $(".lang-tab").click(function() {
        if(somethingChanged == true){
            aviso();
        }
    });
        
    $('.fancybox').fancybox();
    
    $('span#preview').live('click', function (e) {
        var designacao = $('input[name="designacao"]').val();
        var text = tinyMCE.activeEditor.getContent({format : 'raw'});
        var imagem = $("#imgAN.control-group img").attr("src");
        
        $("h3#tituloConteudo").text(designacao);
        
        if (text.match(/<hr\s+id=("|')content-readmore("|')\s*\/*>/i)) {
            var pieces = text.split('<hr id="content-readmore">');
            
            $(".introducaoConteudo").html('<p>'+pieces[0]+'</p>');
            $(".descricaoConteudo").html(pieces[1]);
        }
        else{
            $(".descricaoConteudo").html(text);
        }
        if(imagem){
            $(".imagem-detalhe-cs img").attr("src", ""+imagem+"");
        }
        
    });
    
    
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            },
            pontos: {
                required: true,
                digits: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
          
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    if(action == 'editar'){
        var banner = $("#imgAN.control-group img").attr("src");
        
        if ($('#imgAN')) {
            if(!banner){
                $("#uploadImgAN").css('display', 'block');
            }else{
                $("#uploadImgAN").css('display', 'none');
            }
            
        }else{
            $("#imgAN").css('display', 'none'); 
        }   
    }
    
    createUploader();
    
})(jQuery);

var somethingChanged = false;

function aviso(){
    $(window).on('beforeunload', function(){
        return "Tem a certeza que pretende sair desta página?";
    });
}

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function insertReadMore(element_name, html) {
    var content = tinyMCE.get(element_name).getContent();
    if (content.match(/<hr\s+id=("|')content-readmore("|')\s*\/*>/i)) {
        bootbox.alert("Já existe uma hiperligação Ler mais...");
        return false;
    }
    else{
        tinyMCE.execInstanceCommand(element_name,"mceInsertContent",false,html);
        return true;
    }
        
}
        
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

function createUploader(){  
     
    var uploader = new qq.FileUploader({
        element: document.getElementById('ficheirosmedia'),
        listElement: document.getElementById('separate-list'),
        action: window.location.protocol + "//" + window.location.host + window.location.pathname,
        debug: true,
        onComplete: function(id, fileName, responseJSON){
            if(responseJSON.success == true){
                $("#uploadImgAN").css('display', 'none');
                $("#imgAN.control-group img").attr("src", "/uploads/images/premios/"+responseJSON.nameFile+"");
                $("#foto").val(responseJSON.nameFile);
                $("#imgAN").css('display', 'block');
                
            }else{
                bootbox.alert("Não foi possível carregar o ficheiro.");
            }
        },
        template: '<div class="qq-uploader">' + 
        '<div class="qq-upload-drop-area"><span>Arraste ficheiros para carregá-los</span></div>' +
        '<div class="qq-upload-button">carregar imagem<br /> dimensões aconselhada 654x245 píxeis</div>' +
        '<ul class="qq-upload-list"></ul>' + 
        '</div>',
        showMessage: function(message){ 
            bootbox.alert(message);
        }

    });           
}


function deleteImage(){
     var imagem = imagemNameByURL($('#imgAN.control-group').find('img').attr('src'));
   // var imagem = imagemNameByURL($('foto').val());
    bootbox.confirm('Tem a certeza que deseja remover a imagem ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/premios/eliminar-imagem',{
                imagem: imagem
            }, 
            function(data){
                var dadosDevolvidos = data.split("\n");
                var resultado = dadosDevolvidos[0];
                if(resultado == 'sucess'){

                    $("#uploadImgAN").css('display', 'block');
                    $("#imgAN img").attr("src", "");
                    $("#imgAN").css('display', 'none');

                }else{ 
                    bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                }
            });
        }
    });
}

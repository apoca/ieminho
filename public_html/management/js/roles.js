$(function(){
    $("#designacao").focus();
    $('.alert-success').delay(4000).fadeOut("slow");
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
        $("#containerAN div#leftSide ul.nav li").removeClass('active');
    });
    
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
    //$("#opActions").css('top', $("#main-contentIdiomas-nav").height());
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        minlength: 2,
	        required: true
	      },
	      descricao: {
	        required: true,
	        minlength: 2
	      }
	    }, 
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });

    
})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function obterRecursos(recurso, role, accao){
    $('#loading').css('display', 'block');
    
    $('#leftSide ul#main-contentIdiomas-nav.nav li').removeClass('active');
    $('#rightSide').animate({
            right: '50%'
    });
    $('#leftSide ul#main-contentIdiomas-nav.nav li#tipo_'+recurso).addClass('active');
    
    var dados = {};
    dados.recurso = recurso;
    dados.role = role;
    dados.accao = accao;
    $.post(window.location.protocol + "//" + window.location.host + '/management/roles/recursos',{dadosPOST: dados}, 
	function(data){
                $('#rightSide').html(data);			
        });
    
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
}
function enviaPermissao(varRecurso, role) {
        
    var dados = {};
    dados.privilegio = varRecurso;
    dados.role = role;

    var checked =  $('#'+varRecurso).is(':checked');      
    var flash = $("#flash_"+varRecurso);

    if (checked) {
            $('#'+varRecurso).hide();
            flash.css('display','block');
            dados.permitir = 'S';

            $.post(window.location.protocol + "//" + window.location.host + '/management/roles/alterar-privilegio',{dadosPOST: dados}, 

            function(data){
                var data = $.parseJSON(data);
                if(data.success===false){
                    bootbox.alert(data.mensagem);
                }else{
                  if(data.quantos === '0'){
                      $('.'+data.recurso+'.checked').css("display", "none");
                  }else{
                      $('.'+data.recurso+'.checked').css("display", "block");
                  }
                }
                flash.css('display','none');
                $('#'+varRecurso).show();
            });

        } else {
            flash.css('display','block');
            $('#'+varRecurso).hide();
            dados.permitir = 'N';

            $.post(window.location.protocol + "//" + window.location.host + '/management/roles/alterar-privilegio',{dadosPOST: dados}, 

            function(data){
                var data = $.parseJSON(data);
                if(data.success===false){
                    bootbox.alert(data.mensagem);
                }else{
                  if(data.quantos === '0'){
                      $('.'+data.recurso+'.checked').css("display", "none");
                  }else{
                      $('.'+data.recurso+'.checked').css("display", "block");
                  }
                }
                flash.css('display','none');
                $('#'+varRecurso).show();
            });
        }
}

function updateRole(id){
    var dados = {};
    dados.id = id;
    dados.designacao = $('#designacao').val();
    dados.descricao = $('#descricao').val();
    dados.estado = $('#estado').val();
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/roles/update-role',{dadosPOST: dados}, 
    function(data){
            window.location = window.location.protocol + "//" + window.location.host + '/management/roles';
    });
}

/* FROM provilégios**/
function inserirPrivilegio(controller, action){
    
    bootbox.prompt('Nome da action ('+controller+') ', 'Cancelar', 'OK', function(result) {
            if (result != null) {
                
                var dados = {};
                dados.acao = action;
                dados.descricao = result;
                dados.controladorNome = controller;
    
                $.post(window.location.protocol + "//" + window.location.host + '/management/roles/adicionar-privilegio',{dadosPOST: dados}, 
                function(data){
                        var data = $.parseJSON(data);
                        if(data.success===true){
                           $('#'+controller+'_'+action).prop('checked',true);
                           $('#'+controller+'_'+action).prop('disabled',true);
                        }else{
                            
                            $('#'+controller+'_'+action).prop('checked',false);
                            bootbox.alert('Problema ao inserir registo.');
                        }
                        
                });
                }else{
                $('#'+controller+'_'+action).prop('checked',false);
            }
        }, ucfirst(action));
        
        
}

function ucfirst(str) {
  str += '';
  var f = str.charAt(0).toUpperCase();
  return f + str.substr(1);
}

function inserirRecurso(controller){
    
    bootbox.prompt('Nome do recurso (controller) ', 'Cancelar', 'OK', function(result) {
            if (result != null) {
                
                var dados = {};
                dados.descricao = result;
                dados.controladorNome = controller;
    
                $.post(window.location.protocol + "//" + window.location.host + '/management/roles/adicionar-recurso',{dadosPOST: dados}, 
                function(data){
                        var data = $.parseJSON(data);
                        if(data.success===true){
                           window.location.reload(); 
                        }else{
                            bootbox.alert('Problema ao inserir registo.');
                        }
                        
                });
                
            }else{
                $('#'+controller).prop('checked',false);
            }
        }, ucfirst(controller));
        
        
}
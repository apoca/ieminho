$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
    
    //window.location.protocol + "//" + window.location.host + '/management/perguntas-respostas/resposta',
    
    $.fn.editable.defaults.mode = 'inline';
    
    $('.resposta').editable({clear:false});
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
    
    $('.addResposta').live('click', function (e) {
        var ulID = $(this).attr("id");
        var pergunta_id = ulID.split("_")[1];
            
        
        bootbox.prompt("Nova opção de resposta", function(result) {
            if (result != null) {
                var dados = {};
                dados.designacao = result;
                dados.pergunta_id = pergunta_id;
                dados.idioma_iso = $('ul.idiomas li a.selected').text();
                dados.pos = $('ul#'+ulID+' > li').length+1;
    
                $.post(window.location.protocol + "//" + window.location.host + '/management/perguntas-respostas/resposta',{dadosPOST: dados}, 
                function(data){
                        var dadosDevolvidos = data.split("\n"); 
                        var index = dadosDevolvidos[0];
                        
                        $('ul#'+ulID).append('<li><a href="#" class="resposta" data-type="text" data-pk="'+index+'" data-url="'+window.location.protocol + "//" + window.location.host+'/management/perguntas-respostas/editar-resposta" data-original-title="Resposta...">'+result+'</a></li>');
                        $('.resposta').editable();
                });
                
            } 
        });
    });
    
        
    $('.fancybox').fancybox();
    
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
    
})(jQuery);


function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

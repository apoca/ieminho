$(function(){
    //$('.fancybox').fancybox(); 
    $('.alert-success').delay(4000).fadeOut("slow");
    
    $("#donebutton").css('display', 'none');
    
    $("#deleteFile").click(function() {
        var imagem = $("#upload #preview img").attr("src");   
        //alert("Irei remover a imagem ("+imagem+") do servidor");
        bootbox.alert("Irei remover a imagem ("+imagem+") do servidor");
    });
    
     $( "#preview" ).sortable({
        opacity: 0.7,
        connectWith: ".preview",
        stop: function(event, ui) {
            result = "";
            order = [];
            $('.preview').each(function(i) {
                
                result += "["+i+"]: " +imagemNameByURL($(this).find('img').attr('src')) + " , ";
                changeIdDiv(imagemNameWthExtByURL($(this).find('img').attr('src')), i);
                
                order.push(imagemNameByURL($(this).find('img').attr('src')));
            });
            $.post(window.location.protocol + "//" + window.location.host + '/management/banners/ordenar',{
                'order[]': order
            }, function(data){});
        }
    });
    
    
    $('.preview').click(function(e) {
            e.preventDefault();   
            var id = $(this).attr("data");
            if(id){
                $("#waiting").css('display', 'block');
                
                $.get(window.location.protocol + "//" + window.location.host + '/management/banners/info-banner/id/'+id, function(data){
                    
                    $("#formFields").html(data);
                    $("#datas-publicacao").stop(true).slideDown('fast');
                    $("#donebutton").css('display', 'block');
                    
                    
                    $("#waiting").css('display', 'none');
                });
            }
            
            
                        
            var imagem = $(this).find('img').attr('src');
            var imgInDiv = $("#showImage div.image").find('img').attr('src');


            if(imgInDiv != imagem ){ 
                $("#ficheirosmedia").css('display', 'none');
                
                $(".image > img").remove();
                $("#showImage .image").append('<img id="img_'+imagemNameWthExtByURL(imagem)+'" src="'+imagem+'" alt=""/>');

                $("#configuracoesIdioma .banners-langs #showImage").css('display', 'block');
                 $("#addbutton").css('display', 'block');
            }
            else
            if($("#showImage .image").find('img').length == 0){
                $("#ficheirosmedia").css('display', 'none');

                $("#showImage .image").append('<img id="'+imagemNameWthExtByURL(imagem)+'" src="'+imagem+'" alt=""/>');

                $(".actions ul li a.edit").attr('href', '/management/banners/editar-imagem?imagem='+imagemNameByURL(imagem));

                $("#configuracoesIdioma .banners-langs #showImage").css('display', 'block');
                $("#addbutton").css('display', 'block');
            }

    });
    
   
   var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
   var pieces = url.split("/banners/editar")[1].split("/");
   var seccao = (typeof pieces[2] != 'undefined' ? pieces[2] : 'homePage');
        
createUploader(seccao);
    
})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
    
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

function changeIdDiv(div_id, newPos){
    $("#"+div_id+".preview span").html(newPos);
}

function deleteImage(){
    
    var imagem = imagemNameByURL($('#showImage div.image').find('img').attr('src'));
    bootbox.confirm(confirmDeletingFile+' ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/banners/eliminar-imagem',{imagem: imagem}, 
                function(data){
                        var dadosDevolvidos = data.split("\n");
                        
                        var resultado = dadosDevolvidos[0];
                        if(resultado == 'sucess'){

                            $("#"+imagemNameWthExtByURL('/'+imagem)+".preview").remove();
                            $("#configuracoesIdioma .banners-langs #showImage").css('display', 'none');
                            $("#datas-publicacao").css('display', 'none');
                            $("#ficheirosmedia").css('display', 'block');
                            $("#addbutton").css('display', 'none');
                            $("#donebutton").css('display', 'none');

                        }else{
                            bootbox.alert(errorDeletingFile);
                        }
                });
        }});
    
    
}

function createUploader(seccao){    
        var idioma_iso = $('ul.languages li a.language-tab.selected').text();
                
        var uploader = new qq.FileUploader({
            element: document.getElementById('ficheirosmedia'),
            listElement: document.getElementById('separate-list'),
            action: window.location.protocol + "//" + window.location.host + '/management/banners/editar/idioma/'+idioma_iso+'/seccao/'+seccao,
            debug: true,
            onComplete: function(id, fileName, responseJSON){
                if(responseJSON.success == true){
                    
                    var count = $(".preview").size();
                    $("#preview").append('<div class="preview" id="'+imagemNameWthExtByURL(responseJSON.nameFile)+'" data="'+responseJSON.id+'"><img src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/banners/'+responseJSON.nameFile+'" alt=""/><span>'+count+'</span></div>');
                    
                    
                     $( "#preview" ).sortable({
                            opacity: 0.7,
                            connectWith: ".preview",
                            stop: function(event, ui) {
                                result = "";
                                order = [];
                                $('.preview').each(function(i) {

                                    result += "["+i+"]: " +imagemNameByURL($(this).find('img').attr('src')) + " , ";
                                    changeIdDiv(imagemNameWthExtByURL($(this).find('img').attr('src')), i);

                                    order.push(imagemNameByURL($(this).find('img').attr('src')));
                                });
                                $.post(window.location.protocol + "//" + window.location.host + '/management/banners/ordenar',{
                                    'order[]': order
                                }, function(data){});
                            }
                        });
                                       
                    
                    
                    $('.preview').click(function(e) {
                            e.preventDefault();
                            
                            var id = $(this).attr("data");
                            if(id){
                                $("#waiting").css('display', 'block');
                                $.get(window.location.protocol + "//" + window.location.host + '/management/banners/info-banner/id/'+id, function(data){
                                    $("#formFields").html(data);
                                    $("#datas-publicacao").stop(true).slideDown('fast');
                                    $("#donebutton").css('display', 'block');

                                    $("#waiting").css('display', 'none');
                                });
                            }
                            
                            
                            var imagem = $(this).find('img').attr('src');
                            var imgInDiv = $("#showImage div.image").find('img').attr('src');
                                                                                    
                            if(imgInDiv != imagem ){ 
                                $("#ficheirosmedia").css('display', 'none');
                                
                                
                                $(".image > img").remove();
                                $("#showImage .image").append('<img id="img_'+imagemNameWthExtByURL(imagem)+'" src="'+imagem+'" alt=""/>');
                                                                
                                //$(".actions ul li a.edit").attr('href', '/management/banners/editar-imagem?imagem='+imagemNameByURL(imagem)); "+window.location.protocol +"//"+window.location.host +"
                                
                                $("#configuracoesIdioma .banners-langs #showImage").css('display', 'block');
                            }
                            else
                            if($("#showImage .image").find('img').length == 0){
                                $("#ficheirosmedia").css('display', 'none');
                                
                                $("#showImage .image").append('<img id="'+imagemNameWthExtByURL(imagem)+'" src="'+imagem+'" alt=""/>');
                                
                                //$(".actions ul li a.edit").attr('href', '/management/banners/editar-imagem?imagem='+imagemNameByURL(imagem));

                                $("#configuracoesIdioma .banners-langs #showImage").css('display', 'block');
                            }
                            
                    });
                    
                }else{
                    bootbox.alert(errorLoadingFile);
                }
                /*
                console.debug(responseJSON);
                bootbox.alert(responseJSON)*/
            },
            template: '<div class="qq-uploader">' + 
                        '<div class="qq-upload-drop-area"><span>'+dropToUpload+'</span></div>' +
                        '<div class="qq-upload-button">'+uploadImage+'<br /> '+dimensionsRecommended+' X x Y '+pxs+'</div>' +
                        '<ul class="qq-upload-list"></ul>' + 
                        '</div>',
            
            showMessage: function(message){
                bootbox.alert(message);
            }

        });           
}   

function validarPDF(file){
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    if(extension != '' && extension != 'pdf'){
        bootbox.alert(noPdfFile);
        $('.fileupload').fileupload('clear');
    }
}
    

function voltar(){ 
    $(".image > img").remove();
    $("#datas-publicacao").css('display', 'none');
    $("#configuracoesIdioma .banners-langs #showImage").css('display', 'none');
    $("#ficheirosmedia").css('display', 'block');
    $("#addbutton").css('display', 'none');
    $("#donebutton").css('display', 'none');
}
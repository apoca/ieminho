var starDateTime = '';
var endDateTime = '';
$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
    
    $('.demo1').bootstrapDualListbox({
        preserveselectiononmove: 'moved',
        moveonselect: true,
        infotext: 'Selec. {0} resultado(s)',
        infotextfiltered: '<span class="label label-warning">Res.:</span> {0} de {1}',
        infotextempty: 'Vazio'
    });
    
    $('#starDateTime').datetime({
        value: ''
    });
    
  $("table.tablesorter").tablesorter({
        theme : "bootstrap",
        widthFixed: true,
        headerTemplate : '{content} {icon}',
        widgets : [ "uitheme", "filter", "zebra" ]
    }).tablesorterPager({
        container: $(".pager"),
        cssGoto  : ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
    });
    
    $('#endDateTime').datetime({value: ''});
    
    
    $("form.target").submit(function () {
        
        if($("#starDateTime").val() == ''){
                bootbox.alert('Não se esqueça da data hora de início.');
                $('#starDateTime').focus();
                return false;
        }else
        if($("#endDateTime").val() == ''){
                bootbox.alert('Não se esqueça da data hora de fim.');
                $('#endDateTime').focus();
                return false;
        }else
        if(($("#starDateTime").val() >= $("#endDateTime").val()))
        {
            bootbox.alert("Atenção: A data e hora de fim terá de ser superior à data hora de início.");
            $('#endDateTime').focus();
            return false;
        }else
        if($("#pontos").val() == ''){
                bootbox.alert('Introduza os pontos');
                $('#pontos').focus();
                return false;
        }
        else{
            return true;
        }
    });
    
    $("select#paises").change(function () {
          //$("input:radio[name=tipoLocal]").index(1).removeAttr("disabled");
         
         var valores = ($(this).val().toString());
         
         var array = valores.split(',');
         
         if(array.length>1){ //se existir 1 país seleccionado
             $("#cidades").css('display', 'none');
             $("#zonas").trigger("destroy");
             var pais_id = array[0];
         }
         
    });
    
    
    
    $("input:radio[name=tipoLocal]").click( function () {
        
         var tipoLocal = $(this+':checked').val();
         
         
         if(tipoLocal=='z'){ //se seleccionar cidade (de um país)
             //mostra
            var valores = ($("#paises").val().toString());
            var array = valores.split(',');
            
            if(array.length==1){ //se existir 1 país seleccionado
                
                $("#cidades").css('display', 'block');
                var pais_id = array[0];
                $("#zonas").fcbkcomplete({
                    json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/zonas/pais/'+pais_id,
                    addontab: true,  
                    maxitems: 10,
                    input_min_size: 0,
                    height: 10,
                    cache: true,
                    newel: false,
                    complete_text: ''
                });
            }else{
                $("#zonas").trigger("destroy");
                $("#cidades").css('display', 'none');
                bootbox.alert('Seleccione apenas um país.');
                $('input:radio[name=tipoLocal]').filter('[value=p]').attr('checked', true);
            }
         }else{
             //esconde
             $("#zonas").trigger("destroy");
             $("#cidades").css('display', 'none');
         }
    });
    
    
    var idioma_iso = $('ul.idiomas li a.selected').text();
    
     $("#paises").fcbkcomplete({
        json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/paises/idioma/'+idioma_iso,
        addontab: true,  
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        newel: false,
        complete_text: 'Escreva e seleccione o registo'
    });
    
    $("#profissao_id").fcbkcomplete({
        json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/profissoes/idioma/'+idioma_iso,
        addontab: true,  
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        newel: false,
        complete_text: 'Escreva e seleccione o registo'
    });
    
    $("#habilitacoes_id").fcbkcomplete({
        json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/habilitacoes/idioma/'+idioma_iso,
        addontab: true,  
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        newel: false,
        complete_text: 'Escreva e seleccione o registo'
    });
    
    $("#conhecimento_id").fcbkcomplete({
        json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/conhecimento/idioma/'+idioma_iso,
        addontab: true,  
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        newel: false,
        complete_text: 'Escreva e seleccione o registo'
    });
    
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
            
            if($('div.controls .tipos').hasClass('active')){

                $('#tipo_'+localStorage['id']).removeClass('active');
                $('#tipo_'+localStorage['id']).slideUp('normal', function(){ });//.controls .tipos 
                
                if($(this).val() == 1 || $(this).val() == 2){
                    
                    $(this).val() == 2 ? $('#addressURL').css('display', 'none') : $('#addressURL').css('display', 'block');
                    
                    $('#tipo_1_2').slideDown('normal');
                    $('#tipo_1_2').addClass('active'); 
                    localStorage['id'] = '1_2';
                }else{
                    $('#tipo_'+$(this).val()).addClass('active');
                    $('#tipo_'+$(this).val()).slideDown('normal');
                    localStorage['id'] = $(this).val();
                }
                
                
            }else{
                if($(this).val() == 1 || $(this).val() == 2){
                    
                    $(this).val() == 2 ? $('#addressURL').css('display', 'none') : $('#addressURL').css('display', 'block');
                    
                    $('#tipo_1_2').slideDown('normal');
                    $('#tipo_1_2').addClass('active'); 
                    localStorage['id'] = '1_2';
                }else{
                    $('#tipo_'+$(this).val()).slideDown('normal');
                    $('#tipo_'+$(this).val()).addClass('active'); 
                    localStorage['id'] = $(this).val();
                }
            }
            
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
            
        }
        });
    });
    
    
    $('#idade').slider().on('slide', function() {
        var valores = ($(this).val().toString());
        var min = valores.split(',')[0];
        var max = valores.split(',')[1];
        $("#idadesSelected").html("[" + min + " - " + max+'] anos');
    });
    
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
            
        $("ul.nav li").removeClass('active');
            
    });
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
        
            action: window.location.protocol + "//" + window.location.host + '/management/campanhas/imagens',
            
            name: 'uploadfile',
            onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                            // extension is not allowed 
                            alert(allowedFiles+' JPG, JPEG, PNG, GIF.');
                            return false;
                    }
                    //status.text('Uploading...');
                    $("#preview").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Uploading...."/>');
            },
            onComplete: function(file, response){
                    var data = $.parseJSON(response);
                    if(data.success===true){
                            $("span#imgRepres").css('display', 'none'); 
                            $('#preview').html('<img width="570" height="120" src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/campanhas/'+data.file+'" alt="" />');
                            $('span#deleteFile').css('display', 'block'); 
                            $("#width").val(data.width);
                            $("#height").val(data.height);
                            $('#dimensions').css('display', 'block'); 
                            $("#fileImage").val(data.file);
                            
                            $('.previewCampanha').html(livePreviewBanner($('input[name=clickUrl]').val(), data.file, data.width, data.height));
                    } else{
                            $('#preview').html('Erro ao efectuar upload');
                    }
            }
    });
    
    $("#deleteFile").click(function() {
        var imagem = imagemNameByURL($("#upload #preview img").attr("src")); 
        
        bootbox.confirm('Tem a certeza que deseja remover a imagem?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/campanhas/eliminar-imagem',{imagem: imagem}, 
                function(data){
                    var dadosDevolvidos = data.split("\n");
                    var resultado = dadosDevolvidos[0];
                    if(resultado == 'sucess'){
                        $('span#deleteFile').delay(500).fadeOut(500);
                        $('#preview').html('');
                        $("#fileImage").val('');
                        $("span#imgRepres").css('display', 'block'); 
                        $('#dimensions').css('display', 'none'); 

                    }else{
                        bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                    }
                });
        }});
    
    });
        
    $('.fancybox').fancybox();
    
    $('textarea[name=custombannercode]').keyup(function() {
        $('.previewCampanha').html($(this).val());
    });
    
    $('input[name=clickUrl]').blur(function() {
        var filter = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        var file = $('#fileImage').val();
        var width = $('#width').val();
        var height = $('#height').val();
        
        if(file){
           if(filter.test($(this).val())){
                $('.previewCampanha').html(livePreviewBanner($(this).val(), file, width, height));
            }else{
                $('.previewCampanha').html(livePreviewBanner('', file, width, height));
            } 
        }
    });
    
    $('#width').blur(function() {
        var filter = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        var file = $('#fileImage').val();
        var width = $('#width').val();
        var height = $('#height').val();
        
        if(file && width && height){
           if(filter.test($(this).val())){
                $('.previewCampanha').html(livePreviewBanner($(this).val(), file, width, height));
            }else{
                $('.previewCampanha').html(livePreviewBanner('', file, width, height));
            } 
        }
    });
    
    $('#height').blur(function() {
        var filter = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        var file = $('#fileImage').val();
        var width = $('#width').val();
        var height = $('#height').val();
        
        if(file && width && height){
           if(filter.test($(this).val())){
                $('.previewCampanha').html(livePreviewBanner($(this).val(), file, width, height));
            }else{
                $('.previewCampanha').html(livePreviewBanner('', file, width, height));
            } 
        }
    });
    
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            },
            pontos:{
                required: true,
                digits: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
    
})(jQuery);

function livePreviewBanner(href, image, width, height){
    var preview = '';
    if(href!=''){
        preview = '<a href="'+href+'">';
        preview += '<img '+((width!='') ? 'width="'+width+'"' : '') + ((height!='') ? 'height="'+height+'"' : '')+' src="/uploads/images/campanhas/'+image+'">'; 
        preview += '</a>';
    }else{
        preview = '<img '+((width!='') ? 'width="'+width+'"' : '') + ((height!='') ? 'height="'+height+'"' : '')+' src="/uploads/images/campanhas/'+image+'">';
    }
    $('#custombannercode').val(preview);
    return preview;
}



function obterCampanhas(tipo, page){
    $('#loading').css('display', 'block');
    $(".campanha").find('button.addNew').css('display', 'none');
    
    $('#rightSide').animate({
            right: '50%'
        });
    $('#leftSide ul#main-contentIdiomas-nav.nav li').removeClass('active');
    $("li#tipo_"+tipo).addClass('active');  
    $("li#tipo_"+tipo).find('button.addNew').css('display', 'block');
    
    

    $('#rightSide').load(window.location.protocol + "//" + window.location.host + '/management/campanhas/obter-campanhas/tipo/'+tipo+'/page/'+page);
    
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
   
}
function notificarUtilizadores(campanha_id, idioma_iso){
   // bootbox.alert(campanha_id+' // '+idioma_iso);
    $("#waiting").css('display', 'block');
    $("#registos").css('display', 'none');	
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/campanhas/notificar',{id: campanha_id, idioma:idioma_iso}, 
    function(data){
            $("#registos").html(data);
            $("#registos").css('display', 'block');
            $("#waiting").css('display', 'none');
    });
    
}





function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}


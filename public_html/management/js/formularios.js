
$(function() {
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();

    $('.demo1').bootstrapDualListbox({
        preserveselectiononmove: 'moved',
        moveonselect: true,
        infotext: 'Selec. {0} resultado(s)',
        infotextfiltered: '<span class="label label-warning">Res.:</span> {0} de {1}',
        infotextempty: 'Vazio'
    });


    $('#data_inicio').datetime({
        withTime: false,
        format: 'yy-mm-dd'
    });
    
    $('#data_fim').datetime({
        withTime: false,
        format: 'yy-mm-dd'
    });
    
    $("form#formularios").submit(function() {
        if ($('#data_inicio').val() === '') {
            bootbox.alert('Não se esqueça da data hora de início.');
            $('#data_inicio').focus();
            return false;
        } else
        if ($('#data_fim').val() === '') {
            bootbox.alert('Não se esqueça da data hora de fim.');
            $('#data_fim').focus();
            return false;
        } else
        if (($('#data_inicio').val() >= $('#data_fim').val()))
        {
            bootbox.alert("Atenção: A data e hora de fim terá de ser superior à data hora de início.");
            $('#data_fim').focus();
            return false;
        } else {
            return true;
        }
    });
    
    $('div.btn-group[data-toggle-name=*]').each(function() {
        var group = $(this);
        var form = group.parents('form').eq(0);
        var name = group.attr('data-toggle-name');
        var hidden = $('input[name="' + name + '"]', form);
        $('button', group).each(function() {
            var button = $(this);
            button.live('click', function() {
                hidden.val($(this).val());
            });
            if (button.val() === hidden.val()) {
                button.addClass('active');
            }
        });
    });
    
    $('.fancybox').fancybox();
    
    $('#formularios').validate({
        rules: {
            titulo: {
                required: true
            },
            google_link: {
                required: true,
                url: true
            },
            valor: {
                required: true,
                number: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
                    .text('OK!').addClass('valid')
                    .closest('.control-group').addClass('success');
        }
    });

})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}



$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    
    $("#titulo").focus();
    
    $( "#allProducts").sortable({
        opacity: 0.7,
        connectWith: ".plan-container",
        stop: function(event, ui) {
            result = "";
            order = [];
            $('.plan-container').each(function(i, elm) {
                order.push(elm.id.split('_')[1]);
            });
         /*    bootbox.alert("A actualizar nova ordenação BD: <br />"+order);*/
           $.post(window.location.protocol + "//" + window.location.host + '/management/produtos/ordenar',{'order[]': order}, 
                function(data){   
                });
        }
    });
    
    $("input#productFind").keyup(function(){
        var q = $(this).val();
    
        if(q.length > 2){
            $("#products").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Loading...."/>');
            $("#whereImI").html('<h4>'+produto+'s: "'+q+'"</h4>')
            $.get(window.location.protocol + "//" + window.location.host +"/management/produtos/pesquisa/item/"+q, function(data){
                if(q){
                    $("#products").html(data);
                } else {
                    $("#products").html("Sem resultados");
                }
            });
        }
        if(q.length == 0){
            window.location.reload();
        }
    
    });
      
            
     $('.closeItens').live('click', function (e) {
            var $lefty = $('#rightSide');
                $lefty.animate({
                right: '50%'
            });
            
            $("ul.nav li").removeClass('active');
   //         $("#rightSide").delay(500).fadeOut(100).html('');
   //         $('#rightSide').html('');
            
   });
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
        
            action: window.location.protocol + "//" + window.location.host + '/management/produtos/imagens',
            
            name: 'uploadfile',
            onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                            
                            alert(allowedFiles+' JPG, JPEG, PNG, GIF.');
                            return false;
                    }
                    $("#preview").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Uploading...."/>');
            },
            onComplete: function(file, response){
                    var data = $.parseJSON(response);
                    
                    if(data.success===true){
                            $("span#imgRepres").css('display', 'none');   
                            $('#preview').html('<img width="166" height="110" src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/produtos/'+data.file+'" alt="" />');
                            $('span#deleteFile').css('display', 'block'); 
                            $("#fileImage").val(data.file);
                    } else{
                            $('#preview').html(errorUploadingFile);
                    }
            }
    });
    $('.fileupload').fileupload();
    
    
    $("#ficheiroPDF").change(function() {
        var file = $(this).val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if(extension != '' && extension != 'pdf'){
            bootbox.alert(noPdfFile);
            $('.fileupload').fileupload('clear');
        }
    });
    
        
    $("#deleteFile").click(function() {
        var imagem = imagemNameByURL($("#upload #preview img").attr("src"));
        
        bootbox.confirm(confirmDeletingFile+' ('+imagem+') ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/produtos/eliminar-imagem',{imagem: imagem, tipo: 'thumbnail'}, 
                function(data){
                        var dadosDevolvidos = data.split("\n");
                        var resultado = dadosDevolvidos[0];
                        if(resultado == 'sucess'){
                            $('span#deleteFile').css('display', 'none'); 
                            $('#preview').html('');
                            $("#fileImage").val('');
                            $("span#imgRepres").css('display', 'block'); 
                        }else{
                            bootbox.alert(errorDeletingFile);
                        }
                });
        }});
        
        
    });
    
    $("#estado").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'A' : 'I' );
        if(val==true){
            $(".desc").text("ON");
        }else{
            $(".desc").text("OFF");
        } 
    });
    
    
    $('input').change(function() { 
          somethingChanged = true;
    });
    
    $(".lang-tab").click(function() {
        if(somethingChanged == true){
            aviso();
        }
    });
    
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    if(action == 'editar'){    
            $(".fileupload-exists").click(function() {
                $("#apagou").val('S');
            });
    }
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        required: true
	      }
	    },
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });
      
      
    
     
    
})(jQuery);

var somethingChanged = false;

function aviso(){
    $(window).on('beforeunload', function(){
        return confirmExit+' ?';
    });
}

function obterCategorias(areanegocio_id){
    /*if(!$("#rightSide").is(':visible')) {
        $("#rightSide").css('display', 'block');
        
    }*/
    $('#rightSide').animate({
                right: '50%'
            });
    $("ul.nav li").removeClass('active');
    $("li#area_"+areanegocio_id).addClass('active');
    //var areaNeg = $(this).find('table tbody tr td .name').text();
    
    $('#rightSide').load(window.location.protocol + "//" + window.location.host + '/management/produtos/obter-categorias/id/'+areanegocio_id);
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
}

function language(lang){              
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/idioma',{lang: lang}, 
	function(data){
                window.location.reload();			
        });
}

function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

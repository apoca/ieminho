
$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    
        
    $('.fancybox').fancybox();
    
    $('span#preview').live('click', function (e) {
        var designacao = $('input[name="designacao"]').val();
        var text = tinyMCE.activeEditor.getContent({format : 'raw'});
        var imagem = $("#imgAN.control-group img").attr("src");
        
        $("h3#tituloConteudo").text(designacao);
        
        if (text.match(/<hr\s+id=("|')content-readmore("|')\s*\/*>/i)) {
            var pieces = text.split('<hr id="content-readmore">');
            
            $(".introducaoConteudo").html('<p>'+pieces[0]+'</p>');
            $(".descricaoConteudo").html(pieces[1]);
        }
        else{
            $(".descricaoConteudo").html(text);
        }
        if(imagem){
            $(".imagem-detalhe-cs img").attr("src", ""+imagem+"");
        }
        
    });
    
    
})(jQuery);

function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function alterarEstado(id, estado) {
    
    //bootbox.alert(' ID '+id + 'ESTADO '+estado);
    
    $("#waiting").css('display', 'block');
    $("#registos").css('display', 'none');	
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/vencedores-premios/alterar-estado',{id: id, estado:estado}, 
    function(data){
            $("#registos").html(data);
            $("#registos").css('display', 'block');
            $("#waiting").css('display', 'none');
    });
    
 /*   bootbox.confirm('Deseja notificar o utilizador?', function(result) { 
        if (result) { 
         bootbox.alert('Sim ----- ID '+id + 'ESTADO '+estado);
        }else{
            bootbox.alert('Não ----- ID '+id + 'ESTADO '+estado);
        }
    });*/
}

function deleteImage(){
     var imagem = imagemNameByURL($('#imgAN.control-group').find('img').attr('src'));
   // var imagem = imagemNameByURL($('foto').val());
    bootbox.confirm('Tem a certeza que deseja remover a imagem ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/premios/eliminar-imagem',{
                imagem: imagem
            }, 
            function(data){
                var dadosDevolvidos = data.split("\n");
                var resultado = dadosDevolvidos[0];
                if(resultado == 'sucess'){

                    $("#uploadImgAN").css('display', 'block');
                    $("#imgAN img").attr("src", "");
                    $("#imgAN").css('display', 'none');

                }else{ 
                    bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                }
            });
        }
    });
}

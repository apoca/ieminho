

$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    
    
    
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
            
        $("ul.nav li").removeClass('active');
            
    });
    
    $("#estado").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'A' : 'I' );
        if(val===true){
            $(".desc").text("ON");
        }else{
            $(".desc").text("OFF");
        } 
    });
        
    $('.fancybox').fancybox();
    
    
    $(".elemento-change").change( function () {
        var varURL      = $("option:selected", this).val();
        window.open(window.location.protocol + "//" + window.location.host + '/management/privilegios/index/role/'+varURL,'_self');
    });
    
        
})(jQuery);
function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}

function obterRecursos(recurso){
    $('#loading').css('display', 'block');
    
    $('#leftSide ul#main-contentIdiomas-nav.nav li').removeClass('active');
    var role = $('#selectRole').val();
    $('#rightSide').animate({
            right: '50%'
    });
    $('#leftSide ul#main-contentIdiomas-nav.nav li#tipo_'+recurso).addClass('active');
    $('#rightSide').load(window.location.protocol + "//" + window.location.host + '/management/privilegios/recursos/recurso/'+recurso+'/role/'+role);
    
    
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
}
function inserirRecurso(controller){
    
    bootbox.prompt('Nome do recurso (controller) ', 'Cancelar', 'OK', function(result) {
            if (result != null) {
                
                var dados = {};
                dados.descricao = result;
                dados.controladorNome = controller;
    
                $.post(window.location.protocol + "//" + window.location.host + '/management/privilegios/adicionar-recurso',{dadosPOST: dados}, 
                function(data){
                        //var dadosDevolvidos = data.split("\n"); 
                        //var index = dadosDevolvidos[0];
                        var data = $.parseJSON(data);
                        if(data.success===true){
                           window.location.reload(); 
                        }else{
                            bootbox.alert('Problema ao inserir registo.');
                        }
                        
                });
                
            }else{
                $('#'+controller).prop('checked',false);
            }
        }, ucfirst(controller));
        
        
}

function inserirPrivilegio(controller, action){
    
    bootbox.prompt('Nome da action ('+controller+') ', 'Cancelar', 'OK', function(result) {
            if (result != null) {
                
                var dados = {};
                dados.acao = action;
                dados.descricao = result;
                dados.controladorNome = controller;
    
                $.post(window.location.protocol + "//" + window.location.host + '/management/privilegios/adicionar-privilegio',{dadosPOST: dados}, 
                function(data){
                        var data = $.parseJSON(data);
                        if(data.success===true){
                           $('#'+controller+'_'+action).prop('checked',true);
                           $('#'+controller+'_'+action).prop('disabled',true);
                        }else{
                            
                            $('#'+controller+'_'+action).prop('checked',false);
                            bootbox.alert('Problema ao inserir registo.');
                        }
                        
                });
                }else{
                $('#'+controller+'_'+action).prop('checked',false);
            }
        }, ucfirst(action));
        
        
}

function ucfirst(str) {
  str += '';
  var f = str.charAt(0).toUpperCase();
  return f + str.substr(1);
}

function enviaPermissao(varRecurso) {
        
    var varRole = $('#selectRole').val();
    var dados = {};
    dados.privilegio = varRecurso;
    dados.role = varRole;

    var checked =  $('#'+varRecurso).is(':checked');      
    var flash = $("#flash_"+varRecurso);


    if (checked) {
            $('#'+varRecurso).hide();
            flash.css('display','block');
            dados.permitir = 'S';

            $.post(window.location.protocol + "//" + window.location.host + '/management/privilegios/alterar-privilegio',{dadosPOST: dados}, 

            function(data){
                var data = $.parseJSON(data);
                if(data.success===false){
                    bootbox.alert(data.mensagem);
                }
                flash.css('display','none');
                $('#'+varRecurso).show();
            });

        } else {
            flash.css('display','block');
            $('#'+varRecurso).hide();
            dados.permitir = 'N';

            $.post(window.location.protocol + "//" + window.location.host + '/management/privilegios/alterar-privilegio',{dadosPOST: dados}, 

            function(data){
                var data = $.parseJSON(data);
                if(data.success===false){
                    bootbox.alert(data.mensagem);
                }
                flash.css('display','none');
                $('#'+varRecurso).show();
            });
        }
}





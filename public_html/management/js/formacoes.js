

$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#codigo").focus();
    
    var idioma_iso = $('ul.idiomas li a.selected').text();
    //alert(idioma_iso);
    
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
        
        $("#leftSide ul#main-contentIdiomas-nav.nav li.areaNegocio").removeClass('active');
            
    });
    
    
    $("#logo").change(function() {
        var file = $(this).val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        var arr = ["jpg", "jpeg",  "png", "gif"];
        
        if(extension != '' && ($.inArray(extension, arr) == -1)){
            bootbox.alert("Introduza um ficheiro com a extensão: jpg, jpeg, png ou gif.");
            $('.fileupload').fileupload('clear');
        }
    });
    
    
    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
            
    $('.fancybox').fancybox();
    
    $('span#preview').live('click', function (e) {
        var designacao = $('input[name="designacao"]').val();
        var text = tinyMCE.activeEditor.getContent({format : 'raw'});
        var imagem = $("#imgAN.control-group img").attr("src");
        
        $("h3#tituloConteudo").text(designacao);
        
        if (text.match(/<hr\s+id=("|')content-readmore("|')\s*\/*>/i)) {
            var pieces = text.split('<hr id="content-readmore">');
            
            $(".introducaoConteudo").html('<p>'+pieces[0]+'</p>');
            $(".descricaoConteudo").html(pieces[1]);
        }
        else{
            $(".descricaoConteudo").html(text);
        }
        if(imagem){
            $(".imagem-detalhe-cs img").attr("src", ""+imagem+"");
        }
        
    });
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            },
            nHoras:{
                number: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
          
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    if(action == 'editar'){
         $(".fileupload a.close.fileupload-exists").click(function() {
            $("#apagou").val('S');
        });
        var banner = $("#imgAN.control-group img").attr("src");
        
        if ($('#imgAN')) {
            if(!banner){
                $("#uploadImgAN").css('display', 'block');
            }else{
                $("#uploadImgAN").css('display', 'none');
            }
            
        }else{
            $("#imgAN").css('display', 'none'); 
        }   
    }
    
    createUploader();
    
})(jQuery);


function obterFormacoes(tipo, idioma_iso, page){
    $(".areaNegocio").find('button.addNew').css('display', 'none');
    //var idioma_iso = $('ul.idiomas li a.selected').text();
    $('#rightSide').animate({
            right: '50%'
        });
    $("#leftSide ul#main-contentIdiomas-nav.nav li.areaNegocio").removeClass('active');
    $("li#tipo_"+tipo).addClass('active');  
    $("li#tipo_"+tipo).find('button.addNew').css('display', 'block');
    
    $('#rightSide').load(window.location.protocol + "//" + window.location.host + '/management/formacoes/obter-formacoes/tipo/'+tipo+'/idioma/'+idioma_iso+'/page/'+page);
    
    var $lefty = $('#rightSide');
    $lefty.animate({
        right: '1%'
    });
}
function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
        
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

function createUploader(){  
     
    var uploader = new qq.FileUploader({
        element: document.getElementById('ficheirosmedia'),
        listElement: document.getElementById('separate-list'),
        action: window.location.protocol + "//" + window.location.host + window.location.pathname,
        debug: true,
        onComplete: function(id, fileName, responseJSON){
            if(responseJSON.success == true){
                $("#uploadImgAN").css('display', 'none');
                $("#imgAN.control-group img").attr("src", "/uploads/images/conteudos/"+responseJSON.nameFile+"");
                $("#foto").val(responseJSON.nameFile);
                $("#imgAN").css('display', 'block');
                
                
            }else{
                bootbox.alert("Não foi possível carregar o ficheiro.");
            }
        },
        template: '<div class="qq-uploader">' + 
        '<div class="qq-upload-drop-area"><span>Arraste ficheiros para carregá-los</span></div>' +
        '<div class="qq-upload-button">carregar imagem<br /> dimensões aconselhada 654x245 píxeis</div>' +
        '<ul class="qq-upload-list"></ul>' + 
        '</div>',
        showMessage: function(message){ 
            bootbox.alert(message);
        }

    });           
}

function deleteImage(){
    
    var imagem = imagemNameByURL($('#imgAN.control-group').find('img').attr('src'));
    bootbox.confirm('Tem a certeza que deseja remover a imagem ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/formacoes/eliminar-imagem',{
                imagem: imagem, 
                tipo: 'conteudos'
            }, 
            function(data){
                var dadosDevolvidos = data.split("\n");
                var resultado = dadosDevolvidos[0];
                if(resultado == 'sucess'){

                    $("#uploadImgAN").css('display', 'block');
                    $("#imgAN img").attr("src", "");
                    $("#imgAN").css('display', 'none');

                }else{ 
                    bootbox.alert('Surgiu um problema ao remover a imagem do servidor.');
                }
            });
        }
    });
}

$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    
    $("#titulo").focus();
    $( "#main-contentIdiomas-nav" ).sortable({
        opacity: 0.7,
        connectWith: "li.categoria",
        stop: function(event, ui) {
            result = "";
            order = [];
            $('li.categoria').each(function(i, elm) {
                order.push(elm.id.split('_')[1]);
            });
            $.post(window.location.protocol + "//" + window.location.host + '/management/categorias/ordenar',{'order[]': order}, 
                function(data){
                        
                });
        }
    });
    
    
    
    $("#finishSubcat").click(function() {
        $("#rightForm").css('display', 'none');
    });
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
        
            action: window.location.protocol + "//" + window.location.host + '/management/categorias/imagens',
            
            name: 'uploadfile',
            onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                            // extension is not allowed 
                            alert(allowedFiles+' JPG, JPEG, PNG, GIF.');
                            return false;
                    }
                    //status.text('Uploading...');
                    $("#preview").html('<img src="'+window.location.protocol + "//" + window.location.host + '/management/img/loading.gif" alt="Uploading...."/>');
            },
            onComplete: function(file, response){
                    var data = $.parseJSON(response);
                    if(data.success===true){
                            $("span#imgRepres").css('display', 'none'); 
                            $('#preview').html('<img width="166" height="110" src="'+window.location.protocol + "//" + window.location.host + '/uploads/images/categorias/miniaturas/'+data.file+'" alt="" />');
                            $('span#deleteFile').css('display', 'block'); 
                            $("#fileImage").val(data.file);
                    } else{
                            $('#preview').html(errorUploadingFile);
                    }
            }
    });
    
    $("#deleteFile").click(function() {
        var imagem = imagemNameByURL($("#upload #preview img").attr("src"));
        
        bootbox.confirm(confirmDeletingFile+' ('+imagem+') ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/categorias/eliminar-imagem',{imagem: imagem, tipo: 'thumbnail'}, 
                function(data){
                        var dadosDevolvidos = data.split("\n");
                        var resultado = dadosDevolvidos[0];
                        if(resultado == 'sucess'){
                            $('span#deleteFile').css('display', 'none'); 
                            $('#preview').html('');
                            $("#fileImage").val('');
                            $("span#imgRepres").css('display', 'block'); 
                            
                        }else{
                            bootbox.alert(errorDeletingFile);
                        }
                });
        }});
        
        
    });
    
    $("#estado").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'A' : 'I' );
        if(val==true){
            $(".desc").text("ON");
        }else{
            $(".desc").text("OFF");
        } 
    });
    
    $("#subCategorias").change(function() {
        var val = $(this).is(':checked');
        $(this).val( $(this).is(':checked') ? 'S' : 'N' );
        if(val==true){
            $(".descSubCat").text("NÃO");
            $("#rightForm").css('display', 'block'); 
        }else{
            $(".descSubCat").text("SIM");
            $("#rightForm").css('display', 'none'); 
        } 
    });

    
    $('input').change(function() { 
          somethingChanged = true;
    });
    
    $(".lang-tab").click(function() {
        if(somethingChanged == true){
            aviso();
        }
    });
    
    var pathname = window.location.pathname.split("/");
    var action = pathname[pathname.length-3];
    
    if(action == 'editar'){    
            
        $("#subCategories").sortable({
            opacity: 0.7,
            connectWith: ".subCategoriaItem",
            stop: function(event, ui) {
                result = "";
                order = [];
                $('.subCategoriaItem').each(function(i, elm) {
                    order.push(elm.id.split('_')[1]);
                });
                $.post(window.location.protocol + "//" + window.location.host + '/management/categorias/ordenar',{'order[]': order}, 
                    function(data){

                    });
            }
        });

    }
    
    
    
    $('#createItemSubCat').click(function(){
        $("#subCatShowHide").css('display', 'block'); 
        $("#createItemSubCat").css('display', 'none');
        $("input#subCategoria").focus();
        /*
            var i = generateString(4);
            $('.listagem').append("<div class='subCategoriaItem' id='subCat_"+i+"'><input type='text' name='subCategoriaItem[]' id='focus_"+i+"' value='"+$('#subCategoria').val()+"' /><button class='del' onClick='javascript:deleteSubCatFromList(\""+i+"\");' type='button'>"+del+"</button><button class='edit' onClick='javascript:editSubCatFromList(\""+i+"\");' type='button'>"+edit+"</button></div>");
            list.push($('#subCategoria').val());
            $("input#subCategoria").val('');
            $("input#focus_"+i+"").focus();
            //adicionarSubCategoria(i)
            actionsInputs();
    */
    });
    
    $('#addSubCat').click(function(){
            var i = generateString(4);
            $('.listagem').append("<div class='subCategoriaItem' id='subCat_"+i+"'><input type='text' name='subCategoriaItem[]' id='focus_"+i+"' value='"+$('#subCategoria').val()+"' /><button class='del' onClick='javascript:deleteSubCatFromList(\""+i+"\");' type='button'>"+del+"</button><button class='edit' onClick='javascript:editSubCatFromList(\""+i+"\");' type='button'>"+edit+"</button></div>");
            
            $("#subCatShowHide").css('display', 'none'); 
            $("#createItemSubCat").css('display', 'block');
            
            $("input#subCategoria").val('');
            adicionarSubCategoria(i);
            actionsInputs();
            
    });
        
    $('#addSubCatToList').click(function(){
            var i = generateString(4);
            $('.listagem').append("<div class='subCategoriaItem' id='subCat_"+i+"'><input type='text' name='subCategoriaItem[]' id='focus_"+i+"' value='"+$('#subCategoria').val()+"' /><button class='del' onClick='javascript:deleteSubCatFromList(\""+i+"\");' type='button'>"+del+"</button><button class='edit' onClick='javascript:editSubCatFromList(\""+i+"\");' type='button'>"+edit+"</button></div>");
            
            $("#subCatShowHide").css('display', 'none'); 
            $("#createItemSubCat").css('display', 'block');
            
            $("input#subCategoria").val('');
            actionsInputs();
            sort();
            
    });
    
    sort();
    actionsInputs();
    
    
    
    
    
     $('#edit-profile').validate({
	    rules: {
	      designacao: {
	        required: true
	      },
	      areanegocio_id: {
	        required: true
	      }
	    },
	    highlight: function(label) {
	    	$(label).closest('.control-group').addClass('error');
	    },
	    success: function(label) {
	    	label
	    		.text('OK!').addClass('valid')
	    		.closest('.control-group').addClass('success');
	    }
      });
    
})(jQuery);

var somethingChanged = false;

function aviso(){
    $(window).on('beforeunload', function(){
        return confirmExit+' ?';
    });
}

function adicionarSubCategoria(subcat){
    var dados = {};
    dados.designacao = $('#subCat_'+subcat+' input').val();
    dados.pos = ($('.subCategoriaItem').length-1);
    dados.idioma_iso = $('ul.langs li a.selected').text();
    dados.categoria_pai = document.URL.split('id')[1].substring(document.URL.split('id')[1].lastIndexOf("/") + 1, document.URL.split('id')[1].length);
    
    //alert('designação: '+ designacao+ ' pos: '+pos+ ' idioma: '+idioma_iso+ ' areamegocio_id: '+areamegocio_id);
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/categorias/adicionar-sub-categoria',{dadosPOST: dados}, 
    function(data){
            var dadosDevolvidos = data.split("\n"); 
            var novo_id = dadosDevolvidos[0];
    
            $("#subCat_"+subcat+" button.edit").attr("onClick","javascript:editSubCatFromList("+novo_id+");");
            $("#subCat_"+subcat+" button.del").attr("onClick","javascript:deleteSubCatFromList("+novo_id+");");
            $("#subCat_"+subcat+"").attr("id", "subCat_"+novo_id+"");  
    });
    
    
    
}

function actionsInputs(){
    
    
    $(".subCategoriaItem input").bind('keydown', function(){ 
            $(this).parent().find("button.del").css('display', 'none'); 
            $(this).parent().find("button.edit").css('display', 'block'); 
    });

    $('.subCategoriaItem input').focusc(function(){ 
        
        $(this).data('val', $(this).val());
        $(this).parent().find("button.del").css('display', 'none'); 
        $(this).parent().find("button.edit").css('display', 'block'); 

    });
    $(".subCategoriaItem input").focusout(function() {
        
        if( $(this).val() == $(this).data('val') ){
            
            //var designacao = $('#subCat_'+subcat+' input').val();
            $(this).parent().find("button.edit").css('display', 'none'); 
            $(this).parent().find("button.del").css('display', 'block');
        }
    });
}

function sort(){
    $("#subCategories").sortable({
            opacity: 0.7,
            connectWith: ".subCategoriaItem",
            stop: function(event, ui) {
                result = "";
                order = [];
                $('.subCategoriaItem').each(function(i, elm) {
                    order.push(elm.id.split('_')[1]);
                });
                bootbox.alert(order);
            }
        });
}

function language(lang){              
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/idioma',{lang: lang}, 
	function(data){
                window.location.reload();			
        });
}

function deleteSubCatFromList(subcat){  
        bootbox.confirm(confirm+' ?', function(result) { 
        if (result) { 
            $.post(window.location.protocol + "//" + window.location.host + '/management/categorias/eliminar-sub-categoria',{'id': subcat}, 
            function(data){
                    $("#subCat_"+subcat+".subCategoriaItem").remove();   
            });
            
        }});
}

function editSubCatFromList(subcat){
    var designacao = $('#subCat_'+subcat+' input').val();
    
    $.post(window.location.protocol + "//" + window.location.host + '/management/categorias/actualizar-designacao',{'id': subcat, 'designacao': designacao}, 
    function(data){
            $('#subCat_'+subcat).parent().find("button.edit").css('display', 'none'); 
            $('#subCat_'+subcat).parent().find("button.del").css('display', 'block');   
    });
        
}

function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

function generateString(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
        if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
        if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
        if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
        if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;

}
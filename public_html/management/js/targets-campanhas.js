$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
        
    $('#starDateTime').datetimepicker({
        format: 'yyyy-MM-dd hh:mm:ss'
    });
    
    $('#endDateTime').datetimepicker({
        format: 'yyyy-MM-dd hh:mm:ss'
    });
    
    $('.sqlbuild').sqlquerybuilder({ 
		fields:[ 
		   {field:'-',name:'Utilizadores >', id:0,ftype:'{',defaultval:'-'}, 
		     {field:'utilizadores.id',name:'ID Utilizador', id:1,ftype:'string',defaultval:'1'}, 
		     {field:'utilizadores.nome',name:'Nome',id:2,ftype:'string',defaultval:'Ana'}, 
		     {field:'utilizadores.apelido',name:'Apelido', id:3,ftype:'string',defaultval:'Domingues'},
		     {field:'utilizadores.sexo',name:'Sexo', id:3,ftype:'string',defaultval:'M'},
                     {field:'utilizadores.dataNascimento',name:'Data de nascimento', id:4,ftype:'date',defaultval:'1985-10-26'},
		   {field:'-',name:' >', id:0,ftype:'}',defaultval:'-'}, 
		   {field:'-',name:'Freguesias >', id:0,ftype:'{',defaultval:'-'}, 
		     {field:'utilizador_freguesias.designacao',name:'Freguesia', id:4,ftype:'string',defaultval:'São Lázaro'},			
		   {field:'-',name:' >', id:0,ftype:'}',defaultval:'-'},
                   {field:'-',name:'Concelhos >', id:0,ftype:'{',defaultval:'-'}, 
		     {field:'utilizador_concelhos.designacao',name:'Concelho', id:4,ftype:'string',defaultval:'Braga'},			
		   {field:'-',name:' >', id:0,ftype:'}',defaultval:'-'},
                   {field:'-',name:'Distritos >', id:0,ftype:'{',defaultval:'-'}, 
		     {field:'utilizador_distritos.designacao',name:'Distrito', id:4,ftype:'string',defaultval:'Braga'},			
		   {field:'-',name:' >', id:0,ftype:'}',defaultval:'-'},
                   {field:'-',name:'Profissões >', id:0,ftype:'{',defaultval:'-'}, 
		     {field:'utilizador_profissoes.designacao',name:'Profissão', id:4,ftype:'string',defaultval:'Analista'},			
		   {field:'-',name:' >', id:0,ftype:'}',defaultval:'-'}
		   ],
        reportid:9000,   
        sqldiv: $('p#3000'),
        presetlistdiv: $('p#6000'),
	reporthandler:'reporthandler.php',
        datadiv:$('p#3001'),
        statusmsgdiv:$('p#3009'),		
        showgroup:false,
        showcolumn:false,
        showsort:false,
        showwhere:true,
        joinrules:[
           {table1:'utilizadores',table2:'utilizador_freguesias',rulestr:'JOIN utilizador_freguesias ON utilizadores.freguesia_id=utilizador_freguesias.id'},
           
           {table1:'utilizadores', table2:'utilizador_concelhos',rulestr:'JOIN utilizador_concelhos ON utilizadores.concelho_id=utilizador_concelhos.id'},
           
           {table1:'utilizadores', table2:'utilizador_distritos',rulestr:'JOIN utilizador_distritos ON utilizadores.distrito_id=utilizador_distritos.id'},
           
           {table1:'utilizadores', table2:'utilizador_profissoes',rulestr:'JOIN utilizador_profissoes ON utilizadores.profissao_id=utilizador_profissoes.id'}                      
        ],
        onchange:function(type){
            //alert('sqlbuilder:'+type);    
        },
        
        onselectablelist:function(slotid,fieldid,operatorid,chainid){
                          var vals='XX,YY,ZZ,';//+slotid+','+fieldid+','+operatorid+','+chainid; 
                          switch(fieldid){
                          
                            case '3'://utilizadores unit
                               vals += 'UN,KG,GR,TN';                             
                            break;   
                          
                          
                          }                          
                          return vals;
                         }
        
    }); 


    $('div.btn-group[data-toggle-name=*]').each(function(){
        var group   = $(this);
        var form    = group.parents('form').eq(0);
        var name    = group.attr('data-toggle-name');
        var hidden  = $('input[name="' + name + '"]', form);
        $('button', group).each(function(){
        var button = $(this);
        button.live('click', function(){
            hidden.val($(this).val());
        });
        if(button.val() == hidden.val()) {
            button.addClass('active');
        }
        });
    });
    
        
    $('.fancybox').fancybox();
    
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
    
    
    
    
})(jQuery);


function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}



//conteudos
noPdfFile = "O ficheiro que submeteu não é um pdf.";
readMoreAlreadyExist = "Já existe uma hiperligação Ler mais...";
errorUploadingFile = "Não foi possível carregar o ficheiro. ";
dropToUpload = "Arraste ficheiros para carregá-los";
uploadImage = "carregar imagem";
pxs = "píxeis";
dimensionsRecommended = "dimensões aconselhada";
confirmDeletingFile = "Tem a certeza que deseja remover a imagem";
errorDeletingFile = "Surgiu um problema ao remover a imagem do servidor.";
confirmExit = "Tem a certeza que pretende sair desta página?";

//utilizadores
allowedFiles = "Apenas são permitidos os seguintes ficheiros:";
errorUploadingFile = "Não foi possível carregar o ficheiro. ";
loginAlreadyExit = "Login de utilizador já existente.";
emailAlreadyExit = "E-mail de utilizador já existente.";
atLeastOne = "Seleccione pelo menos 1 opção.";

//websites
allowedFiles = "Apenas são permitidos os seguintes ficheiros:";
errorUploadingFile = "Não foi possível carregar o ficheiro. ";
confirmDeletingFile = "Tem a certeza que deseja remover a imagem";
errorDeletingFile = "Surgiu um problema ao remover a imagem do servidor."
pleaseWaiting = "<p id='waiting'>Por favor aguarde.</p>";
messageWaitingServer = "Por favor aguarde. A establecer ligação com o servidor.";

//banners
confirmDeletingFile = "Tem a certeza que deseja remover a imagem";
errorDeletingFile = "Surgiu um problema ao remover a imagem do servidor.";
errorLoadingFile = "Não foi possível carregar o ficheiro.";
dropToUpload = "Arraste ficheiros para carregá-los";
uploadImage = "carregar imagem";
pxs = "píxeis";
dimensionsRecommended = "dimensões aconselhada";

//areas de negócio
allowedFiles = "Apenas são permitidos os seguintes ficheiros:";
errorUploadingFile = "Não foi possível carregar o ficheiro. ";
confirmDeletingFile = "Tem a certeza que deseja remover a imagem";
errorDeletingFile = "Surgiu um problema ao remover a imagem do servidor.";
errorLoadingFile = "Não foi possível carregar o ficheiro.";
dropToUpload = "Arraste ficheiros para carregá-los";
uploadImage = "carregar imagem";
pxs = "píxeis";
dimensionsRecommended = "dimensões aconselhada";

//categorias
allowedFiles = "Apenas são permitidos os seguintes ficheiros:";
errorUploadingFile = "Não foi possível carregar o ficheiro. ";
confirmDeletingFile = "Tem a certeza que deseja remover a imagem";
errorDeletingFile = "Surgiu um problema ao remover a imagem do servidor.";
errorLoadingFile = "Não foi possível carregar o ficheiro.";
dropToUpload = "Arraste ficheiros para carregá-los";
uploadImage = "carregar imagem";
pxs = "píxeis";
dimensionsRecommended = "dimensões aconselhada";
itemAlreadyExistForceAdd = "Item existente, deseja adicioná-lo mesmo assim?";
edit = "OK";
del = "Apagar";
confirm = "Tem a certeza";

//produtos
produto = "Produto";
noPdfFile = "O ficheiro que submeteu não é um pdf.";
allowedFiles = "Apenas são permitidos os seguintes ficheiros:";
errorUploadingFile = "Não foi possível carregar o ficheiro. ";
confirmDeletingFile = "Tem a certeza que deseja remover a imagem";
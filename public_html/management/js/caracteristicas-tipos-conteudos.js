$(function(){
    $('.alert-success').delay(4000).fadeOut("slow");
    $("#designacao").focus();
    
    $('.fancybox').fancybox();
    
    
    $('#starDateTime').datetime({
        value: ''
    });
    
    
  $("table.tablesorter").tablesorter({
        theme : "bootstrap",
        widthFixed: true,
        headerTemplate : '{content} {icon}',
        widgets : [ "uitheme",  "zebra" ]
    }).tablesorterPager({
        container: $(".pager"),
        cssGoto  : ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
    });
    
    var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
    var pieces = url.split("/caracteristicas-tipos-conteudos/")[1].split("/");
    var accao = pieces[0];
    
    $("#referencia").change(function() {
        var ref = $(this).val();
        rightZone(ref);
    });
    
    var ref = $("#referencia").val();
   // rightZone(ref);
    
    
    
    $('#endDateTime').datetime({value: ''});
    
    
    $("form.target").submit(function () {
        
        if($("#starDateTime").val() == ''){
                bootbox.alert('Não se esqueça da data hora de início.');
                $('#starDateTime').focus();
                return false;
        }else
        if($("#endDateTime").val() == ''){
                bootbox.alert('Não se esqueça da data hora de fim.');
                $('#endDateTime').focus();
                return false;
        }else
        if(($("#starDateTime").val() >= $("#endDateTime").val()))
        {
            bootbox.alert("Atenção: A data e hora de fim terá de ser superior à data hora de início.");
            $('#endDateTime').focus();
            return false;
        }else
        if($("#pontos").val() == ''){
                bootbox.alert('Introduza os pontos');
                $('#pontos').focus();
                return false;
        }
        else{
            return true;
        }
    });
    
    $("select#paises").change(function () {
          //$("input:radio[name=tipoLocal]").index(1).removeAttr("disabled");
         
         var valores = ($(this).val().toString());
         
         var array = valores.split(',');
         
         if(array.length>1){ //se existir 1 país seleccionado
             $("#cidades").css('display', 'none');
             $("#zonas").trigger("destroy");
             var pais_id = array[0];
         }
         
    });
    
    
    
    $("input:radio[name=tipoLocal]").click( function () {
        
         var tipoLocal = $(this+':checked').val();
         
         
         if(tipoLocal=='z'){ //se seleccionar cidade (de um país)
             //mostra
            var valores = ($("#paises").val().toString());
            var array = valores.split(',');
            
            if(array.length==1){ //se existir 1 país seleccionado
                
                $("#cidades").css('display', 'block');
                var pais_id = array[0];
                $("#zonas").fcbkcomplete({
                    json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/zonas/pais/'+pais_id,
                    addontab: true,  
                    maxitems: 10,
                    input_min_size: 0,
                    height: 10,
                    cache: true,
                    newel: false,
                    complete_text: ''
                });
            }else{
                $("#zonas").trigger("destroy");
                $("#cidades").css('display', 'none');
                bootbox.alert('Seleccione apenas um país.');
                $('input:radio[name=tipoLocal]').filter('[value=p]').attr('checked', true);
            }
         }else{
             //esconde
             $("#zonas").trigger("destroy");
             $("#cidades").css('display', 'none');
         }
    });
    
    
    var idioma_iso = $('ul.idiomas li a.selected').text();
    
     $("#paises").fcbkcomplete({
        json_url: window.location.protocol + "//" + window.location.host + '/management/campanhas/paises/idioma/'+idioma_iso,
        addontab: true,  
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: true,
        newel: false,
        complete_text: 'Escreva e seleccione o registo'
    });
    
    $('.closeItens').live('click', function (e) {
        var $lefty = $('#rightSide');
        $lefty.animate({
            right: '50%'
        });
            
        $("ul.nav li").removeClass('active');
            
    });
    
    
    $('textarea[name=custombannercode]').keyup(function() {
        $('.previewCampanha').html($(this).val());
    });
    
    
    
    
    $('#edit-profile').validate({
        rules: {
            designacao: {
                required: true
            },
            pontos:{
                required: true,
                digits: true
            }
        },
        highlight: function(label) {
            $(label).closest('.control-group').addClass('error');
        },
        success: function(label) {
            label
            .text('OK!').addClass('valid')
            .closest('.control-group').addClass('success');
        }
    });
    
})(jQuery);

function obterCamposHTML(){
    $("#waiting").css('display', 'block');
    $("#registos").css('display', 'none');	
    
    $.post(window.location.protocol + "//" + window.location.host +  '/management/caracteristicas-tipos-conteudos/campos-caracteristica-html', 
    function(data){
            $("#registos").html(data);
            $("#registos").css('display', 'block');
            $("#waiting").css('display', 'none');
    });
    
}

function obterCamposConteudo(){
    $("#waiting").css('display', 'block');
    $("#registos").css('display', 'none');	
    
    $.post(window.location.protocol + "//" + window.location.host +  '/management/caracteristicas-tipos-conteudos/campos-caracteristica-conteudo',
    function(data){
            $("#registos").html(data);
            $("#registos").css('display', 'block');
            $("#waiting").css('display', 'none');
    });
}
function rightZone(ref){
    if(ref=='html'){
        $("#rightForm").css('display', 'block');
        obterCamposHTML();
    }
    else if(ref=='conteudo'){
        $("#rightForm").css('display', 'block');     
        obterCamposConteudo();
    }
    else{
        $("#rightForm").css('display', 'none'); 
    }
}


function website(website_id){
        $.post(window.location.protocol + "//" + window.location.host + '/management/index/website',{id: website_id}, 
	function(data){
                window.location.reload();			
        });
}
function imagemNameByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    return file;
}

function imagemNameWthExtByURL(url){
    var tarr = url.split('/'); 
    var file = tarr[tarr.length-1];
    var data = file.split('.')[0];
    return data;
}

